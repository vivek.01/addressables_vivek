﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m76A0389A1F18CDCD3741B68C2506B8D6034393D7 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m8352BB924CC2997E36B6C75AD0CB253C7F009C3D (void);
// 0x00000003 System.Void UnityEngineInternal.MathfInternal::.cctor()
extern void MathfInternal__cctor_mB7CF38BBE41ECBC62A4457C91FDFD0FEE8679895 (void);
// 0x00000004 System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern void TypeInferenceRuleAttribute__ctor_m8B31AC5D44FB102217AB0308EE71EA00379B2ECF (void);
// 0x00000005 System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern void TypeInferenceRuleAttribute__ctor_mE01C01375335FB362405B8ADE483DB62E7DD7B8F (void);
// 0x00000006 System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern void TypeInferenceRuleAttribute_ToString_mD1488CF490AFA2A7F88481AD5766C6E6B865ABC4 (void);
// 0x00000007 System.Void Unity.Jobs.JobHandle::ScheduleBatchedJobs()
extern void JobHandle_ScheduleBatchedJobs_m31A19EE8C93D6BA7F2222001596EBEF313167916 (void);
// 0x00000008 System.Void Unity.IL2CPP.CompilerServices.Il2CppEagerStaticClassConstructionAttribute::.ctor()
extern void Il2CppEagerStaticClassConstructionAttribute__ctor_mF3929BBA8F45FF4DF7AE399C02282C7AF575C459 (void);
// 0x00000009 System.Void Unity.Collections.NativeLeakDetection::Initialize()
extern void NativeLeakDetection_Initialize_m1A20F4DD5DD1EF32704F40F0B05B0DE021399936 (void);
// 0x0000000A System.Int32 Unity.Collections.NativeArray`1::get_Length()
// 0x0000000B T Unity.Collections.NativeArray`1::get_Item(System.Int32)
// 0x0000000C System.Void Unity.Collections.NativeArray`1::set_Item(System.Int32,T)
// 0x0000000D System.Boolean Unity.Collections.NativeArray`1::get_IsCreated()
// 0x0000000E System.Void Unity.Collections.NativeArray`1::Dispose()
// 0x0000000F Unity.Collections.NativeArray`1/Enumerator<T> Unity.Collections.NativeArray`1::GetEnumerator()
// 0x00000010 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.NativeArray`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000011 System.Collections.IEnumerator Unity.Collections.NativeArray`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000012 System.Boolean Unity.Collections.NativeArray`1::Equals(Unity.Collections.NativeArray`1<T>)
// 0x00000013 System.Boolean Unity.Collections.NativeArray`1::Equals(System.Object)
// 0x00000014 System.Int32 Unity.Collections.NativeArray`1::GetHashCode()
// 0x00000015 System.Void Unity.Collections.NativeArray`1/Enumerator::.ctor(Unity.Collections.NativeArray`1<T>&)
// 0x00000016 System.Void Unity.Collections.NativeArray`1/Enumerator::Dispose()
// 0x00000017 System.Boolean Unity.Collections.NativeArray`1/Enumerator::MoveNext()
// 0x00000018 System.Void Unity.Collections.NativeArray`1/Enumerator::Reset()
// 0x00000019 T Unity.Collections.NativeArray`1/Enumerator::get_Current()
// 0x0000001A System.Object Unity.Collections.NativeArray`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000001B System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerAttribute::.ctor()
extern void NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633 (void);
// 0x0000001C System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsMinMaxWriteRestrictionAttribute::.ctor()
extern void NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_mBB573360E0CCDC3FEBD208460D9109485687F1C8 (void);
// 0x0000001D System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsDeallocateOnJobCompletionAttribute::.ctor()
extern void NativeContainerSupportsDeallocateOnJobCompletionAttribute__ctor_mFA8893231D6CAF247A130FA9A9950672757848E6 (void);
// 0x0000001E System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsDeferredConvertListToArray::.ctor()
extern void NativeContainerSupportsDeferredConvertListToArray__ctor_m0C65F9A9FF8862741C56E758FB996C5708E2D975 (void);
// 0x0000001F System.Void Unity.Collections.LowLevel.Unsafe.WriteAccessRequiredAttribute::.ctor()
extern void WriteAccessRequiredAttribute__ctor_m832267CA67398C994C2B666B00253CD9959610B9 (void);
// 0x00000020 System.Void Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestrictionAttribute::.ctor()
extern void NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE (void);
// 0x00000021 Unity.Collections.NativeArray`1<T> Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::ConvertExistingDataToNativeArray(System.Void*,System.Int32,Unity.Collections.Allocator)
// 0x00000022 System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafeReadOnlyPtr(Unity.Collections.NativeArray`1<T>)
// 0x00000023 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::Free(System.Void*,Unity.Collections.Allocator)
extern void UnsafeUtility_Free_mA805168FF1B6728E7DF3AD1DE47400B37F3441F9 (void);
// 0x00000024 T Unity.Collections.LowLevel.Unsafe.UnsafeUtility::ReadArrayElement(System.Void*,System.Int32)
// 0x00000025 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement(System.Void*,System.Int32,T)
// 0x00000026 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf()
// 0x00000027 System.Void UnityEngine.AnimationCurve::Internal_Destroy(System.IntPtr)
extern void AnimationCurve_Internal_Destroy_m29AC7F49DA0754B8C7A451FE946BD9A38B76E61F (void);
// 0x00000028 System.IntPtr UnityEngine.AnimationCurve::Internal_Create(UnityEngine.Keyframe[])
extern void AnimationCurve_Internal_Create_m876905D8C13846F935CA93C0C779368519D01D0C (void);
// 0x00000029 System.Boolean UnityEngine.AnimationCurve::Internal_Equals(System.IntPtr)
extern void AnimationCurve_Internal_Equals_m0D37631AC99BD190E2E753012C2F24A63DD78D05 (void);
// 0x0000002A System.Void UnityEngine.AnimationCurve::Finalize()
extern void AnimationCurve_Finalize_m4F8AF6E43E488439AB1022E7A97FEDE777655375 (void);
// 0x0000002B System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (void);
// 0x0000002C System.Void UnityEngine.AnimationCurve::.ctor()
extern void AnimationCurve__ctor_m68D6F819242C539EC522FEAFFEB6F1579767043E (void);
// 0x0000002D System.Boolean UnityEngine.AnimationCurve::Equals(System.Object)
extern void AnimationCurve_Equals_mE1B90C79209D2E006B96751B48A2F0BA6F60A5B8 (void);
// 0x0000002E System.Boolean UnityEngine.AnimationCurve::Equals(UnityEngine.AnimationCurve)
extern void AnimationCurve_Equals_mFB50636B9BE34BBD83BE401186BC1EB7267C5416 (void);
// 0x0000002F System.Int32 UnityEngine.AnimationCurve::GetHashCode()
extern void AnimationCurve_GetHashCode_mCF18923053E945F39386CE8F1FD149D4020BC4DD (void);
// 0x00000030 System.Boolean UnityEngine.Application::get_isPlaying()
extern void Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (void);
// 0x00000031 System.String UnityEngine.Application::get_streamingAssetsPath()
extern void Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1 (void);
// 0x00000032 System.String UnityEngine.Application::get_persistentDataPath()
extern void Application_get_persistentDataPath_mBD9C84D06693A9DEF2D9D2206B59D4BCF8A03463 (void);
// 0x00000033 UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern void Application_get_platform_mB22F7F39CDD46667C3EF64507E55BB7DA18F66C4 (void);
// 0x00000034 System.Void UnityEngine.Application::CallLowMemory()
extern void Application_CallLowMemory_m508B1899F8865EC715FE37ACB500C98B370F7329 (void);
// 0x00000035 System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern void Application_CallLogCallback_m42BBBDDFC6BAD182D6D574F22C0B73F3F881D681 (void);
// 0x00000036 System.Boolean UnityEngine.Application::Internal_ApplicationWantsToQuit()
extern void Application_Internal_ApplicationWantsToQuit_mF91858E5F03D57EDCCB80F470ABBA60B0FBCC5BE (void);
// 0x00000037 System.Void UnityEngine.Application::Internal_ApplicationQuit()
extern void Application_Internal_ApplicationQuit_mDFB4E615433A0A568182698ADE0E316287EDAFE3 (void);
// 0x00000038 System.Void UnityEngine.Application::Internal_ApplicationUnload()
extern void Application_Internal_ApplicationUnload_m28EA03E5E3B12350E6C4147213DE14297C50BEC7 (void);
// 0x00000039 System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern void Application_InvokeOnBeforeRender_mB7267D4900392FD7E09E15DF3F6CE1C15E6598F9 (void);
// 0x0000003A System.Void UnityEngine.Application::InvokeFocusChanged(System.Boolean)
extern void Application_InvokeFocusChanged_m1A6F45FF1CA2B03A5FB1FA864BFC37248486AC2F (void);
// 0x0000003B System.Void UnityEngine.Application::InvokeDeepLinkActivated(System.String)
extern void Application_InvokeDeepLinkActivated_mCE514E3BC1F10AF34A58A40C95C4CD5ACED1396B (void);
// 0x0000003C System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern void LowMemoryCallback__ctor_m91C04CE7D7A323B50CA6A73B23949639E1EA53C3 (void);
// 0x0000003D System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern void LowMemoryCallback_Invoke_mDF9C72A7F7CD34CC8FAED88642864AE193742437 (void);
// 0x0000003E System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LowMemoryCallback_BeginInvoke_mF446F2B2F047BC877D3819B38D1CB0AFC4C4D953 (void);
// 0x0000003F System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern void LowMemoryCallback_EndInvoke_mB653B34015F110168720FED3201FDD6D2B235212 (void);
// 0x00000040 System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern void LogCallback__ctor_mB5F165ECC180A20258EF4EFF589D88FB9F9E9C57 (void);
// 0x00000041 System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern void LogCallback_Invoke_m5503AB0FDB4D9D1A8FFE13283321AF278B7F6C4E (void);
// 0x00000042 System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern void LogCallback_BeginInvoke_m346CFB69BAB75EF96F9EBA2B3C312A1AD1D4147F (void);
// 0x00000043 System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern void LogCallback_EndInvoke_m940C8D1C936259607F04EA27DA8FBB2828FC9280 (void);
// 0x00000044 UnityEngine.BootConfigData UnityEngine.BootConfigData::WrapBootConfigData(System.IntPtr)
extern void BootConfigData_WrapBootConfigData_mB8682F80DBE83934FFEAFE08A08B376979105E48 (void);
// 0x00000045 System.Void UnityEngine.BootConfigData::.ctor(System.IntPtr)
extern void BootConfigData__ctor_m4BF11252A4EA57BE0B82E6C1657B621D163B8068 (void);
// 0x00000046 System.Void UnityEngine.CachedAssetBundle::.ctor(System.String,UnityEngine.Hash128)
extern void CachedAssetBundle__ctor_m06149D4DAC04AD0183DC3A92FDF9640C7368322A (void);
// 0x00000047 System.String UnityEngine.CachedAssetBundle::get_name()
extern void CachedAssetBundle_get_name_m08907939D330FAB52F5A08EFA5BBB3849A1893C9 (void);
// 0x00000048 UnityEngine.Hash128 UnityEngine.CachedAssetBundle::get_hash()
extern void CachedAssetBundle_get_hash_mCD87C4B6DF6F8971992E1F831EE89983251FF8F6 (void);
// 0x00000049 System.Int32 UnityEngine.Cache::get_handle()
extern void Cache_get_handle_m7E6EBC767CCE0EDB513896C6686A40B9D147A8E8 (void);
// 0x0000004A System.Int32 UnityEngine.Cache::GetHashCode()
extern void Cache_GetHashCode_m3EBFBD645A04B9E5421615E5C465C1FEC37CACC8 (void);
// 0x0000004B System.Boolean UnityEngine.Cache::Equals(System.Object)
extern void Cache_Equals_m85EFA79FA7AA290AFE0DEBE73E556C9EE5B2BEC2 (void);
// 0x0000004C System.Boolean UnityEngine.Cache::Equals(UnityEngine.Cache)
extern void Cache_Equals_m60B08970A74F86BB09E97A2BEB301710F54D0555 (void);
// 0x0000004D System.Boolean UnityEngine.Cache::get_valid()
extern void Cache_get_valid_m02EF848F3AA17F671BA8764BB53EBAFF8CED3688 (void);
// 0x0000004E System.Boolean UnityEngine.Cache::Cache_IsValid(System.Int32)
extern void Cache_Cache_IsValid_m26554118718B8A9D5756FB58D4C26A851704C57E (void);
// 0x0000004F System.String UnityEngine.Cache::get_path()
extern void Cache_get_path_m632CE9F465C2899566AD25CC97D1A13FA7A32946 (void);
// 0x00000050 System.String UnityEngine.Cache::Cache_GetPath(System.Int32)
extern void Cache_Cache_GetPath_m38FC137A94265F8AB9BA0E58BABC8914ACB27AF0 (void);
// 0x00000051 System.Void UnityEngine.Cache::set_maximumAvailableStorageSpace(System.Int64)
extern void Cache_set_maximumAvailableStorageSpace_mEC73DB6924F6B13FB9D477D5268B46620B02B7FE (void);
// 0x00000052 System.Void UnityEngine.Cache::Cache_SetMaximumDiskSpaceAvailable(System.Int32,System.Int64)
extern void Cache_Cache_SetMaximumDiskSpaceAvailable_mC00FD1F45B3125D3168F8FFAEEA4F8A0D9F5524E (void);
// 0x00000053 System.Void UnityEngine.Cache::set_expirationDelay(System.Int32)
extern void Cache_set_expirationDelay_m81253C0D3CEC4A69B5CBE48B991BA083953DA741 (void);
// 0x00000054 System.Void UnityEngine.Cache::Cache_SetExpirationDelay(System.Int32,System.Int32)
extern void Cache_Cache_SetExpirationDelay_m4100A063566C7B15290576ADC40D4D5016F7BF0A (void);
// 0x00000055 System.Void UnityEngine.Caching::set_compressionEnabled(System.Boolean)
extern void Caching_set_compressionEnabled_m7E89ECB481D1DAECE39A82501AB4A5A5A0EDB64E (void);
// 0x00000056 System.Boolean UnityEngine.Caching::get_ready()
extern void Caching_get_ready_m7D496CD9D583200F30DB19DFC3EBEF264268D4FF (void);
// 0x00000057 System.Boolean UnityEngine.Caching::ClearCachedVersion(System.String,UnityEngine.Hash128)
extern void Caching_ClearCachedVersion_m1AD4359C1A95129468CC64542E951FAC3175BCEA (void);
// 0x00000058 System.Boolean UnityEngine.Caching::ClearCachedVersionInternal(System.String,UnityEngine.Hash128)
extern void Caching_ClearCachedVersionInternal_m5B8EB23A329CDEE71ED0FFE743B02A7F2B53E017 (void);
// 0x00000059 System.Boolean UnityEngine.Caching::ClearAllCachedVersions(System.String)
extern void Caching_ClearAllCachedVersions_m35A0640AEBC5AAE1FB9FEA2B9E76C2A852586FDA (void);
// 0x0000005A System.Boolean UnityEngine.Caching::ClearCachedVersions(System.String,UnityEngine.Hash128,System.Boolean)
extern void Caching_ClearCachedVersions_mE7F5A09A2BF0DDA7AC246FC762AD7F875250984E (void);
// 0x0000005B UnityEngine.Hash128[] UnityEngine.Caching::GetCachedVersions(System.String)
extern void Caching_GetCachedVersions_mF62C55E0522B727A653F314864CFFA4E9EBDB7EA (void);
// 0x0000005C System.Void UnityEngine.Caching::GetCachedVersions(System.String,System.Collections.Generic.List`1<UnityEngine.Hash128>)
extern void Caching_GetCachedVersions_m20C16D1CB65DBB35E9EFE342170B3879CCA3975E (void);
// 0x0000005D System.Boolean UnityEngine.Caching::IsVersionCached(UnityEngine.CachedAssetBundle)
extern void Caching_IsVersionCached_m514BB16260B9EBFAE9C6D750ABE76FF769FC4B32 (void);
// 0x0000005E System.Boolean UnityEngine.Caching::IsVersionCached(System.String,System.String,UnityEngine.Hash128)
extern void Caching_IsVersionCached_mF0A77B8B4B3005EC52F235A732C691D13329D1AB (void);
// 0x0000005F UnityEngine.Cache UnityEngine.Caching::AddCache(System.String)
extern void Caching_AddCache_m989A87108B84B398D512F5D7C99207005BB2F99F (void);
// 0x00000060 UnityEngine.Cache UnityEngine.Caching::AddCache(System.String,System.Boolean)
extern void Caching_AddCache_m6EC90CBA189F5719A5595E2F8E64AFF83D5D0498 (void);
// 0x00000061 UnityEngine.Cache UnityEngine.Caching::GetCacheByPath(System.String)
extern void Caching_GetCacheByPath_mC2AB65747AEF97691010EBFA4718652574FBA7B6 (void);
// 0x00000062 UnityEngine.Cache UnityEngine.Caching::get_defaultCache()
extern void Caching_get_defaultCache_m64C755289C7387B3FC70D06857F3532A35AC412C (void);
// 0x00000063 UnityEngine.Cache UnityEngine.Caching::get_currentCacheForWriting()
extern void Caching_get_currentCacheForWriting_mE12E3B8EAB34A075518FA6E87667E171A653CC50 (void);
// 0x00000064 System.Void UnityEngine.Caching::set_currentCacheForWriting(UnityEngine.Cache)
extern void Caching_set_currentCacheForWriting_m6D41A55DD85172B9FB9B8FB9D45273A6DFC3159D (void);
// 0x00000065 System.Boolean UnityEngine.Caching::ClearCachedVersionInternal_Injected(System.String,UnityEngine.Hash128&)
extern void Caching_ClearCachedVersionInternal_Injected_m7D15079EB04CFCCE7BA752AE018D4CE61DEDFE21 (void);
// 0x00000066 System.Boolean UnityEngine.Caching::ClearCachedVersions_Injected(System.String,UnityEngine.Hash128&,System.Boolean)
extern void Caching_ClearCachedVersions_Injected_mDCC05FF246DACEE08B2CCEF81856F3A8FBEBB09A (void);
// 0x00000067 System.Boolean UnityEngine.Caching::IsVersionCached_Injected(System.String,System.String,UnityEngine.Hash128&)
extern void Caching_IsVersionCached_Injected_m6AE45A07F96D768F231A4CA2EA75C98E7FC7C2E5 (void);
// 0x00000068 System.Void UnityEngine.Caching::AddCache_Injected(System.String,System.Boolean,UnityEngine.Cache&)
extern void Caching_AddCache_Injected_m89956E55DB72273E0B13421A1F717DAB676A70B3 (void);
// 0x00000069 System.Void UnityEngine.Caching::GetCacheByPath_Injected(System.String,UnityEngine.Cache&)
extern void Caching_GetCacheByPath_Injected_m9611AE7A21A306B0AB340D9F5DA99732230981B6 (void);
// 0x0000006A System.Void UnityEngine.Caching::get_defaultCache_Injected(UnityEngine.Cache&)
extern void Caching_get_defaultCache_Injected_m19E633CDB3480DE15C5A88C69F53E9BCB7EB42F6 (void);
// 0x0000006B System.Void UnityEngine.Caching::get_currentCacheForWriting_Injected(UnityEngine.Cache&)
extern void Caching_get_currentCacheForWriting_Injected_mA4F2CA8DBC1D03EAD1B999539885DC8407B9274C (void);
// 0x0000006C System.Void UnityEngine.Caching::set_currentCacheForWriting_Injected(UnityEngine.Cache&)
extern void Caching_set_currentCacheForWriting_Injected_m9AF122C6BBE639EC2AFDE6B8DC4967EE81F8D01F (void);
// 0x0000006D System.Void UnityEngine.Camera::.ctor()
extern void Camera__ctor_m30D37AB91648C862FCB8E69805DC4DF728A2804C (void);
// 0x0000006E System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern void Camera_FireOnPreCull_mAA81BB789FC8C7516B71ED4E1452C4D8E99334C9 (void);
// 0x0000006F System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern void Camera_FireOnPreRender_mB282AD49DFA0C9036D92BAE0E28F2567C713099E (void);
// 0x00000070 System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern void Camera_FireOnPostRender_mC07CDF605EEABC89BD814E09F2542E9EE16AE0DA (void);
// 0x00000071 System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern void CameraCallback__ctor_m6E26A220911F56F3E8936DDD217ED76A15B1915E (void);
// 0x00000072 System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern void CameraCallback_Invoke_m52ACC6D0C6BA5A247A24DB9C63D4340B2EF97B24 (void);
// 0x00000073 System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern void CameraCallback_BeginInvoke_m7DD0D3DB7F6ACCBFE090C1E2EEE9BBD065A0925D (void);
// 0x00000074 System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern void CameraCallback_EndInvoke_m2CF9596F172FF401AAF5A606BE966E3D08F62DB9 (void);
// 0x00000075 System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern void CullingGroup_SendEvents_m01D14A887DFA90F3ED208073A2AE283ADF2C8B22 (void);
// 0x00000076 System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern void StateChanged__ctor_mBBB5FB739CB1D1206034FFDE998AE8342CC5C0C2 (void);
// 0x00000077 System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern void StateChanged_Invoke_m6CD13C3770E1EB709C4B125F69F7E4CE6539814D (void);
// 0x00000078 System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern void StateChanged_BeginInvoke_m70DAA3720650BA61CA34446A5E19736213E82D4D (void);
// 0x00000079 System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern void StateChanged_EndInvoke_m742AAC097C6A02605BF7FB6AA283CA24C021ED65 (void);
// 0x0000007A System.Void UnityEngine.ReflectionProbe::CallReflectionProbeEvent(UnityEngine.ReflectionProbe,UnityEngine.ReflectionProbe/ReflectionProbeEvent)
extern void ReflectionProbe_CallReflectionProbeEvent_mD705BC25F93FC786FA7E2B7E1025DF35366AF31D (void);
// 0x0000007B System.Void UnityEngine.ReflectionProbe::CallSetDefaultReflection(UnityEngine.Cubemap)
extern void ReflectionProbe_CallSetDefaultReflection_m88965097CBA94F6B21ED05F6770A1CAF1279486C (void);
// 0x0000007C System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,UnityEngine.LogOption,System.String,UnityEngine.Object)
extern void DebugLogHandler_Internal_Log_mA1D09B6E813ABFAB6358863B6C2D28E3ACA9E101 (void);
// 0x0000007D System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern void DebugLogHandler_Internal_LogException_mD0D1F120433EB1009D23E64F3A221FA234718BFF (void);
// 0x0000007E System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern void DebugLogHandler_LogFormat_mB876FBE8959FC3D9E9950527A82936F779F7A00C (void);
// 0x0000007F System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern void DebugLogHandler_LogException_m131BB407038398CCADB197F19BB4AA2435627386 (void);
// 0x00000080 System.Void UnityEngine.DebugLogHandler::.ctor()
extern void DebugLogHandler__ctor_mA13DBA2C9834013BEC67A4DF3E414F0E970D47C8 (void);
// 0x00000081 UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern void Debug_get_unityLogger_m70D38067C3055104F6C8D050AB7CE0FDFD05EE22 (void);
// 0x00000082 System.Int32 UnityEngine.Debug::ExtractStackTraceNoAlloc(System.Byte*,System.Int32,System.String)
extern void Debug_ExtractStackTraceNoAlloc_mDCD471993A7DDD1C268C960233A14632250E55A0 (void);
// 0x00000083 System.Void UnityEngine.Debug::Log(System.Object)
extern void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (void);
// 0x00000084 System.Void UnityEngine.Debug::LogFormat(System.String,System.Object[])
extern void Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6 (void);
// 0x00000085 System.Void UnityEngine.Debug::LogError(System.Object)
extern void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (void);
// 0x00000086 System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern void Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199 (void);
// 0x00000087 System.Void UnityEngine.Debug::LogErrorFormat(System.String,System.Object[])
extern void Debug_LogErrorFormat_mDBF43684A22EAAB187285C9B4174C9555DB11E83 (void);
// 0x00000088 System.Void UnityEngine.Debug::LogException(System.Exception)
extern void Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA (void);
// 0x00000089 System.Void UnityEngine.Debug::LogWarning(System.Object)
extern void Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7 (void);
// 0x0000008A System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern void Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8 (void);
// 0x0000008B System.Void UnityEngine.Debug::LogWarningFormat(System.String,System.Object[])
extern void Debug_LogWarningFormat_m405E9C0A631F815816F349D7591DD545932FF774 (void);
// 0x0000008C System.Boolean UnityEngine.Debug::CallOverridenDebugHandler(System.Exception,UnityEngine.Object)
extern void Debug_CallOverridenDebugHandler_mDE23EE8FA741C9568B37F4128B0DA7976F8612DC (void);
// 0x0000008D System.Void UnityEngine.Debug::.cctor()
extern void Debug__cctor_m8FC005AAA0C78F065A27FD1E48B12C5C5E38A486 (void);
// 0x0000008E System.Void UnityEngine.LightingSettings::LightingSettingsDontStripMe()
extern void LightingSettings_LightingSettingsDontStripMe_m6503DED1969BC7777F8CE2AA1CBBC7ACB229DDA8 (void);
// 0x0000008F System.Int32 UnityEngine.Bounds::GetHashCode()
extern void Bounds_GetHashCode_m822D1DF4F57180495A4322AADD3FD173C6FC3A1B (void);
// 0x00000090 System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern void Bounds_Equals_mB759250EA283B06481746E9A247B024D273408C9 (void);
// 0x00000091 System.Boolean UnityEngine.Bounds::Equals(UnityEngine.Bounds)
extern void Bounds_Equals_mC558BE6FE3614F7B42F5E22D1F155194A0E3DD0F (void);
// 0x00000092 UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern void Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485 (void);
// 0x00000093 UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern void Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02 (void);
// 0x00000094 System.String UnityEngine.Bounds::ToString()
extern void Bounds_ToString_mC2F42E6634E786CC9A1B847ECDD88226B7880E7B (void);
// 0x00000095 System.String UnityEngine.Bounds::ToString(System.String,System.IFormatProvider)
extern void Bounds_ToString_m493BA40092851F60E7CE73A8BD58489DAE18B3AD (void);
// 0x00000096 System.String UnityEngine.Plane::ToString()
extern void Plane_ToString_mD0E5921B1DFC4E83443BA7267E1182ACDD65C5DD (void);
// 0x00000097 System.String UnityEngine.Plane::ToString(System.String,System.IFormatProvider)
extern void Plane_ToString_m0AF5EF6354605B6F6698346081FBC1A8BE138C4B (void);
// 0x00000098 System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70 (void);
// 0x00000099 UnityEngine.Rect UnityEngine.Rect::get_zero()
extern void Rect_get_zero_m4F738804E40698120CC691AB45A6416C4FF52589 (void);
// 0x0000009A System.Single UnityEngine.Rect::get_x()
extern void Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987 (void);
// 0x0000009B System.Single UnityEngine.Rect::get_y()
extern void Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74 (void);
// 0x0000009C System.Single UnityEngine.Rect::get_width()
extern void Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF (void);
// 0x0000009D System.Single UnityEngine.Rect::get_height()
extern void Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A (void);
// 0x0000009E System.Single UnityEngine.Rect::get_xMax()
extern void Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5 (void);
// 0x0000009F System.Single UnityEngine.Rect::get_yMax()
extern void Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3 (void);
// 0x000000A0 System.Int32 UnityEngine.Rect::GetHashCode()
extern void Rect_GetHashCode_mE5841C54528B29D5E85173AF1973326793AF9311 (void);
// 0x000000A1 System.Boolean UnityEngine.Rect::Equals(System.Object)
extern void Rect_Equals_mF1F747B913CDB083ED803F5DD21E2005736AE4AB (void);
// 0x000000A2 System.Boolean UnityEngine.Rect::Equals(UnityEngine.Rect)
extern void Rect_Equals_mC9EE5E0C234DB174AC16E653ED8D32BB4D356BB8 (void);
// 0x000000A3 System.String UnityEngine.Rect::ToString()
extern void Rect_ToString_mCB7EA3D9B51213304AB0175B2D5E4545AFBCF483 (void);
// 0x000000A4 System.String UnityEngine.Rect::ToString(System.String,System.IFormatProvider)
extern void Rect_ToString_m3DFE65344E06224C23BB7500D069F908D5DDE8F5 (void);
// 0x000000A5 System.Void UnityEngine.BeforeRenderHelper::Invoke()
extern void BeforeRenderHelper_Invoke_m30EA54023BDAB65766E9B5FD6FC90E92D75A7026 (void);
// 0x000000A6 System.Void UnityEngine.BeforeRenderHelper::.cctor()
extern void BeforeRenderHelper__cctor_mAB141E73B12E9FD55D3258F715472F6BA0872282 (void);
// 0x000000A7 System.Void UnityEngine.CustomRenderTextureManager::InvokeOnTextureLoaded_Internal(UnityEngine.CustomRenderTexture)
extern void CustomRenderTextureManager_InvokeOnTextureLoaded_Internal_m90E35A5B7DE448D0E6549F1BBEAC5BFF39916044 (void);
// 0x000000A8 System.Void UnityEngine.CustomRenderTextureManager::InvokeOnTextureUnloaded_Internal(UnityEngine.CustomRenderTexture)
extern void CustomRenderTextureManager_InvokeOnTextureUnloaded_Internal_m88D538AFAE378040C192206E5009EAE9D5856F6B (void);
// 0x000000A9 System.Void UnityEngine.Display::.ctor()
extern void Display__ctor_m12A59C1FBFC6F4BAFCB7ADDACB5BE4E6F61145F0 (void);
// 0x000000AA System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern void Display__ctor_m3FB487510CB9197672FAE63EFF6FD0FEAA542B4F (void);
// 0x000000AB System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern void Display_RecreateDisplayList_m86C6D207FEF8B77696B74ECF530002E260B1591E (void);
// 0x000000AC System.Void UnityEngine.Display::FireDisplaysUpdated()
extern void Display_FireDisplaysUpdated_mA8B70C50D286065B80D47D6D12D195A2FFB223DA (void);
// 0x000000AD System.Void UnityEngine.Display::.cctor()
extern void Display__cctor_m87EA9DCECCE11A1F161F7C451A5018180DE9EB06 (void);
// 0x000000AE System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern void DisplaysUpdatedDelegate__ctor_mE01841FD1E938AA63EF9D1153CB40E44543A78BE (void);
// 0x000000AF System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern void DisplaysUpdatedDelegate_Invoke_mBABCF33A27A4E0A5FBC06AECECA79FBF4293E7F9 (void);
// 0x000000B0 System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void DisplaysUpdatedDelegate_BeginInvoke_m42DDA570D68BFAD055E2FEBB75FBE79EFEE2752F (void);
// 0x000000B1 System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern void DisplaysUpdatedDelegate_EndInvoke_mBE00DC8335AF5142275DCCE3C5CEBC4ED0F60937 (void);
// 0x000000B2 System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
extern void Graphics_Internal_GetMaxDrawMeshInstanceCount_m24408CC79F6E8FECC6931357C34B2A645D6FED0D (void);
// 0x000000B3 UnityEngine.Rendering.GraphicsTier UnityEngine.Graphics::get_activeTier()
extern void Graphics_get_activeTier_m0D7E2A5AE09AF1DCE3BC6C49A3753EEA53B95691 (void);
// 0x000000B4 System.Void UnityEngine.Graphics::set_activeTier(UnityEngine.Rendering.GraphicsTier)
extern void Graphics_set_activeTier_m8F1FCB4062F7ECCA85826B406BC6D600DC3A017D (void);
// 0x000000B5 System.Void UnityEngine.Graphics::.cctor()
extern void Graphics__cctor_m13A73AA242ED3CEFBCC84A86F5DEEDC20ADF7DBF (void);
// 0x000000B6 System.String UnityEngine.Resolution::ToString()
extern void Resolution_ToString_m0F17D03CC087E67DAB7F8F383D86A9D5C3E2587B (void);
// 0x000000B7 UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern void QualitySettings_get_activeColorSpace_m65BE7300D1A12D2981B492329B32673199CCE7F4 (void);
// 0x000000B8 System.Void UnityEngine.Renderer::.ctor()
extern void Renderer__ctor_m998612F51A0E95E387FC2032AB5FEE1304E346EE (void);
// 0x000000B9 UnityEngine.Rendering.ShaderHardwareTier UnityEngine.Shader::get_globalShaderHardwareTier()
extern void Shader_get_globalShaderHardwareTier_m51DFDA02274094C72758474ACE757ADD90799705 (void);
// 0x000000BA System.Void UnityEngine.Shader::set_globalShaderHardwareTier(UnityEngine.Rendering.ShaderHardwareTier)
extern void Shader_set_globalShaderHardwareTier_mAC60FE5F3B582DC9DF6AC560FEF27A0CF53C3CA6 (void);
// 0x000000BB UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern void Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A (void);
// 0x000000BC UnityEngine.Shader UnityEngine.Shader::FindBuiltin(System.String)
extern void Shader_FindBuiltin_m2D105845CABE0AA42E9BF10C467A95E8FFB9DCB4 (void);
// 0x000000BD System.Int32 UnityEngine.Shader::get_maximumLOD()
extern void Shader_get_maximumLOD_mB63644F58B953A1AD3BD129024BEE7648A1D1489 (void);
// 0x000000BE System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern void Shader_set_maximumLOD_m0E42E3AC1D63AEBC9CE42A07AFC729F424365468 (void);
// 0x000000BF System.Int32 UnityEngine.Shader::get_globalMaximumLOD()
extern void Shader_get_globalMaximumLOD_mE9622037DF5BF2CFCA3E08171B11792A167EE631 (void);
// 0x000000C0 System.Void UnityEngine.Shader::set_globalMaximumLOD(System.Int32)
extern void Shader_set_globalMaximumLOD_mF93C8C7A057FD02ACD84BD59C6203E8A8D77284E (void);
// 0x000000C1 System.Boolean UnityEngine.Shader::get_isSupported()
extern void Shader_get_isSupported_m958F4978B3DFAD2FD471549B2C1D8A82639EDA41 (void);
// 0x000000C2 System.String UnityEngine.Shader::get_globalRenderPipeline()
extern void Shader_get_globalRenderPipeline_mA4AC2B965B1BBCB591941256C4F5291C22EAC47E (void);
// 0x000000C3 System.Void UnityEngine.Shader::set_globalRenderPipeline(System.String)
extern void Shader_set_globalRenderPipeline_mAC514D76028093787553E48858949D4B659A8E70 (void);
// 0x000000C4 System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern void Shader_EnableKeyword_m9AC58243D7FBD69DD73D13B73085E0B24E4ECFCE (void);
// 0x000000C5 System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern void Shader_DisableKeyword_mA6360809F3D85D7D0CAE3EE3C9A85987EC60DBCF (void);
// 0x000000C6 System.Boolean UnityEngine.Shader::IsKeywordEnabled(System.String)
extern void Shader_IsKeywordEnabled_m1946E475A01692DDEED4A1E917F1A45FD1C0EB5F (void);
// 0x000000C7 System.Int32 UnityEngine.Shader::get_renderQueue()
extern void Shader_get_renderQueue_mDA06C7EE569BCD2EB09C9EFBE83C9476C7F4C06F (void);
// 0x000000C8 UnityEngine.DisableBatchingType UnityEngine.Shader::get_disableBatching()
extern void Shader_get_disableBatching_m39FF291F83E0FA0895F08875FD1CA84F5BD4FF1A (void);
// 0x000000C9 System.Void UnityEngine.Shader::WarmupAllShaders()
extern void Shader_WarmupAllShaders_m5B8CBFD7A8455878F0FB06964EA4C2F829363C83 (void);
// 0x000000CA System.Int32 UnityEngine.Shader::TagToID(System.String)
extern void Shader_TagToID_m8780A4E444802A1B3FE348EEFADD61139D9CC221 (void);
// 0x000000CB System.String UnityEngine.Shader::IDToTag(System.Int32)
extern void Shader_IDToTag_m933C7042395938264D4C194CFDD636D429B4792E (void);
// 0x000000CC System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern void Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F (void);
// 0x000000CD UnityEngine.Shader UnityEngine.Shader::GetDependency(System.String)
extern void Shader_GetDependency_m022372C07FF66AAB24B3C43E596E883A4C287D0B (void);
// 0x000000CE System.Int32 UnityEngine.Shader::get_passCount()
extern void Shader_get_passCount_m95E7C903DBADD49986B0CEAF45DB4EF6CB61E07B (void);
// 0x000000CF UnityEngine.Rendering.ShaderTagId UnityEngine.Shader::FindPassTagValue(System.Int32,UnityEngine.Rendering.ShaderTagId)
extern void Shader_FindPassTagValue_mA698315B6E0F175D58E7FA02C437FC2485BBA088 (void);
// 0x000000D0 System.Int32 UnityEngine.Shader::Internal_FindPassTagValue(System.Int32,System.Int32)
extern void Shader_Internal_FindPassTagValue_mE3064D6A5EFCFD6D5EBC547E7BD04836FE853A4D (void);
// 0x000000D1 System.Void UnityEngine.Shader::SetGlobalFloatImpl(System.Int32,System.Single)
extern void Shader_SetGlobalFloatImpl_m02A206C366CB1EC84C3A579786F64397CC07EE18 (void);
// 0x000000D2 System.Void UnityEngine.Shader::SetGlobalVectorImpl(System.Int32,UnityEngine.Vector4)
extern void Shader_SetGlobalVectorImpl_m03AB40125EDA538CEDCA098CFA971C0FB5EE2167 (void);
// 0x000000D3 System.Void UnityEngine.Shader::SetGlobalMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern void Shader_SetGlobalMatrixImpl_m032A4268D8E3D3835B5C65234C8FCFD3EF040088 (void);
// 0x000000D4 System.Void UnityEngine.Shader::SetGlobalTextureImpl(System.Int32,UnityEngine.Texture)
extern void Shader_SetGlobalTextureImpl_mA0CAE25859DE59F4AA6A47929D2CB3E183EC6BF2 (void);
// 0x000000D5 System.Void UnityEngine.Shader::SetGlobalRenderTextureImpl(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Shader_SetGlobalRenderTextureImpl_m28CEED6313D0DFFE77A70F265E10E2103673D9E7 (void);
// 0x000000D6 System.Void UnityEngine.Shader::SetGlobalBufferImpl(System.Int32,UnityEngine.ComputeBuffer)
extern void Shader_SetGlobalBufferImpl_m0028959BBDEFA42C7C99D999C2348C0D65491B5D (void);
// 0x000000D7 System.Void UnityEngine.Shader::SetGlobalGraphicsBufferImpl(System.Int32,UnityEngine.GraphicsBuffer)
extern void Shader_SetGlobalGraphicsBufferImpl_mFF1908B1FC6AC9FF26A4356B9ED9AAE2F00A6B79 (void);
// 0x000000D8 System.Void UnityEngine.Shader::SetGlobalConstantBufferImpl(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBufferImpl_m7865DF846A8272BD354A72AA4F3BC23589E2D81C (void);
// 0x000000D9 System.Void UnityEngine.Shader::SetGlobalConstantGraphicsBufferImpl(System.Int32,UnityEngine.GraphicsBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantGraphicsBufferImpl_mC4730629551C7E2CBFF12E6B65E388967B0B0153 (void);
// 0x000000DA System.Single UnityEngine.Shader::GetGlobalFloatImpl(System.Int32)
extern void Shader_GetGlobalFloatImpl_mBB060E0B4DBAC481E179376BE44352574EB2DC13 (void);
// 0x000000DB UnityEngine.Vector4 UnityEngine.Shader::GetGlobalVectorImpl(System.Int32)
extern void Shader_GetGlobalVectorImpl_m8812553E28FC72720595B0F90CDC5787909BB6F7 (void);
// 0x000000DC UnityEngine.Matrix4x4 UnityEngine.Shader::GetGlobalMatrixImpl(System.Int32)
extern void Shader_GetGlobalMatrixImpl_m532EA839934657900AA3B556C806586BBCF27299 (void);
// 0x000000DD UnityEngine.Texture UnityEngine.Shader::GetGlobalTextureImpl(System.Int32)
extern void Shader_GetGlobalTextureImpl_mA596596CB80859C47D027610E87C8784842C3717 (void);
// 0x000000DE System.Void UnityEngine.Shader::SetGlobalFloatArrayImpl(System.Int32,System.Single[],System.Int32)
extern void Shader_SetGlobalFloatArrayImpl_m73D372E2F698BF49AB2301C2BF3162E62957D434 (void);
// 0x000000DF System.Void UnityEngine.Shader::SetGlobalVectorArrayImpl(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Shader_SetGlobalVectorArrayImpl_mAF21E3858C87EEB4F65079C5182B8EC368D2F238 (void);
// 0x000000E0 System.Void UnityEngine.Shader::SetGlobalMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Shader_SetGlobalMatrixArrayImpl_m6FA85F4FD0BD4017CFCB182C80169F2B9F739134 (void);
// 0x000000E1 System.Single[] UnityEngine.Shader::GetGlobalFloatArrayImpl(System.Int32)
extern void Shader_GetGlobalFloatArrayImpl_m28CC07D51EC23DFDC6973D38DDB2DA91CE51ED01 (void);
// 0x000000E2 UnityEngine.Vector4[] UnityEngine.Shader::GetGlobalVectorArrayImpl(System.Int32)
extern void Shader_GetGlobalVectorArrayImpl_mCD23AB6D3478750415DB5929751E9746DC3F248F (void);
// 0x000000E3 UnityEngine.Matrix4x4[] UnityEngine.Shader::GetGlobalMatrixArrayImpl(System.Int32)
extern void Shader_GetGlobalMatrixArrayImpl_m2DCB61AD2CEEE7DCBC90954CBD05A7F73D44012E (void);
// 0x000000E4 System.Int32 UnityEngine.Shader::GetGlobalFloatArrayCountImpl(System.Int32)
extern void Shader_GetGlobalFloatArrayCountImpl_m53686A3DB5CC2583AB3F8588310710A10CCC42AF (void);
// 0x000000E5 System.Int32 UnityEngine.Shader::GetGlobalVectorArrayCountImpl(System.Int32)
extern void Shader_GetGlobalVectorArrayCountImpl_mE901291496FA664ADDB75E0D5E5F795AF9B2DC85 (void);
// 0x000000E6 System.Int32 UnityEngine.Shader::GetGlobalMatrixArrayCountImpl(System.Int32)
extern void Shader_GetGlobalMatrixArrayCountImpl_m23583D31BB71BE46C4367A286D64994942DFC814 (void);
// 0x000000E7 System.Void UnityEngine.Shader::ExtractGlobalFloatArrayImpl(System.Int32,System.Single[])
extern void Shader_ExtractGlobalFloatArrayImpl_m338DCC73DD8053E80A81428F835C5A3CC0C902B8 (void);
// 0x000000E8 System.Void UnityEngine.Shader::ExtractGlobalVectorArrayImpl(System.Int32,UnityEngine.Vector4[])
extern void Shader_ExtractGlobalVectorArrayImpl_m75E222BC38D7FC4BDBB2A2C71AC32F607F3888FE (void);
// 0x000000E9 System.Void UnityEngine.Shader::ExtractGlobalMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[])
extern void Shader_ExtractGlobalMatrixArrayImpl_mEDAA95D17FAADE89471FDD21070AD1B1C00ABC00 (void);
// 0x000000EA System.Void UnityEngine.Shader::SetGlobalFloatArray(System.Int32,System.Single[],System.Int32)
extern void Shader_SetGlobalFloatArray_m14C1625175930F89C5708A70CC5A0658E019B389 (void);
// 0x000000EB System.Void UnityEngine.Shader::SetGlobalVectorArray(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Shader_SetGlobalVectorArray_m47EAA60D5E7BF3FA3C74CDDBBF0308F160F64993 (void);
// 0x000000EC System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Shader_SetGlobalMatrixArray_m4960680B6D4B7547B0F475DD8823934F0722B414 (void);
// 0x000000ED System.Void UnityEngine.Shader::ExtractGlobalFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Shader_ExtractGlobalFloatArray_m3AC1D6144DEE97E7CF80388CFFF94FC38178F821 (void);
// 0x000000EE System.Void UnityEngine.Shader::ExtractGlobalVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_ExtractGlobalVectorArray_mC3A9058EEB85D27D96F68252593688BE44D3C4BC (void);
// 0x000000EF System.Void UnityEngine.Shader::ExtractGlobalMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_ExtractGlobalMatrixArray_mF3BED25B2724A361AE018BDAF751E40A2B101A73 (void);
// 0x000000F0 System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern void Shader_SetGlobalFloat_mD653B388A2C2E91C323FDA07C9D9FA295738A420 (void);
// 0x000000F1 System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
extern void Shader_SetGlobalFloat_mF1EAD6190DF2F8CFC3F1B660FE4878DC4EF6C509 (void);
// 0x000000F2 System.Void UnityEngine.Shader::SetGlobalInt(System.String,System.Int32)
extern void Shader_SetGlobalInt_mF4533D79C99D03B3D14C1DE84D074938C1403B26 (void);
// 0x000000F3 System.Void UnityEngine.Shader::SetGlobalInt(System.Int32,System.Int32)
extern void Shader_SetGlobalInt_mF80ED20B4BEF02CE4222A4EA2BE653FDE24EF72E (void);
// 0x000000F4 System.Void UnityEngine.Shader::SetGlobalVector(System.String,UnityEngine.Vector4)
extern void Shader_SetGlobalVector_m0887A74FF8E1A2586B2D9ABEC6D6A06245923C7C (void);
// 0x000000F5 System.Void UnityEngine.Shader::SetGlobalVector(System.Int32,UnityEngine.Vector4)
extern void Shader_SetGlobalVector_m241FC10C437094156CA0C6CC299A3F09404CE1F3 (void);
// 0x000000F6 System.Void UnityEngine.Shader::SetGlobalColor(System.String,UnityEngine.Color)
extern void Shader_SetGlobalColor_m7F33ABD41EE010CA6B805F100FA02992381A8C7F (void);
// 0x000000F7 System.Void UnityEngine.Shader::SetGlobalColor(System.Int32,UnityEngine.Color)
extern void Shader_SetGlobalColor_mC317375EA15179FE45810070AE7EE354C664C406 (void);
// 0x000000F8 System.Void UnityEngine.Shader::SetGlobalMatrix(System.String,UnityEngine.Matrix4x4)
extern void Shader_SetGlobalMatrix_m8BB791924D9A2333F71E7F0FDB432E622FEAECA1 (void);
// 0x000000F9 System.Void UnityEngine.Shader::SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4)
extern void Shader_SetGlobalMatrix_mD35BB59CA44958430C76208064B372484ED349F3 (void);
// 0x000000FA System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.Texture)
extern void Shader_SetGlobalTexture_m0566B97B8028F374E445F7CC59DBFDD74911FCA4 (void);
// 0x000000FB System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.Texture)
extern void Shader_SetGlobalTexture_m0C81952FBBF80467F736DA58434E4C2CEF757792 (void);
// 0x000000FC System.Void UnityEngine.Shader::SetGlobalTexture(System.String,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Shader_SetGlobalTexture_m8F8FF0AFDA3ADC15D87FA1456ACBB593ED75B4F9 (void);
// 0x000000FD System.Void UnityEngine.Shader::SetGlobalTexture(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Shader_SetGlobalTexture_m266FB7CA0E7D7A20AFB9209050A7CAF7821C2509 (void);
// 0x000000FE System.Void UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.ComputeBuffer)
extern void Shader_SetGlobalBuffer_mC937FC917A146B62DCA7FE6B9C4232A64BA80FD6 (void);
// 0x000000FF System.Void UnityEngine.Shader::SetGlobalBuffer(System.Int32,UnityEngine.ComputeBuffer)
extern void Shader_SetGlobalBuffer_m14EBB9B2872D98B1AD440895A340F9351AB11EDE (void);
// 0x00000100 System.Void UnityEngine.Shader::SetGlobalBuffer(System.String,UnityEngine.GraphicsBuffer)
extern void Shader_SetGlobalBuffer_m449B84AAFAB18C13362EE30974155F36A46FD81C (void);
// 0x00000101 System.Void UnityEngine.Shader::SetGlobalBuffer(System.Int32,UnityEngine.GraphicsBuffer)
extern void Shader_SetGlobalBuffer_m29F697F11D494122BC1CBE1F4EA998BF1A4F617B (void);
// 0x00000102 System.Void UnityEngine.Shader::SetGlobalConstantBuffer(System.String,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBuffer_mD19D72D2513ED491D63306AD081B461A5FCA2B88 (void);
// 0x00000103 System.Void UnityEngine.Shader::SetGlobalConstantBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBuffer_mC945B54A8E90FDC5DA4AD72A6B5940AD88C4949F (void);
// 0x00000104 System.Void UnityEngine.Shader::SetGlobalConstantBuffer(System.String,UnityEngine.GraphicsBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBuffer_mAF9345BDBD6C7B45B94E107E4CF7686991D3A9C6 (void);
// 0x00000105 System.Void UnityEngine.Shader::SetGlobalConstantBuffer(System.Int32,UnityEngine.GraphicsBuffer,System.Int32,System.Int32)
extern void Shader_SetGlobalConstantBuffer_m1D325F05855A9236349E32638239EDCFC27C95FF (void);
// 0x00000106 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Shader_SetGlobalFloatArray_m40F5C11C676CAC5D6056D09E02C303E9A824E59C (void);
// 0x00000107 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Shader_SetGlobalFloatArray_m94EE850560B6A8684244873F0006D4D0F8CC2989 (void);
// 0x00000108 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.String,System.Single[])
extern void Shader_SetGlobalFloatArray_m76EB422D7F94F76527B33D5D3C5B57B9A910DB8D (void);
// 0x00000109 System.Void UnityEngine.Shader::SetGlobalFloatArray(System.Int32,System.Single[])
extern void Shader_SetGlobalFloatArray_m5272EC92BDD84B6EE7B13963442EC84226B7AE9A (void);
// 0x0000010A System.Void UnityEngine.Shader::SetGlobalVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_SetGlobalVectorArray_m5129922FF216B7501BCCF16364EAC352A5F39380 (void);
// 0x0000010B System.Void UnityEngine.Shader::SetGlobalVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_SetGlobalVectorArray_mD410749B5698DC3A8AAE57BA1F4A4634F12D3DA7 (void);
// 0x0000010C System.Void UnityEngine.Shader::SetGlobalVectorArray(System.String,UnityEngine.Vector4[])
extern void Shader_SetGlobalVectorArray_m80B7A069CC4D10C5ACBDA3B203F1F6AF6D33C46D (void);
// 0x0000010D System.Void UnityEngine.Shader::SetGlobalVectorArray(System.Int32,UnityEngine.Vector4[])
extern void Shader_SetGlobalVectorArray_m6797D46DB52D5F71E07996A3D45F01ADDA16FE44 (void);
// 0x0000010E System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_SetGlobalMatrixArray_mB0C41FBE352110F6369196E7CB409A32059692E7 (void);
// 0x0000010F System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_SetGlobalMatrixArray_mB176EEBDC2881BF07B857F4443D7F56FB0FEC100 (void);
// 0x00000110 System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.String,UnityEngine.Matrix4x4[])
extern void Shader_SetGlobalMatrixArray_m70152C8276CF8EEE230A030773CA5482559B2F8F (void);
// 0x00000111 System.Void UnityEngine.Shader::SetGlobalMatrixArray(System.Int32,UnityEngine.Matrix4x4[])
extern void Shader_SetGlobalMatrixArray_m0EBFA12BAB872464B7730520DDEE6D85BC327BF8 (void);
// 0x00000112 System.Single UnityEngine.Shader::GetGlobalFloat(System.String)
extern void Shader_GetGlobalFloat_m0AA121ADA3DDE99A75C23F51A5E685309B9E72D0 (void);
// 0x00000113 System.Single UnityEngine.Shader::GetGlobalFloat(System.Int32)
extern void Shader_GetGlobalFloat_m9225741D7AF10ADD361C414ACD00228D3196DA28 (void);
// 0x00000114 System.Int32 UnityEngine.Shader::GetGlobalInt(System.String)
extern void Shader_GetGlobalInt_mC63FCE1FD39AD33344A2ADA75DB921B073827E89 (void);
// 0x00000115 System.Int32 UnityEngine.Shader::GetGlobalInt(System.Int32)
extern void Shader_GetGlobalInt_mC6A5B3732F2720C3D01DCBF9A67FAFBED4B4BBED (void);
// 0x00000116 UnityEngine.Vector4 UnityEngine.Shader::GetGlobalVector(System.String)
extern void Shader_GetGlobalVector_mDE9B96D96E38B80479D55E8339ED54E2C6C3BE94 (void);
// 0x00000117 UnityEngine.Vector4 UnityEngine.Shader::GetGlobalVector(System.Int32)
extern void Shader_GetGlobalVector_m2624DC4423F51AC17F8F9E75F7CD060598CB6908 (void);
// 0x00000118 UnityEngine.Color UnityEngine.Shader::GetGlobalColor(System.String)
extern void Shader_GetGlobalColor_m0022C061FB4916CE6B8F8FC4486504DDB7A43E4C (void);
// 0x00000119 UnityEngine.Color UnityEngine.Shader::GetGlobalColor(System.Int32)
extern void Shader_GetGlobalColor_mD65E4B0DA68019B51B911A6B5AC01D41660517D2 (void);
// 0x0000011A UnityEngine.Matrix4x4 UnityEngine.Shader::GetGlobalMatrix(System.String)
extern void Shader_GetGlobalMatrix_mC15E21E2A151DEE1BC6F4DB7C41B5A919B8D814D (void);
// 0x0000011B UnityEngine.Matrix4x4 UnityEngine.Shader::GetGlobalMatrix(System.Int32)
extern void Shader_GetGlobalMatrix_m8FFD5582126359C5DCA38B1BC637EB8EA939CB0C (void);
// 0x0000011C UnityEngine.Texture UnityEngine.Shader::GetGlobalTexture(System.String)
extern void Shader_GetGlobalTexture_m8DB772CBFD54446AE6261F6C25367EE9F95C5B15 (void);
// 0x0000011D UnityEngine.Texture UnityEngine.Shader::GetGlobalTexture(System.Int32)
extern void Shader_GetGlobalTexture_m24F5EDA116DB3FE5157F5947D3D20C066004457E (void);
// 0x0000011E System.Single[] UnityEngine.Shader::GetGlobalFloatArray(System.String)
extern void Shader_GetGlobalFloatArray_m78C27618B3C750739457CE658D7F05CB2D96625C (void);
// 0x0000011F System.Single[] UnityEngine.Shader::GetGlobalFloatArray(System.Int32)
extern void Shader_GetGlobalFloatArray_m1C73FA81264519BE3CCB333D6A1C7C3039E0E0C7 (void);
// 0x00000120 UnityEngine.Vector4[] UnityEngine.Shader::GetGlobalVectorArray(System.String)
extern void Shader_GetGlobalVectorArray_mFF6239B9B61237B31A5B54DF15ECAFDBF0CAF104 (void);
// 0x00000121 UnityEngine.Vector4[] UnityEngine.Shader::GetGlobalVectorArray(System.Int32)
extern void Shader_GetGlobalVectorArray_mDE4D944AD3F79D65B7F44C19261A9834585BA678 (void);
// 0x00000122 UnityEngine.Matrix4x4[] UnityEngine.Shader::GetGlobalMatrixArray(System.String)
extern void Shader_GetGlobalMatrixArray_m77D1E93AE4B29BCBE46AEF8DBE03B9134D8FA736 (void);
// 0x00000123 UnityEngine.Matrix4x4[] UnityEngine.Shader::GetGlobalMatrixArray(System.Int32)
extern void Shader_GetGlobalMatrixArray_m7223C12BD2962AFBA9890307700814A42D859E90 (void);
// 0x00000124 System.Void UnityEngine.Shader::GetGlobalFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Shader_GetGlobalFloatArray_mCE35ECA90772B1F72D0BA35D646359971ECAC004 (void);
// 0x00000125 System.Void UnityEngine.Shader::GetGlobalFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Shader_GetGlobalFloatArray_m67B6A671DA5E8A24CB0C82747AA87A5445717143 (void);
// 0x00000126 System.Void UnityEngine.Shader::GetGlobalVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_GetGlobalVectorArray_m271076D1A5F3B87304B5A76CE04B5C75602C4346 (void);
// 0x00000127 System.Void UnityEngine.Shader::GetGlobalVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Shader_GetGlobalVectorArray_m1E470B38DCFC18A6309720A38F2CF0A35CBCBB92 (void);
// 0x00000128 System.Void UnityEngine.Shader::GetGlobalMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_GetGlobalMatrixArray_m474385E9F1E3007A257A0A98C014E68CFFEF2996 (void);
// 0x00000129 System.Void UnityEngine.Shader::GetGlobalMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Shader_GetGlobalMatrixArray_mC3B29E27923E5F7441F0CACEA706F7C0CC3A6CB1 (void);
// 0x0000012A System.Void UnityEngine.Shader::.ctor()
extern void Shader__ctor_m51131927C3747131EBA5F99732E86E5C9F176DC8 (void);
// 0x0000012B System.String UnityEngine.Shader::GetPropertyName(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyName_m75660E1DF89FDBC9C40ECF65494F045DB82B11CF (void);
// 0x0000012C System.Int32 UnityEngine.Shader::GetPropertyNameId(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyNameId_mBF67C831C46846EBF38942AA58E33DAD45F7EE4A (void);
// 0x0000012D UnityEngine.Rendering.ShaderPropertyType UnityEngine.Shader::GetPropertyType(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyType_m71AE3BBDFAB1A334799737CAF101C0A41CBEA55D (void);
// 0x0000012E System.String UnityEngine.Shader::GetPropertyDescription(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyDescription_m5D147D2E05969C3AB1FE6B5E44AB03F68FABD384 (void);
// 0x0000012F UnityEngine.Rendering.ShaderPropertyFlags UnityEngine.Shader::GetPropertyFlags(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyFlags_mB115BF9517C2C886524F2379C4A5E6A3A2A09DE1 (void);
// 0x00000130 System.String[] UnityEngine.Shader::GetPropertyAttributes(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyAttributes_m2258B81777AF60364D3CB7F48F353657BB2F4234 (void);
// 0x00000131 UnityEngine.Vector4 UnityEngine.Shader::GetPropertyDefaultValue(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyDefaultValue_mF94D4F3954E6BBAA7AC3EA16CF17E04448341B37 (void);
// 0x00000132 UnityEngine.Rendering.TextureDimension UnityEngine.Shader::GetPropertyTextureDimension(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyTextureDimension_mFBB9D809218B66AD772C35357813A14770F1CEB0 (void);
// 0x00000133 System.String UnityEngine.Shader::GetPropertyTextureDefaultName(UnityEngine.Shader,System.Int32)
extern void Shader_GetPropertyTextureDefaultName_m09C03C4CDBFEC5B200EBE51B1B2C3F283C07605A (void);
// 0x00000134 System.Boolean UnityEngine.Shader::FindTextureStackImpl(UnityEngine.Shader,System.Int32,System.String&,System.Int32&)
extern void Shader_FindTextureStackImpl_mC0188CAF11C08FA1EFC5DAAA72428D6334FEDF8F (void);
// 0x00000135 System.Void UnityEngine.Shader::CheckPropertyIndex(UnityEngine.Shader,System.Int32)
extern void Shader_CheckPropertyIndex_mD0FC0B59CFE24E7E5A914DC2FE3F463D27D13698 (void);
// 0x00000136 System.Int32 UnityEngine.Shader::GetPropertyCount()
extern void Shader_GetPropertyCount_m79ADA1FD02641FB2703FB95FED05C8F139DA5F11 (void);
// 0x00000137 System.Int32 UnityEngine.Shader::FindPropertyIndex(System.String)
extern void Shader_FindPropertyIndex_mE54DB73372576479B3B4EF3101548E44B6AC4A43 (void);
// 0x00000138 System.String UnityEngine.Shader::GetPropertyName(System.Int32)
extern void Shader_GetPropertyName_m49B75159E345F44CF82960CB5D4C8DC0B8D18421 (void);
// 0x00000139 System.Int32 UnityEngine.Shader::GetPropertyNameId(System.Int32)
extern void Shader_GetPropertyNameId_m6AEE0191B0F5A01937646B15B264BD7BF7F70D08 (void);
// 0x0000013A UnityEngine.Rendering.ShaderPropertyType UnityEngine.Shader::GetPropertyType(System.Int32)
extern void Shader_GetPropertyType_mBAB71A122727D5239C66985F79779C0BDDD35F68 (void);
// 0x0000013B System.String UnityEngine.Shader::GetPropertyDescription(System.Int32)
extern void Shader_GetPropertyDescription_mDA1164DC3F88F87846A3C5F615E57504648DEE53 (void);
// 0x0000013C UnityEngine.Rendering.ShaderPropertyFlags UnityEngine.Shader::GetPropertyFlags(System.Int32)
extern void Shader_GetPropertyFlags_m70751D92515F18DF9951C229E4ECBA8A75CC239F (void);
// 0x0000013D System.String[] UnityEngine.Shader::GetPropertyAttributes(System.Int32)
extern void Shader_GetPropertyAttributes_m23DBB49148AA204991A2E561AB30740EA6BAA0C8 (void);
// 0x0000013E System.Single UnityEngine.Shader::GetPropertyDefaultFloatValue(System.Int32)
extern void Shader_GetPropertyDefaultFloatValue_m7FD4C492871562F546B95CC97A221B4F989485B3 (void);
// 0x0000013F UnityEngine.Vector4 UnityEngine.Shader::GetPropertyDefaultVectorValue(System.Int32)
extern void Shader_GetPropertyDefaultVectorValue_mA39DFDC17447FEE943CEF2F256E966D449998EBC (void);
// 0x00000140 UnityEngine.Vector2 UnityEngine.Shader::GetPropertyRangeLimits(System.Int32)
extern void Shader_GetPropertyRangeLimits_mBB7654CFDE5C3455ACE91FC3B98E981CEDA7B67F (void);
// 0x00000141 UnityEngine.Rendering.TextureDimension UnityEngine.Shader::GetPropertyTextureDimension(System.Int32)
extern void Shader_GetPropertyTextureDimension_mFAE01E3C61A79F64F74C4D864F076891B2289904 (void);
// 0x00000142 System.String UnityEngine.Shader::GetPropertyTextureDefaultName(System.Int32)
extern void Shader_GetPropertyTextureDefaultName_m94DC8917CBA07846233220F7D8596C6D247F9BD0 (void);
// 0x00000143 System.Boolean UnityEngine.Shader::FindTextureStack(System.Int32,System.String&,System.Int32&)
extern void Shader_FindTextureStack_m3E9E4DB7BDA750EEB6CB289D956748D82EA97D16 (void);
// 0x00000144 System.Void UnityEngine.Shader::SetGlobalVectorImpl_Injected(System.Int32,UnityEngine.Vector4&)
extern void Shader_SetGlobalVectorImpl_Injected_mAA9C501ED50FE158BE6961E15B63A0EB2A165D0F (void);
// 0x00000145 System.Void UnityEngine.Shader::SetGlobalMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Shader_SetGlobalMatrixImpl_Injected_m3A595B62D27E0E2A6FA33EF2475C8B3F1F1C7706 (void);
// 0x00000146 System.Void UnityEngine.Shader::GetGlobalVectorImpl_Injected(System.Int32,UnityEngine.Vector4&)
extern void Shader_GetGlobalVectorImpl_Injected_m0511DCFED19B18B9CD529483C52E215538777C18 (void);
// 0x00000147 System.Void UnityEngine.Shader::GetGlobalMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Shader_GetGlobalMatrixImpl_Injected_m3E0928349E8A9EE89D46505952BDF6A5358AB385 (void);
// 0x00000148 System.Void UnityEngine.Shader::GetPropertyDefaultValue_Injected(UnityEngine.Shader,System.Int32,UnityEngine.Vector4&)
extern void Shader_GetPropertyDefaultValue_Injected_m5D19714000334A9D797511A21A34F1DD5D524088 (void);
// 0x00000149 UnityEngine.Material UnityEngine.Material::Create(System.String)
extern void Material_Create_mF2507FFE60F2646914EF94F7D68B6C1D6D82C288 (void);
// 0x0000014A System.Void UnityEngine.Material::CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern void Material_CreateWithShader_mD4E25791C111800AB1E98BEAD7D45CE72B912E62 (void);
// 0x0000014B System.Void UnityEngine.Material::CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern void Material_CreateWithMaterial_mD2035B551DB7CFA1296B91C0CCC3475C5706EE41 (void);
// 0x0000014C System.Void UnityEngine.Material::CreateWithString(UnityEngine.Material)
extern void Material_CreateWithString_m0C0F291654AFD2D5643A1A7826844F2416284F1F (void);
// 0x0000014D System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern void Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E (void);
// 0x0000014E System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern void Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245 (void);
// 0x0000014F System.Void UnityEngine.Material::.ctor(System.String)
extern void Material__ctor_m71FEB02D66A71A0FF513ABC40339E1561A8192E0 (void);
// 0x00000150 UnityEngine.Material UnityEngine.Material::GetDefaultMaterial()
extern void Material_GetDefaultMaterial_mCFE73E76C23B9309DA9724F85B35DF134930DE9A (void);
// 0x00000151 UnityEngine.Material UnityEngine.Material::GetDefaultParticleMaterial()
extern void Material_GetDefaultParticleMaterial_m298F5EC56227A02E33A50D57B11101F0210F8BDA (void);
// 0x00000152 UnityEngine.Material UnityEngine.Material::GetDefaultLineMaterial()
extern void Material_GetDefaultLineMaterial_m73EB67935F12E8B2912649E4A2F863817B8367DC (void);
// 0x00000153 UnityEngine.Shader UnityEngine.Material::get_shader()
extern void Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097 (void);
// 0x00000154 System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
extern void Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D (void);
// 0x00000155 UnityEngine.Color UnityEngine.Material::get_color()
extern void Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821 (void);
// 0x00000156 System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern void Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0 (void);
// 0x00000157 UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern void Material_get_mainTexture_mD1F98F8E09F68857D5408796A76A521925A04FAC (void);
// 0x00000158 System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern void Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5 (void);
// 0x00000159 UnityEngine.Vector2 UnityEngine.Material::get_mainTextureOffset()
extern void Material_get_mainTextureOffset_m515864AC74B322365689879CC668D001C41577D4 (void);
// 0x0000015A System.Void UnityEngine.Material::set_mainTextureOffset(UnityEngine.Vector2)
extern void Material_set_mainTextureOffset_m3045530900C547D17F181858EC245CC02CA5F3FE (void);
// 0x0000015B UnityEngine.Vector2 UnityEngine.Material::get_mainTextureScale()
extern void Material_get_mainTextureScale_m45E6FA2A0D95A6DD3BC8993A00A703697BA8C526 (void);
// 0x0000015C System.Void UnityEngine.Material::set_mainTextureScale(UnityEngine.Vector2)
extern void Material_set_mainTextureScale_m58D117C22164FBC930E153E4EEBD16B721D65048 (void);
// 0x0000015D System.Int32 UnityEngine.Material::GetFirstPropertyNameIdByAttribute(UnityEngine.Rendering.ShaderPropertyFlags)
extern void Material_GetFirstPropertyNameIdByAttribute_m7192A1C1FCDB6F58A21C00571D2A67948E944CC0 (void);
// 0x0000015E System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern void Material_HasProperty_m699B4D99152E3A99733B8BD7D41EAE08BB8B1657 (void);
// 0x0000015F System.Boolean UnityEngine.Material::HasProperty(System.String)
extern void Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E (void);
// 0x00000160 System.Int32 UnityEngine.Material::get_renderQueue()
extern void Material_get_renderQueue_m6057A6297BDA9EB2828AAD344D2C8CC82F81939C (void);
// 0x00000161 System.Void UnityEngine.Material::set_renderQueue(System.Int32)
extern void Material_set_renderQueue_m239F950307B3B71DC41AF02F9030DD0A80A3A201 (void);
// 0x00000162 System.Int32 UnityEngine.Material::get_rawRenderQueue()
extern void Material_get_rawRenderQueue_m27C504A8B68F778434262879034BCB8EB8AB0CB8 (void);
// 0x00000163 System.Void UnityEngine.Material::EnableKeyword(System.String)
extern void Material_EnableKeyword_mBD03896F11814C3EF67F73A414DC66D5B577171D (void);
// 0x00000164 System.Void UnityEngine.Material::DisableKeyword(System.String)
extern void Material_DisableKeyword_mD43BE3ED8D792B7242F5487ADC074DF2A5A1BD18 (void);
// 0x00000165 System.Boolean UnityEngine.Material::IsKeywordEnabled(System.String)
extern void Material_IsKeywordEnabled_m21EB58B980BA61215B281A9C18BC861BF6CF126E (void);
// 0x00000166 UnityEngine.MaterialGlobalIlluminationFlags UnityEngine.Material::get_globalIlluminationFlags()
extern void Material_get_globalIlluminationFlags_m79774B060B292AE18FA2E16DD393D954BBB0308A (void);
// 0x00000167 System.Void UnityEngine.Material::set_globalIlluminationFlags(UnityEngine.MaterialGlobalIlluminationFlags)
extern void Material_set_globalIlluminationFlags_mB0BC9D95E532E707756DAFF6943B08E88D99C116 (void);
// 0x00000168 System.Boolean UnityEngine.Material::get_doubleSidedGI()
extern void Material_get_doubleSidedGI_m1FEC566DADB34F040D7EA539155F642F680E1C2D (void);
// 0x00000169 System.Void UnityEngine.Material::set_doubleSidedGI(System.Boolean)
extern void Material_set_doubleSidedGI_m5349C6EB64AC4354330352DAC30DBEA0CCC45D67 (void);
// 0x0000016A System.Boolean UnityEngine.Material::get_enableInstancing()
extern void Material_get_enableInstancing_mCF6C2CC6CD5D6A19EC3B9EC1C89F9C46ADD360A8 (void);
// 0x0000016B System.Void UnityEngine.Material::set_enableInstancing(System.Boolean)
extern void Material_set_enableInstancing_m508CAE1A82C2688E92491BBDEE4F5A00089BCD4B (void);
// 0x0000016C System.Int32 UnityEngine.Material::get_passCount()
extern void Material_get_passCount_m8604F2400F17AC2524B95F1C4F39C785896EEE3A (void);
// 0x0000016D System.Void UnityEngine.Material::SetShaderPassEnabled(System.String,System.Boolean)
extern void Material_SetShaderPassEnabled_m3A46537F0188B13D53623E100089435DC2A6FFD8 (void);
// 0x0000016E System.Boolean UnityEngine.Material::GetShaderPassEnabled(System.String)
extern void Material_GetShaderPassEnabled_mE1094E87BF526A4D774A8101201E737A95EDC4E7 (void);
// 0x0000016F System.String UnityEngine.Material::GetPassName(System.Int32)
extern void Material_GetPassName_m47887B0FFEE94EE1CFD5ECB60E1082806420891A (void);
// 0x00000170 System.Int32 UnityEngine.Material::FindPass(System.String)
extern void Material_FindPass_m67037364014DC5AA06F927CA222A61F8EBBEA134 (void);
// 0x00000171 System.Void UnityEngine.Material::SetOverrideTag(System.String,System.String)
extern void Material_SetOverrideTag_m5786C05270AD48B5B0756D5BC367017965B880C4 (void);
// 0x00000172 System.String UnityEngine.Material::GetTagImpl(System.String,System.Boolean,System.String)
extern void Material_GetTagImpl_m33F2C64FC3861174F902B816B79EEC04884A18E0 (void);
// 0x00000173 System.String UnityEngine.Material::GetTag(System.String,System.Boolean,System.String)
extern void Material_GetTag_m027493F0D50B211CA83694B98014F1A9D833A121 (void);
// 0x00000174 System.String UnityEngine.Material::GetTag(System.String,System.Boolean)
extern void Material_GetTag_mEAF0285CDE4B5747603C684BB900CA34DCB8B5F9 (void);
// 0x00000175 System.Void UnityEngine.Material::Lerp(UnityEngine.Material,UnityEngine.Material,System.Single)
extern void Material_Lerp_m1A80EE6FD77F49F0A22DFBF2836D3E8359484CB0 (void);
// 0x00000176 System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern void Material_SetPass_mC888245491A21488CFF2FD64CA45E8404CB9FF9C (void);
// 0x00000177 System.Void UnityEngine.Material::CopyPropertiesFromMaterial(UnityEngine.Material)
extern void Material_CopyPropertiesFromMaterial_m5A6DE308328EAB762EF5BE3253B728C8078773CF (void);
// 0x00000178 System.String[] UnityEngine.Material::GetShaderKeywords()
extern void Material_GetShaderKeywords_mC4CE7855606BC17C1CD3ABD4FC90F6E184DFE88B (void);
// 0x00000179 System.Void UnityEngine.Material::SetShaderKeywords(System.String[])
extern void Material_SetShaderKeywords_m73BF3A5DE699A06D214972A546644D37524D53CB (void);
// 0x0000017A System.String[] UnityEngine.Material::get_shaderKeywords()
extern void Material_get_shaderKeywords_mDAD743C090C3CEE0B2883140B244853D71C5E9E0 (void);
// 0x0000017B System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
extern void Material_set_shaderKeywords_m9EC5EFA52BF30597B1692C623806E7167B1C7688 (void);
// 0x0000017C System.Int32 UnityEngine.Material::ComputeCRC()
extern void Material_ComputeCRC_mD20600C93DB99E529EA788F543DFB52FA7E8DCE8 (void);
// 0x0000017D System.String[] UnityEngine.Material::GetTexturePropertyNames()
extern void Material_GetTexturePropertyNames_m0000FE197B3A0673C0DCD22799BF3A6CF81D4638 (void);
// 0x0000017E System.Int32[] UnityEngine.Material::GetTexturePropertyNameIDs()
extern void Material_GetTexturePropertyNameIDs_m8D5B3EC9CFD27C8C7C1488C44F84C87BA27F6952 (void);
// 0x0000017F System.Void UnityEngine.Material::GetTexturePropertyNamesInternal(System.Object)
extern void Material_GetTexturePropertyNamesInternal_mF3B38415D7C48B9E447D3D284F0E987022FFCBB1 (void);
// 0x00000180 System.Void UnityEngine.Material::GetTexturePropertyNameIDsInternal(System.Object)
extern void Material_GetTexturePropertyNameIDsInternal_m0F6934334E1D4188E7F1BF4640F4EA70EFB52398 (void);
// 0x00000181 System.Void UnityEngine.Material::GetTexturePropertyNames(System.Collections.Generic.List`1<System.String>)
extern void Material_GetTexturePropertyNames_mD301350C6315CE2DD5B8D1107A125D14A58A9EEB (void);
// 0x00000182 System.Void UnityEngine.Material::GetTexturePropertyNameIDs(System.Collections.Generic.List`1<System.Int32>)
extern void Material_GetTexturePropertyNameIDs_m671CE49968832D01B7260734455E157DF8BFAB74 (void);
// 0x00000183 System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
extern void Material_SetFloatImpl_m07966D17C660588628A2ACDBBE2DD5FE0F830F1E (void);
// 0x00000184 System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern void Material_SetColorImpl_m0981AC6BCE99E2692B1859AC5A3A4334C5F52185 (void);
// 0x00000185 System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern void Material_SetMatrixImpl_mF2A4040AEEC3AC166441A54E0ED04B7222AFD5D8 (void);
// 0x00000186 System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern void Material_SetTextureImpl_m671ED2C28CE1AE0E0E94E3E7633ABFA75186317C (void);
// 0x00000187 System.Void UnityEngine.Material::SetRenderTextureImpl(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Material_SetRenderTextureImpl_m9ED2858414AE05755D876039939B62EDC3BF5BA1 (void);
// 0x00000188 System.Void UnityEngine.Material::SetBufferImpl(System.Int32,UnityEngine.ComputeBuffer)
extern void Material_SetBufferImpl_m21218ADB88A54C2923032EDD633F786C44FE8FD6 (void);
// 0x00000189 System.Void UnityEngine.Material::SetGraphicsBufferImpl(System.Int32,UnityEngine.GraphicsBuffer)
extern void Material_SetGraphicsBufferImpl_mD731715286F3A3A59FBF110B2238F8D2363CB819 (void);
// 0x0000018A System.Void UnityEngine.Material::SetConstantBufferImpl(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBufferImpl_m8618F656C40B12768D6F628237E4CCE88D450A0D (void);
// 0x0000018B System.Void UnityEngine.Material::SetConstantGraphicsBufferImpl(System.Int32,UnityEngine.GraphicsBuffer,System.Int32,System.Int32)
extern void Material_SetConstantGraphicsBufferImpl_m7E0C2E9EBF2D5E26E1EB1F6A8FBDEE53D02D9E3C (void);
// 0x0000018C System.Single UnityEngine.Material::GetFloatImpl(System.Int32)
extern void Material_GetFloatImpl_mB3186CDAD4244298ABA911133FB672182F465DB3 (void);
// 0x0000018D UnityEngine.Color UnityEngine.Material::GetColorImpl(System.Int32)
extern void Material_GetColorImpl_m317B54F3AB71AEE4527C34202EF69DA52DC7D313 (void);
// 0x0000018E UnityEngine.Matrix4x4 UnityEngine.Material::GetMatrixImpl(System.Int32)
extern void Material_GetMatrixImpl_m54541B942ABC7033DEE3ACB310C39D90CF79E8F5 (void);
// 0x0000018F UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern void Material_GetTextureImpl_mD8BBF7EC67606802B01BF2BB55FEA981DBFFC1AE (void);
// 0x00000190 System.Void UnityEngine.Material::SetFloatArrayImpl(System.Int32,System.Single[],System.Int32)
extern void Material_SetFloatArrayImpl_mC037B0DA7F4DE5A28E8AE3BE098B143B654D22A1 (void);
// 0x00000191 System.Void UnityEngine.Material::SetVectorArrayImpl(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Material_SetVectorArrayImpl_m769824B7E629A78EBAA8C9E6303124463C2B01C9 (void);
// 0x00000192 System.Void UnityEngine.Material::SetColorArrayImpl(System.Int32,UnityEngine.Color[],System.Int32)
extern void Material_SetColorArrayImpl_mCCB2C638AB0718C6DCCBA8F7152A8CBE296DAA85 (void);
// 0x00000193 System.Void UnityEngine.Material::SetMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Material_SetMatrixArrayImpl_m8BEE06794E5293903842BAABDD23FEB4A666F471 (void);
// 0x00000194 System.Single[] UnityEngine.Material::GetFloatArrayImpl(System.Int32)
extern void Material_GetFloatArrayImpl_mBDC373E2BDA6E582F2B18AD9C42D98CCECB36E24 (void);
// 0x00000195 UnityEngine.Vector4[] UnityEngine.Material::GetVectorArrayImpl(System.Int32)
extern void Material_GetVectorArrayImpl_m6F127A9BDAD5A8533ADA82B30444823D4508410E (void);
// 0x00000196 UnityEngine.Color[] UnityEngine.Material::GetColorArrayImpl(System.Int32)
extern void Material_GetColorArrayImpl_m45E9F9CFE8E5BF5DE68F42004F7A4D58787774B1 (void);
// 0x00000197 UnityEngine.Matrix4x4[] UnityEngine.Material::GetMatrixArrayImpl(System.Int32)
extern void Material_GetMatrixArrayImpl_m12E9166D607B0A77AAC8006A2C4D6908C00537E2 (void);
// 0x00000198 System.Int32 UnityEngine.Material::GetFloatArrayCountImpl(System.Int32)
extern void Material_GetFloatArrayCountImpl_m5F9AB30ECAC4FEFB4C59969DF916BAC3915F75C2 (void);
// 0x00000199 System.Int32 UnityEngine.Material::GetVectorArrayCountImpl(System.Int32)
extern void Material_GetVectorArrayCountImpl_m28B9A7CBCF7C725A657136B7477D27C389153183 (void);
// 0x0000019A System.Int32 UnityEngine.Material::GetColorArrayCountImpl(System.Int32)
extern void Material_GetColorArrayCountImpl_m030163B3DEE17735104F3CD3F9FBDB22A7C2C086 (void);
// 0x0000019B System.Int32 UnityEngine.Material::GetMatrixArrayCountImpl(System.Int32)
extern void Material_GetMatrixArrayCountImpl_m3B7A202653C140E8FAAA7068C8458875E6E50A5B (void);
// 0x0000019C System.Void UnityEngine.Material::ExtractFloatArrayImpl(System.Int32,System.Single[])
extern void Material_ExtractFloatArrayImpl_mDC8253E653E5EB5F129B21FE880CB61A46314E3E (void);
// 0x0000019D System.Void UnityEngine.Material::ExtractVectorArrayImpl(System.Int32,UnityEngine.Vector4[])
extern void Material_ExtractVectorArrayImpl_mFE7D2D0BB5704263D37DA1AF5A64EC18542821F1 (void);
// 0x0000019E System.Void UnityEngine.Material::ExtractColorArrayImpl(System.Int32,UnityEngine.Color[])
extern void Material_ExtractColorArrayImpl_m6D9535F710478D7238A1C865AE5C836237B4808B (void);
// 0x0000019F System.Void UnityEngine.Material::ExtractMatrixArrayImpl(System.Int32,UnityEngine.Matrix4x4[])
extern void Material_ExtractMatrixArrayImpl_m98B9F6400AB6D4719EC9937CAD32205DBE75E169 (void);
// 0x000001A0 UnityEngine.Vector4 UnityEngine.Material::GetTextureScaleAndOffsetImpl(System.Int32)
extern void Material_GetTextureScaleAndOffsetImpl_mF918F1F031FD450C8444D34659AC7A5F2E18DFED (void);
// 0x000001A1 System.Void UnityEngine.Material::SetTextureOffsetImpl(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureOffsetImpl_mEAC642283EF0DF8934BECC60878D9550D0E5632E (void);
// 0x000001A2 System.Void UnityEngine.Material::SetTextureScaleImpl(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureScaleImpl_m750D0AC112837AA44E17DDCA120FB608708E4CD0 (void);
// 0x000001A3 System.Void UnityEngine.Material::SetFloatArray(System.Int32,System.Single[],System.Int32)
extern void Material_SetFloatArray_mD49C0DA02C83980DEC04C03046DC11348A9ECFCE (void);
// 0x000001A4 System.Void UnityEngine.Material::SetVectorArray(System.Int32,UnityEngine.Vector4[],System.Int32)
extern void Material_SetVectorArray_m9FAC0B14179B47AD6B0B68D12B0CC8B6D6DAD1DA (void);
// 0x000001A5 System.Void UnityEngine.Material::SetColorArray(System.Int32,UnityEngine.Color[],System.Int32)
extern void Material_SetColorArray_m29A037788EEE97E346EF92A07FA4ED1D5C575230 (void);
// 0x000001A6 System.Void UnityEngine.Material::SetMatrixArray(System.Int32,UnityEngine.Matrix4x4[],System.Int32)
extern void Material_SetMatrixArray_m7A66FBD36E9C9689DF9C4E5B72A17B650BBA2B5E (void);
// 0x000001A7 System.Void UnityEngine.Material::ExtractFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Material_ExtractFloatArray_m720DB5DD8FEFFEC7666ABA24541395123A5F7900 (void);
// 0x000001A8 System.Void UnityEngine.Material::ExtractVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_ExtractVectorArray_mFE051A1A8594CA52DA21549F562621F7DA9261E2 (void);
// 0x000001A9 System.Void UnityEngine.Material::ExtractColorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_ExtractColorArray_m20CAD6F3ED903A5D85783CA6DC69F95F2A360EA6 (void);
// 0x000001AA System.Void UnityEngine.Material::ExtractMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_ExtractMatrixArray_m5C2E372B7851046DDA65EFC5CEE57AD49EB4FA86 (void);
// 0x000001AB System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern void Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768 (void);
// 0x000001AC System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern void Material_SetFloat_mAC7DC962B356565CF6743E358C7A19D0322EA060 (void);
// 0x000001AD System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern void Material_SetInt_m15D944E498726C9BB3A60A41DAAA45000F570F87 (void);
// 0x000001AE System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern void Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C (void);
// 0x000001AF System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern void Material_SetColor_m5CAAF4A8D7F839597B4E14588E341462EEB81698 (void);
// 0x000001B0 System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern void Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4 (void);
// 0x000001B1 System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern void Material_SetVector_mCB22CD5FDA6D8C7C282D7998A9244E12CC293F0D (void);
// 0x000001B2 System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
extern void Material_SetVector_m47F7F5B5B21FA28885C4E747AF1C32F40C1022CB (void);
// 0x000001B3 System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern void Material_SetMatrix_m4B2718A5E264BFDBAD965D8A67399746F6799DCD (void);
// 0x000001B4 System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern void Material_SetMatrix_m7266FA4400474D08A30181EE08E01760CCAEBA0A (void);
// 0x000001B5 System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern void Material_SetTexture_m04A1CD55201841F85E475992931A210229C782CF (void);
// 0x000001B6 System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern void Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60 (void);
// 0x000001B7 System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Material_SetTexture_mF33D909426F549DFDB7375BE7EA5B73FA9A6BD65 (void);
// 0x000001B8 System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.RenderTexture,UnityEngine.Rendering.RenderTextureSubElement)
extern void Material_SetTexture_m07CFDA267BA9094E5AB39EE4392043D39251EDD6 (void);
// 0x000001B9 System.Void UnityEngine.Material::SetBuffer(System.String,UnityEngine.ComputeBuffer)
extern void Material_SetBuffer_m8E57EF9FF4D5A9C6F7C76A3C4EADC7058FFEA341 (void);
// 0x000001BA System.Void UnityEngine.Material::SetBuffer(System.Int32,UnityEngine.ComputeBuffer)
extern void Material_SetBuffer_m28E2A70144AB475F0FEF65A2E978DFF263FC1473 (void);
// 0x000001BB System.Void UnityEngine.Material::SetBuffer(System.String,UnityEngine.GraphicsBuffer)
extern void Material_SetBuffer_m07A0492E3B3D908ED34491A7C9429A1EF3DA1138 (void);
// 0x000001BC System.Void UnityEngine.Material::SetBuffer(System.Int32,UnityEngine.GraphicsBuffer)
extern void Material_SetBuffer_m7A8E7D1E33F1A3B1D7E8C8668804BAC4365443E7 (void);
// 0x000001BD System.Void UnityEngine.Material::SetConstantBuffer(System.String,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBuffer_m6FEDEB1C027A8DBE001BC85D8DCC306711CDC256 (void);
// 0x000001BE System.Void UnityEngine.Material::SetConstantBuffer(System.Int32,UnityEngine.ComputeBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBuffer_m4D41D1A10C09709EADCA84421551194123A77AC1 (void);
// 0x000001BF System.Void UnityEngine.Material::SetConstantBuffer(System.String,UnityEngine.GraphicsBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBuffer_mDD8603DCDEC64E4495E8F0F07A37359B90CBE7C3 (void);
// 0x000001C0 System.Void UnityEngine.Material::SetConstantBuffer(System.Int32,UnityEngine.GraphicsBuffer,System.Int32,System.Int32)
extern void Material_SetConstantBuffer_m46F3BC2583F8A61035BA7831644CACA0A7D50984 (void);
// 0x000001C1 System.Void UnityEngine.Material::SetFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Material_SetFloatArray_m931F78A84AAAEBD42CF133171C1175105CC8D3F0 (void);
// 0x000001C2 System.Void UnityEngine.Material::SetFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Material_SetFloatArray_mDD7AE25B23B052A413E94CA1699E08D00E47AD34 (void);
// 0x000001C3 System.Void UnityEngine.Material::SetFloatArray(System.String,System.Single[])
extern void Material_SetFloatArray_mC4AF9243FC2948F31AFB440571C7AEB0D2D5F731 (void);
// 0x000001C4 System.Void UnityEngine.Material::SetFloatArray(System.Int32,System.Single[])
extern void Material_SetFloatArray_m1FB37CF36F58B3B0260641A3AA6583C695E399AB (void);
// 0x000001C5 System.Void UnityEngine.Material::SetColorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_SetColorArray_mC8AD46E03A81CC7BE301A61D5AF131C1CD7A5207 (void);
// 0x000001C6 System.Void UnityEngine.Material::SetColorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_SetColorArray_mDEB1B10614F0CC19EF413E9EEA992A57E6D3AAD6 (void);
// 0x000001C7 System.Void UnityEngine.Material::SetColorArray(System.String,UnityEngine.Color[])
extern void Material_SetColorArray_m74756B75D7E44A247F029D67D51466E6AE54FA51 (void);
// 0x000001C8 System.Void UnityEngine.Material::SetColorArray(System.Int32,UnityEngine.Color[])
extern void Material_SetColorArray_mEAE2F8CD96F280516C954D3547B4CEC3BA67DDAE (void);
// 0x000001C9 System.Void UnityEngine.Material::SetVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_SetVectorArray_mF138E4A9E4FF67CF380401508DCEE75E6293C45F (void);
// 0x000001CA System.Void UnityEngine.Material::SetVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_SetVectorArray_m2BEB5B4AEA8B7FE622E512F55155143797C2C8A9 (void);
// 0x000001CB System.Void UnityEngine.Material::SetVectorArray(System.String,UnityEngine.Vector4[])
extern void Material_SetVectorArray_m5F3A82ACE7E80D282C8933168D6FD914B04A8278 (void);
// 0x000001CC System.Void UnityEngine.Material::SetVectorArray(System.Int32,UnityEngine.Vector4[])
extern void Material_SetVectorArray_m535BD4D3AE5EE82FF1B42F1838A02D0D386F0244 (void);
// 0x000001CD System.Void UnityEngine.Material::SetMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_SetMatrixArray_m60BBF403BE88D8CDCDF786B706D7C820A89BEC72 (void);
// 0x000001CE System.Void UnityEngine.Material::SetMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_SetMatrixArray_mC27E23A24132BEF6DDD1618CAC5B688E583C408D (void);
// 0x000001CF System.Void UnityEngine.Material::SetMatrixArray(System.String,UnityEngine.Matrix4x4[])
extern void Material_SetMatrixArray_m05B4D34C41F266BA8F2EEAA92F9A67D659B12D6F (void);
// 0x000001D0 System.Void UnityEngine.Material::SetMatrixArray(System.Int32,UnityEngine.Matrix4x4[])
extern void Material_SetMatrixArray_m743BFF0B7C4EB6B693CB94F88EFB41E270F176D2 (void);
// 0x000001D1 System.Single UnityEngine.Material::GetFloat(System.String)
extern void Material_GetFloat_mF2F48AFBDFC1E1E72A00F614EF20B656262EB167 (void);
// 0x000001D2 System.Single UnityEngine.Material::GetFloat(System.Int32)
extern void Material_GetFloat_m508B992651DD512ECB2A51336C9A4E87AED82D27 (void);
// 0x000001D3 System.Int32 UnityEngine.Material::GetInt(System.String)
extern void Material_GetInt_m5B62B127CF60B5226EC9B8930060B7F48BF6F614 (void);
// 0x000001D4 System.Int32 UnityEngine.Material::GetInt(System.Int32)
extern void Material_GetInt_mD8C4E5D90A29C5284A342EAA047DB9BD20817B0B (void);
// 0x000001D5 UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern void Material_GetColor_m5B75B83FE5821381064306ECFEEF0CC44BE66688 (void);
// 0x000001D6 UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern void Material_GetColor_m87CBA0F1030841DE18DED76EA658006A86060EA7 (void);
// 0x000001D7 UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern void Material_GetVector_m0E41ED876B69FCFC4B9EA715D0286EE714CD201F (void);
// 0x000001D8 UnityEngine.Vector4 UnityEngine.Material::GetVector(System.Int32)
extern void Material_GetVector_m0F76C999BC936C571A3C20054D266DF122A85E88 (void);
// 0x000001D9 UnityEngine.Matrix4x4 UnityEngine.Material::GetMatrix(System.String)
extern void Material_GetMatrix_m621FBE2E83EE12867FBACCB69ECCCB6739D200D0 (void);
// 0x000001DA UnityEngine.Matrix4x4 UnityEngine.Material::GetMatrix(System.Int32)
extern void Material_GetMatrix_m7FB09833D8392AF85F65631EC19A438B66739690 (void);
// 0x000001DB UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern void Material_GetTexture_m559F9134FDF1311F3D39B8C22A90A50A9F80A5FB (void);
// 0x000001DC UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern void Material_GetTexture_m02A9C3BA6C1396C0F1AAA4C248B9A81D7ABED680 (void);
// 0x000001DD System.Single[] UnityEngine.Material::GetFloatArray(System.String)
extern void Material_GetFloatArray_mB3E884C57C20F72F1DA1D756C31C8DF7DA2DCD69 (void);
// 0x000001DE System.Single[] UnityEngine.Material::GetFloatArray(System.Int32)
extern void Material_GetFloatArray_mE3A98640D629EC8B62F5772C7E83236906B7D398 (void);
// 0x000001DF UnityEngine.Color[] UnityEngine.Material::GetColorArray(System.String)
extern void Material_GetColorArray_m4C1BBB0AFAAEC5A8D1A162AC1BE5FA3454FEDCD5 (void);
// 0x000001E0 UnityEngine.Color[] UnityEngine.Material::GetColorArray(System.Int32)
extern void Material_GetColorArray_m3B7BBF75ED8C3325BC9C0FB4CD1D65E63ABD5428 (void);
// 0x000001E1 UnityEngine.Vector4[] UnityEngine.Material::GetVectorArray(System.String)
extern void Material_GetVectorArray_mCFDB72AB5C5B80A013207DC2FA0AA20B1739FFC2 (void);
// 0x000001E2 UnityEngine.Vector4[] UnityEngine.Material::GetVectorArray(System.Int32)
extern void Material_GetVectorArray_mA39D41D1E334A97EAA4265DAD920F728D637E663 (void);
// 0x000001E3 UnityEngine.Matrix4x4[] UnityEngine.Material::GetMatrixArray(System.String)
extern void Material_GetMatrixArray_mD95D21B73C67F3C84067AE7A7C1A107C1E9D1F88 (void);
// 0x000001E4 UnityEngine.Matrix4x4[] UnityEngine.Material::GetMatrixArray(System.Int32)
extern void Material_GetMatrixArray_m354193A34FFD10BD093D5C8520E351576ECB447E (void);
// 0x000001E5 System.Void UnityEngine.Material::GetFloatArray(System.String,System.Collections.Generic.List`1<System.Single>)
extern void Material_GetFloatArray_m0AA0B6E9FFAF23A2C38A82DAA7DDF3A33A0F3013 (void);
// 0x000001E6 System.Void UnityEngine.Material::GetFloatArray(System.Int32,System.Collections.Generic.List`1<System.Single>)
extern void Material_GetFloatArray_m9564EAB70FF98ED31CE06E3B3D9D51EBC254DBC6 (void);
// 0x000001E7 System.Void UnityEngine.Material::GetColorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_GetColorArray_m5A0CF637A50A23AC5C56EE5BF70DB39E042815FF (void);
// 0x000001E8 System.Void UnityEngine.Material::GetColorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Material_GetColorArray_m851186B62AA7BDDA1AC1B7D8AB4B577D26AEADC4 (void);
// 0x000001E9 System.Void UnityEngine.Material::GetVectorArray(System.String,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_GetVectorArray_mC98BCB5303C968A702110DCA6ED8FE95866A21A8 (void);
// 0x000001EA System.Void UnityEngine.Material::GetVectorArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Material_GetVectorArray_m2627258B9DB0A91341D6B5891616CF986857854C (void);
// 0x000001EB System.Void UnityEngine.Material::GetMatrixArray(System.String,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_GetMatrixArray_m4A5B4E6DBCF9AF629A5E211F6AD35C5AFBE0D3EB (void);
// 0x000001EC System.Void UnityEngine.Material::GetMatrixArray(System.Int32,System.Collections.Generic.List`1<UnityEngine.Matrix4x4>)
extern void Material_GetMatrixArray_m5C8D1AF77DBDBCBDC94FF1F53301C5EE412B4A90 (void);
// 0x000001ED System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
extern void Material_SetTextureOffset_m8917660179EA847BFFCC28D4DED115237388DAA5 (void);
// 0x000001EE System.Void UnityEngine.Material::SetTextureOffset(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureOffset_mDEE0C861BD2FC8D38924087590BE8FD123195A78 (void);
// 0x000001EF System.Void UnityEngine.Material::SetTextureScale(System.String,UnityEngine.Vector2)
extern void Material_SetTextureScale_mFE5FD3E241619F11D65543CFB252EAC38B2174EB (void);
// 0x000001F0 System.Void UnityEngine.Material::SetTextureScale(System.Int32,UnityEngine.Vector2)
extern void Material_SetTextureScale_m9F02CF20C15805224119E8A1AE57B1B064CB72C1 (void);
// 0x000001F1 UnityEngine.Vector2 UnityEngine.Material::GetTextureOffset(System.String)
extern void Material_GetTextureOffset_m47FBA39C48B10DEAF3431284315C558BF642A2C6 (void);
// 0x000001F2 UnityEngine.Vector2 UnityEngine.Material::GetTextureOffset(System.Int32)
extern void Material_GetTextureOffset_m53C54C035DFCB16181F0226D9C41C9EAB2301617 (void);
// 0x000001F3 UnityEngine.Vector2 UnityEngine.Material::GetTextureScale(System.String)
extern void Material_GetTextureScale_mEE8950B66B5B60BDB92D41A6902E41AA1EDDEBE7 (void);
// 0x000001F4 UnityEngine.Vector2 UnityEngine.Material::GetTextureScale(System.Int32)
extern void Material_GetTextureScale_mE494CDECFF6B59EDB43D6000608A91A4B7289A9C (void);
// 0x000001F5 System.Void UnityEngine.Material::SetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void Material_SetColorImpl_Injected_mB1B35D7949FB31533E2DF99F5A0C5DC3B798EC39 (void);
// 0x000001F6 System.Void UnityEngine.Material::SetMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Material_SetMatrixImpl_Injected_m15ABD6B185339B78F89DE924539ABB632DEEC7D8 (void);
// 0x000001F7 System.Void UnityEngine.Material::GetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void Material_GetColorImpl_Injected_mFF3241EC1855296376026105A435E5B394D5AF38 (void);
// 0x000001F8 System.Void UnityEngine.Material::GetMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Material_GetMatrixImpl_Injected_m54DB03F4E0FDDE2DC3B5700E0070E34F24A67AEA (void);
// 0x000001F9 System.Void UnityEngine.Material::GetTextureScaleAndOffsetImpl_Injected(System.Int32,UnityEngine.Vector4&)
extern void Material_GetTextureScaleAndOffsetImpl_Injected_mA6164E2F51E4AB705E279E83F8874CB1F83AEF2E (void);
// 0x000001FA System.Void UnityEngine.Material::SetTextureOffsetImpl_Injected(System.Int32,UnityEngine.Vector2&)
extern void Material_SetTextureOffsetImpl_Injected_mE1E0CE112196D507111A23A59B61D94302039740 (void);
// 0x000001FB System.Void UnityEngine.Material::SetTextureScaleImpl_Injected(System.Int32,UnityEngine.Vector2&)
extern void Material_SetTextureScaleImpl_Injected_m2C59C3D2A805FA63BE7115AA869BFA2D756AFDB6 (void);
// 0x000001FC UnityEngine.LightType UnityEngine.Light::get_type()
extern void Light_get_type_mDBBEC33D93952330EED5B02B15865C59D5C355A0 (void);
// 0x000001FD System.Single UnityEngine.Light::get_spotAngle()
extern void Light_get_spotAngle_m7BFB3B329103477AFFBB9F9E059718AB212D5FD7 (void);
// 0x000001FE UnityEngine.Color UnityEngine.Light::get_color()
extern void Light_get_color_mB587B97487FFA7F7E0415F270283E48D2D901F4B (void);
// 0x000001FF System.Single UnityEngine.Light::get_intensity()
extern void Light_get_intensity_mFABC9E1EA23E954E1072233C33C2211D64262326 (void);
// 0x00000200 System.Single UnityEngine.Light::get_bounceIntensity()
extern void Light_get_bounceIntensity_m6B586C8D305CE352E537E4AC8E8F957E512C4D50 (void);
// 0x00000201 System.Single UnityEngine.Light::get_range()
extern void Light_get_range_m94D58A8FE80BC5B13571D9CC8EBF88336BA0F831 (void);
// 0x00000202 UnityEngine.LightBakingOutput UnityEngine.Light::get_bakingOutput()
extern void Light_get_bakingOutput_m3696BB20EFCAFCB3CB579E74A5FE00EAA0E71CB5 (void);
// 0x00000203 UnityEngine.LightShadows UnityEngine.Light::get_shadows()
extern void Light_get_shadows_mE77B8235C26E28A797CDDF283D167EE034226AD5 (void);
// 0x00000204 System.Single UnityEngine.Light::get_cookieSize()
extern void Light_get_cookieSize_mE1168D491F8BC5DB1BA10D6E9C3B39A46177DF2B (void);
// 0x00000205 UnityEngine.Texture UnityEngine.Light::get_cookie()
extern void Light_get_cookie_mC164223C67736F4E027DC020685D4C6D24E5A705 (void);
// 0x00000206 System.Void UnityEngine.Light::get_color_Injected(UnityEngine.Color&)
extern void Light_get_color_Injected_mFC80DFA291AB496FAE0BC39E82460F6653B3362D (void);
// 0x00000207 System.Void UnityEngine.Light::get_bakingOutput_Injected(UnityEngine.LightBakingOutput&)
extern void Light_get_bakingOutput_Injected_m7B026203BB40826D50299070138CF8F6A3519DED (void);
// 0x00000208 System.Void UnityEngine.MeshFilter::DontStripMeshFilter()
extern void MeshFilter_DontStripMeshFilter_m8982FEABBD1847BE8B3E53E9DD2A15FBC7370DE2 (void);
// 0x00000209 System.Void UnityEngine.MeshRenderer::DontStripMeshRenderer()
extern void MeshRenderer_DontStripMeshRenderer_m68A34690B98E3BF30C620117C323B48A31DE512F (void);
// 0x0000020A System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern void Mesh_Internal_Create_m6802A5B262F48CF3D72E58C2C234EF063C2552B7 (void);
// 0x0000020B System.Void UnityEngine.Mesh::.ctor()
extern void Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6 (void);
// 0x0000020C System.Void UnityEngine.Texture::.ctor()
extern void Texture__ctor_mA6FE9CC0AF05A99FADCEF0BED2FB0C95571AAF4A (void);
// 0x0000020D System.Int32 UnityEngine.Texture::GetDataWidth()
extern void Texture_GetDataWidth_m5EE88F5417E01649909C3E11408491DB88AA9442 (void);
// 0x0000020E System.Int32 UnityEngine.Texture::GetDataHeight()
extern void Texture_GetDataHeight_m1DFF41FBC7542D2CDB0247CF02A0FE0ACB60FB99 (void);
// 0x0000020F System.Int32 UnityEngine.Texture::get_width()
extern void Texture_get_width_m98E7185116DB24A73E1647878013B023FF275B98 (void);
// 0x00000210 System.Void UnityEngine.Texture::set_width(System.Int32)
extern void Texture_set_width_m6BCD23D97A9883DE0FB34E6FF48883F6C09D9F8F (void);
// 0x00000211 System.Int32 UnityEngine.Texture::get_height()
extern void Texture_get_height_m3D849F551F396027D4483C9B9FFF31F8AF4533B6 (void);
// 0x00000212 System.Void UnityEngine.Texture::set_height(System.Int32)
extern void Texture_set_height_mAC3CA245CB260972C0537919C451DBF3BA1A4554 (void);
// 0x00000213 System.Boolean UnityEngine.Texture::get_isReadable()
extern void Texture_get_isReadable_mF9C36F23F3632802946D4BCBA6FE3D27098407BC (void);
// 0x00000214 System.Int32 UnityEngine.Texture::Internal_GetActiveTextureColorSpace()
extern void Texture_Internal_GetActiveTextureColorSpace_m5D0FE0578B76D37F863DB9FDC8BD0608467EE59D (void);
// 0x00000215 UnityEngine.ColorSpace UnityEngine.Texture::get_activeTextureColorSpace()
extern void Texture_get_activeTextureColorSpace_m0553908E0813E6ABD035A2453AA073BADB5680F2 (void);
// 0x00000216 System.Int32 UnityEngine.Texture::GetPixelDataSize(System.Int32,System.Int32)
extern void Texture_GetPixelDataSize_m9330921FF2160620308A8B71F7F6221562C4FF32 (void);
// 0x00000217 System.Int32 UnityEngine.Texture::GetPixelDataOffset(System.Int32,System.Int32)
extern void Texture_GetPixelDataOffset_m5B16383E02C286C54F04B965649E1F1994ED0CF0 (void);
// 0x00000218 System.Boolean UnityEngine.Texture::ValidateFormat(UnityEngine.TextureFormat)
extern void Texture_ValidateFormat_mC3C7A3FE51CA18357ABE027958BF97D3C6675D39 (void);
// 0x00000219 System.Boolean UnityEngine.Texture::ValidateFormat(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void Texture_ValidateFormat_mB721DB544C78FC025FC3D3F85AD705D54F1B52CE (void);
// 0x0000021A UnityEngine.UnityException UnityEngine.Texture::CreateNonReadableException(UnityEngine.Texture)
extern void Texture_CreateNonReadableException_m5BFE30599C50688EEDE149FB1CEF834BE1633306 (void);
// 0x0000021B System.Void UnityEngine.Texture::.cctor()
extern void Texture__cctor_m6474E45E076B9A176073F458843BAC631033F2B7 (void);
// 0x0000021C UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern void Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4 (void);
// 0x0000021D UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern void Texture2D_get_whiteTexture_m4ED96995BA1D42F7D2823BD9D18023CFE3C680A0 (void);
// 0x0000021E UnityEngine.Texture2D UnityEngine.Texture2D::get_blackTexture()
extern void Texture2D_get_blackTexture_m7D668B534925CDB6AF411D4AA3B1733E9CF2D3BE (void);
// 0x0000021F UnityEngine.Texture2D UnityEngine.Texture2D::get_redTexture()
extern void Texture2D_get_redTexture_m3D4D263FE5A18C72AEB01593260B853F70F4639F (void);
// 0x00000220 UnityEngine.Texture2D UnityEngine.Texture2D::get_grayTexture()
extern void Texture2D_get_grayTexture_m1CB755568BB0F4B9697CA1E9B0929332BBC6204E (void);
// 0x00000221 UnityEngine.Texture2D UnityEngine.Texture2D::get_linearGrayTexture()
extern void Texture2D_get_linearGrayTexture_m4FF36C6207C32393FDFD305950CBD01E80284CF5 (void);
// 0x00000222 UnityEngine.Texture2D UnityEngine.Texture2D::get_normalTexture()
extern void Texture2D_get_normalTexture_m48A6707D82774C2E2D8D6563A856A591DEB5C2F8 (void);
// 0x00000223 System.Void UnityEngine.Texture2D::Compress(System.Boolean)
extern void Texture2D_Compress_m9EEAE939AF8538D56FA39D03FAA5AD6DA7EC5D60 (void);
// 0x00000224 System.Boolean UnityEngine.Texture2D::Internal_CreateImpl(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture2D_Internal_CreateImpl_m48CD1B0F76E8671515956DFA43329604E29EB7B3 (void);
// 0x00000225 System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture2D_Internal_Create_mEAA34D0081C646C185D322FDE203E5C6C7B05800 (void);
// 0x00000226 System.Boolean UnityEngine.Texture2D::get_isReadable()
extern void Texture2D_get_isReadable_mD31C50788F7268E65EE9DA611B6F66199AA9D109 (void);
// 0x00000227 System.Boolean UnityEngine.Texture2D::get_vtOnly()
extern void Texture2D_get_vtOnly_mF3BD6A180F07B9D3940B1AF9E020BCB481A7F225 (void);
// 0x00000228 System.Void UnityEngine.Texture2D::ApplyImpl(System.Boolean,System.Boolean)
extern void Texture2D_ApplyImpl_mC56607643B71223E3294F6BA352A5538FCC5915C (void);
// 0x00000229 System.Boolean UnityEngine.Texture2D::ResizeImpl(System.Int32,System.Int32)
extern void Texture2D_ResizeImpl_m484CD2126E70AA80396A6F7C1539A72CC03BA71C (void);
// 0x0000022A System.Void UnityEngine.Texture2D::SetPixelImpl(System.Int32,System.Int32,System.Int32,UnityEngine.Color)
extern void Texture2D_SetPixelImpl_m9790950013B3DF46008381D971548B82C0378D91 (void);
// 0x0000022B UnityEngine.Color UnityEngine.Texture2D::GetPixelImpl(System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixelImpl_m2224B987F48D881F71083B9472DE5DD11580977B (void);
// 0x0000022C UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinearImpl(System.Int32,System.Single,System.Single)
extern void Texture2D_GetPixelBilinearImpl_m688F5C550710DA1B1ECBE38C1354B0A15C89778E (void);
// 0x0000022D System.Boolean UnityEngine.Texture2D::ResizeWithFormatImpl(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Boolean)
extern void Texture2D_ResizeWithFormatImpl_m0A2C1EDF44282998B107D541509BD196AEBF65C6 (void);
// 0x0000022E System.Void UnityEngine.Texture2D::ReadPixelsImpl(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixelsImpl_m8C1FDBC0E9EA531BF107C9A60F931B15910BE260 (void);
// 0x0000022F System.Void UnityEngine.Texture2D::SetPixelsImpl(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32,System.Int32)
extern void Texture2D_SetPixelsImpl_m6F5B06278B956BFCCF360B135F73B19D4648AE92 (void);
// 0x00000230 System.Boolean UnityEngine.Texture2D::LoadRawTextureDataImpl(System.IntPtr,System.Int32)
extern void Texture2D_LoadRawTextureDataImpl_m62693BD5CB00C42253A640095380ED8892278D5D (void);
// 0x00000231 System.Boolean UnityEngine.Texture2D::LoadRawTextureDataImplArray(System.Byte[])
extern void Texture2D_LoadRawTextureDataImplArray_m1466F03F81A7FE4AA81F45B7EF388969753F1D85 (void);
// 0x00000232 System.Boolean UnityEngine.Texture2D::SetPixelDataImplArray(System.Array,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_SetPixelDataImplArray_mE1B92CCC94D3F0D191577762409226126F09054E (void);
// 0x00000233 System.Boolean UnityEngine.Texture2D::SetPixelDataImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_SetPixelDataImpl_mD34DFA0E7B7418EDD224BC82B75A725BDF85E34C (void);
// 0x00000234 System.IntPtr UnityEngine.Texture2D::GetWritableImageData(System.Int32)
extern void Texture2D_GetWritableImageData_m9CD85D0E659C6BCF58A6559F70AE4484EA0A0562 (void);
// 0x00000235 System.Int64 UnityEngine.Texture2D::GetRawImageDataSize()
extern void Texture2D_GetRawImageDataSize_m80AED06866925594D48E624AD7B4D5ADFA733B8B (void);
// 0x00000236 System.Void UnityEngine.Texture2D::GenerateAtlasImpl(UnityEngine.Vector2[],System.Int32,System.Int32,UnityEngine.Rect[])
extern void Texture2D_GenerateAtlasImpl_m1308D4415DA6626CCA6CE32F0B26836DBDEBFB65 (void);
// 0x00000237 System.Boolean UnityEngine.Texture2D::get_isPreProcessed()
extern void Texture2D_get_isPreProcessed_m6FE7E0C4E9B3EF486BC4C21A0B61B3377055475F (void);
// 0x00000238 System.Boolean UnityEngine.Texture2D::get_streamingMipmaps()
extern void Texture2D_get_streamingMipmaps_m7401E9E24E197563C6724C5962C9B244BD36416E (void);
// 0x00000239 System.Int32 UnityEngine.Texture2D::get_streamingMipmapsPriority()
extern void Texture2D_get_streamingMipmapsPriority_mF9D27663C787B18D2FBFA90B319E671FB42F3992 (void);
// 0x0000023A System.Int32 UnityEngine.Texture2D::get_requestedMipmapLevel()
extern void Texture2D_get_requestedMipmapLevel_mBF3C93384BB7D3E98D3A09281EDE442F8C84BD36 (void);
// 0x0000023B System.Void UnityEngine.Texture2D::set_requestedMipmapLevel(System.Int32)
extern void Texture2D_set_requestedMipmapLevel_m211C9DDC65257633E6448709A451BF56946DF341 (void);
// 0x0000023C System.Int32 UnityEngine.Texture2D::get_minimumMipmapLevel()
extern void Texture2D_get_minimumMipmapLevel_mA901DC7C57E160CC41A32725A49A105A64B36325 (void);
// 0x0000023D System.Void UnityEngine.Texture2D::set_minimumMipmapLevel(System.Int32)
extern void Texture2D_set_minimumMipmapLevel_m678DE0EDCD999BE2242F72C3A6526F3044CAE793 (void);
// 0x0000023E System.Boolean UnityEngine.Texture2D::get_loadAllMips()
extern void Texture2D_get_loadAllMips_m18E5EFF10409F14752954ED2AFCB39DDF5864293 (void);
// 0x0000023F System.Void UnityEngine.Texture2D::set_loadAllMips(System.Boolean)
extern void Texture2D_set_loadAllMips_mB819857C5FD8F1E5A81A091A82BF5549D4311552 (void);
// 0x00000240 System.Int32 UnityEngine.Texture2D::get_calculatedMipmapLevel()
extern void Texture2D_get_calculatedMipmapLevel_m514504EB2E9ECAC255653D4BDDC8310ED44922A9 (void);
// 0x00000241 System.Int32 UnityEngine.Texture2D::get_desiredMipmapLevel()
extern void Texture2D_get_desiredMipmapLevel_mB9F475FAA3C63FF6DA7E4C39721FA1D86A468397 (void);
// 0x00000242 System.Int32 UnityEngine.Texture2D::get_loadingMipmapLevel()
extern void Texture2D_get_loadingMipmapLevel_m9E40F55ACE81D3E3E82EAECA28756F3EB2AAD305 (void);
// 0x00000243 System.Int32 UnityEngine.Texture2D::get_loadedMipmapLevel()
extern void Texture2D_get_loadedMipmapLevel_m49320EA948DC83336BA972089811546927315404 (void);
// 0x00000244 System.Void UnityEngine.Texture2D::ClearRequestedMipmapLevel()
extern void Texture2D_ClearRequestedMipmapLevel_m02BA825BA17349495A3DEBAC5C7EA7DD6C6DC14C (void);
// 0x00000245 System.Boolean UnityEngine.Texture2D::IsRequestedMipmapLevelLoaded()
extern void Texture2D_IsRequestedMipmapLevelLoaded_m84F0314A56C7D912817C3829EE6D32C90CB44F79 (void);
// 0x00000246 System.Void UnityEngine.Texture2D::ClearMinimumMipmapLevel()
extern void Texture2D_ClearMinimumMipmapLevel_mA826A98BD812603F548A8571BDED401F14D4162D (void);
// 0x00000247 System.Void UnityEngine.Texture2D::UpdateExternalTexture(System.IntPtr)
extern void Texture2D_UpdateExternalTexture_m73D76F5FDEDA9682032D602A09F984429B9CE1A5 (void);
// 0x00000248 System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetAllPixels32_mC1C1E76040E72CAFB60D3CA3F2B95A92620F4A46 (void);
// 0x00000249 System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetBlockOfPixels32_mFCEE9885A7D2E79C14F4840AF7FF03E9AACF468B (void);
// 0x0000024A System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
extern void Texture2D_GetRawTextureData_m60C0B5EF034F31FE1824B31AC1DE71042E2ACB55 (void);
// 0x0000024B UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixels_m63492D1225FC7E937BBF236538510E29B5866BEB (void);
// 0x0000024C UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixels_m677BB0ECBC49B9BAC6C3AE78A916AFF126C62CD3 (void);
// 0x0000024D UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern void Texture2D_GetPixels32_mA4E2C09B4077716ECEFC0162ABEB8C3A66F623FA (void);
// 0x0000024E UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern void Texture2D_GetPixels32_m419F7BF2D2D374C08247BE66838148DA485A6ECA (void);
// 0x0000024F UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
extern void Texture2D_PackTextures_m7946134DE42C42397876C7A67BA9FF755030DA1E (void);
// 0x00000250 UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32)
extern void Texture2D_PackTextures_m77163B065DB65C86DD0DC579D809EB3B67784791 (void);
// 0x00000251 UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32)
extern void Texture2D_PackTextures_m0865B18933721A6D8D0BE3A0254974E6B2220720 (void);
// 0x00000252 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32,System.IntPtr)
extern void Texture2D__ctor_m448DD469801FCAED830E35F777C4F6E91AE57DA9 (void);
// 0x00000253 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2D__ctor_mF966C7EA6C64D25F515906BC88F76A652A3BE792 (void);
// 0x00000254 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2D__ctor_m18609022B3F1C638BD088788C083BDDD3A3D7E7E (void);
// 0x00000255 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2D__ctor_mFAD7EA85C577A1608852DD7AF8DADD29A9F99B10 (void);
// 0x00000256 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean,System.IntPtr)
extern void Texture2D__ctor_mF706AD5FFC4EC2805E746C80630D1255A8867004 (void);
// 0x00000257 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void Texture2D__ctor_m1A96AA8B05C35D32FF0F487E5D105E95CEF0E8F8 (void);
// 0x00000258 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3 (void);
// 0x00000259 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092 (void);
// 0x0000025A System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern void Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4 (void);
// 0x0000025B UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern void Texture2D_CreateExternalTexture_m2747FE79F3AAF5959F03CD1E7C014A839673E2E6 (void);
// 0x0000025C System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern void Texture2D_SetPixel_m78878905E58C5DE9BCFED8D9262D025789E22F92 (void);
// 0x0000025D System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color,System.Int32)
extern void Texture2D_SetPixel_m6256A1B782B57103E45DFB9C2C35CBAA7ADEA5F8 (void);
// 0x0000025E System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern void Texture2D_SetPixels_m39DFC67A52779E657C4B3AFA69FC586E2DB6AB50 (void);
// 0x0000025F System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[])
extern void Texture2D_SetPixels_m802BA835119C0F93478BBA752BA23192013EA4F7 (void);
// 0x00000260 System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern void Texture2D_SetPixels_mEA2B0371D35EA2D6468DD2937D1DB95E79EAEF42 (void);
// 0x00000261 System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern void Texture2D_SetPixels_m5FBA81041D65F8641C3107195D390EE65467FB4F (void);
// 0x00000262 UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern void Texture2D_GetPixel_m50474A401DE4CB3B567F1695546DF1D2C610A022 (void);
// 0x00000263 UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixel_m1B466564EA6529CC4573D06B53FFE566E3699ECE (void);
// 0x00000264 UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern void Texture2D_GetPixelBilinear_mE25550DD7E9FD26DA7CB1E38FFCA2101F9D3D28D (void);
// 0x00000265 UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single,System.Int32)
extern void Texture2D_GetPixelBilinear_m66B07F81930504BB39F8B3A39F6B507EB9902AD2 (void);
// 0x00000266 System.Void UnityEngine.Texture2D::LoadRawTextureData(System.IntPtr,System.Int32)
extern void Texture2D_LoadRawTextureData_mC98EE7575456ACE2E7C3B4CDC1CE0A34FA22B24E (void);
// 0x00000267 System.Void UnityEngine.Texture2D::LoadRawTextureData(System.Byte[])
extern void Texture2D_LoadRawTextureData_m93A620CC97332F351305E3A93AD11CB2E0EFDAF4 (void);
// 0x00000268 System.Void UnityEngine.Texture2D::LoadRawTextureData(Unity.Collections.NativeArray`1<T>)
// 0x00000269 System.Void UnityEngine.Texture2D::SetPixelData(T[],System.Int32,System.Int32)
// 0x0000026A System.Void UnityEngine.Texture2D::SetPixelData(Unity.Collections.NativeArray`1<T>,System.Int32,System.Int32)
// 0x0000026B Unity.Collections.NativeArray`1<T> UnityEngine.Texture2D::GetPixelData(System.Int32)
// 0x0000026C Unity.Collections.NativeArray`1<T> UnityEngine.Texture2D::GetRawTextureData()
// 0x0000026D System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern void Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660 (void);
// 0x0000026E System.Void UnityEngine.Texture2D::Apply(System.Boolean)
extern void Texture2D_Apply_mA7D80A8D5DBA5A9334508F23EAEFC6E9C7019CB6 (void);
// 0x0000026F System.Void UnityEngine.Texture2D::Apply()
extern void Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0 (void);
// 0x00000270 System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32)
extern void Texture2D_Resize_m3B472A6ED37D683DC4162504F6DCF42E1FA2195C (void);
// 0x00000271 System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D_Resize_m051609799A8F11F4E344C099DC81A50B69528A6B (void);
// 0x00000272 System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Boolean)
extern void Texture2D_Resize_mE55C29D20722089571EC05F5477A995F7B21D6A0 (void);
// 0x00000273 System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixels_m87ACCC9FDCD8FC8851AE8D3BE56A7C2CAF09C75E (void);
// 0x00000274 System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern void Texture2D_ReadPixels_m4C6FE8C2798C39C20A14DAFC963C720D17F4F987 (void);
// 0x00000275 System.Boolean UnityEngine.Texture2D::GenerateAtlas(UnityEngine.Vector2[],System.Int32,System.Int32,System.Collections.Generic.List`1<UnityEngine.Rect>)
extern void Texture2D_GenerateAtlas_mE880ED2481A83FB4A0685B3F79F79A176365F11E (void);
// 0x00000276 System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetPixels32_mBDD42381EB43E024214D81792B0A96D952911D4F (void);
// 0x00000277 System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern void Texture2D_SetPixels32_m6C2602EBE75F9C70DBC36D0B67EA4C12638518BB (void);
// 0x00000278 System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetPixels32_m5C3B90CC4B9E104C5E278176E119945C353852D9 (void);
// 0x00000279 System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern void Texture2D_SetPixels32_m80C3D047272131066BA3B918855264376D2D1407 (void);
// 0x0000027A UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern void Texture2D_GetPixels_mDBE68956E50997CB02CB0419318E0D19493288A6 (void);
// 0x0000027B UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern void Texture2D_GetPixels_m702E1E59DE60A5A11197DA3F6474F9E6716D9699 (void);
// 0x0000027C System.Void UnityEngine.Texture2D::SetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
extern void Texture2D_SetPixelImpl_Injected_mF0EB1B200D5BB3B2BC3BB50C708901785C1E1772 (void);
// 0x0000027D System.Void UnityEngine.Texture2D::GetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
extern void Texture2D_GetPixelImpl_Injected_mFD898C97E14FC52C38BA6B42BE88C762F6BB2082 (void);
// 0x0000027E System.Void UnityEngine.Texture2D::GetPixelBilinearImpl_Injected(System.Int32,System.Single,System.Single,UnityEngine.Color&)
extern void Texture2D_GetPixelBilinearImpl_Injected_m378D7A9BC9E6079B59950C664419E04FB1E894FE (void);
// 0x0000027F System.Void UnityEngine.Texture2D::ReadPixelsImpl_Injected(UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixelsImpl_Injected_mEC297796C9AA8F8A08551EEDBB144C95EA3F560C (void);
// 0x00000280 System.Boolean UnityEngine.Cubemap::Internal_CreateImpl(UnityEngine.Cubemap,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Cubemap_Internal_CreateImpl_m05F9BCB0CFD280E2121062B6F8DA9467789B27A2 (void);
// 0x00000281 System.Void UnityEngine.Cubemap::Internal_Create(UnityEngine.Cubemap,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Cubemap_Internal_Create_m1647AD0EFBE0E383F5CDF6ABBC51842959E61931 (void);
// 0x00000282 System.Boolean UnityEngine.Cubemap::get_isReadable()
extern void Cubemap_get_isReadable_m169779CDA9E9AF98599E60A47C665E68B3256F5D (void);
// 0x00000283 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap__ctor_mBC1AD85DB40124D557615D0B391CACCDF76F1579 (void);
// 0x00000284 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap__ctor_mEC3B9661FA3DB1DFF54C7A3F4FEA41903EFE2C11 (void);
// 0x00000285 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Int32)
extern void Cubemap__ctor_mDEB5F10F08868EEBBF7891F00EAB793F4C5498FB (void);
// 0x00000286 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Cubemap__ctor_mF667EBD2A38E2D5DDBD46BC80ABF2DCE97A9411B (void);
// 0x00000287 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Int32,System.IntPtr)
extern void Cubemap__ctor_mD760725AC038C20E54F8EC514DA075DFF97A8B56 (void);
// 0x00000288 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean,System.IntPtr)
extern void Cubemap__ctor_m766D71B1731BAD5C01FAEA35A73D1980C6CAE57B (void);
// 0x00000289 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Cubemap__ctor_m3F121EC86FF615007F0BB6FD8162779EE7D19037 (void);
// 0x0000028A System.Void UnityEngine.Cubemap::ValidateIsNotCrunched(UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap_ValidateIsNotCrunched_m06F63FF9899DB5C440BF49BC9750D674EA44C537 (void);
// 0x0000028B System.Boolean UnityEngine.Texture3D::get_isReadable()
extern void Texture3D_get_isReadable_mFE7B549E8E368B00CEAB4A297106AB135FA6E962 (void);
// 0x0000028C System.Boolean UnityEngine.Texture3D::Internal_CreateImpl(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture3D_Internal_CreateImpl_m520D8FF1C3C58769BD66FA8532BD4DE7E334A2DE (void);
// 0x0000028D System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture3D_Internal_Create_mE009FC1F1A74589E29C6A2DC294B312ABA03693C (void);
// 0x0000028E System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D__ctor_mA422DEB7F88AA34806E6AA2D91258AA093F3C3AE (void);
// 0x0000028F System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D__ctor_m666A8D01B0E3B7773C7CDAB624D69E16331CFA36 (void);
// 0x00000290 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Texture3D__ctor_m6DC8372EBD1146127A4CE86F3E65930ABAB6539D (void);
// 0x00000291 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32)
extern void Texture3D__ctor_m7AE9A6F7E67FE89DEA087647FB3375343D997F4C (void);
// 0x00000292 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.IntPtr)
extern void Texture3D__ctor_m36323FC008295FF8B8118811676141646C3B88A3 (void);
// 0x00000293 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture3D__ctor_m2B875ADAA935AC50C758ECEBA69F13172FD620FC (void);
// 0x00000294 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.IntPtr)
extern void Texture3D__ctor_mF3432D49750206B70A487C865F62CDA11FE4995B (void);
// 0x00000295 System.Void UnityEngine.Texture3D::ValidateIsNotCrunched(UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D_ValidateIsNotCrunched_m0B19D1B555B25C568EF9F79CE389C2F3534E3154 (void);
// 0x00000296 System.Boolean UnityEngine.Texture2DArray::get_isReadable()
extern void Texture2DArray_get_isReadable_m7676C4021DD3D435A3D2655A34C7A1FCCA00C042 (void);
// 0x00000297 System.Boolean UnityEngine.Texture2DArray::Internal_CreateImpl(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_Internal_CreateImpl_m5B8B806393D443E6F0CB49AB019C8E9A1C8644B1 (void);
// 0x00000298 System.Void UnityEngine.Texture2DArray::Internal_Create(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_Internal_Create_m5DD4264F3965FBE126FAA447C79876C22D36D39C (void);
// 0x00000299 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray__ctor_mAE2D5B259CE352E6F4F10A28FDDCE52B771672B2 (void);
// 0x0000029A System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray__ctor_m139056CD509EAC819F9713F6A2CAE801D49CA13F (void);
// 0x0000029B System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Texture2DArray__ctor_mB19D8E34783F95713A23A0F06F63EF1B1613E9C5 (void);
// 0x0000029C System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void Texture2DArray__ctor_mEE6D4AD1D7469894FA16627A222EDC34647F6DB3 (void);
// 0x0000029D System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void Texture2DArray__ctor_m4772A79C577E6E246301F31D86FE6F150B1B68E2 (void);
// 0x0000029E System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2DArray__ctor_mED6E22E57F51628D68F219E5C01FF01A265CE386 (void);
// 0x0000029F System.Void UnityEngine.Texture2DArray::ValidateIsNotCrunched(UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_ValidateIsNotCrunched_m107843937B0A25BD7B22013481934C1A3FD83103 (void);
// 0x000002A0 System.Boolean UnityEngine.CubemapArray::get_isReadable()
extern void CubemapArray_get_isReadable_m8948D737E2D417F489BCFF3C5CA87B4D536A8F44 (void);
// 0x000002A1 System.Boolean UnityEngine.CubemapArray::Internal_CreateImpl(UnityEngine.CubemapArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_Internal_CreateImpl_mC6544EE7BDDE76EC9B3B8703CB13B08497921994 (void);
// 0x000002A2 System.Void UnityEngine.CubemapArray::Internal_Create(UnityEngine.CubemapArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_Internal_Create_mC44055052CD006718B5C1964110B692B30DE1F1F (void);
// 0x000002A3 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray__ctor_mAEA6A6E06CDE3F825976EFA242AAE00AE41C0247 (void);
// 0x000002A4 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray__ctor_mB5CEC4BD06765D072E96B663B4E9E09E0DCCEE17 (void);
// 0x000002A5 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void CubemapArray__ctor_mD891BB1565BECEEEDFB5F12EE3C64C34A8A93B78 (void);
// 0x000002A6 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void CubemapArray__ctor_m696D238938D8A70B273DE5D05F60C8C4834506D8 (void);
// 0x000002A7 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void CubemapArray__ctor_m593EF9F31E1FC6357957584ACD550B434D4C9563 (void);
// 0x000002A8 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void CubemapArray__ctor_mCEB65D7A82E8C98530D970424F4B125E35A03205 (void);
// 0x000002A9 System.Void UnityEngine.CubemapArray::ValidateIsNotCrunched(UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_ValidateIsNotCrunched_m8B33D23010B48658FFB0D0DFBF6D91804950A32F (void);
// 0x000002AA System.Int32 UnityEngine.RenderTexture::get_width()
extern void RenderTexture_get_width_m7E2915C08E3B59DE86707FAF9990A73AD62609BA (void);
// 0x000002AB System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern void RenderTexture_set_width_m24E7C6AD852FA9DB089253755A450E1FC53F5CC5 (void);
// 0x000002AC System.Int32 UnityEngine.RenderTexture::get_height()
extern void RenderTexture_get_height_m2A29272DA6BAFE2051A228B15E3BC4AECD97473D (void);
// 0x000002AD System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern void RenderTexture_set_height_m131ECC892E6EA0CEA1E656C66862A272FF6F0376 (void);
// 0x000002AE System.Void UnityEngine.RenderTexture::set_graphicsFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTexture_set_graphicsFormat_mA378AAD8BD876EE96637FF0A80A2AFEA4D852FAB (void);
// 0x000002AF System.Void UnityEngine.RenderTexture::SetSRGBReadWrite(System.Boolean)
extern void RenderTexture_SetSRGBReadWrite_m1C0BEC5511CE36A91F44E1C40AEF2399BA347D14 (void);
// 0x000002B0 System.Void UnityEngine.RenderTexture::Internal_Create(UnityEngine.RenderTexture)
extern void RenderTexture_Internal_Create_m6374BF6C59B7A2307975D6D1A70C82859EF0D8F3 (void);
// 0x000002B1 System.Void UnityEngine.RenderTexture::SetRenderTextureDescriptor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_SetRenderTextureDescriptor_mD39CEA1EAF6810889EDB9D5CE08A84F14706F499 (void);
// 0x000002B2 UnityEngine.RenderTextureDescriptor UnityEngine.RenderTexture::GetDescriptor()
extern void RenderTexture_GetDescriptor_mC6D87F96283042B76AA07994AC73E8131FA65F79 (void);
// 0x000002B3 System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern void RenderTexture_set_depth_mA57CEFEDDB45D0429FAC9532A631839F523944A3 (void);
// 0x000002B4 System.Void UnityEngine.RenderTexture::.ctor()
extern void RenderTexture__ctor_m41C9973A79AF4CAD32E6C8987874BB20C9C87DF7 (void);
// 0x000002B5 System.Void UnityEngine.RenderTexture::.ctor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture__ctor_m96C4C4C7B41EE884420046EFE4B8EC528B10D8BD (void);
// 0x000002B6 System.Void UnityEngine.RenderTexture::.ctor(UnityEngine.RenderTexture)
extern void RenderTexture__ctor_m26C29617F265AAA52563A260A5D2EDAAC22CA1C5 (void);
// 0x000002B7 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat)
extern void RenderTexture__ctor_mE4898D07FB66535165C92D4AA6E37DAC7FF57D50 (void);
// 0x000002B8 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTexture__ctor_mBE300C716D0DD565F63442E58077515EC82E7BA8 (void);
// 0x000002B9 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32)
extern void RenderTexture__ctor_mBE459F2C0FB9B65A5201F7C646C7EC653408A3D6 (void);
// 0x000002BA System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture__ctor_m2C35C7AD5162A6CFB7F6CF638B2DAC0DDC9282FD (void);
// 0x000002BB System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern void RenderTexture__ctor_m8E4220FDA652BA3CACE60FBA59D868B921C0F533 (void);
// 0x000002BC System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern void RenderTexture__ctor_m5D8D36B284951F95A048C6B9ACA24FC7509564FF (void);
// 0x000002BD System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,System.Int32)
extern void RenderTexture__ctor_m262905210EC882BA3F8B34B322848879561240F6 (void);
// 0x000002BE UnityEngine.RenderTextureDescriptor UnityEngine.RenderTexture::get_descriptor()
extern void RenderTexture_get_descriptor_mBD2530599DF6A24EB0C8F502718B862FC4BF1B9E (void);
// 0x000002BF System.Void UnityEngine.RenderTexture::set_descriptor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_set_descriptor_m3C8E31AE4644B23719A12345771D1B85EB6E5881 (void);
// 0x000002C0 System.Void UnityEngine.RenderTexture::ValidateRenderTextureDesc(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_ValidateRenderTextureDesc_m5D363CF342A8C617A326B982D209893F76E30404 (void);
// 0x000002C1 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTexture::GetCompatibleFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture_GetCompatibleFormat_m21C46AD608AAA27D85641330E6F273AEF566FFB7 (void);
// 0x000002C2 System.Void UnityEngine.RenderTexture::SetRenderTextureDescriptor_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_SetRenderTextureDescriptor_Injected_m37024A53E72A10E7F192E51100E2224AA7D0169A (void);
// 0x000002C3 System.Void UnityEngine.RenderTexture::GetDescriptor_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_GetDescriptor_Injected_mF6F57BE0A174E81000F35E1E46A38311B661C2A3 (void);
// 0x000002C4 System.Int32 UnityEngine.RenderTextureDescriptor::get_width()
extern void RenderTextureDescriptor_get_width_m5DD56A0652453FDDB51FF030FC5ED914F83F5E31 (void);
// 0x000002C5 System.Void UnityEngine.RenderTextureDescriptor::set_width(System.Int32)
extern void RenderTextureDescriptor_set_width_m8D4BAEBB8089FD77F4DC81088ACB511F2BCA41EA (void);
// 0x000002C6 System.Int32 UnityEngine.RenderTextureDescriptor::get_height()
extern void RenderTextureDescriptor_get_height_m661881AD8E078D6C1FD6C549207AACC2B179D201 (void);
// 0x000002C7 System.Void UnityEngine.RenderTextureDescriptor::set_height(System.Int32)
extern void RenderTextureDescriptor_set_height_m1300AF31BCDCF2E14E86A598AFDC5569B682A46D (void);
// 0x000002C8 System.Int32 UnityEngine.RenderTextureDescriptor::get_msaaSamples()
extern void RenderTextureDescriptor_get_msaaSamples_m332912610A1FF2B7C05B0BA9939D733F2E7F0646 (void);
// 0x000002C9 System.Void UnityEngine.RenderTextureDescriptor::set_msaaSamples(System.Int32)
extern void RenderTextureDescriptor_set_msaaSamples_m84320452D8BF3A8DD5662F6229FE666C299B5AEF (void);
// 0x000002CA System.Int32 UnityEngine.RenderTextureDescriptor::get_volumeDepth()
extern void RenderTextureDescriptor_get_volumeDepth_m05E4A20A05286909E65D394D0BA5F6904D653688 (void);
// 0x000002CB System.Void UnityEngine.RenderTextureDescriptor::set_volumeDepth(System.Int32)
extern void RenderTextureDescriptor_set_volumeDepth_mC4D9C6B86B6799BA752855DE5C385CC24F6E3733 (void);
// 0x000002CC System.Void UnityEngine.RenderTextureDescriptor::set_mipCount(System.Int32)
extern void RenderTextureDescriptor_set_mipCount_mE713137D106256F44EF3E7B7CF33D5F146874659 (void);
// 0x000002CD UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::get_graphicsFormat()
extern void RenderTextureDescriptor_get_graphicsFormat_m9D77E42E017808FE3181673152A69CBC9A9B8B85 (void);
// 0x000002CE System.Void UnityEngine.RenderTextureDescriptor::set_graphicsFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTextureDescriptor_set_graphicsFormat_m946B6FE4422E8CD33EB13ADAFDB53669EBD361C4 (void);
// 0x000002CF System.Int32 UnityEngine.RenderTextureDescriptor::get_depthBufferBits()
extern void RenderTextureDescriptor_get_depthBufferBits_m92A95D5A1DCA7B844B3AC81AADCDFDD37D26333C (void);
// 0x000002D0 System.Void UnityEngine.RenderTextureDescriptor::set_depthBufferBits(System.Int32)
extern void RenderTextureDescriptor_set_depthBufferBits_m68BF4BF942828FF70442841A22D356E5D17BCF85 (void);
// 0x000002D1 System.Void UnityEngine.RenderTextureDescriptor::set_dimension(UnityEngine.Rendering.TextureDimension)
extern void RenderTextureDescriptor_set_dimension_m4D3F1486F761F3C52308F00267B918BD7DB8137F (void);
// 0x000002D2 System.Void UnityEngine.RenderTextureDescriptor::set_shadowSamplingMode(UnityEngine.Rendering.ShadowSamplingMode)
extern void RenderTextureDescriptor_set_shadowSamplingMode_m92B77BB68CC465F38790F5865A7402C5DE77B8D1 (void);
// 0x000002D3 System.Void UnityEngine.RenderTextureDescriptor::set_vrUsage(UnityEngine.VRTextureUsage)
extern void RenderTextureDescriptor_set_vrUsage_m5E4F43CB35EF142D55AC22996B641483566A2097 (void);
// 0x000002D4 System.Void UnityEngine.RenderTextureDescriptor::set_memoryless(UnityEngine.RenderTextureMemoryless)
extern void RenderTextureDescriptor_set_memoryless_m6C34CD3938C6C92F98227E3864E665026C50BCE3 (void);
// 0x000002D5 System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,System.Int32)
extern void RenderTextureDescriptor__ctor_m320C821459C7856A088415334267C2963B270A9D (void);
// 0x000002D6 System.Void UnityEngine.RenderTextureDescriptor::SetOrClearRenderTextureCreationFlag(System.Boolean,UnityEngine.RenderTextureCreationFlags)
extern void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m33FD234885342E9D0C6450C90C0F2E1B6B6A1044 (void);
// 0x000002D7 System.Void UnityEngine.RenderTextureDescriptor::.cctor()
extern void RenderTextureDescriptor__cctor_mD4015195DE93496CA03515BD76A118751F6CB88F (void);
// 0x000002D8 System.Boolean UnityEngine.Hash128::get_isValid()
extern void Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB (void);
// 0x000002D9 System.Int32 UnityEngine.Hash128::CompareTo(UnityEngine.Hash128)
extern void Hash128_CompareTo_m577A27A07658268AD07B09F4FFFF1D3216AD963B (void);
// 0x000002DA System.String UnityEngine.Hash128::ToString()
extern void Hash128_ToString_mE6E0973B9B42A6AB9BEB8ACC679291CDAD2D03AC (void);
// 0x000002DB UnityEngine.Hash128 UnityEngine.Hash128::Parse(System.String)
extern void Hash128_Parse_m717F9D24F3C0537AE4CE0534A8CEC34D721C1A89 (void);
// 0x000002DC System.String UnityEngine.Hash128::Hash128ToStringImpl(UnityEngine.Hash128)
extern void Hash128_Hash128ToStringImpl_mB227F7E22C9558FDDE51A8E9D839FFB708F331FE (void);
// 0x000002DD System.Void UnityEngine.Hash128::ComputeFromString(System.String,UnityEngine.Hash128&)
extern void Hash128_ComputeFromString_mAB2F22A4CD167B729C3A7CE56ACAC250C06FDF02 (void);
// 0x000002DE UnityEngine.Hash128 UnityEngine.Hash128::Compute(System.String)
extern void Hash128_Compute_m6A62DCDE72D17F89013487150F0D7B469AEDDC6A (void);
// 0x000002DF System.Boolean UnityEngine.Hash128::Equals(System.Object)
extern void Hash128_Equals_m2FEA62200ECEC6BA066924F3153C9FBA96B0E3FF (void);
// 0x000002E0 System.Boolean UnityEngine.Hash128::Equals(UnityEngine.Hash128)
extern void Hash128_Equals_mA77DD31AF975A04BF4BD9D1F5B2A143497C9F468 (void);
// 0x000002E1 System.Int32 UnityEngine.Hash128::GetHashCode()
extern void Hash128_GetHashCode_mBEB470B9988886E4EB3FDA22903EBB699D1B7EA6 (void);
// 0x000002E2 System.Int32 UnityEngine.Hash128::CompareTo(System.Object)
extern void Hash128_CompareTo_m28ACD34C28C044C2BEF2109446DAAEB53F4EC619 (void);
// 0x000002E3 System.Boolean UnityEngine.Hash128::op_Equality(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_Equality_m721DA198FB723CC694EE07A75766F1E173D5CD4F (void);
// 0x000002E4 System.Boolean UnityEngine.Hash128::op_LessThan(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_LessThan_m2BD510597CBD5898BF81AFBAB25D10AA696AFBE5 (void);
// 0x000002E5 System.Boolean UnityEngine.Hash128::op_GreaterThan(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_GreaterThan_m1F45666EBE8039F9187FF344CB86D0D8D0E367BB (void);
// 0x000002E6 System.Void UnityEngine.Hash128::Parse_Injected(System.String,UnityEngine.Hash128&)
extern void Hash128_Parse_Injected_m0DB13A1F604BB3FC73E276279E0AC33F14531FEB (void);
// 0x000002E7 System.String UnityEngine.Hash128::Hash128ToStringImpl_Injected(UnityEngine.Hash128&)
extern void Hash128_Hash128ToStringImpl_Injected_mDC1801343943BC014300C43AE3EC444D18A19F98 (void);
// 0x000002E8 System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
// 0x000002E9 System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object)
// 0x000002EA UnityEngine.ILogHandler UnityEngine.ILogger::get_logHandler()
// 0x000002EB System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object)
// 0x000002EC System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
// 0x000002ED System.Void UnityEngine.ILogger::LogFormat(UnityEngine.LogType,System.String,System.Object[])
// 0x000002EE System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern void Logger__ctor_m01E91C7EFD28E110D491C1A6F316E5DD32616DE1 (void);
// 0x000002EF UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern void Logger_get_logHandler_mE3531B52B745AAF6D304ED9CC5AC5D7BAF7E2024 (void);
// 0x000002F0 System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern void Logger_set_logHandler_m07110CE12B6DC759DB4292CF11B0CC2288DA4114 (void);
// 0x000002F1 System.Boolean UnityEngine.Logger::get_logEnabled()
extern void Logger_get_logEnabled_m12171AB0161FEDC83121C7A7ABA52AA3609F2D1F (void);
// 0x000002F2 System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern void Logger_set_logEnabled_mE7274CE2DFF3669A88486479F7E2ED3DE208AA07 (void);
// 0x000002F3 UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern void Logger_get_filterLogType_mCD8726167BE9731AF85A23FE65AAFAD9353AE8A4 (void);
// 0x000002F4 System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern void Logger_set_filterLogType_m5AFFB4C91E331F17DBEF4B85232EE07F73C040A2 (void);
// 0x000002F5 System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern void Logger_IsLogTypeAllowed_m1AB436520161E88D0A7DDEF6F955BB88BE47A278 (void);
// 0x000002F6 System.String UnityEngine.Logger::GetString(System.Object)
extern void Logger_GetString_mDC6359E20D3C69C29FAE80797B7CA0340506BA7B (void);
// 0x000002F7 System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern void Logger_Log_mBAF75BD87C8B66198F52DEFF72132C42CA369881 (void);
// 0x000002F8 System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern void Logger_Log_mD84CAE986DDEB614141DEDBDD023F7EB2EA511E7 (void);
// 0x000002F9 System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,System.String,System.Object[])
extern void Logger_LogFormat_mD6C153D96E0A869D48B2866E4D72D76A3E7CA2EF (void);
// 0x000002FA System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern void Logger_LogFormat_m7D6FBEBB9C9708A50311878D7AF5A96E6E9A11F4 (void);
// 0x000002FB System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern void Logger_LogException_m207DC0A45A598148B848CF37BE3A20E6C3BB10F1 (void);
// 0x000002FC System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern void UnityLogWriter_WriteStringToUnityLog_m7DF2A8AB78591F20C87B8947A22D2C845F207A20 (void);
// 0x000002FD System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLogImpl(System.String)
extern void UnityLogWriter_WriteStringToUnityLogImpl_mBD8544A13E44C89FF1BCBD8685EDB0D1E760487F (void);
// 0x000002FE System.Void UnityEngine.UnityLogWriter::Init()
extern void UnityLogWriter_Init_m84D1792BF114717225B36DD1AA45DC1201BA77FE (void);
// 0x000002FF System.Void UnityEngine.UnityLogWriter::Write(System.Char)
extern void UnityLogWriter_Write_m26CB2B40367CCA97725387637F0457998DED9230 (void);
// 0x00000300 System.Void UnityEngine.UnityLogWriter::Write(System.String)
extern void UnityLogWriter_Write_m256BEE6E2FB31EEFCD721BFEE676653E9CD00AD1 (void);
// 0x00000301 System.Void UnityEngine.UnityLogWriter::Write(System.Char[],System.Int32,System.Int32)
extern void UnityLogWriter_Write_m28FD8721A9896EE519A36770139213E697C57372 (void);
// 0x00000302 System.Void UnityEngine.UnityLogWriter::.ctor()
extern void UnityLogWriter__ctor_mB7AF0B7C8C546F210699D5F3AA23F370F1963A25 (void);
// 0x00000303 System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (void);
// 0x00000304 System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern void Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494 (void);
// 0x00000305 System.String UnityEngine.Color::ToString()
extern void Color_ToString_m2C9303D88F39CDAE35C613A3C816D8982C58B5FC (void);
// 0x00000306 System.String UnityEngine.Color::ToString(System.String,System.IFormatProvider)
extern void Color_ToString_m927424DFCE95E13D26A1F8BF91FA0AB3F6C2FC02 (void);
// 0x00000307 System.Int32 UnityEngine.Color::GetHashCode()
extern void Color_GetHashCode_mAF5E7EE6AFA983D3FA5E3D316E672EE1511F97CF (void);
// 0x00000308 System.Boolean UnityEngine.Color::Equals(System.Object)
extern void Color_Equals_m90F8A5EF85416D809F5E3C0ACCADDD4F299AD8FC (void);
// 0x00000309 System.Boolean UnityEngine.Color::Equals(UnityEngine.Color)
extern void Color_Equals_mB531F532B5F7BE6168CFD4A6C89358C16F058D00 (void);
// 0x0000030A UnityEngine.Color UnityEngine.Color::RGBMultiplied(System.Single)
extern void Color_RGBMultiplied_mEE82A8761F22790ECD29CD8AE13B1184441CB6C8 (void);
// 0x0000030B UnityEngine.Color UnityEngine.Color::get_linear()
extern void Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904 (void);
// 0x0000030C System.Single UnityEngine.Color::get_maxColorComponent()
extern void Color_get_maxColorComponent_mAB6964B3523DC9FDDF312F3329EB224DBFECE761 (void);
// 0x0000030D UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern void Color_op_Implicit_mECB4D0C812888ADAEE478E633B2ECF8F8FDB96C5 (void);
// 0x0000030E UnityEngine.Color UnityEngine.Color::op_Implicit(UnityEngine.Vector4)
extern void Color_op_Implicit_m9B1A4B721726FCDA1844A0DC505C2FF8F8C50FC0 (void);
// 0x0000030F System.String UnityEngine.Color32::ToString()
extern void Color32_ToString_m11295D5492D1FB41F25635A83B87C20058DEA256 (void);
// 0x00000310 System.String UnityEngine.Color32::ToString(System.String,System.IFormatProvider)
extern void Color32_ToString_m5BB9D04F00C5B22C5B295F6253C99972767102F5 (void);
// 0x00000311 System.IntPtr UnityEngine.Gradient::Init()
extern void Gradient_Init_mF271EE940AEEA629E2646BADD07DF0BFFDC5EBA1 (void);
// 0x00000312 System.Void UnityEngine.Gradient::Cleanup()
extern void Gradient_Cleanup_m0F4C6F0E90C86E8A5855170AA5B3FC6EC80DEF0C (void);
// 0x00000313 System.Boolean UnityEngine.Gradient::Internal_Equals(System.IntPtr)
extern void Gradient_Internal_Equals_mA15F4C17B0910C9C9B0BAE3825F673C9F46B2054 (void);
// 0x00000314 System.Void UnityEngine.Gradient::.ctor()
extern void Gradient__ctor_m4B95822B3C5187566CE4FA66E283600DCC916C5A (void);
// 0x00000315 System.Void UnityEngine.Gradient::Finalize()
extern void Gradient_Finalize_m2E940A5D5AE433B43D83B8E676FB9844E86F8CD0 (void);
// 0x00000316 System.Boolean UnityEngine.Gradient::Equals(System.Object)
extern void Gradient_Equals_m75D0B1625C55AAAEC024A951456300FEF4546EFF (void);
// 0x00000317 System.Boolean UnityEngine.Gradient::Equals(UnityEngine.Gradient)
extern void Gradient_Equals_m2F4EB14CAD1222F30E7DA925696DB1AF41CAF691 (void);
// 0x00000318 System.Int32 UnityEngine.Gradient::GetHashCode()
extern void Gradient_GetHashCode_m31528AF94CBACB9F6C453FD35BCDFABB77C9AED5 (void);
// 0x00000319 System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern void Matrix4x4__ctor_mFDDCE13D7171353ED7BA9A9B6885212DFC9E1076 (void);
// 0x0000031A System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern void Matrix4x4_GetHashCode_m102B903082CD1C786C221268A19679820E365B59 (void);
// 0x0000031B System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern void Matrix4x4_Equals_mF6EB7A6D466F5AE1D1A872451359645D1C69843D (void);
// 0x0000031C System.Boolean UnityEngine.Matrix4x4::Equals(UnityEngine.Matrix4x4)
extern void Matrix4x4_Equals_mAE7AC284A922B094E4ACCC04A1C48B247E9A7997 (void);
// 0x0000031D UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern void Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480 (void);
// 0x0000031E System.String UnityEngine.Matrix4x4::ToString()
extern void Matrix4x4_ToString_mF45C45AD892B0707C34BEE0C716C91C0B5FD2831 (void);
// 0x0000031F System.String UnityEngine.Matrix4x4::ToString(System.String,System.IFormatProvider)
extern void Matrix4x4_ToString_mC2CC8C3C358C9C982F25F633BC21105D2C3BCEFB (void);
// 0x00000320 System.Void UnityEngine.Matrix4x4::.cctor()
extern void Matrix4x4__cctor_m98C56DA6312BFD0230C12C0BABB7CF6627A9CC87 (void);
// 0x00000321 System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636 (void);
// 0x00000322 System.Int32 UnityEngine.Vector3::GetHashCode()
extern void Vector3_GetHashCode_m9F18401DA6025110A012F55BBB5ACABD36FA9A0A (void);
// 0x00000323 System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern void Vector3_Equals_m210CB160B594355581D44D4B87CF3D3994ABFED0 (void);
// 0x00000324 System.Boolean UnityEngine.Vector3::Equals(UnityEngine.Vector3)
extern void Vector3_Equals_mA92800CD98ED6A42DD7C55C5DB22DAB4DEAA6397 (void);
// 0x00000325 UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern void Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (void);
// 0x00000326 UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern void Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (void);
// 0x00000327 UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern void Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (void);
// 0x00000328 UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern void Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44 (void);
// 0x00000329 UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0 (void);
// 0x0000032A UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58 (void);
// 0x0000032B UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern void Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A (void);
// 0x0000032C System.String UnityEngine.Vector3::ToString()
extern void Vector3_ToString_mD5085501F9A0483542E9F7B18CD09C0AB977E318 (void);
// 0x0000032D System.String UnityEngine.Vector3::ToString(System.String,System.IFormatProvider)
extern void Vector3_ToString_m8E771CC90555B06B8BDBA5F691EC5D8D87D68414 (void);
// 0x0000032E System.Void UnityEngine.Vector3::.cctor()
extern void Vector3__cctor_m1630C6F57B6D41EFCDFC7A10F52A4D2448BFB2E7 (void);
// 0x0000032F UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Quaternion_FromToRotation_mD0EBB9993FC7C6A45724D0365B09F11F1CEADB80 (void);
// 0x00000330 UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern void Quaternion_Inverse_mE2A449C7AC8A40350AAC3761AE1AFC170062CAC9 (void);
// 0x00000331 UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern void Quaternion_Internal_FromEulerRad_m3D0312F9F199A8ADD7463C450B24081061C884A7 (void);
// 0x00000332 UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern void Quaternion_Internal_ToEulerRad_m442827358B6C9EB81ADCC01F316AA2201453001E (void);
// 0x00000333 UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern void Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398 (void);
// 0x00000334 UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1 (void);
// 0x00000335 UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern void Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF (void);
// 0x00000336 System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421 (void);
// 0x00000337 UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern void Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (void);
// 0x00000338 UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0 (void);
// 0x00000339 UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D (void);
// 0x0000033A UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern void Quaternion_Internal_MakePositive_mD0A2B4D1439FC9AC4B487C2392836C8436323857 (void);
// 0x0000033B UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern void Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3 (void);
// 0x0000033C UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern void Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (void);
// 0x0000033D UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern void Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB (void);
// 0x0000033E System.Int32 UnityEngine.Quaternion::GetHashCode()
extern void Quaternion_GetHashCode_mFCEA4CA542544DC9BD222C66F524C2F3CFE60744 (void);
// 0x0000033F System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern void Quaternion_Equals_m3EDD7DBA22F59A5797B91820AE4BBA64576D240F (void);
// 0x00000340 System.Boolean UnityEngine.Quaternion::Equals(UnityEngine.Quaternion)
extern void Quaternion_Equals_m02CE0D27C1DA0C037D8721750E30BB1FAF1A7DAD (void);
// 0x00000341 System.String UnityEngine.Quaternion::ToString()
extern void Quaternion_ToString_mD3D4C66907C994D30D99E76063623F7000F6998E (void);
// 0x00000342 System.String UnityEngine.Quaternion::ToString(System.String,System.IFormatProvider)
extern void Quaternion_ToString_mF10FE18AAC385F9CFE721ECD8B66F14346774F31 (void);
// 0x00000343 System.Void UnityEngine.Quaternion::.cctor()
extern void Quaternion__cctor_m580F8269E5FCCBE5C27222B87E7726823CEEC5E0 (void);
// 0x00000344 System.Void UnityEngine.Quaternion::FromToRotation_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_FromToRotation_Injected_mF0D47B601B2A983EF001C4BDDA1819DB1CAAC68E (void);
// 0x00000345 System.Void UnityEngine.Quaternion::Inverse_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern void Quaternion_Inverse_Injected_m709B75EDEB9B03431C31D5D5A100237FAB9F34D6 (void);
// 0x00000346 System.Void UnityEngine.Quaternion::Internal_FromEulerRad_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_Internal_FromEulerRad_Injected_m5220AD64F37DB56C6DFF9DAE8B78F4E3F7185110 (void);
// 0x00000347 System.Void UnityEngine.Quaternion::Internal_ToEulerRad_Injected(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void Quaternion_Internal_ToEulerRad_Injected_m4626950CCFC82D6D08FEDC4EA7F3C594C9C3DED0 (void);
// 0x00000348 System.Void UnityEngine.Quaternion::AngleAxis_Injected(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_AngleAxis_Injected_m651561C71005DEB6C7A0BF672B631DBF121B5B61 (void);
// 0x00000349 System.Void UnityEngine.Quaternion::LookRotation_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_LookRotation_Injected_m5D096FB4F030FA09FACCB550040173DE7E3F9356 (void);
// 0x0000034A System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
extern void Mathf_GammaToLinearSpace_mD7A738810039778B4592535A1DB5767C4CAD68FB (void);
// 0x0000034B System.Single UnityEngine.Mathf::Tan(System.Single)
extern void Mathf_Tan_mC7E6A6883BF16BBF77F15A1A0C35AB06DDAF48DC (void);
// 0x0000034C System.Single UnityEngine.Mathf::Atan(System.Single)
extern void Mathf_Atan_m12592B989E7F2CF919DF4223769CC480F888ADE5 (void);
// 0x0000034D System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern void Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14 (void);
// 0x0000034E System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern void Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (void);
// 0x0000034F System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern void Mathf_Clamp_mAD0781EB7470594CD4482DD64A0D739E4E539C3C (void);
// 0x00000350 System.Void UnityEngine.Mathf::.cctor()
extern void Mathf__cctor_mFC5862C195961EAE43A5C7BB96E07648084C1525 (void);
// 0x00000351 System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E (void);
// 0x00000352 System.String UnityEngine.Vector2::ToString()
extern void Vector2_ToString_mBD48EFCDB703ACCDC29E86AEB0D4D62FBA50F840 (void);
// 0x00000353 System.String UnityEngine.Vector2::ToString(System.String,System.IFormatProvider)
extern void Vector2_ToString_m503AFEA3F57B8529C047FF93C2E72126C5591C23 (void);
// 0x00000354 System.Int32 UnityEngine.Vector2::GetHashCode()
extern void Vector2_GetHashCode_m9A5DD8406289F38806CC42C394E324C1C2AB3732 (void);
// 0x00000355 System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern void Vector2_Equals_m67A842D914AA5A4DCC076E9EA20019925E6A85A0 (void);
// 0x00000356 System.Boolean UnityEngine.Vector2::Equals(UnityEngine.Vector2)
extern void Vector2_Equals_m6E08A16717F2B9EE8B24EBA6B234A03098D5F05D (void);
// 0x00000357 UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern void Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (void);
// 0x00000358 System.Void UnityEngine.Vector2::.cctor()
extern void Vector2__cctor_m64DC76912D71BE91E6A3B2D15D63452E2B3AD6EC (void);
// 0x00000359 System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern void Vector4_get_Item_m469B9D88498D0F7CD14B71A9512915BAA0B9B3B7 (void);
// 0x0000035A System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2 (void);
// 0x0000035B System.Int32 UnityEngine.Vector4::GetHashCode()
extern void Vector4_GetHashCode_mCA7B312F8CA141F6F25BABDDF406F3D2BDD5E895 (void);
// 0x0000035C System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern void Vector4_Equals_m71D14F39651C3FBEDE17214455DFA727921F07AA (void);
// 0x0000035D System.Boolean UnityEngine.Vector4::Equals(UnityEngine.Vector4)
extern void Vector4_Equals_m0919D35807550372D1748193EB31E4C5E406AE61 (void);
// 0x0000035E UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern void Vector4_get_zero_m9E807FEBC8B638914DF4A0BA87C0BD95A19F5200 (void);
// 0x0000035F System.String UnityEngine.Vector4::ToString()
extern void Vector4_ToString_mF2D17142EBD75E91BC718B3E347F614AC45E9040 (void);
// 0x00000360 System.String UnityEngine.Vector4::ToString(System.String,System.IFormatProvider)
extern void Vector4_ToString_m0EC6AA83CD606E3EB5BE60108A1D9AC4ECB5517A (void);
// 0x00000361 System.Void UnityEngine.Vector4::.cctor()
extern void Vector4__cctor_m35F167F24C48A767EAD837754896B5A5178C078A (void);
// 0x00000362 System.Void UnityEngine.IPlayerEditorConnectionNative::Initialize()
// 0x00000363 System.Void UnityEngine.IPlayerEditorConnectionNative::DisconnectAll()
// 0x00000364 System.Void UnityEngine.IPlayerEditorConnectionNative::SendMessage(System.Guid,System.Byte[],System.Int32)
// 0x00000365 System.Boolean UnityEngine.IPlayerEditorConnectionNative::TrySendMessage(System.Guid,System.Byte[],System.Int32)
// 0x00000366 System.Void UnityEngine.IPlayerEditorConnectionNative::Poll()
// 0x00000367 System.Void UnityEngine.IPlayerEditorConnectionNative::RegisterInternal(System.Guid)
// 0x00000368 System.Void UnityEngine.IPlayerEditorConnectionNative::UnregisterInternal(System.Guid)
// 0x00000369 System.Boolean UnityEngine.IPlayerEditorConnectionNative::IsConnected()
// 0x0000036A System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.SendMessage(System.Guid,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_mE7A0F096977E9FEF4139A4D66DA05DC07C67399A (void);
// 0x0000036B System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.TrySendMessage(System.Guid,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_TrySendMessage_m9573E63723FDEBB68978AD4A14253DC648071582 (void);
// 0x0000036C System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.Poll()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Poll_mA33C0EA45F7DFF196710206BD472896DD02BB0BF (void);
// 0x0000036D System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.RegisterInternal(System.Guid)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_mC10E40AF3DE9AC1E1DAC42BF2F4F738E42F1131E (void);
// 0x0000036E System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.UnregisterInternal(System.Guid)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_UnregisterInternal_m5E7A02A5A9569D111F9197ED07D5E822357FADBF (void);
// 0x0000036F System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.Initialize()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Initialize_mC461084E67159FF60B78A2B995A0D6C6A5F05847 (void);
// 0x00000370 System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.IsConnected()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_mFBCF58F025DCD0E95E1077019F30A915436221C8 (void);
// 0x00000371 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.DisconnectAll()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m39E3D0780D7BA1BBBAA514AB02C8B7121E6F1607 (void);
// 0x00000372 System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern void PlayerConnectionInternal_IsConnected_m6E50FFCA993DB110A440A175417542F19360878C (void);
// 0x00000373 System.Void UnityEngine.PlayerConnectionInternal::Initialize()
extern void PlayerConnectionInternal_Initialize_m0E2E758B321FDCBA8598FE99E453F371E8544676 (void);
// 0x00000374 System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern void PlayerConnectionInternal_RegisterInternal_mEA39746E226DE13CDA2AD91050A7B49BE6CEDFD6 (void);
// 0x00000375 System.Void UnityEngine.PlayerConnectionInternal::UnregisterInternal(System.String)
extern void PlayerConnectionInternal_UnregisterInternal_m22AB7635F9B4EE0EA1F23F61E212763375479EB0 (void);
// 0x00000376 System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_SendMessage_m28075B14E3C74180BC8B54C013D22F7B0DDEFA8E (void);
// 0x00000377 System.Boolean UnityEngine.PlayerConnectionInternal::TrySendMessage(System.String,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_TrySendMessage_m654FD63C3F37CA5381FA3C956B4C6F68EFF8A202 (void);
// 0x00000378 System.Void UnityEngine.PlayerConnectionInternal::PollInternal()
extern void PlayerConnectionInternal_PollInternal_m51A00BEDBF9EA39C42023981C0A1CA8B38116D86 (void);
// 0x00000379 System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern void PlayerConnectionInternal_DisconnectAll_mBE50046CA1DD3A969860F0D25B5FD3381907881D (void);
// 0x0000037A System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern void PlayerConnectionInternal__ctor_m220EE8E01600348418FFBC1B83BF824C39A4441B (void);
// 0x0000037B System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern void PlayerPrefs_GetString_m5709C9DC233D10A7E9AF4BCC9639E3F18FE36831 (void);
// 0x0000037C System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern void PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159 (void);
// 0x0000037D UnityEngine.Object UnityEngine.ResourceRequest::GetResult()
extern void ResourceRequest_GetResult_mBA8841E95D26D1D2E8026E7228A4A2BBCFB5923C (void);
// 0x0000037E UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern void ResourceRequest_get_asset_m2930BE33A19198B82461486BF40A9E00963A1CD0 (void);
// 0x0000037F System.Void UnityEngine.ResourceRequest::.ctor()
extern void ResourceRequest__ctor_m3E6B88D53CD7B4275A16194A6D94264BFE79D039 (void);
// 0x00000380 UnityEngine.Shader UnityEngine.ResourcesAPIInternal::FindShaderByName(System.String)
extern void ResourcesAPIInternal_FindShaderByName_m20A7CECEF3938084974ECE7F96974F04F70518AE (void);
// 0x00000381 UnityEngine.Object UnityEngine.ResourcesAPIInternal::Load(System.String,System.Type)
extern void ResourcesAPIInternal_Load_m42611D41AFAEBCDD60B948317770FB1AB299BD3F (void);
// 0x00000382 UnityEngine.Object[] UnityEngine.ResourcesAPIInternal::LoadAll(System.String,System.Type)
extern void ResourcesAPIInternal_LoadAll_m47ADA530A4F3474C3E0A245219AC08A7F270F109 (void);
// 0x00000383 UnityEngine.ResourceRequest UnityEngine.ResourcesAPIInternal::LoadAsyncInternal(System.String,System.Type)
extern void ResourcesAPIInternal_LoadAsyncInternal_mEE9EDC76922268050EA729CD2DEB3F2F43437219 (void);
// 0x00000384 System.Void UnityEngine.ResourcesAPIInternal::UnloadAsset(UnityEngine.Object)
extern void ResourcesAPIInternal_UnloadAsset_mB3CC2F26739646A6EA1C4DE8AA47BED3EAA8851C (void);
// 0x00000385 UnityEngine.ResourcesAPI UnityEngine.ResourcesAPI::get_ActiveAPI()
extern void ResourcesAPI_get_ActiveAPI_mA3236B01A2D59991780A82398914A19869714890 (void);
// 0x00000386 UnityEngine.ResourcesAPI UnityEngine.ResourcesAPI::get_overrideAPI()
extern void ResourcesAPI_get_overrideAPI_mD588ADEA9E8093DD30251CC27D053EE53F5ACAA6 (void);
// 0x00000387 System.Void UnityEngine.ResourcesAPI::.ctor()
extern void ResourcesAPI__ctor_m2B10F95A3C78C4AF1433922F9EFAAC532874D912 (void);
// 0x00000388 UnityEngine.Shader UnityEngine.ResourcesAPI::FindShaderByName(System.String)
extern void ResourcesAPI_FindShaderByName_m39FC4C8EA2CA7BF0FB1EB88ED83E913CFC467E84 (void);
// 0x00000389 UnityEngine.Object UnityEngine.ResourcesAPI::Load(System.String,System.Type)
extern void ResourcesAPI_Load_m2E98767AD80F67567FCC40CD2AFD6BCB6AA9B7CD (void);
// 0x0000038A UnityEngine.Object[] UnityEngine.ResourcesAPI::LoadAll(System.String,System.Type)
extern void ResourcesAPI_LoadAll_m2B9D4A8AE6629ACA40D402146223522653518F71 (void);
// 0x0000038B UnityEngine.ResourceRequest UnityEngine.ResourcesAPI::LoadAsync(System.String,System.Type)
extern void ResourcesAPI_LoadAsync_m87E06332626E339F5FD9E7DEBCF5816E87D215B1 (void);
// 0x0000038C System.Void UnityEngine.ResourcesAPI::UnloadAsset(UnityEngine.Object)
extern void ResourcesAPI_UnloadAsset_mE8D84256E168AA7C93818B55889650A583157CC8 (void);
// 0x0000038D System.Void UnityEngine.ResourcesAPI::.cctor()
extern void ResourcesAPI__cctor_mF299A2749244ECE9CA0B1686E6C363EE37BA8952 (void);
// 0x0000038E UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern void Resources_Load_m6E8E5EA02A03F3AFC8FD2D775263DBBC64BF205C (void);
// 0x0000038F UnityEngine.ResourceRequest UnityEngine.Resources::LoadAsync(System.String,System.Type)
extern void Resources_LoadAsync_m37D45F0C403C6CF66A7BE25596B9B57C989F1E70 (void);
// 0x00000390 UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
extern void Resources_LoadAll_m84D60782124BC4C7656F4CE88B03D45DF59F9A3C (void);
// 0x00000391 System.Void UnityEngine.Resources::UnloadAsset(UnityEngine.Object)
extern void Resources_UnloadAsset_mAF04A1651C8F21A39CEB5018785B52FFC6336B00 (void);
// 0x00000392 System.Void UnityEngine.AsyncOperation::InternalDestroy(System.IntPtr)
extern void AsyncOperation_InternalDestroy_mB659E46A7DE3337448BACCD77F5B64927877F482 (void);
// 0x00000393 System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern void AsyncOperation_get_isDone_m4592F121393149E539D2107239639A049493D877 (void);
// 0x00000394 System.Single UnityEngine.AsyncOperation::get_progress()
extern void AsyncOperation_get_progress_m2471A0564D5C2207116737619E2CED05FBBC2D19 (void);
// 0x00000395 System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
extern void AsyncOperation_set_priority_mB731BA4B2803787F73DCB1AA82B3F28AED9A4289 (void);
// 0x00000396 System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
extern void AsyncOperation_set_allowSceneActivation_mFA2C12F4A7D138D4CED4BA72F9E97AF0BD117C33 (void);
// 0x00000397 System.Void UnityEngine.AsyncOperation::Finalize()
extern void AsyncOperation_Finalize_m1A989E71664DD802015D858E7A336A1158BD474C (void);
// 0x00000398 System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern void AsyncOperation_InvokeCompletionEvent_m2BFBB3DD63950957EDE38AE0A8D2587B900CB8F5 (void);
// 0x00000399 System.Void UnityEngine.AsyncOperation::add_completed(System.Action`1<UnityEngine.AsyncOperation>)
extern void AsyncOperation_add_completed_m44D28A82BB10C85AED56A43BB666850D2E9E59E8 (void);
// 0x0000039A System.Void UnityEngine.AsyncOperation::remove_completed(System.Action`1<UnityEngine.AsyncOperation>)
extern void AsyncOperation_remove_completed_m5B81CC486905074B13D3784BD0A061F2B4BCC3F3 (void);
// 0x0000039B System.Void UnityEngine.AsyncOperation::.ctor()
extern void AsyncOperation__ctor_mFC0E13622A23CD19A631B9ABBA506683B71A2E4A (void);
// 0x0000039C System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m9BA10D3F7DEEE7FB825187CF60338BBECA83F4B2 (void);
// 0x0000039D System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern void AttributeHelperEngine_GetRequiredComponents_mE25F6E1B6C8E4BB44FDE3E418DD442A12AA24063 (void);
// 0x0000039E System.Int32 UnityEngine.AttributeHelperEngine::GetExecuteMode(System.Type)
extern void AttributeHelperEngine_GetExecuteMode_mB33B8C49CC1EE05341F8ADFBF9B0EEB4894C0864 (void);
// 0x0000039F System.Int32 UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern void AttributeHelperEngine_CheckIsEditorScript_m109EDB093200EAAA9DA7203E58385429F791CA24 (void);
// 0x000003A0 System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern void AttributeHelperEngine_GetDefaultExecutionOrderFor_m4425CE70A70DB0716D3A5BF8C51C53C6A7891131 (void);
// 0x000003A1 T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType(System.Type)
// 0x000003A2 System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern void AttributeHelperEngine__cctor_m00AE154DE9BE8D99EBFBBE005F9FC40109E8FB19 (void);
// 0x000003A3 System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (void);
// 0x000003A4 System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (void);
// 0x000003A5 System.Void UnityEngine.HideInInspector::.ctor()
extern void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (void);
// 0x000003A6 System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern void DefaultExecutionOrder_get_order_m1C25A6D0F7F67A70D1B6B99D45C5A242DF92A4D2 (void);
// 0x000003A7 System.Void UnityEngine.ExcludeFromPresetAttribute::.ctor()
extern void ExcludeFromPresetAttribute__ctor_mE4AF4E74678A39848AD81121936DEDDA1F89AB8B (void);
// 0x000003A8 System.Void UnityEngine.Behaviour::.ctor()
extern void Behaviour__ctor_mCACD3614226521EA607B0F3640C0FAC7EACCBCE0 (void);
// 0x000003A9 System.Void UnityEngine.ClassLibraryInitializer::Init()
extern void ClassLibraryInitializer_Init_m462E6CE3B1A6017AB1ADA0ACCEACBBB0B614F564 (void);
// 0x000003AA UnityEngine.Transform UnityEngine.Component::get_transform()
extern void Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (void);
// 0x000003AB UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern void Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (void);
// 0x000003AC System.Void UnityEngine.Component::.ctor()
extern void Component__ctor_m0B00FA207EB3E560B78938D8AD877DB2BC1E3722 (void);
// 0x000003AD System.Void UnityEngine.Coroutine::.ctor()
extern void Coroutine__ctor_mCB658B6AD0EABDAB709A53D2B111955E06CE3C61 (void);
// 0x000003AE System.Void UnityEngine.Coroutine::Finalize()
extern void Coroutine_Finalize_m7248D49A93F72CA5E84EAD20A32ADE028905C536 (void);
// 0x000003AF System.Void UnityEngine.Coroutine::ReleaseCoroutine(System.IntPtr)
extern void Coroutine_ReleaseCoroutine_m2C46DD57BDB3BB7B64204EA229F4C5CDE342B18B (void);
// 0x000003B0 System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern void SetupCoroutine_InvokeMoveNext_m036E6EE8C2A4D2DAA957D5702F1A3CA51313F2C7 (void);
// 0x000003B1 System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern void SetupCoroutine_InvokeMember_m953E000F2B95EA72D6B1BC2330F0C844A2C0C680 (void);
// 0x000003B2 System.Void UnityEngine.ExcludeFromObjectFactoryAttribute::.ctor()
extern void ExcludeFromObjectFactoryAttribute__ctor_mAF8163E246AD4F05E98775F7E0904F296770B06C (void);
// 0x000003B3 System.Void UnityEngine.ExtensionOfNativeClassAttribute::.ctor()
extern void ExtensionOfNativeClassAttribute__ctor_mC11B09D547C215DC6E298F78CA6F05C2B28B8043 (void);
// 0x000003B4 UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern void GameObject_CreatePrimitive_mB1E03B8D373EBECCD93444A277316A53EC7812AC (void);
// 0x000003B5 T UnityEngine.GameObject::GetComponent()
// 0x000003B6 UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern void GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473 (void);
// 0x000003B7 System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern void GameObject_GetComponentFastPath_mAC58DB8AC26576ED2A87C843A68A13C325E3C944 (void);
// 0x000003B8 UnityEngine.Component UnityEngine.GameObject::GetComponentByName(System.String)
extern void GameObject_GetComponentByName_m40C10D22A73FF4260A0D686611A58E121773B481 (void);
// 0x000003B9 UnityEngine.Component UnityEngine.GameObject::GetComponent(System.String)
extern void GameObject_GetComponent_mA67EB0D09F51B759C259A15A4B0B8E0E58A9B439 (void);
// 0x000003BA UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern void GameObject_GetComponentInChildren_m56CAFD886686C8F6025B5CDF016E8BC684A20EED (void);
// 0x000003BB UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern void GameObject_GetComponentInChildren_m7D7A7C91CA8B24310C9CECD41C362C327D926B07 (void);
// 0x000003BC T UnityEngine.GameObject::GetComponentInChildren()
// 0x000003BD T UnityEngine.GameObject::GetComponentInChildren(System.Boolean)
// 0x000003BE UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type,System.Boolean)
extern void GameObject_GetComponentInParent_m5C1C3266DE9C2EE078C905787DDCD666AB157C2B (void);
// 0x000003BF UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern void GameObject_GetComponentInParent_m84A8233060CF7F5043960E30EBC1FC715DDF9862 (void);
// 0x000003C0 T UnityEngine.GameObject::GetComponentInParent()
// 0x000003C1 T UnityEngine.GameObject::GetComponentInParent(System.Boolean)
// 0x000003C2 System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern void GameObject_GetComponentsInternal_mF491A337A167109189E2AB839584697EB2672E7D (void);
// 0x000003C3 UnityEngine.Component[] UnityEngine.GameObject::GetComponents(System.Type)
extern void GameObject_GetComponents_mC2A40439A0A5AD8C676BF863FA33CA80A7B31D36 (void);
// 0x000003C4 T[] UnityEngine.GameObject::GetComponents()
// 0x000003C5 System.Void UnityEngine.GameObject::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern void GameObject_GetComponents_m5FC485B63D5CFE373765247B1BA87BDFCCB10619 (void);
// 0x000003C6 System.Void UnityEngine.GameObject::GetComponents(System.Collections.Generic.List`1<T>)
// 0x000003C7 UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type)
extern void GameObject_GetComponentsInChildren_m59898B5E01DFE194028475267F99BEF3B64459FE (void);
// 0x000003C8 UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type,System.Boolean)
extern void GameObject_GetComponentsInChildren_mDDF992919403B6C61AF715176CC8E2EB5C8B1671 (void);
// 0x000003C9 T[] UnityEngine.GameObject::GetComponentsInChildren(System.Boolean)
// 0x000003CA System.Void UnityEngine.GameObject::GetComponentsInChildren(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x000003CB T[] UnityEngine.GameObject::GetComponentsInChildren()
// 0x000003CC System.Void UnityEngine.GameObject::GetComponentsInChildren(System.Collections.Generic.List`1<T>)
// 0x000003CD UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInParent(System.Type)
extern void GameObject_GetComponentsInParent_m4E353C6B98E9AEEB4514AE670F9CAC6E59CFA3B8 (void);
// 0x000003CE UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInParent(System.Type,System.Boolean)
extern void GameObject_GetComponentsInParent_m92FBACB29B3B6B982EAE3CE0D122592B3D53A314 (void);
// 0x000003CF System.Void UnityEngine.GameObject::GetComponentsInParent(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x000003D0 T[] UnityEngine.GameObject::GetComponentsInParent(System.Boolean)
// 0x000003D1 T[] UnityEngine.GameObject::GetComponentsInParent()
// 0x000003D2 System.Boolean UnityEngine.GameObject::TryGetComponent(T&)
// 0x000003D3 System.Boolean UnityEngine.GameObject::TryGetComponent(System.Type,UnityEngine.Component&)
extern void GameObject_TryGetComponent_m467E0D5A83D4238B9EEDE5DB7E280DC0CD88523B (void);
// 0x000003D4 UnityEngine.Component UnityEngine.GameObject::TryGetComponentInternal(System.Type)
extern void GameObject_TryGetComponentInternal_m1A308917341105C8ACCF4B9A87604D871190CCB8 (void);
// 0x000003D5 System.Void UnityEngine.GameObject::TryGetComponentFastPath(System.Type,System.IntPtr)
extern void GameObject_TryGetComponentFastPath_m45DE1DF12AFA1F21F16E0249A472A5FCEE6026FD (void);
// 0x000003D6 UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern void GameObject_FindWithTag_mEF75D1FF1E55B338A77161FDCB68ED0A2A911DF3 (void);
// 0x000003D7 System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessageUpwards_mF936DE9B0E2CE786086B9B55A78407CBDAF4BE59 (void);
// 0x000003D8 System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessage_m0B456BCEAFEC3945E9350D7FFC8E3D98C8DEE94D (void);
// 0x000003D9 System.Void UnityEngine.GameObject::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern void GameObject_BroadcastMessage_mCEDA3C2B8C9BC1664AE2564F2B1858FE6D7A7521 (void);
// 0x000003DA UnityEngine.Component UnityEngine.GameObject::AddComponentInternal(System.String)
extern void GameObject_AddComponentInternal_m075E64DA8B203927479674247AE2C82D369608E7 (void);
// 0x000003DB UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern void GameObject_Internal_AddComponentWithType_mAD63AAF65D0603B157D8CC6C27F3EC73C108ADB4 (void);
// 0x000003DC UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern void GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8 (void);
// 0x000003DD T UnityEngine.GameObject::AddComponent()
// 0x000003DE UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern void GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (void);
// 0x000003DF System.Int32 UnityEngine.GameObject::get_layer()
extern void GameObject_get_layer_m9D4C23A2FD105AF9964445BF18A77E8A49012F9F (void);
// 0x000003E0 System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern void GameObject_set_layer_m2F946916ACB41A59C46346F5243F2BAC235A36A6 (void);
// 0x000003E1 System.Boolean UnityEngine.GameObject::get_active()
extern void GameObject_get_active_mAE45BB4A1D06BE2AF8C460593FC0346A5EF8014D (void);
// 0x000003E2 System.Void UnityEngine.GameObject::set_active(System.Boolean)
extern void GameObject_set_active_mC5C02354788BC2EDF19359EEAE7396BE350C2BFA (void);
// 0x000003E3 System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (void);
// 0x000003E4 System.Boolean UnityEngine.GameObject::get_activeSelf()
extern void GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (void);
// 0x000003E5 System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern void GameObject_get_activeInHierarchy_mA3990AC5F61BB35283188E925C2BE7F7BF67734B (void);
// 0x000003E6 System.Void UnityEngine.GameObject::SetActiveRecursively(System.Boolean)
extern void GameObject_SetActiveRecursively_m819729B12CD529ED804FD9E51070DA8782EECB4D (void);
// 0x000003E7 System.Boolean UnityEngine.GameObject::get_isStatic()
extern void GameObject_get_isStatic_m13AA6BF963533823FD6E78B36D4B035EA0B6B700 (void);
// 0x000003E8 System.Void UnityEngine.GameObject::set_isStatic(System.Boolean)
extern void GameObject_set_isStatic_m4199CD3FECAD0351AED29C9E67220D7E8794976B (void);
// 0x000003E9 System.Boolean UnityEngine.GameObject::get_isStaticBatchable()
extern void GameObject_get_isStaticBatchable_m0DCEB577DB5FCFADCF8DD8AB6300EE70C19977CC (void);
// 0x000003EA System.String UnityEngine.GameObject::get_tag()
extern void GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33 (void);
// 0x000003EB System.Void UnityEngine.GameObject::set_tag(System.String)
extern void GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE (void);
// 0x000003EC System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern void GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001 (void);
// 0x000003ED UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern void GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (void);
// 0x000003EE UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern void GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186 (void);
// 0x000003EF System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessageUpwards_mAFC79A352F0A92A902A1067A9CF833CD28DA71E1 (void);
// 0x000003F0 System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,System.Object)
extern void GameObject_SendMessageUpwards_mD62B84BF7340A8341D9F704434CE15F249F156C7 (void);
// 0x000003F1 System.Void UnityEngine.GameObject::SendMessageUpwards(System.String)
extern void GameObject_SendMessageUpwards_m952AB29F8D6749176E44BD4E95B3AAADD5BB5536 (void);
// 0x000003F2 System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C (void);
// 0x000003F3 System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object)
extern void GameObject_SendMessage_m9727C08D6F0A5E8F309FA9FFF389ADF8130D7BE7 (void);
// 0x000003F4 System.Void UnityEngine.GameObject::SendMessage(System.String)
extern void GameObject_SendMessage_m592E9A21D51BE9E1D9E23A85750548E8CC8DB00D (void);
// 0x000003F5 System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_BroadcastMessage_m2B5D6163ABB0ED80A381A41DC84ED48CC10212AD (void);
// 0x000003F6 System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object)
extern void GameObject_BroadcastMessage_m181D34641FC022B195C4AB970B2AE4F5CB485CEB (void);
// 0x000003F7 System.Void UnityEngine.GameObject::BroadcastMessage(System.String)
extern void GameObject_BroadcastMessage_m70A03FA2B67C28818B43C65870EC3D5E680B9A9E (void);
// 0x000003F8 System.Void UnityEngine.GameObject::.ctor(System.String)
extern void GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144 (void);
// 0x000003F9 System.Void UnityEngine.GameObject::.ctor()
extern void GameObject__ctor_mACDBD7A1F25B33D006A60F67EF901B33DD3D52E9 (void);
// 0x000003FA System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern void GameObject__ctor_m9829583AE3BF1285861C580895202F760F3A82E8 (void);
// 0x000003FB System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern void GameObject_Internal_CreateGameObject_mA5BCF00F09243D45B7E9A1A421D8357610AE8633 (void);
// 0x000003FC UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern void GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B (void);
// 0x000003FD UnityEngine.SceneManagement.Scene UnityEngine.GameObject::get_scene()
extern void GameObject_get_scene_m7EBF95ABB5037FEE6811928F2E83C769C53F86C2 (void);
// 0x000003FE System.UInt64 UnityEngine.GameObject::get_sceneCullingMask()
extern void GameObject_get_sceneCullingMask_m35AC32251FCD8B2E5B61B9E5CB33DC8C1047E985 (void);
// 0x000003FF UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern void GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6 (void);
// 0x00000400 System.Void UnityEngine.GameObject::get_scene_Injected(UnityEngine.SceneManagement.Scene&)
extern void GameObject_get_scene_Injected_mE08525424E75EFD26BD152B4AC37563B0D4053F2 (void);
// 0x00000401 System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern void ManagedStreamHelpers_ValidateLoadFromStream_mDE750EE2AF2986BB8E11941D8513AD18597F3B13 (void);
// 0x00000402 System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamRead(System.Byte[],System.Int32,System.Int32,System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamRead_m5E2F163F844AE93A058C5B4E31A011938FAE236B (void);
// 0x00000403 System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamSeek(System.Int64,System.UInt32,System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamSeek_mD7B16AF1079F3F11EE782A32F851ABD761BD2E9E (void);
// 0x00000404 System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamLength(System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamLength_m352520A9914611ADA61F089CCA8D996F62F9857F (void);
// 0x00000405 System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern void MonoBehaviour_IsInvoking_m7967B0E7FEE6F3FAD9E7565D37B009A8C43FE9A8 (void);
// 0x00000406 System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern void MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D (void);
// 0x00000407 System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern void MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4 (void);
// 0x00000408 System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern void MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70 (void);
// 0x00000409 System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern void MonoBehaviour_CancelInvoke_mAD4E486A74AF79DC1AFA880691EF839CDDE630A9 (void);
// 0x0000040A System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern void MonoBehaviour_IsInvoking_m6BF9433789CE7CE12E12069DBF043D620DFB0B4F (void);
// 0x0000040B UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern void MonoBehaviour_StartCoroutine_m338FBDDDEBF67D9FC1F9E5CDEE50E66726454E2E (void);
// 0x0000040C UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern void MonoBehaviour_StartCoroutine_m81C113F45C28D065C9E31DD6D7566D1BF10CF851 (void);
// 0x0000040D UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (void);
// 0x0000040E UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutine_Auto_m78A5B32C9E51A3C64C19BB73ED4A9A9B7536677A (void);
// 0x0000040F System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern void MonoBehaviour_StopCoroutine_m3AB89AE7770E06BDB33BF39104BE5C57DF90616B (void);
// 0x00000410 System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern void MonoBehaviour_StopCoroutine_m5FF0476C9886FD8A3E6BA82BBE34B896CA279413 (void);
// 0x00000411 System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern void MonoBehaviour_StopCoroutine_m4DB2A899F9BDF8CA3264DD8C4130E767702B626B (void);
// 0x00000412 System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern void MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4 (void);
// 0x00000413 System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern void MonoBehaviour_get_useGUILayout_m42BE255035467866EB989932C62E99D5BC347524 (void);
// 0x00000414 System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern void MonoBehaviour_set_useGUILayout_m2FEF8B91090540BBED30EE904B11F0FB5F7C1E27 (void);
// 0x00000415 System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern void MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090 (void);
// 0x00000416 System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll(UnityEngine.MonoBehaviour)
extern void MonoBehaviour_Internal_CancelInvokeAll_mE619220C0A18AE2657DFABDBCC54E54F53C60D83 (void);
// 0x00000417 System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll(UnityEngine.MonoBehaviour)
extern void MonoBehaviour_Internal_IsInvokingAll_m898D1B5B37BBFC74C98EA5F86544987A5B41C49B (void);
// 0x00000418 System.Void UnityEngine.MonoBehaviour::InvokeDelayed(UnityEngine.MonoBehaviour,System.String,System.Single,System.Single)
extern void MonoBehaviour_InvokeDelayed_mD3EE47F65885A45357A5822A82F48E17D24C62C7 (void);
// 0x00000419 System.Void UnityEngine.MonoBehaviour::CancelInvoke(UnityEngine.MonoBehaviour,System.String)
extern void MonoBehaviour_CancelInvoke_m6BBDCBE18EEBE2584EED4F8706DA3BC6771E2529 (void);
// 0x0000041A System.Boolean UnityEngine.MonoBehaviour::IsInvoking(UnityEngine.MonoBehaviour,System.String)
extern void MonoBehaviour_IsInvoking_m2584CADBA55F43D3A1D9955F1E9EAA579B8FD206 (void);
// 0x0000041B System.Boolean UnityEngine.MonoBehaviour::IsObjectMonoBehaviour(UnityEngine.Object)
extern void MonoBehaviour_IsObjectMonoBehaviour_mE580D905186AD91FD74214B643B26F55D54A46AF (void);
// 0x0000041C UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged(System.String,System.Object)
extern void MonoBehaviour_StartCoroutineManaged_mAA34ABF1564BF65264FF972AF41CF8C0F6A6B6F4 (void);
// 0x0000041D UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged2(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutineManaged2_m46EFC2DA4428D3CC64BA3F1394D24351E316577D (void);
// 0x0000041E System.Void UnityEngine.MonoBehaviour::StopCoroutineManaged(UnityEngine.Coroutine)
extern void MonoBehaviour_StopCoroutineManaged_m8214F615A10BC1C8C78CEE92DF921C892BE3603D (void);
// 0x0000041F System.Void UnityEngine.MonoBehaviour::StopCoroutineFromEnumeratorManaged(System.Collections.IEnumerator)
extern void MonoBehaviour_StopCoroutineFromEnumeratorManaged_m17DB11886ABA10DF9749F5E1DDCCD14AD4B5E31E (void);
// 0x00000420 System.String UnityEngine.MonoBehaviour::GetScriptClassName()
extern void MonoBehaviour_GetScriptClassName_m2968E4771004FC58819BF2D9391DED766D1213E3 (void);
// 0x00000421 System.Void UnityEngine.MonoBehaviour::.ctor()
extern void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (void);
// 0x00000422 System.Void UnityEngine.NoAllocHelpers::ResizeList(System.Collections.Generic.List`1<T>,System.Int32)
// 0x00000423 System.Void UnityEngine.NoAllocHelpers::EnsureListElemCount(System.Collections.Generic.List`1<T>,System.Int32)
// 0x00000424 T[] UnityEngine.NoAllocHelpers::ExtractArrayFromListT(System.Collections.Generic.List`1<T>)
// 0x00000425 System.Void UnityEngine.NoAllocHelpers::Internal_ResizeList(System.Object,System.Int32)
extern void NoAllocHelpers_Internal_ResizeList_m32452578286848AD58CF77E1CA6B078492F723F6 (void);
// 0x00000426 System.Array UnityEngine.NoAllocHelpers::ExtractArrayFromList(System.Object)
extern void NoAllocHelpers_ExtractArrayFromList_m097334749C829402A9634BF025A54F3918D238DD (void);
// 0x00000427 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor()
extern void RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081 (void);
// 0x00000428 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (void);
// 0x00000429 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m5C045AAF89A8C1541871F7F9090B3C0A289E32C6 (void);
// 0x0000042A System.Void UnityEngine.ScriptableObject::.ctor()
extern void ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063 (void);
// 0x0000042B UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern void ScriptableObject_CreateInstance_m5371BDC0B4F60FE15914A7BB3FBE07D0ACA0A8D4 (void);
// 0x0000042C T UnityEngine.ScriptableObject::CreateInstance()
// 0x0000042D System.Void UnityEngine.ScriptableObject::CreateScriptableObject(UnityEngine.ScriptableObject)
extern void ScriptableObject_CreateScriptableObject_m9627DCBB805911280823940601D996E008021D6B (void);
// 0x0000042E UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateScriptableObjectInstanceFromType(System.Type,System.Boolean)
extern void ScriptableObject_CreateScriptableObjectInstanceFromType_mA2EB72F4D5FC5643D7CFFD07A29DD726CAB1B9AD (void);
// 0x0000042F System.Boolean UnityEngine.ScriptingUtility::IsManagedCodeWorking()
extern void ScriptingUtility_IsManagedCodeWorking_m176E49DF0BCA69480A3D9360DAED8DDDB8732F68 (void);
// 0x00000430 System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern void StackTraceUtility_SetProjectFolder_m4CF077574CDE9A65A3546973395B4A228C1F957C (void);
// 0x00000431 System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern void StackTraceUtility_ExtractStackTrace_mDD5E4AEC754FEB52C9FFA3B0675E57088B425EC6 (void);
// 0x00000432 System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern void StackTraceUtility_ExtractStringFromExceptionInternal_mE6192186E0D4CA0B148C602A5CDA6466EFA23D99 (void);
// 0x00000433 System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern void StackTraceUtility_ExtractFormattedStackTrace_m956907F6BE8EFF9BE9847275406FFBBB5FE7F093 (void);
// 0x00000434 System.Void UnityEngine.StackTraceUtility::.cctor()
extern void StackTraceUtility__cctor_m8AFBE529BA4A1737BAF53BB4F8028E18D2D84A5F (void);
// 0x00000435 System.Void UnityEngine.UnityException::.ctor()
extern void UnityException__ctor_m8E5C592F5F76B6385D4EFDC2C28AA93B020279E7 (void);
// 0x00000436 System.Void UnityEngine.UnityException::.ctor(System.String)
extern void UnityException__ctor_mB8EBFD7A68451D56285E7D51B42FBECFC8A141D8 (void);
// 0x00000437 System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnityException__ctor_m00AA4E67E35F95220B5A1C46C9B59AF4ED0D6274 (void);
// 0x00000438 System.Byte[] UnityEngine.TextAsset::get_bytes()
extern void TextAsset_get_bytes_m5F15438DABBBAAF7434D53B6778A97A498C1940F (void);
// 0x00000439 System.String UnityEngine.TextAsset::get_text()
extern void TextAsset_get_text_m89A756483BA3218E173F5D62A582070714BC1218 (void);
// 0x0000043A System.String UnityEngine.TextAsset::ToString()
extern void TextAsset_ToString_m55E5177185AA59560459F579E36C03CF1971EC57 (void);
// 0x0000043B System.String UnityEngine.TextAsset::DecodeString(System.Byte[])
extern void TextAsset_DecodeString_m3CD8D6865DCE58592AE76360F4DB856A6962FE13 (void);
// 0x0000043C System.Void UnityEngine.TextAsset/EncodingUtility::.cctor()
extern void EncodingUtility__cctor_m6BADEB7670563CC438C10AF259028A7FF06AD65F (void);
// 0x0000043D System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern void UnhandledExceptionHandler_RegisterUECatcher_m3A92AB19701D52AE35F525E9518DAD6763D66A93 (void);
// 0x0000043E System.Void UnityEngine.UnhandledExceptionHandler/<>c::.cctor()
extern void U3CU3Ec__cctor_m5B3F5B8A2DD74DC42F3E777C1FDD1C880EFA95BA (void);
// 0x0000043F System.Void UnityEngine.UnhandledExceptionHandler/<>c::.ctor()
extern void U3CU3Ec__ctor_mB628041C94E761F86F2A26819A038D6BC59E324D (void);
// 0x00000440 System.Void UnityEngine.UnhandledExceptionHandler/<>c::<RegisterUECatcher>b__0_0(System.Object,System.UnhandledExceptionEventArgs)
extern void U3CU3Ec_U3CRegisterUECatcherU3Eb__0_0_mB2E6DD6B9C72FA3D5DB8D311DB281F272A587278 (void);
// 0x00000441 System.Int32 UnityEngine.Object::GetInstanceID()
extern void Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501 (void);
// 0x00000442 System.Int32 UnityEngine.Object::GetHashCode()
extern void Object_GetHashCode_m7CC0B54570AA90E51ED2D2D6F6F078BEF9996538 (void);
// 0x00000443 System.Boolean UnityEngine.Object::Equals(System.Object)
extern void Object_Equals_m42425AB7E6C5B6E2BAB15B3184B23371AB0B3435 (void);
// 0x00000444 System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern void Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (void);
// 0x00000445 System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern void Object_CompareBaseObjects_m412CC4C56457345D91A509C83A0B04E3AE5A0AED (void);
// 0x00000446 System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern void Object_IsNativeObjectAlive_mE631050FBED7A72BC8A0394F26FF1984F546B6D4 (void);
// 0x00000447 System.IntPtr UnityEngine.Object::GetCachedPtr()
extern void Object_GetCachedPtr_m9F3F26858655E3CE7D471324271E3E32C7AEFD7A (void);
// 0x00000448 System.String UnityEngine.Object::get_name()
extern void Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (void);
// 0x00000449 System.Void UnityEngine.Object::set_name(System.String)
extern void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (void);
// 0x0000044A UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Instantiate_mADD3254CB5E6AB56FBB6964FE4B930F5187AB5AD (void);
// 0x0000044B UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Object_Instantiate_m0B91F1876CDBE46242A9E5B32F9EE53FAF2BDD99 (void);
// 0x0000044C UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern void Object_Instantiate_m565A02EA45AEA7442E19FDC8E82695491938CB5A (void);
// 0x0000044D UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern void Object_Instantiate_mA9A37F53F535C1FE2603DC002263C85337C1FE18 (void);
// 0x0000044E T UnityEngine.Object::Instantiate(T)
// 0x0000044F T UnityEngine.Object::Instantiate(T,UnityEngine.Vector3,UnityEngine.Quaternion)
// 0x00000450 T UnityEngine.Object::Instantiate(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
// 0x00000451 T UnityEngine.Object::Instantiate(T,UnityEngine.Transform,System.Boolean)
// 0x00000452 System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern void Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7 (void);
// 0x00000453 System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (void);
// 0x00000454 System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern void Object_DestroyImmediate_m69AA5B06236FACFF316DDFAD131B2622B397AC49 (void);
// 0x00000455 System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern void Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422 (void);
// 0x00000456 UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type,System.Boolean)
extern void Object_FindObjectsOfType_m0AEB81CC6F1D224A6F4DCC7D553482D54FC03C5A (void);
// 0x00000457 System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (void);
// 0x00000458 System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (void);
// 0x00000459 T UnityEngine.Object::FindObjectOfType()
// 0x0000045A System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern void Object_CheckNullArgument_mFA979ED3433CACA46AC9AE0029A537B46E17D080 (void);
// 0x0000045B UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type,System.Boolean)
extern void Object_FindObjectOfType_mFDEFCE84CE9C644F2B51DCC26CDAC78AC7E89B1B (void);
// 0x0000045C System.String UnityEngine.Object::ToString()
extern void Object_ToString_m2B1E5081A55425442324F437090FAD552931A7E9 (void);
// 0x0000045D System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern void Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (void);
// 0x0000045E System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern void Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (void);
// 0x0000045F System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern void Object_GetOffsetOfInstanceIDInCPlusPlusObject_m7749BCDBEBAE50378BE38DA313E2D854D77BB18C (void);
// 0x00000460 UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern void Object_Internal_CloneSingle_m6C669D602DFD7BC6C47ACA19B2F4D7C853F124BB (void);
// 0x00000461 UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern void Object_Internal_CloneSingleWithParent_m56C2AADC1D19A4D84AB18E8A2C0247528B8BF6FB (void);
// 0x00000462 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Internal_InstantiateSingle_mE1865EB98332FDFEDC294AD90748D28ED272021A (void);
// 0x00000463 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Internal_InstantiateSingleWithParent_m4702089B268A30D3A6FBE780B7E78833585054E4 (void);
// 0x00000464 System.String UnityEngine.Object::ToString(UnityEngine.Object)
extern void Object_ToString_m4E499FDF54B8B2727FCCE6EB7BEB976BAB670030 (void);
// 0x00000465 System.String UnityEngine.Object::GetName(UnityEngine.Object)
extern void Object_GetName_m6F0498EEECA37CD27B052F53ECDDA019129F3D7A (void);
// 0x00000466 System.Void UnityEngine.Object::SetName(UnityEngine.Object,System.String)
extern void Object_SetName_mD80C3B5E390937EF4885BE21FBAC3D91A1C6EC96 (void);
// 0x00000467 System.Void UnityEngine.Object::.ctor()
extern void Object__ctor_m4DCF5CDB32C2C69290894101A81F473865169279 (void);
// 0x00000468 System.Void UnityEngine.Object::.cctor()
extern void Object__cctor_mD6AB403C7A4ED74F04167372E52C3C876EC422A1 (void);
// 0x00000469 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle_Injected(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Object_Internal_InstantiateSingle_Injected_mD475F2979EDD95363A9AC52EC1BB3E98E6D1D3C2 (void);
// 0x0000046A UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent_Injected(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Object_Internal_InstantiateSingleWithParent_Injected_mC883CE3023EAF9F3336A534D5B698C71457ECD97 (void);
// 0x0000046B System.Void UnityEngine.UnitySynchronizationContext::.ctor(System.Int32)
extern void UnitySynchronizationContext__ctor_mF71A02778FCC672CC2FE4F329D9D6C66C96F70CF (void);
// 0x0000046C System.Void UnityEngine.UnitySynchronizationContext::.ctor(System.Collections.Generic.List`1<UnityEngine.UnitySynchronizationContext/WorkRequest>,System.Int32)
extern void UnitySynchronizationContext__ctor_m266FAB5B25698C86EC1AC37CE4BBA1FA244BB4FC (void);
// 0x0000046D System.Void UnityEngine.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_mEDA081A69B18358B9239E2A46E570466E5FA77F7 (void);
// 0x0000046E System.Void UnityEngine.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_mE3277DB5A0DCB461E497EC7B9C13850D4E7AB076 (void);
// 0x0000046F System.Threading.SynchronizationContext UnityEngine.UnitySynchronizationContext::CreateCopy()
extern void UnitySynchronizationContext_CreateCopy_mBB5F2E7947732680B0715AD92187E89211AE5E61 (void);
// 0x00000470 System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern void UnitySynchronizationContext_Exec_mC89E49BFB922E69AAE753887480031A142016F81 (void);
// 0x00000471 System.Boolean UnityEngine.UnitySynchronizationContext::HasPendingTasks()
extern void UnitySynchronizationContext_HasPendingTasks_mBA93E72DC46200231B7ABCFB5F22FB0617A1B1EA (void);
// 0x00000472 System.Void UnityEngine.UnitySynchronizationContext::InitializeSynchronizationContext()
extern void UnitySynchronizationContext_InitializeSynchronizationContext_mB581D86F8675566DC3A885C15DC5B9A7B8D42CD4 (void);
// 0x00000473 System.Void UnityEngine.UnitySynchronizationContext::ExecuteTasks()
extern void UnitySynchronizationContext_ExecuteTasks_m323E27C0CD442B806D966D024725D9809563E0DD (void);
// 0x00000474 System.Boolean UnityEngine.UnitySynchronizationContext::ExecutePendingTasks(System.Int64)
extern void UnitySynchronizationContext_ExecutePendingTasks_m7FD629962A8BA72CB2F51583DC393A8FDA672DBC (void);
// 0x00000475 System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::.ctor(System.Threading.SendOrPostCallback,System.Object,System.Threading.ManualResetEvent)
extern void WorkRequest__ctor_m13C7B4A89E47F4B97ED9B786DB99849DBC2B5603 (void);
// 0x00000476 System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern void WorkRequest_Invoke_m1C292B7297918C5F2DBE70971895FE8D5C33AA20 (void);
// 0x00000477 System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (void);
// 0x00000478 System.Void UnityEngine.YieldInstruction::.ctor()
extern void YieldInstruction__ctor_mD8203310B47F2C36BED3EEC00CA1944C9D941AEF (void);
// 0x00000479 System.Void UnityEngine.SerializeField::.ctor()
extern void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (void);
// 0x0000047A System.Void UnityEngine.ISerializationCallbackReceiver::OnBeforeSerialize()
// 0x0000047B System.Void UnityEngine.ISerializationCallbackReceiver::OnAfterDeserialize()
// 0x0000047C System.Int32 UnityEngine.ComputeShader::FindKernel(System.String)
extern void ComputeShader_FindKernel_mCA2683905A5DAB573D50389E2B24B48B18CD53D0 (void);
// 0x0000047D System.Void UnityEngine.LowerResBlitTexture::LowerResBlitTextureDontStripMe()
extern void LowerResBlitTexture_LowerResBlitTextureDontStripMe_mFF261E84D36BA5652A3EAB9B45159D6E2FE23A0E (void);
// 0x0000047E System.Void UnityEngine.PreloadData::PreloadDataDontStripMe()
extern void PreloadData_PreloadDataDontStripMe_mE542B732FBF4F1E9F01B1D1C2160C43E37E70B7A (void);
// 0x0000047F System.Boolean UnityEngine.SystemInfo::IsValidEnumValue(System.Enum)
extern void SystemInfo_IsValidEnumValue_mDF4AFDCB30A42032742988AD9BC5E0E00EFA86C8 (void);
// 0x00000480 System.Boolean UnityEngine.SystemInfo::SupportsTextureFormat(UnityEngine.TextureFormat)
extern void SystemInfo_SupportsTextureFormat_mE7DA9DC2B167CB7E9A864924C8772307F1A2F0B9 (void);
// 0x00000481 System.Boolean UnityEngine.SystemInfo::SupportsTextureFormatNative(UnityEngine.TextureFormat)
extern void SystemInfo_SupportsTextureFormatNative_m1514BFE543D7EE39CEF43B429B52E2EC20AB8E75 (void);
// 0x00000482 System.Boolean UnityEngine.SystemInfo::IsFormatSupported(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void SystemInfo_IsFormatSupported_m03EDA316B42377504BA47B256EB094F8A54BC75C (void);
// 0x00000483 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.SystemInfo::GetCompatibleFormat(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void SystemInfo_GetCompatibleFormat_m02B5C5B1F3836661A92F3C83E44B66729C03B228 (void);
// 0x00000484 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.SystemInfo::GetGraphicsFormat(UnityEngine.Experimental.Rendering.DefaultFormat)
extern void SystemInfo_GetGraphicsFormat_mE36FE85F87F085503FEAA34112E454E9F2AFEF55 (void);
// 0x00000485 System.Single UnityEngine.Time::get_unscaledTime()
extern void Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258 (void);
// 0x00000486 System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern void Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84 (void);
// 0x00000487 System.Int32 UnityEngine.Time::get_frameCount()
extern void Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0 (void);
// 0x00000488 System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern void Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B (void);
// 0x00000489 System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern void RectTransform_SendReapplyDrivenProperties_m9139950FCE6E3C596110C5266174D8B2E56DC9CD (void);
// 0x0000048A System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern void ReapplyDrivenProperties__ctor_mD584B5E4A07E3D025352EA0BAE9B10FE5C13A583 (void);
// 0x0000048B System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern void ReapplyDrivenProperties_Invoke_m5B39EC5205C3910AC09DCF378EAA2D8E99391636 (void);
// 0x0000048C System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern void ReapplyDrivenProperties_BeginInvoke_mC7625A8FDFF392D73C7828526490DCB88FD87232 (void);
// 0x0000048D System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern void ReapplyDrivenProperties_EndInvoke_m89A593999C130CA23515BF8A9C02DDE5B39ECF67 (void);
// 0x0000048E System.Void UnityEngine.Transform::.ctor()
extern void Transform__ctor_m629D1F6D054AD8FA5BD74296A23FCA93BEB76803 (void);
// 0x0000048F UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern void Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (void);
// 0x00000490 System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (void);
// 0x00000491 UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern void Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (void);
// 0x00000492 System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (void);
// 0x00000493 UnityEngine.Vector3 UnityEngine.Transform::GetLocalEulerAngles(UnityEngine.RotationOrder)
extern void Transform_GetLocalEulerAngles_m1E0C16457DA4F17051A6D3A2959C0A7F312F9551 (void);
// 0x00000494 System.Void UnityEngine.Transform::SetLocalEulerAngles(UnityEngine.Vector3,UnityEngine.RotationOrder)
extern void Transform_SetLocalEulerAngles_m1D33ECF91062D8AB10C8BC4ABE97B650A6B4A0B6 (void);
// 0x00000495 System.Void UnityEngine.Transform::SetLocalEulerHint(UnityEngine.Vector3)
extern void Transform_SetLocalEulerHint_m3D58FF93969AE51D324A487F922E68B2431EBC6F (void);
// 0x00000496 UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern void Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F (void);
// 0x00000497 System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern void Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E (void);
// 0x00000498 UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern void Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545 (void);
// 0x00000499 System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern void Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B (void);
// 0x0000049A UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern void Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (void);
// 0x0000049B System.Void UnityEngine.Transform::set_right(UnityEngine.Vector3)
extern void Transform_set_right_m2BD2600E354493BDBFCBA5A888C4B5B970E25EE1 (void);
// 0x0000049C UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern void Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (void);
// 0x0000049D System.Void UnityEngine.Transform::set_up(UnityEngine.Vector3)
extern void Transform_set_up_m3D2B71DA51EA167C367FD275B7B28241C565F127 (void);
// 0x0000049E UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern void Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (void);
// 0x0000049F System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
extern void Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D (void);
// 0x000004A0 UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern void Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (void);
// 0x000004A1 System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (void);
// 0x000004A2 UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern void Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5 (void);
// 0x000004A3 System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern void Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C (void);
// 0x000004A4 UnityEngine.RotationOrder UnityEngine.Transform::get_rotationOrder()
extern void Transform_get_rotationOrder_m18BA74EAEE812AD11503FFC079D81558424503D6 (void);
// 0x000004A5 System.Void UnityEngine.Transform::set_rotationOrder(UnityEngine.RotationOrder)
extern void Transform_set_rotationOrder_m45763ACEC58CD42DE785C937DA33E8F97FE7EA23 (void);
// 0x000004A6 System.Int32 UnityEngine.Transform::GetRotationOrderInternal()
extern void Transform_GetRotationOrderInternal_mF2DAB087B528950969DCAD30F3096190086F504F (void);
// 0x000004A7 System.Void UnityEngine.Transform::SetRotationOrderInternal(UnityEngine.RotationOrder)
extern void Transform_SetRotationOrderInternal_m57BAB8ECE4952655C1696A533CD8CD344519BE9F (void);
// 0x000004A8 UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern void Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (void);
// 0x000004A9 System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (void);
// 0x000004AA UnityEngine.Transform UnityEngine.Transform::get_parent()
extern void Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (void);
// 0x000004AB System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (void);
// 0x000004AC UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern void Transform_get_parentInternal_m6477F21AD3A2B2F3FE2C365B1AF64BB1AFDA7B4C (void);
// 0x000004AD System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern void Transform_set_parentInternal_mED1BC58DB05A14DAC354E5A4B24C872A5D69D0C3 (void);
// 0x000004AE UnityEngine.Transform UnityEngine.Transform::GetParent()
extern void Transform_GetParent_mA53F6AE810935DDED00A9FEEE1830F4EF797F73B (void);
// 0x000004AF System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (void);
// 0x000004B0 System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern void Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A (void);
// 0x000004B1 UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern void Transform_get_worldToLocalMatrix_mE22FDE24767E1DE402D3E7A1C9803379B2E8399D (void);
// 0x000004B2 UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern void Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC (void);
// 0x000004B3 System.Void UnityEngine.Transform::SetPositionAndRotation(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Transform_SetPositionAndRotation_m33418A6BDFB6395B98D0B5733F5E522B7EEDDCDE (void);
// 0x000004B4 System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern void Transform_Translate_mFB58CBF3FA00BD0EE09EC67457608F62564D0DDE (void);
// 0x000004B5 System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern void Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0 (void);
// 0x000004B6 System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern void Transform_Translate_m22737F202DE67AAAAC408ADE91BD44F5BAF3DD6B (void);
// 0x000004B7 System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern void Transform_Translate_mC9343E1E646DA8FD42BE37137ACCBB4B52093F5C (void);
// 0x000004B8 System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Transform)
extern void Transform_Translate_m3B18F08276BB86827D21F01E099B74FD6AD80800 (void);
// 0x000004B9 System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Transform)
extern void Transform_Translate_m4C134F0279CAB507FA5FB3FAD6DE4E29E580628E (void);
// 0x000004BA System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern void Transform_Rotate_m61816C8A09C86A5E157EA89965A9CC0510A1B378 (void);
// 0x000004BB System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern void Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7 (void);
// 0x000004BC System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern void Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357 (void);
// 0x000004BD System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern void Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E (void);
// 0x000004BE System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern void Transform_RotateAroundInternal_mEEC98EFC71E388DB313FE62D9FAB642724A38216 (void);
// 0x000004BF System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single,UnityEngine.Space)
extern void Transform_Rotate_m12614C5FABB1F4A9A6800EE65BBFDB433D6D804D (void);
// 0x000004C0 System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
extern void Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC (void);
// 0x000004C1 System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE (void);
// 0x000004C2 System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern void Transform_LookAt_m6037828F5E8329B052D62472060274F9A3261ECA (void);
// 0x000004C3 System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern void Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F (void);
// 0x000004C4 System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Transform_LookAt_m6BB4B39BB829A451C2F63215361D27650AA24D8C (void);
// 0x000004C5 System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern void Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA (void);
// 0x000004C6 System.Void UnityEngine.Transform::Internal_LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Transform_Internal_LookAt_m1A24125A99A766EDA6059424EA3B5FA9C5E8B61B (void);
// 0x000004C7 UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern void Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91 (void);
// 0x000004C8 UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(System.Single,System.Single,System.Single)
extern void Transform_TransformDirection_m3693D464C30F3F22893B6E784EB8DC47F484BCC0 (void);
// 0x000004C9 UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern void Transform_InverseTransformDirection_m9EB6F7A2598FD8D6B52F0A6EBA96A3BAAF68D696 (void);
// 0x000004CA UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(System.Single,System.Single,System.Single)
extern void Transform_InverseTransformDirection_m77816405743CC6CFB7A16D472038138D83E0DE18 (void);
// 0x000004CB UnityEngine.Vector3 UnityEngine.Transform::TransformVector(UnityEngine.Vector3)
extern void Transform_TransformVector_m7C5F87858E82A686A233D1866443ACAEA296AA2B (void);
// 0x000004CC UnityEngine.Vector3 UnityEngine.Transform::TransformVector(System.Single,System.Single,System.Single)
extern void Transform_TransformVector_mF0828C42687BA71AFBBB03F39D444EA7C6FCC998 (void);
// 0x000004CD UnityEngine.Vector3 UnityEngine.Transform::InverseTransformVector(UnityEngine.Vector3)
extern void Transform_InverseTransformVector_mAE27324FC01E136CF80D1A414AC10BA7616024C4 (void);
// 0x000004CE UnityEngine.Vector3 UnityEngine.Transform::InverseTransformVector(System.Single,System.Single,System.Single)
extern void Transform_InverseTransformVector_m8CA63DFC2E7AB3B4F2E7B98B674105DF0ECDC249 (void);
// 0x000004CF UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern void Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2 (void);
// 0x000004D0 UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(System.Single,System.Single,System.Single)
extern void Transform_TransformPoint_m1BCC9D23A57EA333F2849AA3A5121C4746E4ADA3 (void);
// 0x000004D1 UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern void Transform_InverseTransformPoint_m476ABC8F3F14824D7D82FE2C54CEE5A151A669B8 (void);
// 0x000004D2 UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(System.Single,System.Single,System.Single)
extern void Transform_InverseTransformPoint_m2E147F9EC1658515B39932FED5F3E35F34E5737A (void);
// 0x000004D3 UnityEngine.Transform UnityEngine.Transform::get_root()
extern void Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91 (void);
// 0x000004D4 UnityEngine.Transform UnityEngine.Transform::GetRoot()
extern void Transform_GetRoot_mAB553501306732D17B508521058BD5D22A759CF0 (void);
// 0x000004D5 System.Int32 UnityEngine.Transform::get_childCount()
extern void Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (void);
// 0x000004D6 System.Void UnityEngine.Transform::DetachChildren()
extern void Transform_DetachChildren_m0800099F604AB1B59A72AC83E175B964B1EDFEF2 (void);
// 0x000004D7 System.Void UnityEngine.Transform::SetAsFirstSibling()
extern void Transform_SetAsFirstSibling_mD5C02831BA6C7C3408CD491191EAF760ECB7E754 (void);
// 0x000004D8 System.Void UnityEngine.Transform::SetAsLastSibling()
extern void Transform_SetAsLastSibling_m2AF5610CFC845BB1121152BAB38A1251D210A187 (void);
// 0x000004D9 System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
extern void Transform_SetSiblingIndex_mC69C3B37E6C731AA2A0B9BD787CF47AA5B8001FC (void);
// 0x000004DA System.Void UnityEngine.Transform::MoveAfterSibling(UnityEngine.Transform,System.Boolean)
extern void Transform_MoveAfterSibling_mEEBEDA3F217D93209CBB9D94C52ADEEE79388057 (void);
// 0x000004DB System.Int32 UnityEngine.Transform::GetSiblingIndex()
extern void Transform_GetSiblingIndex_mEF9DF6406920F8EBCFBC87C6D0630FE3E9E3C1EE (void);
// 0x000004DC UnityEngine.Transform UnityEngine.Transform::FindRelativeTransformWithPath(UnityEngine.Transform,System.String,System.Boolean)
extern void Transform_FindRelativeTransformWithPath_m8B6DE13079DE11DCCDD2CA40CEC59319FD70A12D (void);
// 0x000004DD UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern void Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1 (void);
// 0x000004DE System.Void UnityEngine.Transform::SendTransformChangedScale()
extern void Transform_SendTransformChangedScale_mC214E7244B412D35A119A04285556A3C751E8BAE (void);
// 0x000004DF UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern void Transform_get_lossyScale_m469A16F93F135C1E4D5955C7EBDB893D1892A331 (void);
// 0x000004E0 System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern void Transform_IsChildOf_m1783A88A490931E98F4D5E361595A518E09FD4BC (void);
// 0x000004E1 System.Boolean UnityEngine.Transform::get_hasChanged()
extern void Transform_get_hasChanged_m59490E3CAC42DF8CB2BCDFC0ED75DB6F89432F06 (void);
// 0x000004E2 System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
extern void Transform_set_hasChanged_mD1CDCAE366DB514FBECD9DAAED0F7834029E1304 (void);
// 0x000004E3 UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern void Transform_FindChild_m0305D8CCABE0EE6D1D0D1028F64E2205963FE652 (void);
// 0x000004E4 System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern void Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (void);
// 0x000004E5 System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,System.Single)
extern void Transform_RotateAround_m44A5768C3A03673AA7FEB4AE943984DBA1D43168 (void);
// 0x000004E6 System.Void UnityEngine.Transform::RotateAroundLocal(UnityEngine.Vector3,System.Single)
extern void Transform_RotateAroundLocal_m73207F5EE6432864BC42A23A679337A2CE90DB01 (void);
// 0x000004E7 UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern void Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (void);
// 0x000004E8 System.Int32 UnityEngine.Transform::GetChildCount()
extern void Transform_GetChildCount_m8EB532F6F7F7C34ACA8FAF363EAB869C202FBB59 (void);
// 0x000004E9 System.Int32 UnityEngine.Transform::get_hierarchyCapacity()
extern void Transform_get_hierarchyCapacity_m8942DA29F2D64D703BABE386CDF4F4FA9340D47E (void);
// 0x000004EA System.Void UnityEngine.Transform::set_hierarchyCapacity(System.Int32)
extern void Transform_set_hierarchyCapacity_mD108C0B4DD243DA144F65D14D84FF7EBE4D1839E (void);
// 0x000004EB System.Int32 UnityEngine.Transform::internal_getHierarchyCapacity()
extern void Transform_internal_getHierarchyCapacity_m4D049161668E75378CEC7FD32CF7C0FF6A961565 (void);
// 0x000004EC System.Void UnityEngine.Transform::internal_setHierarchyCapacity(System.Int32)
extern void Transform_internal_setHierarchyCapacity_m733CAC49839E45E0A7C70D95CD0A7D541BBA2A55 (void);
// 0x000004ED System.Int32 UnityEngine.Transform::get_hierarchyCount()
extern void Transform_get_hierarchyCount_m2C0C221FCB7749D1A0BB705EDEAA8389E87AAD7A (void);
// 0x000004EE System.Int32 UnityEngine.Transform::internal_getHierarchyCount()
extern void Transform_internal_getHierarchyCount_m44535E904CE785CA00BB9DFBE741F8126D7339FD (void);
// 0x000004EF System.Boolean UnityEngine.Transform::IsNonUniformScaleTransform()
extern void Transform_IsNonUniformScaleTransform_mA76F0BE4F08FD55B8C4E2351F92550232ABCA047 (void);
// 0x000004F0 System.Void UnityEngine.Transform::get_position_Injected(UnityEngine.Vector3&)
extern void Transform_get_position_Injected_m43CE3FC8FB3C52896D709B07EB77340407800C13 (void);
// 0x000004F1 System.Void UnityEngine.Transform::set_position_Injected(UnityEngine.Vector3&)
extern void Transform_set_position_Injected_m634DE2302555154065001583E6080CC48D58A602 (void);
// 0x000004F2 System.Void UnityEngine.Transform::get_localPosition_Injected(UnityEngine.Vector3&)
extern void Transform_get_localPosition_Injected_mBBD4D1AAD893D9B5DB40E9946A40E2B94E688782 (void);
// 0x000004F3 System.Void UnityEngine.Transform::set_localPosition_Injected(UnityEngine.Vector3&)
extern void Transform_set_localPosition_Injected_m228521F584224C612AEF8ED500AABF31C8E87E02 (void);
// 0x000004F4 System.Void UnityEngine.Transform::GetLocalEulerAngles_Injected(UnityEngine.RotationOrder,UnityEngine.Vector3&)
extern void Transform_GetLocalEulerAngles_Injected_mB03E9A811E0E734EC641FE15686F30159E9C8905 (void);
// 0x000004F5 System.Void UnityEngine.Transform::SetLocalEulerAngles_Injected(UnityEngine.Vector3&,UnityEngine.RotationOrder)
extern void Transform_SetLocalEulerAngles_Injected_mE07F22A2F00481E419BA148403C22ABA2812E8C5 (void);
// 0x000004F6 System.Void UnityEngine.Transform::SetLocalEulerHint_Injected(UnityEngine.Vector3&)
extern void Transform_SetLocalEulerHint_Injected_mC369F2D8C51558BEF82476E03EF730C8DB31635E (void);
// 0x000004F7 System.Void UnityEngine.Transform::get_rotation_Injected(UnityEngine.Quaternion&)
extern void Transform_get_rotation_Injected_m1F756C98851F36F25BFBAC3401B67A4D2F176DF1 (void);
// 0x000004F8 System.Void UnityEngine.Transform::set_rotation_Injected(UnityEngine.Quaternion&)
extern void Transform_set_rotation_Injected_m9ACF0891D219140A329411F33858C7B0A026407F (void);
// 0x000004F9 System.Void UnityEngine.Transform::get_localRotation_Injected(UnityEngine.Quaternion&)
extern void Transform_get_localRotation_Injected_mCF48B92BAD51A015698EFE3973CD2F595048E74B (void);
// 0x000004FA System.Void UnityEngine.Transform::set_localRotation_Injected(UnityEngine.Quaternion&)
extern void Transform_set_localRotation_Injected_m19EF26CC5E0F8331297D3FB17EFFC7FD217A9FCA (void);
// 0x000004FB System.Void UnityEngine.Transform::get_localScale_Injected(UnityEngine.Vector3&)
extern void Transform_get_localScale_Injected_mC3D90F76FF1C9876761FBE40C5FF567213B86402 (void);
// 0x000004FC System.Void UnityEngine.Transform::set_localScale_Injected(UnityEngine.Vector3&)
extern void Transform_set_localScale_Injected_m7247850A81ED854FD10411376E0EF2C4F7C50B65 (void);
// 0x000004FD System.Void UnityEngine.Transform::get_worldToLocalMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Transform_get_worldToLocalMatrix_Injected_m8B625E30EDAC79587E1D73943D2486385C403BB1 (void);
// 0x000004FE System.Void UnityEngine.Transform::get_localToWorldMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Transform_get_localToWorldMatrix_Injected_m990CE30D1A3D41A3247D4F9E73CA8B725466767B (void);
// 0x000004FF System.Void UnityEngine.Transform::SetPositionAndRotation_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Transform_SetPositionAndRotation_Injected_m935C7A0C1BC5C1BCDC7FFE8B757F58D9540485D3 (void);
// 0x00000500 System.Void UnityEngine.Transform::RotateAroundInternal_Injected(UnityEngine.Vector3&,System.Single)
extern void Transform_RotateAroundInternal_Injected_m70D2B2635B5CB12008A8207829D178BF4970E9BD (void);
// 0x00000501 System.Void UnityEngine.Transform::Internal_LookAt_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_Internal_LookAt_Injected_mE9622F74A863FA172B36D3F453BE0AA4BDEAC092 (void);
// 0x00000502 System.Void UnityEngine.Transform::TransformDirection_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformDirection_Injected_mB27ACC95D32503153AEBE93976553C0A41BF4D2E (void);
// 0x00000503 System.Void UnityEngine.Transform::InverseTransformDirection_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_InverseTransformDirection_Injected_m74168CEED775055AA4C960F6B653DDA0B62322B5 (void);
// 0x00000504 System.Void UnityEngine.Transform::TransformVector_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformVector_Injected_m386F502DB70B8082041933F6C15BC1BF4AD08672 (void);
// 0x00000505 System.Void UnityEngine.Transform::InverseTransformVector_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_InverseTransformVector_Injected_mD3DECDA4FA1753469330D6B3B7D7C6AE40D93CD8 (void);
// 0x00000506 System.Void UnityEngine.Transform::TransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformPoint_Injected_mFCDA82BF83E47142F6115E18D515FA0D0A0E5319 (void);
// 0x00000507 System.Void UnityEngine.Transform::InverseTransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_InverseTransformPoint_Injected_mC6226F53D5631F42658A5CA83FEE16EC24670A36 (void);
// 0x00000508 System.Void UnityEngine.Transform::get_lossyScale_Injected(UnityEngine.Vector3&)
extern void Transform_get_lossyScale_Injected_m5D97E86A5663FF57E1977E5CA13EF00022B61648 (void);
// 0x00000509 System.Void UnityEngine.Transform::RotateAround_Injected(UnityEngine.Vector3&,System.Single)
extern void Transform_RotateAround_Injected_mEDC7F96D95DACA6DD6B67F98EE11C34143A31ABB (void);
// 0x0000050A System.Void UnityEngine.Transform::RotateAroundLocal_Injected(UnityEngine.Vector3&,System.Single)
extern void Transform_RotateAroundLocal_Injected_m2F7BCD8F3886F33F9E33DFFC82387E0FC370495D (void);
// 0x0000050B System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern void Enumerator__ctor_m052C22273F1D789E58A09606D5EE5E87ABC2C91B (void);
// 0x0000050C System.Object UnityEngine.Transform/Enumerator::get_Current()
extern void Enumerator_get_Current_m1CFECBB7AC3EACD05A11CC6848AE7A94A8123E9F (void);
// 0x0000050D System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m346F9A121D9E89ADBA8296E6A7EF8763C5B58A14 (void);
// 0x0000050E System.Void UnityEngine.Transform/Enumerator::Reset()
extern void Enumerator_Reset_mFA289646E280C94D82CC223C024E0B615F811C8E (void);
// 0x0000050F System.Boolean UnityEngine.SpriteRenderer::get_shouldSupportTiling()
extern void SpriteRenderer_get_shouldSupportTiling_m766D3A4A4877366C8E5F01997B92DBDF33F63AA0 (void);
// 0x00000510 UnityEngine.Sprite UnityEngine.SpriteRenderer::get_sprite()
extern void SpriteRenderer_get_sprite_mCA028D776503304A6A59A5E7FD9F198DCF206387 (void);
// 0x00000511 System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern void SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A (void);
// 0x00000512 UnityEngine.SpriteDrawMode UnityEngine.SpriteRenderer::get_drawMode()
extern void SpriteRenderer_get_drawMode_m13220432BF5376A2B9B8994FAE2F80FD932E8DC4 (void);
// 0x00000513 System.Void UnityEngine.SpriteRenderer::set_drawMode(UnityEngine.SpriteDrawMode)
extern void SpriteRenderer_set_drawMode_mD45F452500FE27EF30AFEF295220F72B356457CC (void);
// 0x00000514 UnityEngine.Vector2 UnityEngine.SpriteRenderer::get_size()
extern void SpriteRenderer_get_size_mB0C8D3133ABDB73AA1BCC468F23DD9EE0A8C97C7 (void);
// 0x00000515 System.Void UnityEngine.SpriteRenderer::set_size(UnityEngine.Vector2)
extern void SpriteRenderer_set_size_m92E5651D28AC04743795244ACC1E3F5FABCF1B49 (void);
// 0x00000516 System.Single UnityEngine.SpriteRenderer::get_adaptiveModeThreshold()
extern void SpriteRenderer_get_adaptiveModeThreshold_m2A8E48E04DF7480E32F42CB75ED157F9E3EF3A60 (void);
// 0x00000517 System.Void UnityEngine.SpriteRenderer::set_adaptiveModeThreshold(System.Single)
extern void SpriteRenderer_set_adaptiveModeThreshold_mCA99CEBF00B71DD4E7CF8ACEF3F80310C50B4E31 (void);
// 0x00000518 UnityEngine.SpriteTileMode UnityEngine.SpriteRenderer::get_tileMode()
extern void SpriteRenderer_get_tileMode_m9DA12130B3EC79745C4A7146A82A9F9457121911 (void);
// 0x00000519 System.Void UnityEngine.SpriteRenderer::set_tileMode(UnityEngine.SpriteTileMode)
extern void SpriteRenderer_set_tileMode_mCB119E57EC0C64D4EEF44554E21ED94C88248884 (void);
// 0x0000051A UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern void SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC (void);
// 0x0000051B System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern void SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33 (void);
// 0x0000051C UnityEngine.SpriteMaskInteraction UnityEngine.SpriteRenderer::get_maskInteraction()
extern void SpriteRenderer_get_maskInteraction_mE205D19E746BE0122589D92F451C9E2661D35EEF (void);
// 0x0000051D System.Void UnityEngine.SpriteRenderer::set_maskInteraction(UnityEngine.SpriteMaskInteraction)
extern void SpriteRenderer_set_maskInteraction_m2AF21189C484E01A8107B58424C67DA01B89CCD0 (void);
// 0x0000051E System.Boolean UnityEngine.SpriteRenderer::get_flipX()
extern void SpriteRenderer_get_flipX_mCB73CAF5724B925903C9D9805D3F7B8AD0C509F5 (void);
// 0x0000051F System.Void UnityEngine.SpriteRenderer::set_flipX(System.Boolean)
extern void SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E (void);
// 0x00000520 System.Boolean UnityEngine.SpriteRenderer::get_flipY()
extern void SpriteRenderer_get_flipY_m2E857CA3F06D99D03996A2CB58297186DAFD19BB (void);
// 0x00000521 System.Void UnityEngine.SpriteRenderer::set_flipY(System.Boolean)
extern void SpriteRenderer_set_flipY_m7DFB2D1118142BE6C76470474F2ECAED234B0A01 (void);
// 0x00000522 UnityEngine.SpriteSortPoint UnityEngine.SpriteRenderer::get_spriteSortPoint()
extern void SpriteRenderer_get_spriteSortPoint_m98FF21DA0A1D95935B418499F3D684D1A4775E8A (void);
// 0x00000523 System.Void UnityEngine.SpriteRenderer::set_spriteSortPoint(UnityEngine.SpriteSortPoint)
extern void SpriteRenderer_set_spriteSortPoint_mDDB38BE9F712C6AA5AB52D6E7E5C9E4DF64984BF (void);
// 0x00000524 UnityEngine.Bounds UnityEngine.SpriteRenderer::Internal_GetSpriteBounds(UnityEngine.SpriteDrawMode)
extern void SpriteRenderer_Internal_GetSpriteBounds_m1C5B4E0EAB26D42903950706BFE4846042C34C4F (void);
// 0x00000525 UnityEngine.Bounds UnityEngine.SpriteRenderer::GetSpriteBounds()
extern void SpriteRenderer_GetSpriteBounds_mE9B6EF374ECEC8103A5AFBDF408A40E44BDEC24D (void);
// 0x00000526 System.Void UnityEngine.SpriteRenderer::.ctor()
extern void SpriteRenderer__ctor_m5724D3A61E7A1DDF3EFB2A8B47A8A1348CB188D3 (void);
// 0x00000527 System.Void UnityEngine.SpriteRenderer::get_size_Injected(UnityEngine.Vector2&)
extern void SpriteRenderer_get_size_Injected_mD2E28A7BF46C4883D66521C4008011BC9EDADCFB (void);
// 0x00000528 System.Void UnityEngine.SpriteRenderer::set_size_Injected(UnityEngine.Vector2&)
extern void SpriteRenderer_set_size_Injected_mB3E9D735BC04B08AA7317C23C5DCD904CBBC3E3F (void);
// 0x00000529 System.Void UnityEngine.SpriteRenderer::get_color_Injected(UnityEngine.Color&)
extern void SpriteRenderer_get_color_Injected_m24E9886D0185C54AB944E0795B1581C4B502E507 (void);
// 0x0000052A System.Void UnityEngine.SpriteRenderer::set_color_Injected(UnityEngine.Color&)
extern void SpriteRenderer_set_color_Injected_m937728871B33924329697BD582ECB8F6FA3AF628 (void);
// 0x0000052B System.Void UnityEngine.SpriteRenderer::Internal_GetSpriteBounds_Injected(UnityEngine.SpriteDrawMode,UnityEngine.Bounds&)
extern void SpriteRenderer_Internal_GetSpriteBounds_Injected_m70E8B5485EA2FBD316315BF6E682E2DEEA101327 (void);
// 0x0000052C System.Void UnityEngine.Sprite::.ctor()
extern void Sprite__ctor_m121D88C6A901A2A2FA602306D01FDB8D7A0206F0 (void);
// 0x0000052D System.Int32 UnityEngine.Sprite::GetPackingMode()
extern void Sprite_GetPackingMode_m398C471B7DDCCA1EA0355217EBB7568E851E597A (void);
// 0x0000052E System.Int32 UnityEngine.Sprite::GetPackingRotation()
extern void Sprite_GetPackingRotation_m06FA5511011CD5F4B55ADB2250D037CDA1FBA4C0 (void);
// 0x0000052F System.Int32 UnityEngine.Sprite::GetPacked()
extern void Sprite_GetPacked_m6AC29F35C9ADE1B6394202132FB77DA9249DF5AE (void);
// 0x00000530 UnityEngine.Rect UnityEngine.Sprite::GetTextureRect()
extern void Sprite_GetTextureRect_m6E19823AEA9A3FC4C9FE76E53C30F19316F63954 (void);
// 0x00000531 UnityEngine.Vector2 UnityEngine.Sprite::GetTextureRectOffset()
extern void Sprite_GetTextureRectOffset_m497818989C347690FBE0FEA2E38F64720E34FEBE (void);
// 0x00000532 UnityEngine.Vector4 UnityEngine.Sprite::GetInnerUVs()
extern void Sprite_GetInnerUVs_m394AF466930BBACE6F45425C418D0A8991600AD9 (void);
// 0x00000533 UnityEngine.Vector4 UnityEngine.Sprite::GetOuterUVs()
extern void Sprite_GetOuterUVs_mEB9D18CA03A78C02CAF4FAD386A7AF009187ACDD (void);
// 0x00000534 UnityEngine.Vector4 UnityEngine.Sprite::GetPadding()
extern void Sprite_GetPadding_mA039E911719B85FBB31F4C235B9EF9973F5E7FF3 (void);
// 0x00000535 UnityEngine.Sprite UnityEngine.Sprite::CreateSpriteWithoutTextureScripting(UnityEngine.Rect,UnityEngine.Vector2,System.Single,UnityEngine.Texture2D)
extern void Sprite_CreateSpriteWithoutTextureScripting_m73A67FF6ACACA774AE2E0A9E13199790C121269D (void);
// 0x00000536 UnityEngine.Sprite UnityEngine.Sprite::CreateSprite(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4,System.Boolean)
extern void Sprite_CreateSprite_m377BDA8A0AD33EC2E51E4AE5F024D3DAD4C4D4F4 (void);
// 0x00000537 UnityEngine.Bounds UnityEngine.Sprite::get_bounds()
extern void Sprite_get_bounds_m364F852DE78702F755D1414FF4465F61F3F238EF (void);
// 0x00000538 UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern void Sprite_get_rect_m146D3624E5D8DD6DF5B1F39CE618D701B9008C70 (void);
// 0x00000539 UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern void Sprite_get_border_m6AEB051C1A675509BB786427883FC2EE957F60A7 (void);
// 0x0000053A UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern void Sprite_get_texture_mD03E68058C9F727321FE643CBDB3A469F96E49FB (void);
// 0x0000053B UnityEngine.Texture2D UnityEngine.Sprite::GetSecondaryTexture(System.Int32)
extern void Sprite_GetSecondaryTexture_m75C8D02A7387D4671537481529F0916B2B49F55B (void);
// 0x0000053C System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern void Sprite_get_pixelsPerUnit_mEA3201EE604FB43CB93E3D309B19A5D0B44C739E (void);
// 0x0000053D System.Single UnityEngine.Sprite::get_spriteAtlasTextureScale()
extern void Sprite_get_spriteAtlasTextureScale_m0D6B206834275928FA4974B264B2D03D1E243751 (void);
// 0x0000053E UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern void Sprite_get_associatedAlphaSplitTexture_m212E3C39E4EE3385866E51194F5FC9AEDDEE4F00 (void);
// 0x0000053F UnityEngine.Vector2 UnityEngine.Sprite::get_pivot()
extern void Sprite_get_pivot_m39B1CFCDA5BB126D198CAEAB703EC39E763CC867 (void);
// 0x00000540 System.Boolean UnityEngine.Sprite::get_packed()
extern void Sprite_get_packed_m075910C79D785DC2572B171DA93918CF2793B133 (void);
// 0x00000541 UnityEngine.SpritePackingMode UnityEngine.Sprite::get_packingMode()
extern void Sprite_get_packingMode_m1BF2656F34C1C650D1634F0AE81727074BE85E5F (void);
// 0x00000542 UnityEngine.SpritePackingRotation UnityEngine.Sprite::get_packingRotation()
extern void Sprite_get_packingRotation_m1332F3DE0A67AD59EC2CF1BF773F1EF7A4CD712A (void);
// 0x00000543 UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern void Sprite_get_textureRect_m5B350C2B122C85549960912CBD6343E4A5B02C35 (void);
// 0x00000544 UnityEngine.Vector2 UnityEngine.Sprite::get_textureRectOffset()
extern void Sprite_get_textureRectOffset_m093913C49C4F9195B2C3B3ED841C0693E055EB91 (void);
// 0x00000545 UnityEngine.Vector2[] UnityEngine.Sprite::get_vertices()
extern void Sprite_get_vertices_m4A5EFBEDA14F12E5358C61831150AE368453F301 (void);
// 0x00000546 System.UInt16[] UnityEngine.Sprite::get_triangles()
extern void Sprite_get_triangles_mAE8C32A81703AEF45192E993E6B555AF659C5131 (void);
// 0x00000547 UnityEngine.Vector2[] UnityEngine.Sprite::get_uv()
extern void Sprite_get_uv_mBD902ADCF1FF8AE211C98881A6E3C310D73494B6 (void);
// 0x00000548 System.Int32 UnityEngine.Sprite::GetPhysicsShapeCount()
extern void Sprite_GetPhysicsShapeCount_m854AF4908F18CDBFC1CC829D62D29DBA17B9EA59 (void);
// 0x00000549 System.Int32 UnityEngine.Sprite::GetPhysicsShapePointCount(System.Int32)
extern void Sprite_GetPhysicsShapePointCount_m9525B69C7B95C5506C1D0ABB20FBBFE8BE26AE0D (void);
// 0x0000054A System.Int32 UnityEngine.Sprite::Internal_GetPhysicsShapePointCount(System.Int32)
extern void Sprite_Internal_GetPhysicsShapePointCount_mDBE3DF0F616DB9EB15B865D673D3F64A9FF03C4D (void);
// 0x0000054B System.Int32 UnityEngine.Sprite::GetPhysicsShape(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Sprite_GetPhysicsShape_mC4B024EC518BB44CAB6684DC5FBF2FB5DEA8753D (void);
// 0x0000054C System.Void UnityEngine.Sprite::GetPhysicsShapeImpl(UnityEngine.Sprite,System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Sprite_GetPhysicsShapeImpl_m76E2FAEC9719208E23D342BD2E1A041299D32775 (void);
// 0x0000054D System.Void UnityEngine.Sprite::OverridePhysicsShape(System.Collections.Generic.IList`1<UnityEngine.Vector2[]>)
extern void Sprite_OverridePhysicsShape_m051054FB0802170A2DC5D2B46F899401A1531345 (void);
// 0x0000054E System.Void UnityEngine.Sprite::OverridePhysicsShapeCount(UnityEngine.Sprite,System.Int32)
extern void Sprite_OverridePhysicsShapeCount_mE17ECF1C7F079E5CC3161C827EC39DEA37D3392A (void);
// 0x0000054F System.Void UnityEngine.Sprite::OverridePhysicsShape(UnityEngine.Sprite,UnityEngine.Vector2[],System.Int32)
extern void Sprite_OverridePhysicsShape_m1B9F6A40847DE060A44E09272D26785241AECBA2 (void);
// 0x00000550 System.Void UnityEngine.Sprite::OverrideGeometry(UnityEngine.Vector2[],System.UInt16[])
extern void Sprite_OverrideGeometry_m7044D0D91BB8CED71236706FAC9BA692999F6825 (void);
// 0x00000551 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Rect,UnityEngine.Vector2,System.Single,UnityEngine.Texture2D)
extern void Sprite_Create_m3662813A7A2061C61C50D98BC2DEF4FB20BE6F19 (void);
// 0x00000552 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern void Sprite_Create_mE5A2105E146955704E81F089423DF1912680E853 (void);
// 0x00000553 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4,System.Boolean)
extern void Sprite_Create_mD5BE6F7D884D4FAAFE491551966B730ED1D49FEA (void);
// 0x00000554 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4)
extern void Sprite_Create_mD5A0E9AA50A9652F90D7591284B454F5EAC986F8 (void);
// 0x00000555 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32,UnityEngine.SpriteMeshType)
extern void Sprite_Create_mD3C27EA17D550CB67E709F9C983B5768CE24D995 (void);
// 0x00000556 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single,System.UInt32)
extern void Sprite_Create_mEFBF5BB7286E8095F42995A5B9EBEC3CDAB6153F (void);
// 0x00000557 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
extern void Sprite_Create_mB35E2D3FF3906606000D6682E465CC4773ADBACC (void);
// 0x00000558 UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2)
extern void Sprite_Create_m9817936760193300A6049A788C3446C7ADB49C6B (void);
// 0x00000559 System.Void UnityEngine.Sprite::GetTextureRect_Injected(UnityEngine.Rect&)
extern void Sprite_GetTextureRect_Injected_m5D5B55E003133B5A537764AF7493BC094685F2BD (void);
// 0x0000055A System.Void UnityEngine.Sprite::GetTextureRectOffset_Injected(UnityEngine.Vector2&)
extern void Sprite_GetTextureRectOffset_Injected_mAEFBF984BCCF2B6C3F9677B56EDE38360A869FEC (void);
// 0x0000055B System.Void UnityEngine.Sprite::GetInnerUVs_Injected(UnityEngine.Vector4&)
extern void Sprite_GetInnerUVs_Injected_m6BBD450F64FCAA0EE51E16034E239267E53BADB7 (void);
// 0x0000055C System.Void UnityEngine.Sprite::GetOuterUVs_Injected(UnityEngine.Vector4&)
extern void Sprite_GetOuterUVs_Injected_m386A7B21043ED228AE4BBAB93060AFBFE19C5BD7 (void);
// 0x0000055D System.Void UnityEngine.Sprite::GetPadding_Injected(UnityEngine.Vector4&)
extern void Sprite_GetPadding_Injected_m9C8743817FB7CD12F88DA90769BD653EA35273EE (void);
// 0x0000055E UnityEngine.Sprite UnityEngine.Sprite::CreateSpriteWithoutTextureScripting_Injected(UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,UnityEngine.Texture2D)
extern void Sprite_CreateSpriteWithoutTextureScripting_Injected_m531A7ED5E5361E8474E6C5B9842DBF0B95DE33A0 (void);
// 0x0000055F UnityEngine.Sprite UnityEngine.Sprite::CreateSprite_Injected(UnityEngine.Texture2D,UnityEngine.Rect&,UnityEngine.Vector2&,System.Single,System.UInt32,UnityEngine.SpriteMeshType,UnityEngine.Vector4&,System.Boolean)
extern void Sprite_CreateSprite_Injected_mD94BD3390A496B1A9A3FA6469B551B766DFEEE7D (void);
// 0x00000560 System.Void UnityEngine.Sprite::get_bounds_Injected(UnityEngine.Bounds&)
extern void Sprite_get_bounds_Injected_m4AE096B307AD9788AEDA44AF14C9605D5ABEEE1C (void);
// 0x00000561 System.Void UnityEngine.Sprite::get_rect_Injected(UnityEngine.Rect&)
extern void Sprite_get_rect_Injected_mE5951AA7D9D0CBBF4AF8263F8B77B8B3E203279D (void);
// 0x00000562 System.Void UnityEngine.Sprite::get_border_Injected(UnityEngine.Vector4&)
extern void Sprite_get_border_Injected_m7A2673F6D49E5085CA3CC2436763C7C7BE0F75C8 (void);
// 0x00000563 System.Void UnityEngine.Sprite::get_pivot_Injected(UnityEngine.Vector2&)
extern void Sprite_get_pivot_Injected_mAAE0A9705B766EB97C8732BE5541E800E8090809 (void);
// 0x00000564 System.Boolean UnityEngine._Scripting.APIUpdating.APIUpdaterRuntimeHelpers::GetMovedFromAttributeDataForType(System.Type,System.String&,System.String&,System.String&)
extern void APIUpdaterRuntimeHelpers_GetMovedFromAttributeDataForType_mEDA7447F4AEBCBDE3B6C5A04ED735FA9BA2E7B52 (void);
// 0x00000565 System.Boolean UnityEngine._Scripting.APIUpdating.APIUpdaterRuntimeHelpers::GetObsoleteTypeRedirection(System.Type,System.String&,System.String&,System.String&)
extern void APIUpdaterRuntimeHelpers_GetObsoleteTypeRedirection_mAD9DCC5AEEF51535CB9FCED2F1B38650C766D355 (void);
// 0x00000566 System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern void SpriteAtlasManager_RequestAtlas_m4EB540E080D8444FE4B53D8F3D44EA9C0C8C49A1 (void);
// 0x00000567 System.Void UnityEngine.U2D.SpriteAtlasManager::PostRegisteredAtlas(UnityEngine.U2D.SpriteAtlas)
extern void SpriteAtlasManager_PostRegisteredAtlas_m0F58C324E58E39D7B13803FBF7B1AE16CF6B4B7B (void);
// 0x00000568 System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern void SpriteAtlasManager_Register_m48E996EAD9A5CF419B7738799EB99A78D7095C73 (void);
// 0x00000569 System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern void SpriteAtlasManager__cctor_mDB99D76724E2DB007B46B61C2833878B624D5021 (void);
// 0x0000056A UnityEngine.Sprite UnityEngine.U2D.SpriteAtlas::GetSprite(System.String)
extern void SpriteAtlas_GetSprite_mBDFF27666D4C361D5A5E6F55421A658A045C8DDB (void);
// 0x0000056B System.Int64 UnityEngine.Profiling.Profiler::GetMonoUsedSizeLong()
extern void Profiler_GetMonoUsedSizeLong_mFE5483CFA8A430C8BC7A44EB67A4C244DFF0CE02 (void);
// 0x0000056C System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_rawImageDataReference(Unity.Collections.NativeArray`1<System.Byte>)
extern void DebugScreenCapture_set_rawImageDataReference_m9FE7228E0FB4696A255B2F477B6B50C902D7786B (void);
// 0x0000056D System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_imageFormat(UnityEngine.TextureFormat)
extern void DebugScreenCapture_set_imageFormat_m46E9D97376E844826DAE5C3C69E4AAF4B7A58ECB (void);
// 0x0000056E System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_width(System.Int32)
extern void DebugScreenCapture_set_width_m6928DB2B2BCD54DE97BDA4116D69897921CCACF6 (void);
// 0x0000056F System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_height(System.Int32)
extern void DebugScreenCapture_set_height_m6CD0F6872F123966B784E6F356C51986B8B19F84 (void);
// 0x00000570 System.Void UnityEngine.Profiling.Memory.Experimental.MetaData::.ctor()
extern void MetaData__ctor_mD7868B95DDB386C2F8AC614F09A6760F420A44D5 (void);
// 0x00000571 System.Byte[] UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::PrepareMetadata()
extern void MemoryProfiler_PrepareMetadata_m571D85DE9BEAF3D3E0ED8269AE350960717D947E (void);
// 0x00000572 System.Int32 UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::WriteIntToByteArray(System.Byte[],System.Int32,System.Int32)
extern void MemoryProfiler_WriteIntToByteArray_mEC9056AEB48E7906BAA9FDA7FF538E7341233E8E (void);
// 0x00000573 System.Int32 UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::WriteStringToByteArray(System.Byte[],System.Int32,System.String)
extern void MemoryProfiler_WriteStringToByteArray_m48936038ADD56D3BF498870F4DA6AB12DE0CA9FC (void);
// 0x00000574 System.Void UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::FinalizeSnapshot(System.String,System.Boolean)
extern void MemoryProfiler_FinalizeSnapshot_m7DE2A0E49B6457B64D53255BE00F7F1C7EA30526 (void);
// 0x00000575 System.Void UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::SaveScreenshotToDisk(System.String,System.Boolean,System.IntPtr,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Int32)
extern void MemoryProfiler_SaveScreenshotToDisk_m9419808BAC900EAB213522E34F6268975722495A (void);
// 0x00000576 System.String UnityEngine.Events.UnityEventTools::TidyAssemblyTypeName(System.String)
extern void UnityEventTools_TidyAssemblyTypeName_m7ABD7C25EA8BF24C586CD9BD637421A12D8C5B44 (void);
// 0x00000577 UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern void ArgumentCache_get_unityObjectArgument_m546FEA2DAB93AD1222C0000A882FFAF66DF88B47 (void);
// 0x00000578 System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_mAA84D93F6FE66B3422F4A99D794BCCA11882DE35 (void);
// 0x00000579 System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern void ArgumentCache_get_intArgument_m8BDE2E1DBF5870F5F91041540EC7F867F0D24CFF (void);
// 0x0000057A System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern void ArgumentCache_get_floatArgument_m941EC215EC34675CA224A311BEDBBEF647F02150 (void);
// 0x0000057B System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern void ArgumentCache_get_stringArgument_mE71B434FCF1AA70BF2A922647F419BED0553E7FB (void);
// 0x0000057C System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern void ArgumentCache_get_boolArgument_mB66F6E9F16B1246A82A368E8B6AB94C4E05AF127 (void);
// 0x0000057D System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern void ArgumentCache_OnBeforeSerialize_mF5B9D962D88C22BC20AE330FFBC33FB3042604D0 (void);
// 0x0000057E System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern void ArgumentCache_OnAfterDeserialize_mB50D42B36BF948499C4927084E7589D0B1D9C6FB (void);
// 0x0000057F System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern void ArgumentCache__ctor_mF667890D72F5C3C3AE197688DEF71A23310248A0 (void);
// 0x00000580 System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern void BaseInvokableCall__ctor_mC60A356F5535F98996ADF5AF925032449DFAF2BB (void);
// 0x00000581 System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern void BaseInvokableCall__ctor_mB7F553CF006225E89AFD3C57820E8FF0E777C3FB (void);
// 0x00000582 System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
// 0x00000583 System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
// 0x00000584 System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern void BaseInvokableCall_AllowInvoke_m84704F31555A5C8AD726DAE1C80929D3F75A00DF (void);
// 0x00000585 System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000586 System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern void InvokableCall_add_Delegate_mA80FC3DDF9C96793161FED5B38577EC44E8ECCEC (void);
// 0x00000587 System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern void InvokableCall_remove_Delegate_m32D6AD4353BD281EAAC1C319D211FF323E87FABF (void);
// 0x00000588 System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern void InvokableCall__ctor_mB755E9394048D1920AA344EA3B3905810042E992 (void);
// 0x00000589 System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern void InvokableCall_Invoke_mE87495638C5A778726A99872D041A22CA957159E (void);
// 0x0000058A System.Void UnityEngine.Events.InvokableCall::Invoke()
extern void InvokableCall_Invoke_mC0E0B4D35F0795DCF2626DC15E5E92417430AAD7 (void);
// 0x0000058B System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern void InvokableCall_Find_mE1AF062AF0ADE337AEB2728F401CAB23E95D9360 (void);
// 0x0000058C System.Void UnityEngine.Events.InvokableCall`1::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
// 0x0000058D System.Void UnityEngine.Events.InvokableCall`1::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
// 0x0000058E System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x0000058F System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// 0x00000590 System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
// 0x00000591 System.Void UnityEngine.Events.InvokableCall`1::Invoke(T1)
// 0x00000592 System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000593 System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x00000594 System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
// 0x00000595 System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000596 System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x00000597 System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
// 0x00000598 System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000599 System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x0000059A System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
// 0x0000059B System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
// 0x0000059C System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// 0x0000059D System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
// 0x0000059E System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(T)
// 0x0000059F UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern void PersistentCall_get_target_mE1268D2B40A4618C5E318D439F37022B969A6115 (void);
// 0x000005A0 System.String UnityEngine.Events.PersistentCall::get_targetAssemblyTypeName()
extern void PersistentCall_get_targetAssemblyTypeName_m2F53F996EA6BFFDFCDF1D10B6361DE2A98DD9113 (void);
// 0x000005A1 System.String UnityEngine.Events.PersistentCall::get_methodName()
extern void PersistentCall_get_methodName_m249643D059927A05051B73309FCEAC6A6C9F9C88 (void);
// 0x000005A2 UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern void PersistentCall_get_mode_m7041C70D7CA9CC2D7FCB5D31C81E9C519AF1FEB8 (void);
// 0x000005A3 UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern void PersistentCall_get_arguments_m9FDD073B5551520F0FF4A672C60E237CADBC3435 (void);
// 0x000005A4 System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern void PersistentCall_IsValid_m3BDFC18A3D1AD8ECA8822EA4265127B35237E11B (void);
// 0x000005A5 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern void PersistentCall_GetRuntimeCall_mCD3A4A0287D8A9C2CF00D894F378BD4E4684287A (void);
// 0x000005A6 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern void PersistentCall_GetObjectCall_m5CDF291A373C8FD34513EF1A1D26C9B36ED7A95D (void);
// 0x000005A7 System.Void UnityEngine.Events.PersistentCall::OnBeforeSerialize()
extern void PersistentCall_OnBeforeSerialize_mD5109AA99B22304579296F4B6077647839456346 (void);
// 0x000005A8 System.Void UnityEngine.Events.PersistentCall::OnAfterDeserialize()
extern void PersistentCall_OnAfterDeserialize_m73B5F8F782FC97AE4C4DCE3DB2C1D9F102455D9E (void);
// 0x000005A9 System.Void UnityEngine.Events.PersistentCall::.ctor()
extern void PersistentCall__ctor_mCA080B989171E4FE13761074DD7DE76683969140 (void);
// 0x000005AA System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern void PersistentCallGroup__ctor_m0F94D4591DB6C4D6C7B997FA45A39C680610B1E0 (void);
// 0x000005AB System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern void PersistentCallGroup_Initialize_m162FC343BA2C41CBBB6358D624CBD2CED9468833 (void);
// 0x000005AC System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern void InvokableCallList_AddPersistentInvokableCall_mE7A19335649958FBF5522D7468D6075EEC325D9C (void);
// 0x000005AD System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern void InvokableCallList_AddListener_m07F4E145332E2059D570D8300CB422F56F6B0BF1 (void);
// 0x000005AE System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern void InvokableCallList_RemoveListener_mDE659068D9590D54D00436CCBDB3D27DCD263549 (void);
// 0x000005AF System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern void InvokableCallList_ClearPersistent_m9A59E6161F8E42120439B76FE952A17DA742443C (void);
// 0x000005B0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern void InvokableCallList_PrepareInvoke_mE340D329F5FC08EA77FC0F3662FF5E5BB85AE0B1 (void);
// 0x000005B1 System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern void InvokableCallList__ctor_m986F4056CA5480D44854152DB768E031AF95C5D8 (void);
// 0x000005B2 System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern void UnityEventBase__ctor_m8F423B800E573ED81DF59EB55CD88D48E817B101 (void);
// 0x000005B3 System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4E84FB82333E5AA5812095E536810C4BCAF55727 (void);
// 0x000005B4 System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA1BAB2B28C0E4A62BA06FEF5FCAD8A67C3449472 (void);
// 0x000005B5 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Type)
// 0x000005B6 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x000005B7 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern void UnityEventBase_FindMethod_m665F2360483EC2BE2D190C6EFA4A624FB8722089 (void);
// 0x000005B8 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Type,UnityEngine.Events.PersistentListenerMode,System.Type)
extern void UnityEventBase_FindMethod_m9F887E46F769E2C1D0CFE3C71F98892F9C461025 (void);
// 0x000005B9 System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern void UnityEventBase_DirtyPersistentCalls_m85DA5AEA84F8C32D2FDF5DD58D9B7EF704B7B238 (void);
// 0x000005BA System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern void UnityEventBase_RebuildPersistentCallsIfNeeded_m1FF3E4C46BB16B554103FAAAF3BBCE52C991D21F (void);
// 0x000005BB System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern void UnityEventBase_AddCall_mB4825708CFE71BBF9B167EB16FC911CFF95D101C (void);
// 0x000005BC System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern void UnityEventBase_RemoveListener_m0B091A723044E4120D592AC65CBD152AC3DF929E (void);
// 0x000005BD System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern void UnityEventBase_PrepareInvoke_m999D0F37E0D88375576323D30B67ED7FDC7A779D (void);
// 0x000005BE System.String UnityEngine.Events.UnityEventBase::ToString()
extern void UnityEventBase_ToString_m0A3BBFEC8C23E044D52502CE201FE82EEF60A8E7 (void);
// 0x000005BF System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Type,System.String,System.Type[])
extern void UnityEventBase_GetValidMethodInfo_mBA413E99550A6AD8B36F5BEB8682B1536E976C36 (void);
// 0x000005C0 System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (void);
// 0x000005C1 System.Void UnityEngine.Events.UnityAction::Invoke()
extern void UnityAction_Invoke_mFFF1FFE59D8285F8A81350E6D5C4D791F44F6AC9 (void);
// 0x000005C2 System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void UnityAction_BeginInvoke_m67AAC6F5871162346CBCCA0CA51AA38307A4ABB8 (void);
// 0x000005C3 System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern void UnityAction_EndInvoke_mC02B54511B4220CF6D1B37F14BF07D522E91786B (void);
// 0x000005C4 System.Void UnityEngine.Events.UnityEvent::.ctor()
extern void UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6 (void);
// 0x000005C5 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Type)
extern void UnityEvent_FindMethod_Impl_m763FB43F24EF4A092E26FAE6F6DF561CF360475A (void);
// 0x000005C6 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern void UnityEvent_GetDelegate_m7AD1450601F49B957A87161BE6D14020720A1A56 (void);
// 0x000005C7 System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
// 0x000005C8 System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
// 0x000005C9 System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
// 0x000005CA System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
// 0x000005CB System.Void UnityEngine.Events.UnityEvent`1::.ctor()
// 0x000005CC System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
// 0x000005CD System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
// 0x000005CE System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Type)
// 0x000005CF UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x000005D0 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
// 0x000005D1 System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
// 0x000005D2 System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
// 0x000005D3 System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
// 0x000005D4 System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
// 0x000005D5 System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
// 0x000005D6 System.Void UnityEngine.Events.UnityEvent`2::.ctor()
// 0x000005D7 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Type)
// 0x000005D8 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x000005D9 System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
// 0x000005DA System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
// 0x000005DB System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
// 0x000005DC System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
// 0x000005DD System.Void UnityEngine.Events.UnityEvent`3::.ctor()
// 0x000005DE System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Type)
// 0x000005DF UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x000005E0 System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
// 0x000005E1 System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
// 0x000005E2 System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
// 0x000005E3 System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
// 0x000005E4 System.Void UnityEngine.Events.UnityEvent`4::.ctor()
// 0x000005E5 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Type)
// 0x000005E6 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x000005E7 System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (void);
// 0x000005E8 System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (void);
// 0x000005E9 System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::Set(System.Boolean,System.String,System.String,System.String)
extern void MovedFromAttributeData_Set_mC5572E6033FCE1C53B8C43A8D5280E71CE388640 (void);
// 0x000005EA System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern void MovedFromAttribute__ctor_mA4B2632CE3004A3E0EA8E1241736518320806568 (void);
// 0x000005EB System.Boolean UnityEngine.SceneManagement.Scene::GetIsLoadedInternal(System.Int32)
extern void Scene_GetIsLoadedInternal_m937AE9E73BD838A013A229917558F8C66E2F4082 (void);
// 0x000005EC System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern void Scene_get_handle_m57967C50E461CD48441CA60645AF8FA04F7B132C (void);
// 0x000005ED System.Boolean UnityEngine.SceneManagement.Scene::get_isLoaded()
extern void Scene_get_isLoaded_m040A3D274F4FAD21BC95C6154FEA557ADE5C8E93 (void);
// 0x000005EE System.Boolean UnityEngine.SceneManagement.Scene::op_Equality(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void Scene_op_Equality_m5B02632BCD075BF2857A75AF21F5F82AE22C4E4D (void);
// 0x000005EF System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern void Scene_GetHashCode_mFC620B8CA1EAA64BF0D7B33E8D3EAFAEDC9A6DCB (void);
// 0x000005F0 System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern void Scene_Equals_m78D2F82F3133AD32F35C7981B65D0980A6C3006D (void);
// 0x000005F1 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManagerAPIInternal::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters,System.Boolean)
extern void SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_m17ADD5CEBB41A67CFD806CF61CFED9EB0C288C35 (void);
// 0x000005F2 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManagerAPIInternal::LoadSceneAsyncNameIndexInternal_Injected(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters&,System.Boolean)
extern void SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_Injected_mE2CC557F3CD2085753D6AE5C5E8280E3C931C37D (void);
// 0x000005F3 UnityEngine.SceneManagement.SceneManagerAPI UnityEngine.SceneManagement.SceneManagerAPI::get_ActiveAPI()
extern void SceneManagerAPI_get_ActiveAPI_m0D37AAD13BCEA4851A14AD625B3C6E875EF133A4 (void);
// 0x000005F4 UnityEngine.SceneManagement.SceneManagerAPI UnityEngine.SceneManagement.SceneManagerAPI::get_overrideAPI()
extern void SceneManagerAPI_get_overrideAPI_m481E89994FFE6384A8F0B4F6891E4A0A504C81D7 (void);
// 0x000005F5 System.Void UnityEngine.SceneManagement.SceneManagerAPI::.ctor()
extern void SceneManagerAPI__ctor_m3DD636D3929892F46996A95396A912C589C9EECF (void);
// 0x000005F6 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManagerAPI::LoadSceneAsyncByNameOrIndex(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters,System.Boolean)
extern void SceneManagerAPI_LoadSceneAsyncByNameOrIndex_m351028028E2A19B144F133011B7314DC8714495B (void);
// 0x000005F7 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManagerAPI::LoadFirstScene(System.Boolean)
extern void SceneManagerAPI_LoadFirstScene_m0ECE028BAA91CE0EEAF39C713493667B6BFFE759 (void);
// 0x000005F8 System.Void UnityEngine.SceneManagement.SceneManagerAPI::.cctor()
extern void SceneManagerAPI__cctor_mE2DB55CFCB089B29C626309084CDBFDAE704F8EB (void);
// 0x000005F9 System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCount()
extern void SceneManager_get_sceneCount_m57B8EB790D8B6673BA840442B4F125121CC5456E (void);
// 0x000005FA UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneAt(System.Int32)
extern void SceneManager_GetSceneAt_m46AF96028C6A3A09198ABB313E4206D93A8D1F3F (void);
// 0x000005FB UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsyncInternal(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneManager_UnloadSceneAsyncInternal_m5892CDFCDDEF60109B0C4DB377EC73695AA00B6B (void);
// 0x000005FC UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters,System.Boolean)
extern void SceneManager_LoadSceneAsyncNameIndexInternal_mD72656A5141151775F28886F59D2830B0EC1B659 (void);
// 0x000005FD UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadFirstScene_Internal(System.Boolean)
extern void SceneManager_LoadFirstScene_Internal_mC0778CB81D17E92E17A814E25C7481E0747A573E (void);
// 0x000005FE System.Void UnityEngine.SceneManagement.SceneManager::add_sceneUnloaded(UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>)
extern void SceneManager_add_sceneUnloaded_m2CE4194528D7AAE67F494EB15C8BBE650FBA4C72 (void);
// 0x000005FF System.Void UnityEngine.SceneManagement.SceneManager::remove_sceneUnloaded(UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>)
extern void SceneManager_remove_sceneUnloaded_mA7CD60918C6EE3B8D8623201A487C2CEC8D4EA5E (void);
// 0x00000600 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneParameters)
extern void SceneManager_LoadSceneAsync_m77AB3010DA4EE548FE973D65A2D983F0CC86254E (void);
// 0x00000601 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsync(UnityEngine.SceneManagement.Scene)
extern void SceneManager_UnloadSceneAsync_m94D080FDA6440916AA8F8F4FA13B1002C96BB589 (void);
// 0x00000602 System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneManager_Internal_SceneLoaded_m3546B371F03BC8DC053FFF165AB25ADF44A183BE (void);
// 0x00000603 System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void SceneManager_Internal_SceneUnloaded_m9A68F15B0FA483633F24E0E50574CFB6DD2D5520 (void);
// 0x00000604 System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void SceneManager_Internal_ActiveSceneChanged_m4094F4307C6B3DE0BB87ED646EA9BD6640B831DC (void);
// 0x00000605 System.Void UnityEngine.SceneManagement.SceneManager::.cctor()
extern void SceneManager__cctor_m5D677C6723892CAB16E0F9710AF214D7A328B396 (void);
// 0x00000606 System.Void UnityEngine.SceneManagement.SceneManager::GetSceneAt_Injected(System.Int32,UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetSceneAt_Injected_m2649DB4E01B925CFDF17EBDFB48816447082771A (void);
// 0x00000607 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneAsyncInternal_Injected(UnityEngine.SceneManagement.Scene&,UnityEngine.SceneManagement.UnloadSceneOptions)
extern void SceneManager_UnloadSceneAsyncInternal_Injected_m01CB1AF502BE43217B0062B45D8A71612FB81FF5 (void);
// 0x00000608 System.Void UnityEngine.SceneManagement.LoadSceneParameters::set_loadSceneMode(UnityEngine.SceneManagement.LoadSceneMode)
extern void LoadSceneParameters_set_loadSceneMode_m8AAA5796E9D642FC5C2C95831F22E272A28DD152 (void);
// 0x00000609 System.String UnityEngine.LowLevel.PlayerLoopSystem::ToString()
extern void PlayerLoopSystem_ToString_mAC7EE13A5561966294881362AD59CB55DBBED9EE (void);
// 0x0000060A System.Void UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::.ctor(System.Object,System.IntPtr)
extern void UpdateFunction__ctor_mB10AB83A3F547AC95FF726E8A7B5FF9C16EC1319 (void);
// 0x0000060B System.Void UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::Invoke()
extern void UpdateFunction_Invoke_mB17C55B92FAECE51078028F59A9F1EAC2016B1AD (void);
// 0x0000060C System.IAsyncResult UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern void UpdateFunction_BeginInvoke_m1EB0935AB72A286D153ACEBD0E5D66BB38BD6799 (void);
// 0x0000060D System.Void UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction::EndInvoke(System.IAsyncResult)
extern void UpdateFunction_EndInvoke_mB4BC0AA40E9C83274116DF6467D72DD4902DA624 (void);
// 0x0000060E System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern void MessageEventArgs__ctor_m18272C7358FAC22848ED84A952DCE17E8CD623BA (void);
// 0x0000060F UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern void PlayerConnection_get_instance_mA46C9194C4D2FE0BB06C135C4C0521DD516592F6 (void);
// 0x00000610 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::get_isConnected()
extern void PlayerConnection_get_isConnected_m315E30E90CA2BBEF2C2C366EF68BF0380C2BCF28 (void);
// 0x00000611 UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern void PlayerConnection_CreateInstance_m761E97DB4F693FC1A92418088B90872AA559BFFE (void);
// 0x00000612 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::OnEnable()
extern void PlayerConnection_OnEnable_m89048E6B2F1A820F9FD99C3237CB55FF7F56F558 (void);
// 0x00000613 UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern void PlayerConnection_GetConnectionNativeApi_m94F4E5D341869936857659D92591483D5F6EE95F (void);
// 0x00000614 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Register(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerConnection_Register_m97A0118B2F172AA0236D0D428F2C6F6E8326BF3D (void);
// 0x00000615 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Unregister(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerConnection_Unregister_m1D2F6BE95E82D60F6EA02139AA64D7D58B4B889B (void);
// 0x00000616 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_RegisterConnection_m3DA7DB08B44854C6914F9A154C486EFFCC2B4E8D (void);
// 0x00000617 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_RegisterDisconnection_mA0D4C45243C1244339CAF66E45C65A6130E8B87A (void);
// 0x00000618 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::UnregisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_UnregisterConnection_mC44D98524BD2E21B7658AB898B8283C34A5AD96E (void);
// 0x00000619 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::UnregisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_UnregisterDisconnection_m1DB86C159F601428F59DAB3A4BB912C780F8EAFD (void);
// 0x0000061A System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Send(System.Guid,System.Byte[])
extern void PlayerConnection_Send_m51137877D6E519903AEA4A506ABD48A30AF684F0 (void);
// 0x0000061B System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::TrySend(System.Guid,System.Byte[])
extern void PlayerConnection_TrySend_m9AD3046E615B6DECE991776EEFAB10EA95E478A6 (void);
// 0x0000061C System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::BlockUntilRecvMsg(System.Guid,System.Int32)
extern void PlayerConnection_BlockUntilRecvMsg_m3C3CE1A885657B18DEE2C10066425B45F2B15FAC (void);
// 0x0000061D System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectAll()
extern void PlayerConnection_DisconnectAll_m2A55CA3DECCC5683315504EACB9311E565D1EFF5 (void);
// 0x0000061E System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern void PlayerConnection_MessageCallbackInternal_m223C558BDA58E11AE597D73CEC359718A336FD74 (void);
// 0x0000061F System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern void PlayerConnection_ConnectedCallbackInternal_m9143E59EA71734C96F877CFD685F0935CEEBA13E (void);
// 0x00000620 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern void PlayerConnection_DisconnectedCallback_mCC356CB2D6C8820A435950F630BA349B7CA5E267 (void);
// 0x00000621 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern void PlayerConnection__ctor_mBB02EDC32EB6458CB527F9703CFD4400C7CDCB92 (void);
// 0x00000622 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m66E5B9C72A27F0645F9854522ACE087CE32A5EEE (void);
// 0x00000623 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass12_0::<Register>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass12_0_U3CRegisterU3Eb__0_m27C1416BAD7AF0A1BF83339C8A03865A6487B234 (void);
// 0x00000624 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m75A8930EBA7C6BF81519358930656DAA2FBDDEDB (void);
// 0x00000625 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass13_0::<Unregister>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass13_0_U3CUnregisterU3Eb__0_mBA34804D7BB1B4FFE45D077BBB07BFA81C8DCB00 (void);
// 0x00000626 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m2B215926A23E33037D754DFAFDF8459D82243683 (void);
// 0x00000627 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection/<>c__DisplayClass20_0::<BlockUntilRecvMsg>b__0(UnityEngine.Networking.PlayerConnection.MessageEventArgs)
extern void U3CU3Ec__DisplayClass20_0_U3CBlockUntilRecvMsgU3Eb__0_m1D6C3976419CFCE0B612D04C135A985707C215E2 (void);
// 0x00000628 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m157C5C69D6E22A8BCBDC40929BA67E1B6E586389 (void);
// 0x00000629 UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern void PlayerEditorConnectionEvents_AddAndCreate_m9722774BB976599BF8AD8745CA8EC3218828306B (void);
// 0x0000062A System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::UnregisterManagedCallback(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerEditorConnectionEvents_UnregisterManagedCallback_mEB4C4FBCBA7A9527475914F61AFDB9676B8BEFB3 (void);
// 0x0000062B System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern void PlayerEditorConnectionEvents__ctor_m40946A5B9D9FB40849BEA07DCD600891229249A0 (void);
// 0x0000062C System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageEvent::.ctor()
extern void MessageEvent__ctor_mE4D70D8837C51E422C9A40C3F8F34ACB9AB572BB (void);
// 0x0000062D System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/ConnectionChangeEvent::.ctor()
extern void ConnectionChangeEvent__ctor_m95EBD8B5EA1C4A14A5F2B71CEE1A2C18C73DB0A1 (void);
// 0x0000062E System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::get_MessageTypeId()
extern void MessageTypeSubscribers_get_MessageTypeId_m5A873C55E97728BD12BA52B5DB72FFA366992DD9 (void);
// 0x0000062F System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern void MessageTypeSubscribers_set_MessageTypeId_m7125FF3E71F4E678644F056A4DC5C29EFB749762 (void);
// 0x00000630 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers::.ctor()
extern void MessageTypeSubscribers__ctor_mA51A6D3E38DBAA5B8237BAB1688F669F24982F29 (void);
// 0x00000631 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mEB8DB2BFDA9F6F083E77F1EC1CE3F4861CD1815A (void);
// 0x00000632 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass6_0::<InvokeMessageIdSubscribers>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass6_0_U3CInvokeMessageIdSubscribersU3Eb__0_mEF5D5DFC8F7C2CEC86378AC3BBD40E80A1D9C615 (void);
// 0x00000633 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m97B67DA8AA306A160A790CCDD869669878DDB7F3 (void);
// 0x00000634 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass7_0::<AddAndCreate>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass7_0_U3CAddAndCreateU3Eb__0_mAA3D205F35F7BE200A5DDD14099F56312FBED909 (void);
// 0x00000635 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m18AC0B2825D7BB04F59DC800DFF74D45921A28FA (void);
// 0x00000636 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/<>c__DisplayClass8_0::<UnregisterManagedCallback>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents/MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass8_0_U3CUnregisterManagedCallbackU3Eb__0_m70805C4CE0F8DD258CC3975A8C90313A0A609BE3 (void);
// 0x00000637 System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern void DefaultValueAttribute__ctor_m7A9877491C22E8CDCFDAD240D04156D4FAE7D6C6 (void);
// 0x00000638 System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern void DefaultValueAttribute_get_Value_m333925936EF155BACAEF24CE1ADE07085722606E (void);
// 0x00000639 System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern void DefaultValueAttribute_Equals_mD096E8E7C8C6051AD743AAB7CAADB27428871E51 (void);
// 0x0000063A System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern void DefaultValueAttribute_GetHashCode_mA74BBB26F6C0B49AA44C175DBFDB82F00E497595 (void);
// 0x0000063B System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern void ExcludeFromDocsAttribute__ctor_mFA14E76D8A30ED8CA3ADCDA83BE056E54753825D (void);
// 0x0000063C System.Boolean UnityEngine.Rendering.GraphicsSettings::get_lightsUseLinearIntensity()
extern void GraphicsSettings_get_lightsUseLinearIntensity_m02A37F59F36C77877FC86B93478BC5795F70FFB2 (void);
// 0x0000063D System.Int32 UnityEngine.Rendering.OnDemandRendering::get_renderFrameInterval()
extern void OnDemandRendering_get_renderFrameInterval_m7B3F913B3C16E756BE5DF901D86FC7F869759367 (void);
// 0x0000063E System.Void UnityEngine.Rendering.OnDemandRendering::GetRenderFrameInterval(System.Int32&)
extern void OnDemandRendering_GetRenderFrameInterval_mF254C740E4C04295AC2EF10D05D22BCA179D7685 (void);
// 0x0000063F System.Void UnityEngine.Rendering.OnDemandRendering::.cctor()
extern void OnDemandRendering__cctor_m3C3EFF1AE188B9FACE3E94D9F333CF873D110FD0 (void);
// 0x00000640 System.Boolean UnityEngine.Rendering.LODParameters::Equals(UnityEngine.Rendering.LODParameters)
extern void LODParameters_Equals_mF803671C1F46ECEEE0B376EBF3AAF69D1236B4C0 (void);
// 0x00000641 System.Boolean UnityEngine.Rendering.LODParameters::Equals(System.Object)
extern void LODParameters_Equals_m8F8B356BCB62FAEAE0EFD8C51FFF9C5045A7DB5C (void);
// 0x00000642 System.Int32 UnityEngine.Rendering.LODParameters::GetHashCode()
extern void LODParameters_GetHashCode_m5310697EE3BF4943F7358EF0EA2E7B3A9D7C1BAD (void);
// 0x00000643 System.Void UnityEngine.Rendering.RenderPipeline::Render(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
// 0x00000644 System.Void UnityEngine.Rendering.RenderPipeline::ProcessRenderRequests(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera,System.Collections.Generic.List`1<UnityEngine.Camera/RenderRequest>)
extern void RenderPipeline_ProcessRenderRequests_m730AE65F326D6007A8C3D7C83CAF182C2DD21A11 (void);
// 0x00000645 System.Void UnityEngine.Rendering.RenderPipeline::InternalRender(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
extern void RenderPipeline_InternalRender_mD6C2FEDA607A430F963066B487C02F4D2379FBDA (void);
// 0x00000646 System.Void UnityEngine.Rendering.RenderPipeline::InternalRenderWithRequests(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[],System.Collections.Generic.List`1<UnityEngine.Camera/RenderRequest>)
extern void RenderPipeline_InternalRenderWithRequests_m9883F1C0D6400EB2A0364C92F7F8BB39E9398DCF (void);
// 0x00000647 System.Boolean UnityEngine.Rendering.RenderPipeline::get_disposed()
extern void RenderPipeline_get_disposed_m67EE9900399CE2E9783C5C69BFD1FF08DF521C34 (void);
// 0x00000648 System.Void UnityEngine.Rendering.RenderPipeline::set_disposed(System.Boolean)
extern void RenderPipeline_set_disposed_mB375F2860E0C96CA5E7124154610CB67CE3F80CA (void);
// 0x00000649 System.Void UnityEngine.Rendering.RenderPipeline::Dispose()
extern void RenderPipeline_Dispose_mE06FE618AE8AE1B563CAC05585ACBA8832849A7F (void);
// 0x0000064A System.Void UnityEngine.Rendering.RenderPipeline::Dispose(System.Boolean)
extern void RenderPipeline_Dispose_mC61059BB1A9F1CE9DB36C49FD01A1EB5BF4CFFE8 (void);
// 0x0000064B UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineAsset::InternalCreatePipeline()
extern void RenderPipelineAsset_InternalCreatePipeline_mAE0E11E7E8D2D501F7F0F06D37DB484D37533406 (void);
// 0x0000064C System.String[] UnityEngine.Rendering.RenderPipelineAsset::get_renderingLayerMaskNames()
extern void RenderPipelineAsset_get_renderingLayerMaskNames_m099B8C66F13086B843E997A91D5F29DFA640BD5B (void);
// 0x0000064D UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultMaterial()
extern void RenderPipelineAsset_get_defaultMaterial_mC04A65F5C07B7B186B734ACBDF5DA55DAFC88A4D (void);
// 0x0000064E UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveShader()
extern void RenderPipelineAsset_get_autodeskInteractiveShader_mA096E3713307598F4F0714348A137E3A0F450EAF (void);
// 0x0000064F UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveTransparentShader()
extern void RenderPipelineAsset_get_autodeskInteractiveTransparentShader_m09C4E71DE3D7BD8CDE7BB60CE379DAB150F309FF (void);
// 0x00000650 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveMaskedShader()
extern void RenderPipelineAsset_get_autodeskInteractiveMaskedShader_mD5AB54521766DF6A0FF42971D598761BCF7BC74A (void);
// 0x00000651 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailLitShader()
extern void RenderPipelineAsset_get_terrainDetailLitShader_mB60D130908834F5E6585578DDEA4E175DECCA654 (void);
// 0x00000652 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailGrassShader()
extern void RenderPipelineAsset_get_terrainDetailGrassShader_m3F41ABB79E3672A99A77594D7516855906BB71F8 (void);
// 0x00000653 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailGrassBillboardShader()
extern void RenderPipelineAsset_get_terrainDetailGrassBillboardShader_mED61CD4E3CF38C120FBC20866F99C17C5FE35FBB (void);
// 0x00000654 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultParticleMaterial()
extern void RenderPipelineAsset_get_defaultParticleMaterial_mA67643D5E509468BC5C20EABAD895B9920636961 (void);
// 0x00000655 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultLineMaterial()
extern void RenderPipelineAsset_get_defaultLineMaterial_mA76E800387087F9C4FB510E1C9526D43D7A430F3 (void);
// 0x00000656 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultTerrainMaterial()
extern void RenderPipelineAsset_get_defaultTerrainMaterial_m29231CDF74C3D74809F5C990F6881E17759CE298 (void);
// 0x00000657 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIMaterial()
extern void RenderPipelineAsset_get_defaultUIMaterial_m73C804E2798E17A2452687AB46A5570DD813AEC7 (void);
// 0x00000658 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIOverdrawMaterial()
extern void RenderPipelineAsset_get_defaultUIOverdrawMaterial_m75075F78B2FFC61DB9D05B09340D97B588FE6EB6 (void);
// 0x00000659 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIETC1SupportedMaterial()
extern void RenderPipelineAsset_get_defaultUIETC1SupportedMaterial_m530DC5B2F50F075DAB4CFA35A693293C8C98CA33 (void);
// 0x0000065A UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_default2DMaterial()
extern void RenderPipelineAsset_get_default2DMaterial_mFC4CF7D4F8341D140CC0AB0B471B154AD580C4F0 (void);
// 0x0000065B UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultShader()
extern void RenderPipelineAsset_get_defaultShader_mE3420265AB2F3629C6C86E88CC7FDB7744B177C1 (void);
// 0x0000065C UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultSpeedTree7Shader()
extern void RenderPipelineAsset_get_defaultSpeedTree7Shader_m14E423504D20BC7D296EDD97045790DEA68DBE14 (void);
// 0x0000065D UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultSpeedTree8Shader()
extern void RenderPipelineAsset_get_defaultSpeedTree8Shader_mDAB93B789B21F1D58A6FE11858F8C18527ABD50B (void);
// 0x0000065E UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineAsset::CreatePipeline()
// 0x0000065F System.Void UnityEngine.Rendering.RenderPipelineAsset::OnValidate()
extern void RenderPipelineAsset_OnValidate_mEFE93B16F8AA5C809D95536A557FA9E29FF7B1DB (void);
// 0x00000660 System.Void UnityEngine.Rendering.RenderPipelineAsset::OnDisable()
extern void RenderPipelineAsset_OnDisable_m98A18DF5D40DBB36EF0A33C637CFA41A71FC03C1 (void);
// 0x00000661 System.Void UnityEngine.Rendering.RenderPipelineAsset::.ctor()
extern void RenderPipelineAsset__ctor_mC4E1451327751FBCB6F05982262D3B7ED8538DF4 (void);
// 0x00000662 UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineManager::get_currentPipeline()
extern void RenderPipelineManager_get_currentPipeline_m096347F0BF45316A41A84B9344DB6B29F4D48611 (void);
// 0x00000663 System.Void UnityEngine.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Rendering.RenderPipeline)
extern void RenderPipelineManager_set_currentPipeline_m03D0E14C590848C8D5ABC2C22C026935119CE526 (void);
// 0x00000664 System.Void UnityEngine.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern void RenderPipelineManager_CleanupRenderPipeline_m85443E0BE0778DFA602405C8047DCAE5DCF6D54E (void);
// 0x00000665 System.Void UnityEngine.Rendering.RenderPipelineManager::GetCameras(UnityEngine.Rendering.ScriptableRenderContext)
extern void RenderPipelineManager_GetCameras_m8BB39469D10975B096634537AC44FB7CF6D52938 (void);
// 0x00000666 System.Void UnityEngine.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Rendering.RenderPipelineAsset,System.IntPtr,System.Collections.Generic.List`1<UnityEngine.Camera/RenderRequest>)
extern void RenderPipelineManager_DoRenderLoop_Internal_m43950BFAB0F05B48BE90B2B19679C0249D46273F (void);
// 0x00000667 System.Void UnityEngine.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Rendering.RenderPipelineAsset)
extern void RenderPipelineManager_PrepareRenderPipeline_m4C5F3624BD68AFCAAD4A9D800ED906B9DF4DFB55 (void);
// 0x00000668 System.Void UnityEngine.Rendering.RenderPipelineManager::.cctor()
extern void RenderPipelineManager__cctor_mAF92ABD8F7586C49F9E8AAE720EBE21A2700CF6D (void);
// 0x00000669 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras_Internal()
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_mDD1E260788000AB57C94A2D3F8F02A2B91DA653F (void);
// 0x0000066A UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera_Internal(System.Int32)
extern void ScriptableRenderContext_GetCamera_Internal_m20F51E42E84B8E2BBF2CC72F8C8EBFDCE6737646 (void);
// 0x0000066B System.Void UnityEngine.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern void ScriptableRenderContext__ctor_mEA592FA995EF36C1F8F05EF2E51BC1089D7371CA (void);
// 0x0000066C System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras()
extern void ScriptableRenderContext_GetNumberOfCameras_m43FCE097543E85E40FCA641C570A25E718E3D6F6 (void);
// 0x0000066D UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera(System.Int32)
extern void ScriptableRenderContext_GetCamera_mF8D58C4C1BB5667F63A2DCFDB72CE4C3C7ADBBFA (void);
// 0x0000066E System.Boolean UnityEngine.Rendering.ScriptableRenderContext::Equals(UnityEngine.Rendering.ScriptableRenderContext)
extern void ScriptableRenderContext_Equals_mDC10DFED8A46426E355FE7877624EC2D549EA7B7 (void);
// 0x0000066F System.Boolean UnityEngine.Rendering.ScriptableRenderContext::Equals(System.Object)
extern void ScriptableRenderContext_Equals_mB04CFBF55095DF6179ED2229F4C9FE907F95799D (void);
// 0x00000670 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetHashCode()
extern void ScriptableRenderContext_GetHashCode_mACDECBAC76686105322F409089D9A867DF4BD46D (void);
// 0x00000671 System.Void UnityEngine.Rendering.ScriptableRenderContext::.cctor()
extern void ScriptableRenderContext__cctor_m6993C6472C0532CA5057135C3B5D3015C8729C46 (void);
// 0x00000672 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras_Internal_Injected(UnityEngine.Rendering.ScriptableRenderContext&)
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_Injected_m65EF6B7DE8AFA868E6D83B2D75791315D9841426 (void);
// 0x00000673 UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera_Internal_Injected(UnityEngine.Rendering.ScriptableRenderContext&,System.Int32)
extern void ScriptableRenderContext_GetCamera_Internal_Injected_m17CA2AE7513419CA8FC464A270218F599D48C830 (void);
// 0x00000674 System.Void UnityEngine.Rendering.ShaderTagId::.ctor(System.String)
extern void ShaderTagId__ctor_mC8779BC717DBC52669DDF99900F968216119A830 (void);
// 0x00000675 System.Int32 UnityEngine.Rendering.ShaderTagId::get_id()
extern void ShaderTagId_get_id_m0340687DA0CA3F820D569CAE280E3EBF10A23B6E (void);
// 0x00000676 System.Void UnityEngine.Rendering.ShaderTagId::set_id(System.Int32)
extern void ShaderTagId_set_id_m291EC34E5E1EE99B6D6DF664B331347DBE83364D (void);
// 0x00000677 System.Boolean UnityEngine.Rendering.ShaderTagId::Equals(System.Object)
extern void ShaderTagId_Equals_m13F76C51B5ECF4EC9856579496F93D2B5B9041A7 (void);
// 0x00000678 System.Boolean UnityEngine.Rendering.ShaderTagId::Equals(UnityEngine.Rendering.ShaderTagId)
extern void ShaderTagId_Equals_m19A2CFBFF4915B92F7E2572CCAB00A7CBD934DA3 (void);
// 0x00000679 System.Int32 UnityEngine.Rendering.ShaderTagId::GetHashCode()
extern void ShaderTagId_GetHashCode_m6912AAFF83FFD29FBB2BBE51E2611C2A3667FB67 (void);
// 0x0000067A System.Void UnityEngine.Rendering.ShaderTagId::.cctor()
extern void ShaderTagId__cctor_mDC50E07281EFBA6C8517F9AB20D187C74BCBE89D (void);
// 0x0000067B UnityEngine.Rendering.SupportedRenderingFeatures UnityEngine.Rendering.SupportedRenderingFeatures::get_active()
extern void SupportedRenderingFeatures_get_active_mE16AFBB742C413071F2DB87563826A90A93EA04F (void);
// 0x0000067C System.Void UnityEngine.Rendering.SupportedRenderingFeatures::set_active(UnityEngine.Rendering.SupportedRenderingFeatures)
extern void SupportedRenderingFeatures_set_active_m3BC49234CD45C5EFAE64E319D5198CA159143F54 (void);
// 0x0000067D UnityEngine.Rendering.SupportedRenderingFeatures/LightmapMixedBakeModes UnityEngine.Rendering.SupportedRenderingFeatures::get_defaultMixedLightingModes()
extern void SupportedRenderingFeatures_get_defaultMixedLightingModes_m7B53835BDDAF009835F9A0907BC59E4E88C71D9C (void);
// 0x0000067E UnityEngine.Rendering.SupportedRenderingFeatures/LightmapMixedBakeModes UnityEngine.Rendering.SupportedRenderingFeatures::get_mixedLightingModes()
extern void SupportedRenderingFeatures_get_mixedLightingModes_mE4A171C47A4A685E49F2F382AA51C556B48EE58C (void);
// 0x0000067F UnityEngine.LightmapBakeType UnityEngine.Rendering.SupportedRenderingFeatures::get_lightmapBakeTypes()
extern void SupportedRenderingFeatures_get_lightmapBakeTypes_mAF3B22ACCE625D1C45F411D30C2874E8A847605E (void);
// 0x00000680 UnityEngine.LightmapsMode UnityEngine.Rendering.SupportedRenderingFeatures::get_lightmapsModes()
extern void SupportedRenderingFeatures_get_lightmapsModes_m69B5455BF172B258A0A2DB6F52846E8934AD048A (void);
// 0x00000681 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_enlighten()
extern void SupportedRenderingFeatures_get_enlighten_mA4BDBEBFE0E8F1C161D7BE28B328E6D6E36720A9 (void);
// 0x00000682 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_rendersUIOverlay()
extern void SupportedRenderingFeatures_get_rendersUIOverlay_m8E56255490C51999C7B14F30CD072E49E2C39B4E (void);
// 0x00000683 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::FallbackMixedLightingModeByRef(System.IntPtr)
extern void SupportedRenderingFeatures_FallbackMixedLightingModeByRef_m517CBD3BBD91EEA5D20CD5979DF8841FE0DBEDAC (void);
// 0x00000684 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::IsMixedLightingModeSupported(UnityEngine.MixedLightingMode)
extern void SupportedRenderingFeatures_IsMixedLightingModeSupported_m8AFE3C9D450B1A6E52A31EA84C1B8F626AAC0CD0 (void);
// 0x00000685 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsMixedLightingModeSupportedByRef(UnityEngine.MixedLightingMode,System.IntPtr)
extern void SupportedRenderingFeatures_IsMixedLightingModeSupportedByRef_mC13FADD4B8DCEB26F58C6F5DD9D7899A621F9828 (void);
// 0x00000686 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapBakeTypeSupported(UnityEngine.LightmapBakeType)
extern void SupportedRenderingFeatures_IsLightmapBakeTypeSupported_m8BBA40E9CBAFFD8B176F3812C36DD31D9D60BBEA (void);
// 0x00000687 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapBakeTypeSupportedByRef(UnityEngine.LightmapBakeType,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapBakeTypeSupportedByRef_mB6F953153B328DBFD1F7E444D155F8C53ABCD876 (void);
// 0x00000688 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapsModeSupportedByRef(UnityEngine.LightmapsMode,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapsModeSupportedByRef_m3477F21B073DD73F183FAD40A8EEF53843A8A581 (void);
// 0x00000689 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapperSupportedByRef(System.Int32,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapperSupportedByRef_mEAFB23042E154953C780501A57D605F76510BB11 (void);
// 0x0000068A System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsUIOverlayRenderedBySRP(System.IntPtr)
extern void SupportedRenderingFeatures_IsUIOverlayRenderedBySRP_m829585DA31B1BECD1349A02B69657B1D38D2DDC3 (void);
// 0x0000068B System.Void UnityEngine.Rendering.SupportedRenderingFeatures::FallbackLightmapperByRef(System.IntPtr)
extern void SupportedRenderingFeatures_FallbackLightmapperByRef_mD2BB8B58F329170010CB9B3BF72794755218046A (void);
// 0x0000068C System.Void UnityEngine.Rendering.SupportedRenderingFeatures::.ctor()
extern void SupportedRenderingFeatures__ctor_m0612F2A9F55682A7255B3B276E9BAFCFC0CB4EF4 (void);
// 0x0000068D System.Void UnityEngine.Rendering.SupportedRenderingFeatures::.cctor()
extern void SupportedRenderingFeatures__cctor_mCC9E6E14A59B708435BCF2B25BE36943EC590E28 (void);
// 0x0000068E System.Void UnityEngine.Rendering.BatchCullingContext::.ctor(Unity.Collections.NativeArray`1<UnityEngine.Plane>,Unity.Collections.NativeArray`1<UnityEngine.Rendering.BatchVisibility>,Unity.Collections.NativeArray`1<System.Int32>,Unity.Collections.NativeArray`1<System.Int32>,UnityEngine.Rendering.LODParameters,UnityEngine.Matrix4x4,System.Single)
extern void BatchCullingContext__ctor_m660602A9A31FEDDB5673F26C3FD11B4194395369 (void);
// 0x0000068F System.Void UnityEngine.Rendering.BatchRendererGroup::InvokeOnPerformCulling(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchRendererCullingOutput&,UnityEngine.Rendering.LODParameters&)
extern void BatchRendererGroup_InvokeOnPerformCulling_m164A637514875EBBFF4A12C55521BEA93474739C (void);
// 0x00000690 System.Void UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::.ctor(System.Object,System.IntPtr)
extern void OnPerformCulling__ctor_m1862F8A67E8CA14A19B80DE61BBC1881DF945E9F (void);
// 0x00000691 Unity.Jobs.JobHandle UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::Invoke(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchCullingContext)
extern void OnPerformCulling_Invoke_m804E8422831E63707E5582FBBBFEF08E8A100525 (void);
// 0x00000692 System.IAsyncResult UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::BeginInvoke(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchCullingContext,System.AsyncCallback,System.Object)
extern void OnPerformCulling_BeginInvoke_mAF6E29B6BAC52D4DCAD0C87B8C6939B94566EAA1 (void);
// 0x00000693 Unity.Jobs.JobHandle UnityEngine.Rendering.BatchRendererGroup/OnPerformCulling::EndInvoke(System.IAsyncResult)
extern void OnPerformCulling_EndInvoke_m643216ACF662C78BD6F98E09FA5AA45F92E2F9F0 (void);
// 0x00000694 System.Void UnityEngine.Playables.INotificationReceiver::OnNotify(UnityEngine.Playables.Playable,UnityEngine.Playables.INotification,System.Object)
// 0x00000695 System.Void UnityEngine.Playables.IPlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
// 0x00000696 System.Void UnityEngine.Playables.IPlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
// 0x00000697 System.Void UnityEngine.Playables.IPlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
// 0x00000698 System.Void UnityEngine.Playables.IPlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
// 0x00000699 System.Void UnityEngine.Playables.IPlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x0000069A System.Void UnityEngine.Playables.IPlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x0000069B System.Void UnityEngine.Playables.IPlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x0000069C System.Void UnityEngine.Playables.IPlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
// 0x0000069D UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern void Playable_get_Null_m008AFBC0B01AE584444CF8322CCCC038ED2B8B58 (void);
// 0x0000069E System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern void Playable__ctor_m4B5AC727703A68C00773F99DE1C711EFC973DCA8 (void);
// 0x0000069F UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern void Playable_GetHandle_mA646BB041702651694A643FDE1A835112F718351 (void);
// 0x000006A0 System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern void Playable_Equals_m1BFA06EB851EEEB20705520563A2D504637CDA1C (void);
// 0x000006A1 System.Void UnityEngine.Playables.Playable::.cctor()
extern void Playable__cctor_m83EA9DB5D7F0DAE681E34D0899A2562795301888 (void);
// 0x000006A2 UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
// 0x000006A3 System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern void PlayableAsset_get_duration_m7D57ED7F9E30E414F2981357A61C42CBF3D8367D (void);
// 0x000006A4 System.Collections.Generic.IEnumerable`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Playables.PlayableAsset::get_outputs()
extern void PlayableAsset_get_outputs_m5F941EB83243BB24A93C27A7583BB55FF18CB38C (void);
// 0x000006A5 System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern void PlayableAsset_Internal_CreatePlayable_m5E4984A4BF33518FBE69B43A29DB486AF6501537 (void);
// 0x000006A6 System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern void PlayableAsset_Internal_GetPlayableAssetDuration_mE27BD3B5B7D9E80D1C249D6430401BD81E4AFB4C (void);
// 0x000006A7 System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern void PlayableAsset__ctor_mAE1FA54D280C75ADC9486656C5C36AC1917D5FE4 (void);
// 0x000006A8 System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern void PlayableBehaviour__ctor_m1EA2FC6B8DE3503745344E7BBAA0B40FDCD50FC8 (void);
// 0x000006A9 System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnGraphStart_mB302433EB8460D88FD2FD035D2BF9BF82BD82AA6 (void);
// 0x000006AA System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnGraphStop_m24F852A18B98173C2CBF1B96C0477A1F77BB9CFE (void);
// 0x000006AB System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnPlayableCreate_m62F834EDF37B809612E47CCC9C4848435A89CD12 (void);
// 0x000006AC System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnPlayableDestroy_m9F6E1A185DC7C98C312D9F3A0A7308DE3D32F745 (void);
// 0x000006AD System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_OnBehaviourPlay_m71A792D97CD840F5D6C73889D00C2887F41A0F41 (void);
// 0x000006AE System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_OnBehaviourPause_mCC4EAD0766538FE39656D3EE04268239BB091AD2 (void);
// 0x000006AF System.Void UnityEngine.Playables.PlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_PrepareFrame_m45F96C7EDDA4ABF4C2271DB16163FBD873AA1432 (void);
// 0x000006B0 System.Void UnityEngine.Playables.PlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
extern void PlayableBehaviour_ProcessFrame_m5EB22A817FFF0662E0E3AFAB34C41D7B09D4326F (void);
// 0x000006B1 System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern void PlayableBehaviour_Clone_m3265FD7A4EA58C9A607F0F28EFF108EBBD826C3A (void);
// 0x000006B2 System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern void PlayableBinding__cctor_m75475729474FE236198B967978A11DBA38CC6E47 (void);
// 0x000006B3 System.Void UnityEngine.Playables.PlayableBinding/CreateOutputMethod::.ctor(System.Object,System.IntPtr)
extern void CreateOutputMethod__ctor_m944A1B790F35F52E108EF2CC13BA02BD8DE62EB3 (void);
// 0x000006B4 UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableBinding/CreateOutputMethod::Invoke(UnityEngine.Playables.PlayableGraph,System.String)
extern void CreateOutputMethod_Invoke_mFD551E15D9A6FD8B2C70A2CC96AAD0D2D9D3D109 (void);
// 0x000006B5 System.IAsyncResult UnityEngine.Playables.PlayableBinding/CreateOutputMethod::BeginInvoke(UnityEngine.Playables.PlayableGraph,System.String,System.AsyncCallback,System.Object)
extern void CreateOutputMethod_BeginInvoke_m082AE47F9DFBF0C1787081D2D628E0E5CECCBFF1 (void);
// 0x000006B6 UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableBinding/CreateOutputMethod::EndInvoke(System.IAsyncResult)
extern void CreateOutputMethod_EndInvoke_mAA7AE5BF9E6A38C081A972827A16497ACA9FA9CE (void);
// 0x000006B7 UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern void PlayableHandle_get_Null_mD1C6FC2D7F6A7A23955ACDD87BE934B75463E612 (void);
// 0x000006B8 System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_op_Equality_mFD26CFA8ECF2B622B1A3D4117066CAE965C9F704 (void);
// 0x000006B9 System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern void PlayableHandle_Equals_mEF30690ECF60C980C207198C86E401CB4DA5EF3F (void);
// 0x000006BA System.Boolean UnityEngine.Playables.PlayableHandle::Equals(UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_Equals_mB50663A8BC01BCED4A650EFADE88DCF47309E955 (void);
// 0x000006BB System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern void PlayableHandle_GetHashCode_m26AD05C2D6209A017CA6A079F026BC8D28600D72 (void);
// 0x000006BC System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_CompareVersion_m7B2FDE7E81BCBB16D3E2667AEDAA879FA75EA1AA (void);
// 0x000006BD System.Void UnityEngine.Playables.PlayableHandle::.cctor()
extern void PlayableHandle__cctor_m14F0FDD932E8AB558039FFCD57C2E319AA25C989 (void);
// 0x000006BE System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutput__ctor_m69B5F29DF2D785051B714CFEAAAF09A237978297 (void);
// 0x000006BF UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern void PlayableOutput_GetHandle_mC56A4F912A7AC8640DE78F95DD6C4F346E36820E (void);
// 0x000006C0 System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern void PlayableOutput_Equals_m5A66A7B50C52A8524BB807F439345D22E96FD5CE (void);
// 0x000006C1 System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern void PlayableOutput__cctor_m4F7AC7CBF2D9DD26FF45B6D521C4051CDC1A17D3 (void);
// 0x000006C2 UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern void PlayableOutputHandle_get_Null_m33F7D36A76BFDC0B58633BF931E55FA5BBFFD93D (void);
// 0x000006C3 System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern void PlayableOutputHandle_GetHashCode_mB4BD207CB03FA499179FBEAF64CC66A91F49E59C (void);
// 0x000006C4 System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_op_Equality_mD24E3FB556D12A8D2B6EBDB6953F667B963F5DA9 (void);
// 0x000006C5 System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern void PlayableOutputHandle_Equals_m06CE237BED975C0A3E9B19D071767EF6E7A3F83C (void);
// 0x000006C6 System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_Equals_mA8CE0F7B36B440F067705FA90DC4A836E92542E0 (void);
// 0x000006C7 System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_CompareVersion_mB96AE424417CD9E5610906A1508B299679814D8E (void);
// 0x000006C8 System.Void UnityEngine.Playables.PlayableOutputHandle::.cctor()
extern void PlayableOutputHandle__cctor_mFC855E625CBD628602F990B3A15AF0DB8C7E3AC1 (void);
// 0x000006C9 System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::get_red()
extern void LinearColor_get_red_mA08BA9496EAFF59F4E1EAD61FDB5F57B0B0CC541 (void);
// 0x000006CA System.Void UnityEngine.Experimental.GlobalIllumination.LinearColor::set_red(System.Single)
extern void LinearColor_set_red_mC0D9E4D96C36785145EAF7B0DBA90359EE193928 (void);
// 0x000006CB System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::get_green()
extern void LinearColor_get_green_m03F2EEBC25E91E1102A2D3252725B2BEDA7D7931 (void);
// 0x000006CC System.Void UnityEngine.Experimental.GlobalIllumination.LinearColor::set_green(System.Single)
extern void LinearColor_set_green_m6E196AC12B067ED8AEF3B7C7E9DA1B80B9550B89 (void);
// 0x000006CD System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::get_blue()
extern void LinearColor_get_blue_m0B87E7A2A5A5B6A6F5FD515890F6C3C528FC98FE (void);
// 0x000006CE System.Void UnityEngine.Experimental.GlobalIllumination.LinearColor::set_blue(System.Single)
extern void LinearColor_set_blue_mB65DC7FD20C551501BC181C457D010DCEECDEE7F (void);
// 0x000006CF UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LinearColor::Convert(UnityEngine.Color,System.Single)
extern void LinearColor_Convert_m7C35C63BFFDD93EBCC6E8050567B79562B82823A (void);
// 0x000006D0 UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LinearColor::Black()
extern void LinearColor_Black_m50DB4626285C618DFD9F561573AA77F84AE7E180 (void);
// 0x000006D1 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.DirectionalLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_m135DF5CF6CBECA4CBA0AC71F9CDEEF8DE25606DB (void);
// 0x000006D2 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.PointLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_m4E8BEBD383D5F6F1A1EDFEC763A718A7271C22A6 (void);
// 0x000006D3 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.SpotLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_mC9948FAC4A191C99E3E7D94677B7CFD241857C86 (void);
// 0x000006D4 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.RectangleLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_mA0DF9747C6AD308EAAF2A9530B4225A3D65BB092 (void);
// 0x000006D5 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.DiscLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_mB96C3F3E00F10DD0806BD3DB93B58C2299D59E94 (void);
// 0x000006D6 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::InitNoBake(System.Int32)
extern void LightDataGI_InitNoBake_mF600D612DE9A1CE4355C61317F6173E1AAEFBD57 (void);
// 0x000006D7 UnityEngine.Experimental.GlobalIllumination.LightMode UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.LightmapBakeType)
extern void LightmapperUtils_Extract_m32B54C9DC94AE03162E3896C5FA00FE9678F9081 (void);
// 0x000006D8 UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractIndirect(UnityEngine.Light)
extern void LightmapperUtils_ExtractIndirect_mC17A833A46BAAA01B55F8BA8A5821292AB104F54 (void);
// 0x000006D9 System.Single UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractInnerCone(UnityEngine.Light)
extern void LightmapperUtils_ExtractInnerCone_mEF618AE5A5D8EB690F3BA162196859FE6F662947 (void);
// 0x000006DA UnityEngine.Color UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractColorTemperature(UnityEngine.Light)
extern void LightmapperUtils_ExtractColorTemperature_m5052DE4DC7630A7077EA2B8C6AA903257C95AFA5 (void);
// 0x000006DB System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ApplyColorTemperature(UnityEngine.Color,UnityEngine.Experimental.GlobalIllumination.LinearColor&)
extern void LightmapperUtils_ApplyColorTemperature_mA49FB616EB2F9F31AF4CCB4C964592005DCEA346 (void);
// 0x000006DC System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.DirectionalLight&)
extern void LightmapperUtils_Extract_mCBEC26CC920C0D87DF6E25417533923E26CB6DFC (void);
// 0x000006DD System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.PointLight&)
extern void LightmapperUtils_Extract_mB11D8F3B35F96E176A89517A25CD1A54DCBD7D6E (void);
// 0x000006DE System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.SpotLight&)
extern void LightmapperUtils_Extract_mB1572C38F682F043180745B7A231FBE07CD205E4 (void);
// 0x000006DF System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.RectangleLight&)
extern void LightmapperUtils_Extract_mEABF77895D51E1CA5FD380682539C3DD3FC8A91C (void);
// 0x000006E0 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.DiscLight&)
extern void LightmapperUtils_Extract_m93B350DDA0CB5B624054835BAF46C177472E9212 (void);
// 0x000006E1 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightmapperUtils_Extract_mACAC5E823E243A53EDD2A01CF857DD16C3FDBE2A (void);
// 0x000006E2 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::SetDelegate(UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate)
extern void Lightmapping_SetDelegate_m3C7B041BEEBD50C1EF3C0D9D5BC2162E93E19979 (void);
// 0x000006E3 UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate UnityEngine.Experimental.GlobalIllumination.Lightmapping::GetDelegate()
extern void Lightmapping_GetDelegate_mD44EBB65CFD4038D77119A3F896B55905DF9A9DC (void);
// 0x000006E4 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::ResetDelegate()
extern void Lightmapping_ResetDelegate_m6EF8586283713477679876577D753EFDCA74A6F3 (void);
// 0x000006E5 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::RequestLights(UnityEngine.Light[],System.IntPtr,System.Int32)
extern void Lightmapping_RequestLights_m40C73984B1F2DB34C58965D1C4D321181C7E5976 (void);
// 0x000006E6 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::.cctor()
extern void Lightmapping__cctor_m94640A0363C80E0E1438E4EE650AED85AB3E576C (void);
// 0x000006E7 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::.ctor(System.Object,System.IntPtr)
extern void RequestLightsDelegate__ctor_m47823FBD9D2EE4ABA5EE8D889BBBC0783FB10A42 (void);
// 0x000006E8 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::Invoke(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>)
extern void RequestLightsDelegate_Invoke_m8BC0D55744A3FA2E75444AC72B3D3E4FB858BECB (void);
// 0x000006E9 System.IAsyncResult UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::BeginInvoke(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>,System.AsyncCallback,System.Object)
extern void RequestLightsDelegate_BeginInvoke_m75482E3D4E28205DCB4A202F5C144CB7C95622A6 (void);
// 0x000006EA System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/RequestLightsDelegate::EndInvoke(System.IAsyncResult)
extern void RequestLightsDelegate_EndInvoke_mA1FF787B6F3182ACF6157D13F9A84AFCBB66EE60 (void);
// 0x000006EB System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/<>c::.cctor()
extern void U3CU3Ec__cctor_m2A00D547FF8F6F8A03666B4737BB9A0620719987 (void);
// 0x000006EC System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/<>c::.ctor()
extern void U3CU3Ec__ctor_m8F825BEC75A119B6CDDA0985A3F7BA3DEA8AD5B2 (void);
// 0x000006ED System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping/<>c::<.cctor>b__7_0(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>)
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_m84A19BB5BB12263A1E3C52F1B351E3A24BE546C7 (void);
// 0x000006EE UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.CameraPlayable::GetHandle()
extern void CameraPlayable_GetHandle_m12A0FB549E5257C9AEBCF76B6E2DC49FE5F8B7C5 (void);
// 0x000006EF System.Boolean UnityEngine.Experimental.Playables.CameraPlayable::Equals(UnityEngine.Experimental.Playables.CameraPlayable)
extern void CameraPlayable_Equals_m82D036759943861CAB110D108F966C60216E6327 (void);
// 0x000006F0 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.MaterialEffectPlayable::GetHandle()
extern void MaterialEffectPlayable_GetHandle_mC4AA4C850DFB33EBA45EDA3D0524294EC03044FC (void);
// 0x000006F1 System.Boolean UnityEngine.Experimental.Playables.MaterialEffectPlayable::Equals(UnityEngine.Experimental.Playables.MaterialEffectPlayable)
extern void MaterialEffectPlayable_Equals_mC805BF647B60EA8517040C2C0716CFCB7F04CAFB (void);
// 0x000006F2 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.TextureMixerPlayable::GetHandle()
extern void TextureMixerPlayable_GetHandle_m44A48E52180084F5A93942EA0AFA454C9DFBFD40 (void);
// 0x000006F3 System.Boolean UnityEngine.Experimental.Playables.TextureMixerPlayable::Equals(UnityEngine.Experimental.Playables.TextureMixerPlayable)
extern void TextureMixerPlayable_Equals_m49E1B77DF4F13F35321494AC6B5330538D0A1980 (void);
// 0x000006F4 System.Boolean UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::TickRealtimeProbes()
extern void BuiltinRuntimeReflectionSystem_TickRealtimeProbes_mDE45D3D3E07AB0FF8C10791544CB29FF6D44DFA2 (void);
// 0x000006F5 System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Dispose()
extern void BuiltinRuntimeReflectionSystem_Dispose_mFF4F5CDB37BB93A28975F7BFE970040964B48F5A (void);
// 0x000006F6 System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Dispose(System.Boolean)
extern void BuiltinRuntimeReflectionSystem_Dispose_mFBDDD8FE2956E99DB34533F8C29621C3E938BD13 (void);
// 0x000006F7 System.Boolean UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::BuiltinUpdate()
extern void BuiltinRuntimeReflectionSystem_BuiltinUpdate_mEDD980F13F6200E5B89742D6868C4EF4858AE405 (void);
// 0x000006F8 UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Internal_BuiltinRuntimeReflectionSystem_New()
extern void BuiltinRuntimeReflectionSystem_Internal_BuiltinRuntimeReflectionSystem_New_mA4A701BE60FC41AD01F7AC965DACA950B4C9560C (void);
// 0x000006F9 System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::.ctor()
extern void BuiltinRuntimeReflectionSystem__ctor_m418427E040351EC086975D95B409F2C6E3E41045 (void);
// 0x000006FA System.Boolean UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem::TickRealtimeProbes()
// 0x000006FB System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::set_Internal_ScriptableRuntimeReflectionSystemSettings_system(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem)
extern void ScriptableRuntimeReflectionSystemSettings_set_Internal_ScriptableRuntimeReflectionSystemSettings_system_mE9EF71AD222FC661C616AC9687961D98946D8680 (void);
// 0x000006FC UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::get_Internal_ScriptableRuntimeReflectionSystemSettings_instance()
extern void ScriptableRuntimeReflectionSystemSettings_get_Internal_ScriptableRuntimeReflectionSystemSettings_instance_mC8110BFC8188AAFC7D4EC56780617AB33CB2D71C (void);
// 0x000006FD System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::ScriptingDirtyReflectionSystemInstance()
extern void ScriptableRuntimeReflectionSystemSettings_ScriptingDirtyReflectionSystemInstance_mE4FFB1863BE37B6E20388C15D2C48F4E01FCFEEF (void);
// 0x000006FE System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::.cctor()
extern void ScriptableRuntimeReflectionSystemSettings__cctor_m24D01EC03C21F2E3A40CC9C0DC4A646C8690096A (void);
// 0x000006FF UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::get_implementation()
extern void ScriptableRuntimeReflectionSystemWrapper_get_implementation_mD0D0BB589A80E0B0C53491CC916EE406378649D6 (void);
// 0x00000700 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::set_implementation(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem)
extern void ScriptableRuntimeReflectionSystemWrapper_set_implementation_m95A62C63F5D1D50EDCD5351D74F73EEBC0A239B1 (void);
// 0x00000701 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes(System.Boolean&)
extern void ScriptableRuntimeReflectionSystemWrapper_Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_mC04DACDD9BF402C3D12DE78F286A36B3A81BA547 (void);
// 0x00000702 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::.ctor()
extern void ScriptableRuntimeReflectionSystemWrapper__ctor_m14586B1A430F0316A379C966D52BDE6410BA5FCC (void);
// 0x00000703 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.TextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_mF9AFEB31DE7E63FC76D6A99AE31A108491A9F232 (void);
// 0x00000704 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_TextureFormat(UnityEngine.TextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat_mAE09EC30C3D01C3190767E2590DC0FD9358A1C5C (void);
// 0x00000705 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.RenderTextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_mD73B7F075511D7D392D55B146711F19A76E5AF1C (void);
// 0x00000706 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_RenderTextureFormat(UnityEngine.RenderTextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_Native_RenderTextureFormat_m7F44D525B67F585D711628BC2981852A6DA7633B (void);
// 0x00000707 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void GraphicsFormatUtility_GetGraphicsFormat_m5ED879E5A23929743CD65735DEDDF3BE701946D8 (void);
// 0x00000708 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsSRGBFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void GraphicsFormatUtility_IsSRGBFormat_mDA5982709BD21EE1163A90381100F6C7C6F40F1C (void);
// 0x00000709 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsCompressedTextureFormat(UnityEngine.TextureFormat)
extern void GraphicsFormatUtility_IsCompressedTextureFormat_m740C48D113EFDF97CD6EB48308B486F68C03CF82 (void);
// 0x0000070A System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsCrunchFormat(UnityEngine.TextureFormat)
extern void GraphicsFormatUtility_IsCrunchFormat_mB31D5C0C0D337A3B00D1AED3A7E036CD52540F66 (void);
static Il2CppMethodPointer s_methodPointers[1802] = 
{
	EmbeddedAttribute__ctor_m76A0389A1F18CDCD3741B68C2506B8D6034393D7,
	IsReadOnlyAttribute__ctor_m8352BB924CC2997E36B6C75AD0CB253C7F009C3D,
	MathfInternal__cctor_mB7CF38BBE41ECBC62A4457C91FDFD0FEE8679895,
	TypeInferenceRuleAttribute__ctor_m8B31AC5D44FB102217AB0308EE71EA00379B2ECF,
	TypeInferenceRuleAttribute__ctor_mE01C01375335FB362405B8ADE483DB62E7DD7B8F,
	TypeInferenceRuleAttribute_ToString_mD1488CF490AFA2A7F88481AD5766C6E6B865ABC4,
	JobHandle_ScheduleBatchedJobs_m31A19EE8C93D6BA7F2222001596EBEF313167916,
	Il2CppEagerStaticClassConstructionAttribute__ctor_mF3929BBA8F45FF4DF7AE399C02282C7AF575C459,
	NativeLeakDetection_Initialize_m1A20F4DD5DD1EF32704F40F0B05B0DE021399936,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633,
	NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_mBB573360E0CCDC3FEBD208460D9109485687F1C8,
	NativeContainerSupportsDeallocateOnJobCompletionAttribute__ctor_mFA8893231D6CAF247A130FA9A9950672757848E6,
	NativeContainerSupportsDeferredConvertListToArray__ctor_m0C65F9A9FF8862741C56E758FB996C5708E2D975,
	WriteAccessRequiredAttribute__ctor_m832267CA67398C994C2B666B00253CD9959610B9,
	NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE,
	NULL,
	NULL,
	UnsafeUtility_Free_mA805168FF1B6728E7DF3AD1DE47400B37F3441F9,
	NULL,
	NULL,
	NULL,
	AnimationCurve_Internal_Destroy_m29AC7F49DA0754B8C7A451FE946BD9A38B76E61F,
	AnimationCurve_Internal_Create_m876905D8C13846F935CA93C0C779368519D01D0C,
	AnimationCurve_Internal_Equals_m0D37631AC99BD190E2E753012C2F24A63DD78D05,
	AnimationCurve_Finalize_m4F8AF6E43E488439AB1022E7A97FEDE777655375,
	AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0,
	AnimationCurve__ctor_m68D6F819242C539EC522FEAFFEB6F1579767043E,
	AnimationCurve_Equals_mE1B90C79209D2E006B96751B48A2F0BA6F60A5B8,
	AnimationCurve_Equals_mFB50636B9BE34BBD83BE401186BC1EB7267C5416,
	AnimationCurve_GetHashCode_mCF18923053E945F39386CE8F1FD149D4020BC4DD,
	Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567,
	Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1,
	Application_get_persistentDataPath_mBD9C84D06693A9DEF2D9D2206B59D4BCF8A03463,
	Application_get_platform_mB22F7F39CDD46667C3EF64507E55BB7DA18F66C4,
	Application_CallLowMemory_m508B1899F8865EC715FE37ACB500C98B370F7329,
	Application_CallLogCallback_m42BBBDDFC6BAD182D6D574F22C0B73F3F881D681,
	Application_Internal_ApplicationWantsToQuit_mF91858E5F03D57EDCCB80F470ABBA60B0FBCC5BE,
	Application_Internal_ApplicationQuit_mDFB4E615433A0A568182698ADE0E316287EDAFE3,
	Application_Internal_ApplicationUnload_m28EA03E5E3B12350E6C4147213DE14297C50BEC7,
	Application_InvokeOnBeforeRender_mB7267D4900392FD7E09E15DF3F6CE1C15E6598F9,
	Application_InvokeFocusChanged_m1A6F45FF1CA2B03A5FB1FA864BFC37248486AC2F,
	Application_InvokeDeepLinkActivated_mCE514E3BC1F10AF34A58A40C95C4CD5ACED1396B,
	LowMemoryCallback__ctor_m91C04CE7D7A323B50CA6A73B23949639E1EA53C3,
	LowMemoryCallback_Invoke_mDF9C72A7F7CD34CC8FAED88642864AE193742437,
	LowMemoryCallback_BeginInvoke_mF446F2B2F047BC877D3819B38D1CB0AFC4C4D953,
	LowMemoryCallback_EndInvoke_mB653B34015F110168720FED3201FDD6D2B235212,
	LogCallback__ctor_mB5F165ECC180A20258EF4EFF589D88FB9F9E9C57,
	LogCallback_Invoke_m5503AB0FDB4D9D1A8FFE13283321AF278B7F6C4E,
	LogCallback_BeginInvoke_m346CFB69BAB75EF96F9EBA2B3C312A1AD1D4147F,
	LogCallback_EndInvoke_m940C8D1C936259607F04EA27DA8FBB2828FC9280,
	BootConfigData_WrapBootConfigData_mB8682F80DBE83934FFEAFE08A08B376979105E48,
	BootConfigData__ctor_m4BF11252A4EA57BE0B82E6C1657B621D163B8068,
	CachedAssetBundle__ctor_m06149D4DAC04AD0183DC3A92FDF9640C7368322A,
	CachedAssetBundle_get_name_m08907939D330FAB52F5A08EFA5BBB3849A1893C9,
	CachedAssetBundle_get_hash_mCD87C4B6DF6F8971992E1F831EE89983251FF8F6,
	Cache_get_handle_m7E6EBC767CCE0EDB513896C6686A40B9D147A8E8,
	Cache_GetHashCode_m3EBFBD645A04B9E5421615E5C465C1FEC37CACC8,
	Cache_Equals_m85EFA79FA7AA290AFE0DEBE73E556C9EE5B2BEC2,
	Cache_Equals_m60B08970A74F86BB09E97A2BEB301710F54D0555,
	Cache_get_valid_m02EF848F3AA17F671BA8764BB53EBAFF8CED3688,
	Cache_Cache_IsValid_m26554118718B8A9D5756FB58D4C26A851704C57E,
	Cache_get_path_m632CE9F465C2899566AD25CC97D1A13FA7A32946,
	Cache_Cache_GetPath_m38FC137A94265F8AB9BA0E58BABC8914ACB27AF0,
	Cache_set_maximumAvailableStorageSpace_mEC73DB6924F6B13FB9D477D5268B46620B02B7FE,
	Cache_Cache_SetMaximumDiskSpaceAvailable_mC00FD1F45B3125D3168F8FFAEEA4F8A0D9F5524E,
	Cache_set_expirationDelay_m81253C0D3CEC4A69B5CBE48B991BA083953DA741,
	Cache_Cache_SetExpirationDelay_m4100A063566C7B15290576ADC40D4D5016F7BF0A,
	Caching_set_compressionEnabled_m7E89ECB481D1DAECE39A82501AB4A5A5A0EDB64E,
	Caching_get_ready_m7D496CD9D583200F30DB19DFC3EBEF264268D4FF,
	Caching_ClearCachedVersion_m1AD4359C1A95129468CC64542E951FAC3175BCEA,
	Caching_ClearCachedVersionInternal_m5B8EB23A329CDEE71ED0FFE743B02A7F2B53E017,
	Caching_ClearAllCachedVersions_m35A0640AEBC5AAE1FB9FEA2B9E76C2A852586FDA,
	Caching_ClearCachedVersions_mE7F5A09A2BF0DDA7AC246FC762AD7F875250984E,
	Caching_GetCachedVersions_mF62C55E0522B727A653F314864CFFA4E9EBDB7EA,
	Caching_GetCachedVersions_m20C16D1CB65DBB35E9EFE342170B3879CCA3975E,
	Caching_IsVersionCached_m514BB16260B9EBFAE9C6D750ABE76FF769FC4B32,
	Caching_IsVersionCached_mF0A77B8B4B3005EC52F235A732C691D13329D1AB,
	Caching_AddCache_m989A87108B84B398D512F5D7C99207005BB2F99F,
	Caching_AddCache_m6EC90CBA189F5719A5595E2F8E64AFF83D5D0498,
	Caching_GetCacheByPath_mC2AB65747AEF97691010EBFA4718652574FBA7B6,
	Caching_get_defaultCache_m64C755289C7387B3FC70D06857F3532A35AC412C,
	Caching_get_currentCacheForWriting_mE12E3B8EAB34A075518FA6E87667E171A653CC50,
	Caching_set_currentCacheForWriting_m6D41A55DD85172B9FB9B8FB9D45273A6DFC3159D,
	Caching_ClearCachedVersionInternal_Injected_m7D15079EB04CFCCE7BA752AE018D4CE61DEDFE21,
	Caching_ClearCachedVersions_Injected_mDCC05FF246DACEE08B2CCEF81856F3A8FBEBB09A,
	Caching_IsVersionCached_Injected_m6AE45A07F96D768F231A4CA2EA75C98E7FC7C2E5,
	Caching_AddCache_Injected_m89956E55DB72273E0B13421A1F717DAB676A70B3,
	Caching_GetCacheByPath_Injected_m9611AE7A21A306B0AB340D9F5DA99732230981B6,
	Caching_get_defaultCache_Injected_m19E633CDB3480DE15C5A88C69F53E9BCB7EB42F6,
	Caching_get_currentCacheForWriting_Injected_mA4F2CA8DBC1D03EAD1B999539885DC8407B9274C,
	Caching_set_currentCacheForWriting_Injected_m9AF122C6BBE639EC2AFDE6B8DC4967EE81F8D01F,
	Camera__ctor_m30D37AB91648C862FCB8E69805DC4DF728A2804C,
	Camera_FireOnPreCull_mAA81BB789FC8C7516B71ED4E1452C4D8E99334C9,
	Camera_FireOnPreRender_mB282AD49DFA0C9036D92BAE0E28F2567C713099E,
	Camera_FireOnPostRender_mC07CDF605EEABC89BD814E09F2542E9EE16AE0DA,
	CameraCallback__ctor_m6E26A220911F56F3E8936DDD217ED76A15B1915E,
	CameraCallback_Invoke_m52ACC6D0C6BA5A247A24DB9C63D4340B2EF97B24,
	CameraCallback_BeginInvoke_m7DD0D3DB7F6ACCBFE090C1E2EEE9BBD065A0925D,
	CameraCallback_EndInvoke_m2CF9596F172FF401AAF5A606BE966E3D08F62DB9,
	CullingGroup_SendEvents_m01D14A887DFA90F3ED208073A2AE283ADF2C8B22,
	StateChanged__ctor_mBBB5FB739CB1D1206034FFDE998AE8342CC5C0C2,
	StateChanged_Invoke_m6CD13C3770E1EB709C4B125F69F7E4CE6539814D,
	StateChanged_BeginInvoke_m70DAA3720650BA61CA34446A5E19736213E82D4D,
	StateChanged_EndInvoke_m742AAC097C6A02605BF7FB6AA283CA24C021ED65,
	ReflectionProbe_CallReflectionProbeEvent_mD705BC25F93FC786FA7E2B7E1025DF35366AF31D,
	ReflectionProbe_CallSetDefaultReflection_m88965097CBA94F6B21ED05F6770A1CAF1279486C,
	DebugLogHandler_Internal_Log_mA1D09B6E813ABFAB6358863B6C2D28E3ACA9E101,
	DebugLogHandler_Internal_LogException_mD0D1F120433EB1009D23E64F3A221FA234718BFF,
	DebugLogHandler_LogFormat_mB876FBE8959FC3D9E9950527A82936F779F7A00C,
	DebugLogHandler_LogException_m131BB407038398CCADB197F19BB4AA2435627386,
	DebugLogHandler__ctor_mA13DBA2C9834013BEC67A4DF3E414F0E970D47C8,
	Debug_get_unityLogger_m70D38067C3055104F6C8D050AB7CE0FDFD05EE22,
	Debug_ExtractStackTraceNoAlloc_mDCD471993A7DDD1C268C960233A14632250E55A0,
	Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8,
	Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6,
	Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485,
	Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199,
	Debug_LogErrorFormat_mDBF43684A22EAAB187285C9B4174C9555DB11E83,
	Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA,
	Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7,
	Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8,
	Debug_LogWarningFormat_m405E9C0A631F815816F349D7591DD545932FF774,
	Debug_CallOverridenDebugHandler_mDE23EE8FA741C9568B37F4128B0DA7976F8612DC,
	Debug__cctor_m8FC005AAA0C78F065A27FD1E48B12C5C5E38A486,
	LightingSettings_LightingSettingsDontStripMe_m6503DED1969BC7777F8CE2AA1CBBC7ACB229DDA8,
	Bounds_GetHashCode_m822D1DF4F57180495A4322AADD3FD173C6FC3A1B,
	Bounds_Equals_mB759250EA283B06481746E9A247B024D273408C9,
	Bounds_Equals_mC558BE6FE3614F7B42F5E22D1F155194A0E3DD0F,
	Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485,
	Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02,
	Bounds_ToString_mC2F42E6634E786CC9A1B847ECDD88226B7880E7B,
	Bounds_ToString_m493BA40092851F60E7CE73A8BD58489DAE18B3AD,
	Plane_ToString_mD0E5921B1DFC4E83443BA7267E1182ACDD65C5DD,
	Plane_ToString_m0AF5EF6354605B6F6698346081FBC1A8BE138C4B,
	Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70,
	Rect_get_zero_m4F738804E40698120CC691AB45A6416C4FF52589,
	Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987,
	Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74,
	Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF,
	Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A,
	Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5,
	Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3,
	Rect_GetHashCode_mE5841C54528B29D5E85173AF1973326793AF9311,
	Rect_Equals_mF1F747B913CDB083ED803F5DD21E2005736AE4AB,
	Rect_Equals_mC9EE5E0C234DB174AC16E653ED8D32BB4D356BB8,
	Rect_ToString_mCB7EA3D9B51213304AB0175B2D5E4545AFBCF483,
	Rect_ToString_m3DFE65344E06224C23BB7500D069F908D5DDE8F5,
	BeforeRenderHelper_Invoke_m30EA54023BDAB65766E9B5FD6FC90E92D75A7026,
	BeforeRenderHelper__cctor_mAB141E73B12E9FD55D3258F715472F6BA0872282,
	CustomRenderTextureManager_InvokeOnTextureLoaded_Internal_m90E35A5B7DE448D0E6549F1BBEAC5BFF39916044,
	CustomRenderTextureManager_InvokeOnTextureUnloaded_Internal_m88D538AFAE378040C192206E5009EAE9D5856F6B,
	Display__ctor_m12A59C1FBFC6F4BAFCB7ADDACB5BE4E6F61145F0,
	Display__ctor_m3FB487510CB9197672FAE63EFF6FD0FEAA542B4F,
	Display_RecreateDisplayList_m86C6D207FEF8B77696B74ECF530002E260B1591E,
	Display_FireDisplaysUpdated_mA8B70C50D286065B80D47D6D12D195A2FFB223DA,
	Display__cctor_m87EA9DCECCE11A1F161F7C451A5018180DE9EB06,
	DisplaysUpdatedDelegate__ctor_mE01841FD1E938AA63EF9D1153CB40E44543A78BE,
	DisplaysUpdatedDelegate_Invoke_mBABCF33A27A4E0A5FBC06AECECA79FBF4293E7F9,
	DisplaysUpdatedDelegate_BeginInvoke_m42DDA570D68BFAD055E2FEBB75FBE79EFEE2752F,
	DisplaysUpdatedDelegate_EndInvoke_mBE00DC8335AF5142275DCCE3C5CEBC4ED0F60937,
	Graphics_Internal_GetMaxDrawMeshInstanceCount_m24408CC79F6E8FECC6931357C34B2A645D6FED0D,
	Graphics_get_activeTier_m0D7E2A5AE09AF1DCE3BC6C49A3753EEA53B95691,
	Graphics_set_activeTier_m8F1FCB4062F7ECCA85826B406BC6D600DC3A017D,
	Graphics__cctor_m13A73AA242ED3CEFBCC84A86F5DEEDC20ADF7DBF,
	Resolution_ToString_m0F17D03CC087E67DAB7F8F383D86A9D5C3E2587B,
	QualitySettings_get_activeColorSpace_m65BE7300D1A12D2981B492329B32673199CCE7F4,
	Renderer__ctor_m998612F51A0E95E387FC2032AB5FEE1304E346EE,
	Shader_get_globalShaderHardwareTier_m51DFDA02274094C72758474ACE757ADD90799705,
	Shader_set_globalShaderHardwareTier_mAC60FE5F3B582DC9DF6AC560FEF27A0CF53C3CA6,
	Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A,
	Shader_FindBuiltin_m2D105845CABE0AA42E9BF10C467A95E8FFB9DCB4,
	Shader_get_maximumLOD_mB63644F58B953A1AD3BD129024BEE7648A1D1489,
	Shader_set_maximumLOD_m0E42E3AC1D63AEBC9CE42A07AFC729F424365468,
	Shader_get_globalMaximumLOD_mE9622037DF5BF2CFCA3E08171B11792A167EE631,
	Shader_set_globalMaximumLOD_mF93C8C7A057FD02ACD84BD59C6203E8A8D77284E,
	Shader_get_isSupported_m958F4978B3DFAD2FD471549B2C1D8A82639EDA41,
	Shader_get_globalRenderPipeline_mA4AC2B965B1BBCB591941256C4F5291C22EAC47E,
	Shader_set_globalRenderPipeline_mAC514D76028093787553E48858949D4B659A8E70,
	Shader_EnableKeyword_m9AC58243D7FBD69DD73D13B73085E0B24E4ECFCE,
	Shader_DisableKeyword_mA6360809F3D85D7D0CAE3EE3C9A85987EC60DBCF,
	Shader_IsKeywordEnabled_m1946E475A01692DDEED4A1E917F1A45FD1C0EB5F,
	Shader_get_renderQueue_mDA06C7EE569BCD2EB09C9EFBE83C9476C7F4C06F,
	Shader_get_disableBatching_m39FF291F83E0FA0895F08875FD1CA84F5BD4FF1A,
	Shader_WarmupAllShaders_m5B8CBFD7A8455878F0FB06964EA4C2F829363C83,
	Shader_TagToID_m8780A4E444802A1B3FE348EEFADD61139D9CC221,
	Shader_IDToTag_m933C7042395938264D4C194CFDD636D429B4792E,
	Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F,
	Shader_GetDependency_m022372C07FF66AAB24B3C43E596E883A4C287D0B,
	Shader_get_passCount_m95E7C903DBADD49986B0CEAF45DB4EF6CB61E07B,
	Shader_FindPassTagValue_mA698315B6E0F175D58E7FA02C437FC2485BBA088,
	Shader_Internal_FindPassTagValue_mE3064D6A5EFCFD6D5EBC547E7BD04836FE853A4D,
	Shader_SetGlobalFloatImpl_m02A206C366CB1EC84C3A579786F64397CC07EE18,
	Shader_SetGlobalVectorImpl_m03AB40125EDA538CEDCA098CFA971C0FB5EE2167,
	Shader_SetGlobalMatrixImpl_m032A4268D8E3D3835B5C65234C8FCFD3EF040088,
	Shader_SetGlobalTextureImpl_mA0CAE25859DE59F4AA6A47929D2CB3E183EC6BF2,
	Shader_SetGlobalRenderTextureImpl_m28CEED6313D0DFFE77A70F265E10E2103673D9E7,
	Shader_SetGlobalBufferImpl_m0028959BBDEFA42C7C99D999C2348C0D65491B5D,
	Shader_SetGlobalGraphicsBufferImpl_mFF1908B1FC6AC9FF26A4356B9ED9AAE2F00A6B79,
	Shader_SetGlobalConstantBufferImpl_m7865DF846A8272BD354A72AA4F3BC23589E2D81C,
	Shader_SetGlobalConstantGraphicsBufferImpl_mC4730629551C7E2CBFF12E6B65E388967B0B0153,
	Shader_GetGlobalFloatImpl_mBB060E0B4DBAC481E179376BE44352574EB2DC13,
	Shader_GetGlobalVectorImpl_m8812553E28FC72720595B0F90CDC5787909BB6F7,
	Shader_GetGlobalMatrixImpl_m532EA839934657900AA3B556C806586BBCF27299,
	Shader_GetGlobalTextureImpl_mA596596CB80859C47D027610E87C8784842C3717,
	Shader_SetGlobalFloatArrayImpl_m73D372E2F698BF49AB2301C2BF3162E62957D434,
	Shader_SetGlobalVectorArrayImpl_mAF21E3858C87EEB4F65079C5182B8EC368D2F238,
	Shader_SetGlobalMatrixArrayImpl_m6FA85F4FD0BD4017CFCB182C80169F2B9F739134,
	Shader_GetGlobalFloatArrayImpl_m28CC07D51EC23DFDC6973D38DDB2DA91CE51ED01,
	Shader_GetGlobalVectorArrayImpl_mCD23AB6D3478750415DB5929751E9746DC3F248F,
	Shader_GetGlobalMatrixArrayImpl_m2DCB61AD2CEEE7DCBC90954CBD05A7F73D44012E,
	Shader_GetGlobalFloatArrayCountImpl_m53686A3DB5CC2583AB3F8588310710A10CCC42AF,
	Shader_GetGlobalVectorArrayCountImpl_mE901291496FA664ADDB75E0D5E5F795AF9B2DC85,
	Shader_GetGlobalMatrixArrayCountImpl_m23583D31BB71BE46C4367A286D64994942DFC814,
	Shader_ExtractGlobalFloatArrayImpl_m338DCC73DD8053E80A81428F835C5A3CC0C902B8,
	Shader_ExtractGlobalVectorArrayImpl_m75E222BC38D7FC4BDBB2A2C71AC32F607F3888FE,
	Shader_ExtractGlobalMatrixArrayImpl_mEDAA95D17FAADE89471FDD21070AD1B1C00ABC00,
	Shader_SetGlobalFloatArray_m14C1625175930F89C5708A70CC5A0658E019B389,
	Shader_SetGlobalVectorArray_m47EAA60D5E7BF3FA3C74CDDBBF0308F160F64993,
	Shader_SetGlobalMatrixArray_m4960680B6D4B7547B0F475DD8823934F0722B414,
	Shader_ExtractGlobalFloatArray_m3AC1D6144DEE97E7CF80388CFFF94FC38178F821,
	Shader_ExtractGlobalVectorArray_mC3A9058EEB85D27D96F68252593688BE44D3C4BC,
	Shader_ExtractGlobalMatrixArray_mF3BED25B2724A361AE018BDAF751E40A2B101A73,
	Shader_SetGlobalFloat_mD653B388A2C2E91C323FDA07C9D9FA295738A420,
	Shader_SetGlobalFloat_mF1EAD6190DF2F8CFC3F1B660FE4878DC4EF6C509,
	Shader_SetGlobalInt_mF4533D79C99D03B3D14C1DE84D074938C1403B26,
	Shader_SetGlobalInt_mF80ED20B4BEF02CE4222A4EA2BE653FDE24EF72E,
	Shader_SetGlobalVector_m0887A74FF8E1A2586B2D9ABEC6D6A06245923C7C,
	Shader_SetGlobalVector_m241FC10C437094156CA0C6CC299A3F09404CE1F3,
	Shader_SetGlobalColor_m7F33ABD41EE010CA6B805F100FA02992381A8C7F,
	Shader_SetGlobalColor_mC317375EA15179FE45810070AE7EE354C664C406,
	Shader_SetGlobalMatrix_m8BB791924D9A2333F71E7F0FDB432E622FEAECA1,
	Shader_SetGlobalMatrix_mD35BB59CA44958430C76208064B372484ED349F3,
	Shader_SetGlobalTexture_m0566B97B8028F374E445F7CC59DBFDD74911FCA4,
	Shader_SetGlobalTexture_m0C81952FBBF80467F736DA58434E4C2CEF757792,
	Shader_SetGlobalTexture_m8F8FF0AFDA3ADC15D87FA1456ACBB593ED75B4F9,
	Shader_SetGlobalTexture_m266FB7CA0E7D7A20AFB9209050A7CAF7821C2509,
	Shader_SetGlobalBuffer_mC937FC917A146B62DCA7FE6B9C4232A64BA80FD6,
	Shader_SetGlobalBuffer_m14EBB9B2872D98B1AD440895A340F9351AB11EDE,
	Shader_SetGlobalBuffer_m449B84AAFAB18C13362EE30974155F36A46FD81C,
	Shader_SetGlobalBuffer_m29F697F11D494122BC1CBE1F4EA998BF1A4F617B,
	Shader_SetGlobalConstantBuffer_mD19D72D2513ED491D63306AD081B461A5FCA2B88,
	Shader_SetGlobalConstantBuffer_mC945B54A8E90FDC5DA4AD72A6B5940AD88C4949F,
	Shader_SetGlobalConstantBuffer_mAF9345BDBD6C7B45B94E107E4CF7686991D3A9C6,
	Shader_SetGlobalConstantBuffer_m1D325F05855A9236349E32638239EDCFC27C95FF,
	Shader_SetGlobalFloatArray_m40F5C11C676CAC5D6056D09E02C303E9A824E59C,
	Shader_SetGlobalFloatArray_m94EE850560B6A8684244873F0006D4D0F8CC2989,
	Shader_SetGlobalFloatArray_m76EB422D7F94F76527B33D5D3C5B57B9A910DB8D,
	Shader_SetGlobalFloatArray_m5272EC92BDD84B6EE7B13963442EC84226B7AE9A,
	Shader_SetGlobalVectorArray_m5129922FF216B7501BCCF16364EAC352A5F39380,
	Shader_SetGlobalVectorArray_mD410749B5698DC3A8AAE57BA1F4A4634F12D3DA7,
	Shader_SetGlobalVectorArray_m80B7A069CC4D10C5ACBDA3B203F1F6AF6D33C46D,
	Shader_SetGlobalVectorArray_m6797D46DB52D5F71E07996A3D45F01ADDA16FE44,
	Shader_SetGlobalMatrixArray_mB0C41FBE352110F6369196E7CB409A32059692E7,
	Shader_SetGlobalMatrixArray_mB176EEBDC2881BF07B857F4443D7F56FB0FEC100,
	Shader_SetGlobalMatrixArray_m70152C8276CF8EEE230A030773CA5482559B2F8F,
	Shader_SetGlobalMatrixArray_m0EBFA12BAB872464B7730520DDEE6D85BC327BF8,
	Shader_GetGlobalFloat_m0AA121ADA3DDE99A75C23F51A5E685309B9E72D0,
	Shader_GetGlobalFloat_m9225741D7AF10ADD361C414ACD00228D3196DA28,
	Shader_GetGlobalInt_mC63FCE1FD39AD33344A2ADA75DB921B073827E89,
	Shader_GetGlobalInt_mC6A5B3732F2720C3D01DCBF9A67FAFBED4B4BBED,
	Shader_GetGlobalVector_mDE9B96D96E38B80479D55E8339ED54E2C6C3BE94,
	Shader_GetGlobalVector_m2624DC4423F51AC17F8F9E75F7CD060598CB6908,
	Shader_GetGlobalColor_m0022C061FB4916CE6B8F8FC4486504DDB7A43E4C,
	Shader_GetGlobalColor_mD65E4B0DA68019B51B911A6B5AC01D41660517D2,
	Shader_GetGlobalMatrix_mC15E21E2A151DEE1BC6F4DB7C41B5A919B8D814D,
	Shader_GetGlobalMatrix_m8FFD5582126359C5DCA38B1BC637EB8EA939CB0C,
	Shader_GetGlobalTexture_m8DB772CBFD54446AE6261F6C25367EE9F95C5B15,
	Shader_GetGlobalTexture_m24F5EDA116DB3FE5157F5947D3D20C066004457E,
	Shader_GetGlobalFloatArray_m78C27618B3C750739457CE658D7F05CB2D96625C,
	Shader_GetGlobalFloatArray_m1C73FA81264519BE3CCB333D6A1C7C3039E0E0C7,
	Shader_GetGlobalVectorArray_mFF6239B9B61237B31A5B54DF15ECAFDBF0CAF104,
	Shader_GetGlobalVectorArray_mDE4D944AD3F79D65B7F44C19261A9834585BA678,
	Shader_GetGlobalMatrixArray_m77D1E93AE4B29BCBE46AEF8DBE03B9134D8FA736,
	Shader_GetGlobalMatrixArray_m7223C12BD2962AFBA9890307700814A42D859E90,
	Shader_GetGlobalFloatArray_mCE35ECA90772B1F72D0BA35D646359971ECAC004,
	Shader_GetGlobalFloatArray_m67B6A671DA5E8A24CB0C82747AA87A5445717143,
	Shader_GetGlobalVectorArray_m271076D1A5F3B87304B5A76CE04B5C75602C4346,
	Shader_GetGlobalVectorArray_m1E470B38DCFC18A6309720A38F2CF0A35CBCBB92,
	Shader_GetGlobalMatrixArray_m474385E9F1E3007A257A0A98C014E68CFFEF2996,
	Shader_GetGlobalMatrixArray_mC3B29E27923E5F7441F0CACEA706F7C0CC3A6CB1,
	Shader__ctor_m51131927C3747131EBA5F99732E86E5C9F176DC8,
	Shader_GetPropertyName_m75660E1DF89FDBC9C40ECF65494F045DB82B11CF,
	Shader_GetPropertyNameId_mBF67C831C46846EBF38942AA58E33DAD45F7EE4A,
	Shader_GetPropertyType_m71AE3BBDFAB1A334799737CAF101C0A41CBEA55D,
	Shader_GetPropertyDescription_m5D147D2E05969C3AB1FE6B5E44AB03F68FABD384,
	Shader_GetPropertyFlags_mB115BF9517C2C886524F2379C4A5E6A3A2A09DE1,
	Shader_GetPropertyAttributes_m2258B81777AF60364D3CB7F48F353657BB2F4234,
	Shader_GetPropertyDefaultValue_mF94D4F3954E6BBAA7AC3EA16CF17E04448341B37,
	Shader_GetPropertyTextureDimension_mFBB9D809218B66AD772C35357813A14770F1CEB0,
	Shader_GetPropertyTextureDefaultName_m09C03C4CDBFEC5B200EBE51B1B2C3F283C07605A,
	Shader_FindTextureStackImpl_mC0188CAF11C08FA1EFC5DAAA72428D6334FEDF8F,
	Shader_CheckPropertyIndex_mD0FC0B59CFE24E7E5A914DC2FE3F463D27D13698,
	Shader_GetPropertyCount_m79ADA1FD02641FB2703FB95FED05C8F139DA5F11,
	Shader_FindPropertyIndex_mE54DB73372576479B3B4EF3101548E44B6AC4A43,
	Shader_GetPropertyName_m49B75159E345F44CF82960CB5D4C8DC0B8D18421,
	Shader_GetPropertyNameId_m6AEE0191B0F5A01937646B15B264BD7BF7F70D08,
	Shader_GetPropertyType_mBAB71A122727D5239C66985F79779C0BDDD35F68,
	Shader_GetPropertyDescription_mDA1164DC3F88F87846A3C5F615E57504648DEE53,
	Shader_GetPropertyFlags_m70751D92515F18DF9951C229E4ECBA8A75CC239F,
	Shader_GetPropertyAttributes_m23DBB49148AA204991A2E561AB30740EA6BAA0C8,
	Shader_GetPropertyDefaultFloatValue_m7FD4C492871562F546B95CC97A221B4F989485B3,
	Shader_GetPropertyDefaultVectorValue_mA39DFDC17447FEE943CEF2F256E966D449998EBC,
	Shader_GetPropertyRangeLimits_mBB7654CFDE5C3455ACE91FC3B98E981CEDA7B67F,
	Shader_GetPropertyTextureDimension_mFAE01E3C61A79F64F74C4D864F076891B2289904,
	Shader_GetPropertyTextureDefaultName_m94DC8917CBA07846233220F7D8596C6D247F9BD0,
	Shader_FindTextureStack_m3E9E4DB7BDA750EEB6CB289D956748D82EA97D16,
	Shader_SetGlobalVectorImpl_Injected_mAA9C501ED50FE158BE6961E15B63A0EB2A165D0F,
	Shader_SetGlobalMatrixImpl_Injected_m3A595B62D27E0E2A6FA33EF2475C8B3F1F1C7706,
	Shader_GetGlobalVectorImpl_Injected_m0511DCFED19B18B9CD529483C52E215538777C18,
	Shader_GetGlobalMatrixImpl_Injected_m3E0928349E8A9EE89D46505952BDF6A5358AB385,
	Shader_GetPropertyDefaultValue_Injected_m5D19714000334A9D797511A21A34F1DD5D524088,
	Material_Create_mF2507FFE60F2646914EF94F7D68B6C1D6D82C288,
	Material_CreateWithShader_mD4E25791C111800AB1E98BEAD7D45CE72B912E62,
	Material_CreateWithMaterial_mD2035B551DB7CFA1296B91C0CCC3475C5706EE41,
	Material_CreateWithString_m0C0F291654AFD2D5643A1A7826844F2416284F1F,
	Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E,
	Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245,
	Material__ctor_m71FEB02D66A71A0FF513ABC40339E1561A8192E0,
	Material_GetDefaultMaterial_mCFE73E76C23B9309DA9724F85B35DF134930DE9A,
	Material_GetDefaultParticleMaterial_m298F5EC56227A02E33A50D57B11101F0210F8BDA,
	Material_GetDefaultLineMaterial_m73EB67935F12E8B2912649E4A2F863817B8367DC,
	Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097,
	Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D,
	Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821,
	Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0,
	Material_get_mainTexture_mD1F98F8E09F68857D5408796A76A521925A04FAC,
	Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5,
	Material_get_mainTextureOffset_m515864AC74B322365689879CC668D001C41577D4,
	Material_set_mainTextureOffset_m3045530900C547D17F181858EC245CC02CA5F3FE,
	Material_get_mainTextureScale_m45E6FA2A0D95A6DD3BC8993A00A703697BA8C526,
	Material_set_mainTextureScale_m58D117C22164FBC930E153E4EEBD16B721D65048,
	Material_GetFirstPropertyNameIdByAttribute_m7192A1C1FCDB6F58A21C00571D2A67948E944CC0,
	Material_HasProperty_m699B4D99152E3A99733B8BD7D41EAE08BB8B1657,
	Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E,
	Material_get_renderQueue_m6057A6297BDA9EB2828AAD344D2C8CC82F81939C,
	Material_set_renderQueue_m239F950307B3B71DC41AF02F9030DD0A80A3A201,
	Material_get_rawRenderQueue_m27C504A8B68F778434262879034BCB8EB8AB0CB8,
	Material_EnableKeyword_mBD03896F11814C3EF67F73A414DC66D5B577171D,
	Material_DisableKeyword_mD43BE3ED8D792B7242F5487ADC074DF2A5A1BD18,
	Material_IsKeywordEnabled_m21EB58B980BA61215B281A9C18BC861BF6CF126E,
	Material_get_globalIlluminationFlags_m79774B060B292AE18FA2E16DD393D954BBB0308A,
	Material_set_globalIlluminationFlags_mB0BC9D95E532E707756DAFF6943B08E88D99C116,
	Material_get_doubleSidedGI_m1FEC566DADB34F040D7EA539155F642F680E1C2D,
	Material_set_doubleSidedGI_m5349C6EB64AC4354330352DAC30DBEA0CCC45D67,
	Material_get_enableInstancing_mCF6C2CC6CD5D6A19EC3B9EC1C89F9C46ADD360A8,
	Material_set_enableInstancing_m508CAE1A82C2688E92491BBDEE4F5A00089BCD4B,
	Material_get_passCount_m8604F2400F17AC2524B95F1C4F39C785896EEE3A,
	Material_SetShaderPassEnabled_m3A46537F0188B13D53623E100089435DC2A6FFD8,
	Material_GetShaderPassEnabled_mE1094E87BF526A4D774A8101201E737A95EDC4E7,
	Material_GetPassName_m47887B0FFEE94EE1CFD5ECB60E1082806420891A,
	Material_FindPass_m67037364014DC5AA06F927CA222A61F8EBBEA134,
	Material_SetOverrideTag_m5786C05270AD48B5B0756D5BC367017965B880C4,
	Material_GetTagImpl_m33F2C64FC3861174F902B816B79EEC04884A18E0,
	Material_GetTag_m027493F0D50B211CA83694B98014F1A9D833A121,
	Material_GetTag_mEAF0285CDE4B5747603C684BB900CA34DCB8B5F9,
	Material_Lerp_m1A80EE6FD77F49F0A22DFBF2836D3E8359484CB0,
	Material_SetPass_mC888245491A21488CFF2FD64CA45E8404CB9FF9C,
	Material_CopyPropertiesFromMaterial_m5A6DE308328EAB762EF5BE3253B728C8078773CF,
	Material_GetShaderKeywords_mC4CE7855606BC17C1CD3ABD4FC90F6E184DFE88B,
	Material_SetShaderKeywords_m73BF3A5DE699A06D214972A546644D37524D53CB,
	Material_get_shaderKeywords_mDAD743C090C3CEE0B2883140B244853D71C5E9E0,
	Material_set_shaderKeywords_m9EC5EFA52BF30597B1692C623806E7167B1C7688,
	Material_ComputeCRC_mD20600C93DB99E529EA788F543DFB52FA7E8DCE8,
	Material_GetTexturePropertyNames_m0000FE197B3A0673C0DCD22799BF3A6CF81D4638,
	Material_GetTexturePropertyNameIDs_m8D5B3EC9CFD27C8C7C1488C44F84C87BA27F6952,
	Material_GetTexturePropertyNamesInternal_mF3B38415D7C48B9E447D3D284F0E987022FFCBB1,
	Material_GetTexturePropertyNameIDsInternal_m0F6934334E1D4188E7F1BF4640F4EA70EFB52398,
	Material_GetTexturePropertyNames_mD301350C6315CE2DD5B8D1107A125D14A58A9EEB,
	Material_GetTexturePropertyNameIDs_m671CE49968832D01B7260734455E157DF8BFAB74,
	Material_SetFloatImpl_m07966D17C660588628A2ACDBBE2DD5FE0F830F1E,
	Material_SetColorImpl_m0981AC6BCE99E2692B1859AC5A3A4334C5F52185,
	Material_SetMatrixImpl_mF2A4040AEEC3AC166441A54E0ED04B7222AFD5D8,
	Material_SetTextureImpl_m671ED2C28CE1AE0E0E94E3E7633ABFA75186317C,
	Material_SetRenderTextureImpl_m9ED2858414AE05755D876039939B62EDC3BF5BA1,
	Material_SetBufferImpl_m21218ADB88A54C2923032EDD633F786C44FE8FD6,
	Material_SetGraphicsBufferImpl_mD731715286F3A3A59FBF110B2238F8D2363CB819,
	Material_SetConstantBufferImpl_m8618F656C40B12768D6F628237E4CCE88D450A0D,
	Material_SetConstantGraphicsBufferImpl_m7E0C2E9EBF2D5E26E1EB1F6A8FBDEE53D02D9E3C,
	Material_GetFloatImpl_mB3186CDAD4244298ABA911133FB672182F465DB3,
	Material_GetColorImpl_m317B54F3AB71AEE4527C34202EF69DA52DC7D313,
	Material_GetMatrixImpl_m54541B942ABC7033DEE3ACB310C39D90CF79E8F5,
	Material_GetTextureImpl_mD8BBF7EC67606802B01BF2BB55FEA981DBFFC1AE,
	Material_SetFloatArrayImpl_mC037B0DA7F4DE5A28E8AE3BE098B143B654D22A1,
	Material_SetVectorArrayImpl_m769824B7E629A78EBAA8C9E6303124463C2B01C9,
	Material_SetColorArrayImpl_mCCB2C638AB0718C6DCCBA8F7152A8CBE296DAA85,
	Material_SetMatrixArrayImpl_m8BEE06794E5293903842BAABDD23FEB4A666F471,
	Material_GetFloatArrayImpl_mBDC373E2BDA6E582F2B18AD9C42D98CCECB36E24,
	Material_GetVectorArrayImpl_m6F127A9BDAD5A8533ADA82B30444823D4508410E,
	Material_GetColorArrayImpl_m45E9F9CFE8E5BF5DE68F42004F7A4D58787774B1,
	Material_GetMatrixArrayImpl_m12E9166D607B0A77AAC8006A2C4D6908C00537E2,
	Material_GetFloatArrayCountImpl_m5F9AB30ECAC4FEFB4C59969DF916BAC3915F75C2,
	Material_GetVectorArrayCountImpl_m28B9A7CBCF7C725A657136B7477D27C389153183,
	Material_GetColorArrayCountImpl_m030163B3DEE17735104F3CD3F9FBDB22A7C2C086,
	Material_GetMatrixArrayCountImpl_m3B7A202653C140E8FAAA7068C8458875E6E50A5B,
	Material_ExtractFloatArrayImpl_mDC8253E653E5EB5F129B21FE880CB61A46314E3E,
	Material_ExtractVectorArrayImpl_mFE7D2D0BB5704263D37DA1AF5A64EC18542821F1,
	Material_ExtractColorArrayImpl_m6D9535F710478D7238A1C865AE5C836237B4808B,
	Material_ExtractMatrixArrayImpl_m98B9F6400AB6D4719EC9937CAD32205DBE75E169,
	Material_GetTextureScaleAndOffsetImpl_mF918F1F031FD450C8444D34659AC7A5F2E18DFED,
	Material_SetTextureOffsetImpl_mEAC642283EF0DF8934BECC60878D9550D0E5632E,
	Material_SetTextureScaleImpl_m750D0AC112837AA44E17DDCA120FB608708E4CD0,
	Material_SetFloatArray_mD49C0DA02C83980DEC04C03046DC11348A9ECFCE,
	Material_SetVectorArray_m9FAC0B14179B47AD6B0B68D12B0CC8B6D6DAD1DA,
	Material_SetColorArray_m29A037788EEE97E346EF92A07FA4ED1D5C575230,
	Material_SetMatrixArray_m7A66FBD36E9C9689DF9C4E5B72A17B650BBA2B5E,
	Material_ExtractFloatArray_m720DB5DD8FEFFEC7666ABA24541395123A5F7900,
	Material_ExtractVectorArray_mFE051A1A8594CA52DA21549F562621F7DA9261E2,
	Material_ExtractColorArray_m20CAD6F3ED903A5D85783CA6DC69F95F2A360EA6,
	Material_ExtractMatrixArray_m5C2E372B7851046DDA65EFC5CEE57AD49EB4FA86,
	Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768,
	Material_SetFloat_mAC7DC962B356565CF6743E358C7A19D0322EA060,
	Material_SetInt_m15D944E498726C9BB3A60A41DAAA45000F570F87,
	Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C,
	Material_SetColor_m5CAAF4A8D7F839597B4E14588E341462EEB81698,
	Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4,
	Material_SetVector_mCB22CD5FDA6D8C7C282D7998A9244E12CC293F0D,
	Material_SetVector_m47F7F5B5B21FA28885C4E747AF1C32F40C1022CB,
	Material_SetMatrix_m4B2718A5E264BFDBAD965D8A67399746F6799DCD,
	Material_SetMatrix_m7266FA4400474D08A30181EE08E01760CCAEBA0A,
	Material_SetTexture_m04A1CD55201841F85E475992931A210229C782CF,
	Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60,
	Material_SetTexture_mF33D909426F549DFDB7375BE7EA5B73FA9A6BD65,
	Material_SetTexture_m07CFDA267BA9094E5AB39EE4392043D39251EDD6,
	Material_SetBuffer_m8E57EF9FF4D5A9C6F7C76A3C4EADC7058FFEA341,
	Material_SetBuffer_m28E2A70144AB475F0FEF65A2E978DFF263FC1473,
	Material_SetBuffer_m07A0492E3B3D908ED34491A7C9429A1EF3DA1138,
	Material_SetBuffer_m7A8E7D1E33F1A3B1D7E8C8668804BAC4365443E7,
	Material_SetConstantBuffer_m6FEDEB1C027A8DBE001BC85D8DCC306711CDC256,
	Material_SetConstantBuffer_m4D41D1A10C09709EADCA84421551194123A77AC1,
	Material_SetConstantBuffer_mDD8603DCDEC64E4495E8F0F07A37359B90CBE7C3,
	Material_SetConstantBuffer_m46F3BC2583F8A61035BA7831644CACA0A7D50984,
	Material_SetFloatArray_m931F78A84AAAEBD42CF133171C1175105CC8D3F0,
	Material_SetFloatArray_mDD7AE25B23B052A413E94CA1699E08D00E47AD34,
	Material_SetFloatArray_mC4AF9243FC2948F31AFB440571C7AEB0D2D5F731,
	Material_SetFloatArray_m1FB37CF36F58B3B0260641A3AA6583C695E399AB,
	Material_SetColorArray_mC8AD46E03A81CC7BE301A61D5AF131C1CD7A5207,
	Material_SetColorArray_mDEB1B10614F0CC19EF413E9EEA992A57E6D3AAD6,
	Material_SetColorArray_m74756B75D7E44A247F029D67D51466E6AE54FA51,
	Material_SetColorArray_mEAE2F8CD96F280516C954D3547B4CEC3BA67DDAE,
	Material_SetVectorArray_mF138E4A9E4FF67CF380401508DCEE75E6293C45F,
	Material_SetVectorArray_m2BEB5B4AEA8B7FE622E512F55155143797C2C8A9,
	Material_SetVectorArray_m5F3A82ACE7E80D282C8933168D6FD914B04A8278,
	Material_SetVectorArray_m535BD4D3AE5EE82FF1B42F1838A02D0D386F0244,
	Material_SetMatrixArray_m60BBF403BE88D8CDCDF786B706D7C820A89BEC72,
	Material_SetMatrixArray_mC27E23A24132BEF6DDD1618CAC5B688E583C408D,
	Material_SetMatrixArray_m05B4D34C41F266BA8F2EEAA92F9A67D659B12D6F,
	Material_SetMatrixArray_m743BFF0B7C4EB6B693CB94F88EFB41E270F176D2,
	Material_GetFloat_mF2F48AFBDFC1E1E72A00F614EF20B656262EB167,
	Material_GetFloat_m508B992651DD512ECB2A51336C9A4E87AED82D27,
	Material_GetInt_m5B62B127CF60B5226EC9B8930060B7F48BF6F614,
	Material_GetInt_mD8C4E5D90A29C5284A342EAA047DB9BD20817B0B,
	Material_GetColor_m5B75B83FE5821381064306ECFEEF0CC44BE66688,
	Material_GetColor_m87CBA0F1030841DE18DED76EA658006A86060EA7,
	Material_GetVector_m0E41ED876B69FCFC4B9EA715D0286EE714CD201F,
	Material_GetVector_m0F76C999BC936C571A3C20054D266DF122A85E88,
	Material_GetMatrix_m621FBE2E83EE12867FBACCB69ECCCB6739D200D0,
	Material_GetMatrix_m7FB09833D8392AF85F65631EC19A438B66739690,
	Material_GetTexture_m559F9134FDF1311F3D39B8C22A90A50A9F80A5FB,
	Material_GetTexture_m02A9C3BA6C1396C0F1AAA4C248B9A81D7ABED680,
	Material_GetFloatArray_mB3E884C57C20F72F1DA1D756C31C8DF7DA2DCD69,
	Material_GetFloatArray_mE3A98640D629EC8B62F5772C7E83236906B7D398,
	Material_GetColorArray_m4C1BBB0AFAAEC5A8D1A162AC1BE5FA3454FEDCD5,
	Material_GetColorArray_m3B7BBF75ED8C3325BC9C0FB4CD1D65E63ABD5428,
	Material_GetVectorArray_mCFDB72AB5C5B80A013207DC2FA0AA20B1739FFC2,
	Material_GetVectorArray_mA39D41D1E334A97EAA4265DAD920F728D637E663,
	Material_GetMatrixArray_mD95D21B73C67F3C84067AE7A7C1A107C1E9D1F88,
	Material_GetMatrixArray_m354193A34FFD10BD093D5C8520E351576ECB447E,
	Material_GetFloatArray_m0AA0B6E9FFAF23A2C38A82DAA7DDF3A33A0F3013,
	Material_GetFloatArray_m9564EAB70FF98ED31CE06E3B3D9D51EBC254DBC6,
	Material_GetColorArray_m5A0CF637A50A23AC5C56EE5BF70DB39E042815FF,
	Material_GetColorArray_m851186B62AA7BDDA1AC1B7D8AB4B577D26AEADC4,
	Material_GetVectorArray_mC98BCB5303C968A702110DCA6ED8FE95866A21A8,
	Material_GetVectorArray_m2627258B9DB0A91341D6B5891616CF986857854C,
	Material_GetMatrixArray_m4A5B4E6DBCF9AF629A5E211F6AD35C5AFBE0D3EB,
	Material_GetMatrixArray_m5C8D1AF77DBDBCBDC94FF1F53301C5EE412B4A90,
	Material_SetTextureOffset_m8917660179EA847BFFCC28D4DED115237388DAA5,
	Material_SetTextureOffset_mDEE0C861BD2FC8D38924087590BE8FD123195A78,
	Material_SetTextureScale_mFE5FD3E241619F11D65543CFB252EAC38B2174EB,
	Material_SetTextureScale_m9F02CF20C15805224119E8A1AE57B1B064CB72C1,
	Material_GetTextureOffset_m47FBA39C48B10DEAF3431284315C558BF642A2C6,
	Material_GetTextureOffset_m53C54C035DFCB16181F0226D9C41C9EAB2301617,
	Material_GetTextureScale_mEE8950B66B5B60BDB92D41A6902E41AA1EDDEBE7,
	Material_GetTextureScale_mE494CDECFF6B59EDB43D6000608A91A4B7289A9C,
	Material_SetColorImpl_Injected_mB1B35D7949FB31533E2DF99F5A0C5DC3B798EC39,
	Material_SetMatrixImpl_Injected_m15ABD6B185339B78F89DE924539ABB632DEEC7D8,
	Material_GetColorImpl_Injected_mFF3241EC1855296376026105A435E5B394D5AF38,
	Material_GetMatrixImpl_Injected_m54DB03F4E0FDDE2DC3B5700E0070E34F24A67AEA,
	Material_GetTextureScaleAndOffsetImpl_Injected_mA6164E2F51E4AB705E279E83F8874CB1F83AEF2E,
	Material_SetTextureOffsetImpl_Injected_mE1E0CE112196D507111A23A59B61D94302039740,
	Material_SetTextureScaleImpl_Injected_m2C59C3D2A805FA63BE7115AA869BFA2D756AFDB6,
	Light_get_type_mDBBEC33D93952330EED5B02B15865C59D5C355A0,
	Light_get_spotAngle_m7BFB3B329103477AFFBB9F9E059718AB212D5FD7,
	Light_get_color_mB587B97487FFA7F7E0415F270283E48D2D901F4B,
	Light_get_intensity_mFABC9E1EA23E954E1072233C33C2211D64262326,
	Light_get_bounceIntensity_m6B586C8D305CE352E537E4AC8E8F957E512C4D50,
	Light_get_range_m94D58A8FE80BC5B13571D9CC8EBF88336BA0F831,
	Light_get_bakingOutput_m3696BB20EFCAFCB3CB579E74A5FE00EAA0E71CB5,
	Light_get_shadows_mE77B8235C26E28A797CDDF283D167EE034226AD5,
	Light_get_cookieSize_mE1168D491F8BC5DB1BA10D6E9C3B39A46177DF2B,
	Light_get_cookie_mC164223C67736F4E027DC020685D4C6D24E5A705,
	Light_get_color_Injected_mFC80DFA291AB496FAE0BC39E82460F6653B3362D,
	Light_get_bakingOutput_Injected_m7B026203BB40826D50299070138CF8F6A3519DED,
	MeshFilter_DontStripMeshFilter_m8982FEABBD1847BE8B3E53E9DD2A15FBC7370DE2,
	MeshRenderer_DontStripMeshRenderer_m68A34690B98E3BF30C620117C323B48A31DE512F,
	Mesh_Internal_Create_m6802A5B262F48CF3D72E58C2C234EF063C2552B7,
	Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6,
	Texture__ctor_mA6FE9CC0AF05A99FADCEF0BED2FB0C95571AAF4A,
	Texture_GetDataWidth_m5EE88F5417E01649909C3E11408491DB88AA9442,
	Texture_GetDataHeight_m1DFF41FBC7542D2CDB0247CF02A0FE0ACB60FB99,
	Texture_get_width_m98E7185116DB24A73E1647878013B023FF275B98,
	Texture_set_width_m6BCD23D97A9883DE0FB34E6FF48883F6C09D9F8F,
	Texture_get_height_m3D849F551F396027D4483C9B9FFF31F8AF4533B6,
	Texture_set_height_mAC3CA245CB260972C0537919C451DBF3BA1A4554,
	Texture_get_isReadable_mF9C36F23F3632802946D4BCBA6FE3D27098407BC,
	Texture_Internal_GetActiveTextureColorSpace_m5D0FE0578B76D37F863DB9FDC8BD0608467EE59D,
	Texture_get_activeTextureColorSpace_m0553908E0813E6ABD035A2453AA073BADB5680F2,
	Texture_GetPixelDataSize_m9330921FF2160620308A8B71F7F6221562C4FF32,
	Texture_GetPixelDataOffset_m5B16383E02C286C54F04B965649E1F1994ED0CF0,
	Texture_ValidateFormat_mC3C7A3FE51CA18357ABE027958BF97D3C6675D39,
	Texture_ValidateFormat_mB721DB544C78FC025FC3D3F85AD705D54F1B52CE,
	Texture_CreateNonReadableException_m5BFE30599C50688EEDE149FB1CEF834BE1633306,
	Texture__cctor_m6474E45E076B9A176073F458843BAC631033F2B7,
	Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4,
	Texture2D_get_whiteTexture_m4ED96995BA1D42F7D2823BD9D18023CFE3C680A0,
	Texture2D_get_blackTexture_m7D668B534925CDB6AF411D4AA3B1733E9CF2D3BE,
	Texture2D_get_redTexture_m3D4D263FE5A18C72AEB01593260B853F70F4639F,
	Texture2D_get_grayTexture_m1CB755568BB0F4B9697CA1E9B0929332BBC6204E,
	Texture2D_get_linearGrayTexture_m4FF36C6207C32393FDFD305950CBD01E80284CF5,
	Texture2D_get_normalTexture_m48A6707D82774C2E2D8D6563A856A591DEB5C2F8,
	Texture2D_Compress_m9EEAE939AF8538D56FA39D03FAA5AD6DA7EC5D60,
	Texture2D_Internal_CreateImpl_m48CD1B0F76E8671515956DFA43329604E29EB7B3,
	Texture2D_Internal_Create_mEAA34D0081C646C185D322FDE203E5C6C7B05800,
	Texture2D_get_isReadable_mD31C50788F7268E65EE9DA611B6F66199AA9D109,
	Texture2D_get_vtOnly_mF3BD6A180F07B9D3940B1AF9E020BCB481A7F225,
	Texture2D_ApplyImpl_mC56607643B71223E3294F6BA352A5538FCC5915C,
	Texture2D_ResizeImpl_m484CD2126E70AA80396A6F7C1539A72CC03BA71C,
	Texture2D_SetPixelImpl_m9790950013B3DF46008381D971548B82C0378D91,
	Texture2D_GetPixelImpl_m2224B987F48D881F71083B9472DE5DD11580977B,
	Texture2D_GetPixelBilinearImpl_m688F5C550710DA1B1ECBE38C1354B0A15C89778E,
	Texture2D_ResizeWithFormatImpl_m0A2C1EDF44282998B107D541509BD196AEBF65C6,
	Texture2D_ReadPixelsImpl_m8C1FDBC0E9EA531BF107C9A60F931B15910BE260,
	Texture2D_SetPixelsImpl_m6F5B06278B956BFCCF360B135F73B19D4648AE92,
	Texture2D_LoadRawTextureDataImpl_m62693BD5CB00C42253A640095380ED8892278D5D,
	Texture2D_LoadRawTextureDataImplArray_m1466F03F81A7FE4AA81F45B7EF388969753F1D85,
	Texture2D_SetPixelDataImplArray_mE1B92CCC94D3F0D191577762409226126F09054E,
	Texture2D_SetPixelDataImpl_mD34DFA0E7B7418EDD224BC82B75A725BDF85E34C,
	Texture2D_GetWritableImageData_m9CD85D0E659C6BCF58A6559F70AE4484EA0A0562,
	Texture2D_GetRawImageDataSize_m80AED06866925594D48E624AD7B4D5ADFA733B8B,
	Texture2D_GenerateAtlasImpl_m1308D4415DA6626CCA6CE32F0B26836DBDEBFB65,
	Texture2D_get_isPreProcessed_m6FE7E0C4E9B3EF486BC4C21A0B61B3377055475F,
	Texture2D_get_streamingMipmaps_m7401E9E24E197563C6724C5962C9B244BD36416E,
	Texture2D_get_streamingMipmapsPriority_mF9D27663C787B18D2FBFA90B319E671FB42F3992,
	Texture2D_get_requestedMipmapLevel_mBF3C93384BB7D3E98D3A09281EDE442F8C84BD36,
	Texture2D_set_requestedMipmapLevel_m211C9DDC65257633E6448709A451BF56946DF341,
	Texture2D_get_minimumMipmapLevel_mA901DC7C57E160CC41A32725A49A105A64B36325,
	Texture2D_set_minimumMipmapLevel_m678DE0EDCD999BE2242F72C3A6526F3044CAE793,
	Texture2D_get_loadAllMips_m18E5EFF10409F14752954ED2AFCB39DDF5864293,
	Texture2D_set_loadAllMips_mB819857C5FD8F1E5A81A091A82BF5549D4311552,
	Texture2D_get_calculatedMipmapLevel_m514504EB2E9ECAC255653D4BDDC8310ED44922A9,
	Texture2D_get_desiredMipmapLevel_mB9F475FAA3C63FF6DA7E4C39721FA1D86A468397,
	Texture2D_get_loadingMipmapLevel_m9E40F55ACE81D3E3E82EAECA28756F3EB2AAD305,
	Texture2D_get_loadedMipmapLevel_m49320EA948DC83336BA972089811546927315404,
	Texture2D_ClearRequestedMipmapLevel_m02BA825BA17349495A3DEBAC5C7EA7DD6C6DC14C,
	Texture2D_IsRequestedMipmapLevelLoaded_m84F0314A56C7D912817C3829EE6D32C90CB44F79,
	Texture2D_ClearMinimumMipmapLevel_mA826A98BD812603F548A8571BDED401F14D4162D,
	Texture2D_UpdateExternalTexture_m73D76F5FDEDA9682032D602A09F984429B9CE1A5,
	Texture2D_SetAllPixels32_mC1C1E76040E72CAFB60D3CA3F2B95A92620F4A46,
	Texture2D_SetBlockOfPixels32_mFCEE9885A7D2E79C14F4840AF7FF03E9AACF468B,
	Texture2D_GetRawTextureData_m60C0B5EF034F31FE1824B31AC1DE71042E2ACB55,
	Texture2D_GetPixels_m63492D1225FC7E937BBF236538510E29B5866BEB,
	Texture2D_GetPixels_m677BB0ECBC49B9BAC6C3AE78A916AFF126C62CD3,
	Texture2D_GetPixels32_mA4E2C09B4077716ECEFC0162ABEB8C3A66F623FA,
	Texture2D_GetPixels32_m419F7BF2D2D374C08247BE66838148DA485A6ECA,
	Texture2D_PackTextures_m7946134DE42C42397876C7A67BA9FF755030DA1E,
	Texture2D_PackTextures_m77163B065DB65C86DD0DC579D809EB3B67784791,
	Texture2D_PackTextures_m0865B18933721A6D8D0BE3A0254974E6B2220720,
	Texture2D__ctor_m448DD469801FCAED830E35F777C4F6E91AE57DA9,
	Texture2D__ctor_mF966C7EA6C64D25F515906BC88F76A652A3BE792,
	Texture2D__ctor_m18609022B3F1C638BD088788C083BDDD3A3D7E7E,
	Texture2D__ctor_mFAD7EA85C577A1608852DD7AF8DADD29A9F99B10,
	Texture2D__ctor_mF706AD5FFC4EC2805E746C80630D1255A8867004,
	Texture2D__ctor_m1A96AA8B05C35D32FF0F487E5D105E95CEF0E8F8,
	Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3,
	Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092,
	Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4,
	Texture2D_CreateExternalTexture_m2747FE79F3AAF5959F03CD1E7C014A839673E2E6,
	Texture2D_SetPixel_m78878905E58C5DE9BCFED8D9262D025789E22F92,
	Texture2D_SetPixel_m6256A1B782B57103E45DFB9C2C35CBAA7ADEA5F8,
	Texture2D_SetPixels_m39DFC67A52779E657C4B3AFA69FC586E2DB6AB50,
	Texture2D_SetPixels_m802BA835119C0F93478BBA752BA23192013EA4F7,
	Texture2D_SetPixels_mEA2B0371D35EA2D6468DD2937D1DB95E79EAEF42,
	Texture2D_SetPixels_m5FBA81041D65F8641C3107195D390EE65467FB4F,
	Texture2D_GetPixel_m50474A401DE4CB3B567F1695546DF1D2C610A022,
	Texture2D_GetPixel_m1B466564EA6529CC4573D06B53FFE566E3699ECE,
	Texture2D_GetPixelBilinear_mE25550DD7E9FD26DA7CB1E38FFCA2101F9D3D28D,
	Texture2D_GetPixelBilinear_m66B07F81930504BB39F8B3A39F6B507EB9902AD2,
	Texture2D_LoadRawTextureData_mC98EE7575456ACE2E7C3B4CDC1CE0A34FA22B24E,
	Texture2D_LoadRawTextureData_m93A620CC97332F351305E3A93AD11CB2E0EFDAF4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660,
	Texture2D_Apply_mA7D80A8D5DBA5A9334508F23EAEFC6E9C7019CB6,
	Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0,
	Texture2D_Resize_m3B472A6ED37D683DC4162504F6DCF42E1FA2195C,
	Texture2D_Resize_m051609799A8F11F4E344C099DC81A50B69528A6B,
	Texture2D_Resize_mE55C29D20722089571EC05F5477A995F7B21D6A0,
	Texture2D_ReadPixels_m87ACCC9FDCD8FC8851AE8D3BE56A7C2CAF09C75E,
	Texture2D_ReadPixels_m4C6FE8C2798C39C20A14DAFC963C720D17F4F987,
	Texture2D_GenerateAtlas_mE880ED2481A83FB4A0685B3F79F79A176365F11E,
	Texture2D_SetPixels32_mBDD42381EB43E024214D81792B0A96D952911D4F,
	Texture2D_SetPixels32_m6C2602EBE75F9C70DBC36D0B67EA4C12638518BB,
	Texture2D_SetPixels32_m5C3B90CC4B9E104C5E278176E119945C353852D9,
	Texture2D_SetPixels32_m80C3D047272131066BA3B918855264376D2D1407,
	Texture2D_GetPixels_mDBE68956E50997CB02CB0419318E0D19493288A6,
	Texture2D_GetPixels_m702E1E59DE60A5A11197DA3F6474F9E6716D9699,
	Texture2D_SetPixelImpl_Injected_mF0EB1B200D5BB3B2BC3BB50C708901785C1E1772,
	Texture2D_GetPixelImpl_Injected_mFD898C97E14FC52C38BA6B42BE88C762F6BB2082,
	Texture2D_GetPixelBilinearImpl_Injected_m378D7A9BC9E6079B59950C664419E04FB1E894FE,
	Texture2D_ReadPixelsImpl_Injected_mEC297796C9AA8F8A08551EEDBB144C95EA3F560C,
	Cubemap_Internal_CreateImpl_m05F9BCB0CFD280E2121062B6F8DA9467789B27A2,
	Cubemap_Internal_Create_m1647AD0EFBE0E383F5CDF6ABBC51842959E61931,
	Cubemap_get_isReadable_m169779CDA9E9AF98599E60A47C665E68B3256F5D,
	Cubemap__ctor_mBC1AD85DB40124D557615D0B391CACCDF76F1579,
	Cubemap__ctor_mEC3B9661FA3DB1DFF54C7A3F4FEA41903EFE2C11,
	Cubemap__ctor_mDEB5F10F08868EEBBF7891F00EAB793F4C5498FB,
	Cubemap__ctor_mF667EBD2A38E2D5DDBD46BC80ABF2DCE97A9411B,
	Cubemap__ctor_mD760725AC038C20E54F8EC514DA075DFF97A8B56,
	Cubemap__ctor_m766D71B1731BAD5C01FAEA35A73D1980C6CAE57B,
	Cubemap__ctor_m3F121EC86FF615007F0BB6FD8162779EE7D19037,
	Cubemap_ValidateIsNotCrunched_m06F63FF9899DB5C440BF49BC9750D674EA44C537,
	Texture3D_get_isReadable_mFE7B549E8E368B00CEAB4A297106AB135FA6E962,
	Texture3D_Internal_CreateImpl_m520D8FF1C3C58769BD66FA8532BD4DE7E334A2DE,
	Texture3D_Internal_Create_mE009FC1F1A74589E29C6A2DC294B312ABA03693C,
	Texture3D__ctor_mA422DEB7F88AA34806E6AA2D91258AA093F3C3AE,
	Texture3D__ctor_m666A8D01B0E3B7773C7CDAB624D69E16331CFA36,
	Texture3D__ctor_m6DC8372EBD1146127A4CE86F3E65930ABAB6539D,
	Texture3D__ctor_m7AE9A6F7E67FE89DEA087647FB3375343D997F4C,
	Texture3D__ctor_m36323FC008295FF8B8118811676141646C3B88A3,
	Texture3D__ctor_m2B875ADAA935AC50C758ECEBA69F13172FD620FC,
	Texture3D__ctor_mF3432D49750206B70A487C865F62CDA11FE4995B,
	Texture3D_ValidateIsNotCrunched_m0B19D1B555B25C568EF9F79CE389C2F3534E3154,
	Texture2DArray_get_isReadable_m7676C4021DD3D435A3D2655A34C7A1FCCA00C042,
	Texture2DArray_Internal_CreateImpl_m5B8B806393D443E6F0CB49AB019C8E9A1C8644B1,
	Texture2DArray_Internal_Create_m5DD4264F3965FBE126FAA447C79876C22D36D39C,
	Texture2DArray__ctor_mAE2D5B259CE352E6F4F10A28FDDCE52B771672B2,
	Texture2DArray__ctor_m139056CD509EAC819F9713F6A2CAE801D49CA13F,
	Texture2DArray__ctor_mB19D8E34783F95713A23A0F06F63EF1B1613E9C5,
	Texture2DArray__ctor_mEE6D4AD1D7469894FA16627A222EDC34647F6DB3,
	Texture2DArray__ctor_m4772A79C577E6E246301F31D86FE6F150B1B68E2,
	Texture2DArray__ctor_mED6E22E57F51628D68F219E5C01FF01A265CE386,
	Texture2DArray_ValidateIsNotCrunched_m107843937B0A25BD7B22013481934C1A3FD83103,
	CubemapArray_get_isReadable_m8948D737E2D417F489BCFF3C5CA87B4D536A8F44,
	CubemapArray_Internal_CreateImpl_mC6544EE7BDDE76EC9B3B8703CB13B08497921994,
	CubemapArray_Internal_Create_mC44055052CD006718B5C1964110B692B30DE1F1F,
	CubemapArray__ctor_mAEA6A6E06CDE3F825976EFA242AAE00AE41C0247,
	CubemapArray__ctor_mB5CEC4BD06765D072E96B663B4E9E09E0DCCEE17,
	CubemapArray__ctor_mD891BB1565BECEEEDFB5F12EE3C64C34A8A93B78,
	CubemapArray__ctor_m696D238938D8A70B273DE5D05F60C8C4834506D8,
	CubemapArray__ctor_m593EF9F31E1FC6357957584ACD550B434D4C9563,
	CubemapArray__ctor_mCEB65D7A82E8C98530D970424F4B125E35A03205,
	CubemapArray_ValidateIsNotCrunched_m8B33D23010B48658FFB0D0DFBF6D91804950A32F,
	RenderTexture_get_width_m7E2915C08E3B59DE86707FAF9990A73AD62609BA,
	RenderTexture_set_width_m24E7C6AD852FA9DB089253755A450E1FC53F5CC5,
	RenderTexture_get_height_m2A29272DA6BAFE2051A228B15E3BC4AECD97473D,
	RenderTexture_set_height_m131ECC892E6EA0CEA1E656C66862A272FF6F0376,
	RenderTexture_set_graphicsFormat_mA378AAD8BD876EE96637FF0A80A2AFEA4D852FAB,
	RenderTexture_SetSRGBReadWrite_m1C0BEC5511CE36A91F44E1C40AEF2399BA347D14,
	RenderTexture_Internal_Create_m6374BF6C59B7A2307975D6D1A70C82859EF0D8F3,
	RenderTexture_SetRenderTextureDescriptor_mD39CEA1EAF6810889EDB9D5CE08A84F14706F499,
	RenderTexture_GetDescriptor_mC6D87F96283042B76AA07994AC73E8131FA65F79,
	RenderTexture_set_depth_mA57CEFEDDB45D0429FAC9532A631839F523944A3,
	RenderTexture__ctor_m41C9973A79AF4CAD32E6C8987874BB20C9C87DF7,
	RenderTexture__ctor_m96C4C4C7B41EE884420046EFE4B8EC528B10D8BD,
	RenderTexture__ctor_m26C29617F265AAA52563A260A5D2EDAAC22CA1C5,
	RenderTexture__ctor_mE4898D07FB66535165C92D4AA6E37DAC7FF57D50,
	RenderTexture__ctor_mBE300C716D0DD565F63442E58077515EC82E7BA8,
	RenderTexture__ctor_mBE459F2C0FB9B65A5201F7C646C7EC653408A3D6,
	RenderTexture__ctor_m2C35C7AD5162A6CFB7F6CF638B2DAC0DDC9282FD,
	RenderTexture__ctor_m8E4220FDA652BA3CACE60FBA59D868B921C0F533,
	RenderTexture__ctor_m5D8D36B284951F95A048C6B9ACA24FC7509564FF,
	RenderTexture__ctor_m262905210EC882BA3F8B34B322848879561240F6,
	RenderTexture_get_descriptor_mBD2530599DF6A24EB0C8F502718B862FC4BF1B9E,
	RenderTexture_set_descriptor_m3C8E31AE4644B23719A12345771D1B85EB6E5881,
	RenderTexture_ValidateRenderTextureDesc_m5D363CF342A8C617A326B982D209893F76E30404,
	RenderTexture_GetCompatibleFormat_m21C46AD608AAA27D85641330E6F273AEF566FFB7,
	RenderTexture_SetRenderTextureDescriptor_Injected_m37024A53E72A10E7F192E51100E2224AA7D0169A,
	RenderTexture_GetDescriptor_Injected_mF6F57BE0A174E81000F35E1E46A38311B661C2A3,
	RenderTextureDescriptor_get_width_m5DD56A0652453FDDB51FF030FC5ED914F83F5E31,
	RenderTextureDescriptor_set_width_m8D4BAEBB8089FD77F4DC81088ACB511F2BCA41EA,
	RenderTextureDescriptor_get_height_m661881AD8E078D6C1FD6C549207AACC2B179D201,
	RenderTextureDescriptor_set_height_m1300AF31BCDCF2E14E86A598AFDC5569B682A46D,
	RenderTextureDescriptor_get_msaaSamples_m332912610A1FF2B7C05B0BA9939D733F2E7F0646,
	RenderTextureDescriptor_set_msaaSamples_m84320452D8BF3A8DD5662F6229FE666C299B5AEF,
	RenderTextureDescriptor_get_volumeDepth_m05E4A20A05286909E65D394D0BA5F6904D653688,
	RenderTextureDescriptor_set_volumeDepth_mC4D9C6B86B6799BA752855DE5C385CC24F6E3733,
	RenderTextureDescriptor_set_mipCount_mE713137D106256F44EF3E7B7CF33D5F146874659,
	RenderTextureDescriptor_get_graphicsFormat_m9D77E42E017808FE3181673152A69CBC9A9B8B85,
	RenderTextureDescriptor_set_graphicsFormat_m946B6FE4422E8CD33EB13ADAFDB53669EBD361C4,
	RenderTextureDescriptor_get_depthBufferBits_m92A95D5A1DCA7B844B3AC81AADCDFDD37D26333C,
	RenderTextureDescriptor_set_depthBufferBits_m68BF4BF942828FF70442841A22D356E5D17BCF85,
	RenderTextureDescriptor_set_dimension_m4D3F1486F761F3C52308F00267B918BD7DB8137F,
	RenderTextureDescriptor_set_shadowSamplingMode_m92B77BB68CC465F38790F5865A7402C5DE77B8D1,
	RenderTextureDescriptor_set_vrUsage_m5E4F43CB35EF142D55AC22996B641483566A2097,
	RenderTextureDescriptor_set_memoryless_m6C34CD3938C6C92F98227E3864E665026C50BCE3,
	RenderTextureDescriptor__ctor_m320C821459C7856A088415334267C2963B270A9D,
	RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m33FD234885342E9D0C6450C90C0F2E1B6B6A1044,
	RenderTextureDescriptor__cctor_mD4015195DE93496CA03515BD76A118751F6CB88F,
	Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB,
	Hash128_CompareTo_m577A27A07658268AD07B09F4FFFF1D3216AD963B,
	Hash128_ToString_mE6E0973B9B42A6AB9BEB8ACC679291CDAD2D03AC,
	Hash128_Parse_m717F9D24F3C0537AE4CE0534A8CEC34D721C1A89,
	Hash128_Hash128ToStringImpl_mB227F7E22C9558FDDE51A8E9D839FFB708F331FE,
	Hash128_ComputeFromString_mAB2F22A4CD167B729C3A7CE56ACAC250C06FDF02,
	Hash128_Compute_m6A62DCDE72D17F89013487150F0D7B469AEDDC6A,
	Hash128_Equals_m2FEA62200ECEC6BA066924F3153C9FBA96B0E3FF,
	Hash128_Equals_mA77DD31AF975A04BF4BD9D1F5B2A143497C9F468,
	Hash128_GetHashCode_mBEB470B9988886E4EB3FDA22903EBB699D1B7EA6,
	Hash128_CompareTo_m28ACD34C28C044C2BEF2109446DAAEB53F4EC619,
	Hash128_op_Equality_m721DA198FB723CC694EE07A75766F1E173D5CD4F,
	Hash128_op_LessThan_m2BD510597CBD5898BF81AFBAB25D10AA696AFBE5,
	Hash128_op_GreaterThan_m1F45666EBE8039F9187FF344CB86D0D8D0E367BB,
	Hash128_Parse_Injected_m0DB13A1F604BB3FC73E276279E0AC33F14531FEB,
	Hash128_Hash128ToStringImpl_Injected_mDC1801343943BC014300C43AE3EC444D18A19F98,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Logger__ctor_m01E91C7EFD28E110D491C1A6F316E5DD32616DE1,
	Logger_get_logHandler_mE3531B52B745AAF6D304ED9CC5AC5D7BAF7E2024,
	Logger_set_logHandler_m07110CE12B6DC759DB4292CF11B0CC2288DA4114,
	Logger_get_logEnabled_m12171AB0161FEDC83121C7A7ABA52AA3609F2D1F,
	Logger_set_logEnabled_mE7274CE2DFF3669A88486479F7E2ED3DE208AA07,
	Logger_get_filterLogType_mCD8726167BE9731AF85A23FE65AAFAD9353AE8A4,
	Logger_set_filterLogType_m5AFFB4C91E331F17DBEF4B85232EE07F73C040A2,
	Logger_IsLogTypeAllowed_m1AB436520161E88D0A7DDEF6F955BB88BE47A278,
	Logger_GetString_mDC6359E20D3C69C29FAE80797B7CA0340506BA7B,
	Logger_Log_mBAF75BD87C8B66198F52DEFF72132C42CA369881,
	Logger_Log_mD84CAE986DDEB614141DEDBDD023F7EB2EA511E7,
	Logger_LogFormat_mD6C153D96E0A869D48B2866E4D72D76A3E7CA2EF,
	Logger_LogFormat_m7D6FBEBB9C9708A50311878D7AF5A96E6E9A11F4,
	Logger_LogException_m207DC0A45A598148B848CF37BE3A20E6C3BB10F1,
	UnityLogWriter_WriteStringToUnityLog_m7DF2A8AB78591F20C87B8947A22D2C845F207A20,
	UnityLogWriter_WriteStringToUnityLogImpl_mBD8544A13E44C89FF1BCBD8685EDB0D1E760487F,
	UnityLogWriter_Init_m84D1792BF114717225B36DD1AA45DC1201BA77FE,
	UnityLogWriter_Write_m26CB2B40367CCA97725387637F0457998DED9230,
	UnityLogWriter_Write_m256BEE6E2FB31EEFCD721BFEE676653E9CD00AD1,
	UnityLogWriter_Write_m28FD8721A9896EE519A36770139213E697C57372,
	UnityLogWriter__ctor_mB7AF0B7C8C546F210699D5F3AA23F370F1963A25,
	Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5,
	Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494,
	Color_ToString_m2C9303D88F39CDAE35C613A3C816D8982C58B5FC,
	Color_ToString_m927424DFCE95E13D26A1F8BF91FA0AB3F6C2FC02,
	Color_GetHashCode_mAF5E7EE6AFA983D3FA5E3D316E672EE1511F97CF,
	Color_Equals_m90F8A5EF85416D809F5E3C0ACCADDD4F299AD8FC,
	Color_Equals_mB531F532B5F7BE6168CFD4A6C89358C16F058D00,
	Color_RGBMultiplied_mEE82A8761F22790ECD29CD8AE13B1184441CB6C8,
	Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904,
	Color_get_maxColorComponent_mAB6964B3523DC9FDDF312F3329EB224DBFECE761,
	Color_op_Implicit_mECB4D0C812888ADAEE478E633B2ECF8F8FDB96C5,
	Color_op_Implicit_m9B1A4B721726FCDA1844A0DC505C2FF8F8C50FC0,
	Color32_ToString_m11295D5492D1FB41F25635A83B87C20058DEA256,
	Color32_ToString_m5BB9D04F00C5B22C5B295F6253C99972767102F5,
	Gradient_Init_mF271EE940AEEA629E2646BADD07DF0BFFDC5EBA1,
	Gradient_Cleanup_m0F4C6F0E90C86E8A5855170AA5B3FC6EC80DEF0C,
	Gradient_Internal_Equals_mA15F4C17B0910C9C9B0BAE3825F673C9F46B2054,
	Gradient__ctor_m4B95822B3C5187566CE4FA66E283600DCC916C5A,
	Gradient_Finalize_m2E940A5D5AE433B43D83B8E676FB9844E86F8CD0,
	Gradient_Equals_m75D0B1625C55AAAEC024A951456300FEF4546EFF,
	Gradient_Equals_m2F4EB14CAD1222F30E7DA925696DB1AF41CAF691,
	Gradient_GetHashCode_m31528AF94CBACB9F6C453FD35BCDFABB77C9AED5,
	Matrix4x4__ctor_mFDDCE13D7171353ED7BA9A9B6885212DFC9E1076,
	Matrix4x4_GetHashCode_m102B903082CD1C786C221268A19679820E365B59,
	Matrix4x4_Equals_mF6EB7A6D466F5AE1D1A872451359645D1C69843D,
	Matrix4x4_Equals_mAE7AC284A922B094E4ACCC04A1C48B247E9A7997,
	Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480,
	Matrix4x4_ToString_mF45C45AD892B0707C34BEE0C716C91C0B5FD2831,
	Matrix4x4_ToString_mC2CC8C3C358C9C982F25F633BC21105D2C3BCEFB,
	Matrix4x4__cctor_m98C56DA6312BFD0230C12C0BABB7CF6627A9CC87,
	Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636,
	Vector3_GetHashCode_m9F18401DA6025110A012F55BBB5ACABD36FA9A0A,
	Vector3_Equals_m210CB160B594355581D44D4B87CF3D3994ABFED0,
	Vector3_Equals_mA92800CD98ED6A42DD7C55C5DB22DAB4DEAA6397,
	Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6,
	Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90,
	Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50,
	Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44,
	Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0,
	Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58,
	Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A,
	Vector3_ToString_mD5085501F9A0483542E9F7B18CD09C0AB977E318,
	Vector3_ToString_m8E771CC90555B06B8BDBA5F691EC5D8D87D68414,
	Vector3__cctor_m1630C6F57B6D41EFCDFC7A10F52A4D2448BFB2E7,
	Quaternion_FromToRotation_mD0EBB9993FC7C6A45724D0365B09F11F1CEADB80,
	Quaternion_Inverse_mE2A449C7AC8A40350AAC3761AE1AFC170062CAC9,
	Quaternion_Internal_FromEulerRad_m3D0312F9F199A8ADD7463C450B24081061C884A7,
	Quaternion_Internal_ToEulerRad_m442827358B6C9EB81ADCC01F316AA2201453001E,
	Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398,
	Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1,
	Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF,
	Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421,
	Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702,
	Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0,
	Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D,
	Quaternion_Internal_MakePositive_mD0A2B4D1439FC9AC4B487C2392836C8436323857,
	Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3,
	Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3,
	Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB,
	Quaternion_GetHashCode_mFCEA4CA542544DC9BD222C66F524C2F3CFE60744,
	Quaternion_Equals_m3EDD7DBA22F59A5797B91820AE4BBA64576D240F,
	Quaternion_Equals_m02CE0D27C1DA0C037D8721750E30BB1FAF1A7DAD,
	Quaternion_ToString_mD3D4C66907C994D30D99E76063623F7000F6998E,
	Quaternion_ToString_mF10FE18AAC385F9CFE721ECD8B66F14346774F31,
	Quaternion__cctor_m580F8269E5FCCBE5C27222B87E7726823CEEC5E0,
	Quaternion_FromToRotation_Injected_mF0D47B601B2A983EF001C4BDDA1819DB1CAAC68E,
	Quaternion_Inverse_Injected_m709B75EDEB9B03431C31D5D5A100237FAB9F34D6,
	Quaternion_Internal_FromEulerRad_Injected_m5220AD64F37DB56C6DFF9DAE8B78F4E3F7185110,
	Quaternion_Internal_ToEulerRad_Injected_m4626950CCFC82D6D08FEDC4EA7F3C594C9C3DED0,
	Quaternion_AngleAxis_Injected_m651561C71005DEB6C7A0BF672B631DBF121B5B61,
	Quaternion_LookRotation_Injected_m5D096FB4F030FA09FACCB550040173DE7E3F9356,
	Mathf_GammaToLinearSpace_mD7A738810039778B4592535A1DB5767C4CAD68FB,
	Mathf_Tan_mC7E6A6883BF16BBF77F15A1A0C35AB06DDAF48DC,
	Mathf_Atan_m12592B989E7F2CF919DF4223769CC480F888ADE5,
	Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14,
	Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775,
	Mathf_Clamp_mAD0781EB7470594CD4482DD64A0D739E4E539C3C,
	Mathf__cctor_mFC5862C195961EAE43A5C7BB96E07648084C1525,
	Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E,
	Vector2_ToString_mBD48EFCDB703ACCDC29E86AEB0D4D62FBA50F840,
	Vector2_ToString_m503AFEA3F57B8529C047FF93C2E72126C5591C23,
	Vector2_GetHashCode_m9A5DD8406289F38806CC42C394E324C1C2AB3732,
	Vector2_Equals_m67A842D914AA5A4DCC076E9EA20019925E6A85A0,
	Vector2_Equals_m6E08A16717F2B9EE8B24EBA6B234A03098D5F05D,
	Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828,
	Vector2__cctor_m64DC76912D71BE91E6A3B2D15D63452E2B3AD6EC,
	Vector4_get_Item_m469B9D88498D0F7CD14B71A9512915BAA0B9B3B7,
	Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2,
	Vector4_GetHashCode_mCA7B312F8CA141F6F25BABDDF406F3D2BDD5E895,
	Vector4_Equals_m71D14F39651C3FBEDE17214455DFA727921F07AA,
	Vector4_Equals_m0919D35807550372D1748193EB31E4C5E406AE61,
	Vector4_get_zero_m9E807FEBC8B638914DF4A0BA87C0BD95A19F5200,
	Vector4_ToString_mF2D17142EBD75E91BC718B3E347F614AC45E9040,
	Vector4_ToString_m0EC6AA83CD606E3EB5BE60108A1D9AC4ECB5517A,
	Vector4__cctor_m35F167F24C48A767EAD837754896B5A5178C078A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_mE7A0F096977E9FEF4139A4D66DA05DC07C67399A,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_TrySendMessage_m9573E63723FDEBB68978AD4A14253DC648071582,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Poll_mA33C0EA45F7DFF196710206BD472896DD02BB0BF,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_mC10E40AF3DE9AC1E1DAC42BF2F4F738E42F1131E,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_UnregisterInternal_m5E7A02A5A9569D111F9197ED07D5E822357FADBF,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Initialize_mC461084E67159FF60B78A2B995A0D6C6A5F05847,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_mFBCF58F025DCD0E95E1077019F30A915436221C8,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m39E3D0780D7BA1BBBAA514AB02C8B7121E6F1607,
	PlayerConnectionInternal_IsConnected_m6E50FFCA993DB110A440A175417542F19360878C,
	PlayerConnectionInternal_Initialize_m0E2E758B321FDCBA8598FE99E453F371E8544676,
	PlayerConnectionInternal_RegisterInternal_mEA39746E226DE13CDA2AD91050A7B49BE6CEDFD6,
	PlayerConnectionInternal_UnregisterInternal_m22AB7635F9B4EE0EA1F23F61E212763375479EB0,
	PlayerConnectionInternal_SendMessage_m28075B14E3C74180BC8B54C013D22F7B0DDEFA8E,
	PlayerConnectionInternal_TrySendMessage_m654FD63C3F37CA5381FA3C956B4C6F68EFF8A202,
	PlayerConnectionInternal_PollInternal_m51A00BEDBF9EA39C42023981C0A1CA8B38116D86,
	PlayerConnectionInternal_DisconnectAll_mBE50046CA1DD3A969860F0D25B5FD3381907881D,
	PlayerConnectionInternal__ctor_m220EE8E01600348418FFBC1B83BF824C39A4441B,
	PlayerPrefs_GetString_m5709C9DC233D10A7E9AF4BCC9639E3F18FE36831,
	PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159,
	ResourceRequest_GetResult_mBA8841E95D26D1D2E8026E7228A4A2BBCFB5923C,
	ResourceRequest_get_asset_m2930BE33A19198B82461486BF40A9E00963A1CD0,
	ResourceRequest__ctor_m3E6B88D53CD7B4275A16194A6D94264BFE79D039,
	ResourcesAPIInternal_FindShaderByName_m20A7CECEF3938084974ECE7F96974F04F70518AE,
	ResourcesAPIInternal_Load_m42611D41AFAEBCDD60B948317770FB1AB299BD3F,
	ResourcesAPIInternal_LoadAll_m47ADA530A4F3474C3E0A245219AC08A7F270F109,
	ResourcesAPIInternal_LoadAsyncInternal_mEE9EDC76922268050EA729CD2DEB3F2F43437219,
	ResourcesAPIInternal_UnloadAsset_mB3CC2F26739646A6EA1C4DE8AA47BED3EAA8851C,
	ResourcesAPI_get_ActiveAPI_mA3236B01A2D59991780A82398914A19869714890,
	ResourcesAPI_get_overrideAPI_mD588ADEA9E8093DD30251CC27D053EE53F5ACAA6,
	ResourcesAPI__ctor_m2B10F95A3C78C4AF1433922F9EFAAC532874D912,
	ResourcesAPI_FindShaderByName_m39FC4C8EA2CA7BF0FB1EB88ED83E913CFC467E84,
	ResourcesAPI_Load_m2E98767AD80F67567FCC40CD2AFD6BCB6AA9B7CD,
	ResourcesAPI_LoadAll_m2B9D4A8AE6629ACA40D402146223522653518F71,
	ResourcesAPI_LoadAsync_m87E06332626E339F5FD9E7DEBCF5816E87D215B1,
	ResourcesAPI_UnloadAsset_mE8D84256E168AA7C93818B55889650A583157CC8,
	ResourcesAPI__cctor_mF299A2749244ECE9CA0B1686E6C363EE37BA8952,
	Resources_Load_m6E8E5EA02A03F3AFC8FD2D775263DBBC64BF205C,
	Resources_LoadAsync_m37D45F0C403C6CF66A7BE25596B9B57C989F1E70,
	Resources_LoadAll_m84D60782124BC4C7656F4CE88B03D45DF59F9A3C,
	Resources_UnloadAsset_mAF04A1651C8F21A39CEB5018785B52FFC6336B00,
	AsyncOperation_InternalDestroy_mB659E46A7DE3337448BACCD77F5B64927877F482,
	AsyncOperation_get_isDone_m4592F121393149E539D2107239639A049493D877,
	AsyncOperation_get_progress_m2471A0564D5C2207116737619E2CED05FBBC2D19,
	AsyncOperation_set_priority_mB731BA4B2803787F73DCB1AA82B3F28AED9A4289,
	AsyncOperation_set_allowSceneActivation_mFA2C12F4A7D138D4CED4BA72F9E97AF0BD117C33,
	AsyncOperation_Finalize_m1A989E71664DD802015D858E7A336A1158BD474C,
	AsyncOperation_InvokeCompletionEvent_m2BFBB3DD63950957EDE38AE0A8D2587B900CB8F5,
	AsyncOperation_add_completed_m44D28A82BB10C85AED56A43BB666850D2E9E59E8,
	AsyncOperation_remove_completed_m5B81CC486905074B13D3784BD0A061F2B4BCC3F3,
	AsyncOperation__ctor_mFC0E13622A23CD19A631B9ABBA506683B71A2E4A,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m9BA10D3F7DEEE7FB825187CF60338BBECA83F4B2,
	AttributeHelperEngine_GetRequiredComponents_mE25F6E1B6C8E4BB44FDE3E418DD442A12AA24063,
	AttributeHelperEngine_GetExecuteMode_mB33B8C49CC1EE05341F8ADFBF9B0EEB4894C0864,
	AttributeHelperEngine_CheckIsEditorScript_m109EDB093200EAAA9DA7203E58385429F791CA24,
	AttributeHelperEngine_GetDefaultExecutionOrderFor_m4425CE70A70DB0716D3A5BF8C51C53C6A7891131,
	NULL,
	AttributeHelperEngine__cctor_m00AE154DE9BE8D99EBFBBE005F9FC40109E8FB19,
	RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4,
	ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A,
	HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9,
	DefaultExecutionOrder_get_order_m1C25A6D0F7F67A70D1B6B99D45C5A242DF92A4D2,
	ExcludeFromPresetAttribute__ctor_mE4AF4E74678A39848AD81121936DEDDA1F89AB8B,
	Behaviour__ctor_mCACD3614226521EA607B0F3640C0FAC7EACCBCE0,
	ClassLibraryInitializer_Init_m462E6CE3B1A6017AB1ADA0ACCEACBBB0B614F564,
	Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F,
	Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B,
	Component__ctor_m0B00FA207EB3E560B78938D8AD877DB2BC1E3722,
	Coroutine__ctor_mCB658B6AD0EABDAB709A53D2B111955E06CE3C61,
	Coroutine_Finalize_m7248D49A93F72CA5E84EAD20A32ADE028905C536,
	Coroutine_ReleaseCoroutine_m2C46DD57BDB3BB7B64204EA229F4C5CDE342B18B,
	SetupCoroutine_InvokeMoveNext_m036E6EE8C2A4D2DAA957D5702F1A3CA51313F2C7,
	SetupCoroutine_InvokeMember_m953E000F2B95EA72D6B1BC2330F0C844A2C0C680,
	ExcludeFromObjectFactoryAttribute__ctor_mAF8163E246AD4F05E98775F7E0904F296770B06C,
	ExtensionOfNativeClassAttribute__ctor_mC11B09D547C215DC6E298F78CA6F05C2B28B8043,
	GameObject_CreatePrimitive_mB1E03B8D373EBECCD93444A277316A53EC7812AC,
	NULL,
	GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473,
	GameObject_GetComponentFastPath_mAC58DB8AC26576ED2A87C843A68A13C325E3C944,
	GameObject_GetComponentByName_m40C10D22A73FF4260A0D686611A58E121773B481,
	GameObject_GetComponent_mA67EB0D09F51B759C259A15A4B0B8E0E58A9B439,
	GameObject_GetComponentInChildren_m56CAFD886686C8F6025B5CDF016E8BC684A20EED,
	GameObject_GetComponentInChildren_m7D7A7C91CA8B24310C9CECD41C362C327D926B07,
	NULL,
	NULL,
	GameObject_GetComponentInParent_m5C1C3266DE9C2EE078C905787DDCD666AB157C2B,
	GameObject_GetComponentInParent_m84A8233060CF7F5043960E30EBC1FC715DDF9862,
	NULL,
	NULL,
	GameObject_GetComponentsInternal_mF491A337A167109189E2AB839584697EB2672E7D,
	GameObject_GetComponents_mC2A40439A0A5AD8C676BF863FA33CA80A7B31D36,
	NULL,
	GameObject_GetComponents_m5FC485B63D5CFE373765247B1BA87BDFCCB10619,
	NULL,
	GameObject_GetComponentsInChildren_m59898B5E01DFE194028475267F99BEF3B64459FE,
	GameObject_GetComponentsInChildren_mDDF992919403B6C61AF715176CC8E2EB5C8B1671,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObject_GetComponentsInParent_m4E353C6B98E9AEEB4514AE670F9CAC6E59CFA3B8,
	GameObject_GetComponentsInParent_m92FBACB29B3B6B982EAE3CE0D122592B3D53A314,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObject_TryGetComponent_m467E0D5A83D4238B9EEDE5DB7E280DC0CD88523B,
	GameObject_TryGetComponentInternal_m1A308917341105C8ACCF4B9A87604D871190CCB8,
	GameObject_TryGetComponentFastPath_m45DE1DF12AFA1F21F16E0249A472A5FCEE6026FD,
	GameObject_FindWithTag_mEF75D1FF1E55B338A77161FDCB68ED0A2A911DF3,
	GameObject_SendMessageUpwards_mF936DE9B0E2CE786086B9B55A78407CBDAF4BE59,
	GameObject_SendMessage_m0B456BCEAFEC3945E9350D7FFC8E3D98C8DEE94D,
	GameObject_BroadcastMessage_mCEDA3C2B8C9BC1664AE2564F2B1858FE6D7A7521,
	GameObject_AddComponentInternal_m075E64DA8B203927479674247AE2C82D369608E7,
	GameObject_Internal_AddComponentWithType_mAD63AAF65D0603B157D8CC6C27F3EC73C108ADB4,
	GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8,
	NULL,
	GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34,
	GameObject_get_layer_m9D4C23A2FD105AF9964445BF18A77E8A49012F9F,
	GameObject_set_layer_m2F946916ACB41A59C46346F5243F2BAC235A36A6,
	GameObject_get_active_mAE45BB4A1D06BE2AF8C460593FC0346A5EF8014D,
	GameObject_set_active_mC5C02354788BC2EDF19359EEAE7396BE350C2BFA,
	GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86,
	GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE,
	GameObject_get_activeInHierarchy_mA3990AC5F61BB35283188E925C2BE7F7BF67734B,
	GameObject_SetActiveRecursively_m819729B12CD529ED804FD9E51070DA8782EECB4D,
	GameObject_get_isStatic_m13AA6BF963533823FD6E78B36D4B035EA0B6B700,
	GameObject_set_isStatic_m4199CD3FECAD0351AED29C9E67220D7E8794976B,
	GameObject_get_isStaticBatchable_m0DCEB577DB5FCFADCF8DD8AB6300EE70C19977CC,
	GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33,
	GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE,
	GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001,
	GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F,
	GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186,
	GameObject_SendMessageUpwards_mAFC79A352F0A92A902A1067A9CF833CD28DA71E1,
	GameObject_SendMessageUpwards_mD62B84BF7340A8341D9F704434CE15F249F156C7,
	GameObject_SendMessageUpwards_m952AB29F8D6749176E44BD4E95B3AAADD5BB5536,
	GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C,
	GameObject_SendMessage_m9727C08D6F0A5E8F309FA9FFF389ADF8130D7BE7,
	GameObject_SendMessage_m592E9A21D51BE9E1D9E23A85750548E8CC8DB00D,
	GameObject_BroadcastMessage_m2B5D6163ABB0ED80A381A41DC84ED48CC10212AD,
	GameObject_BroadcastMessage_m181D34641FC022B195C4AB970B2AE4F5CB485CEB,
	GameObject_BroadcastMessage_m70A03FA2B67C28818B43C65870EC3D5E680B9A9E,
	GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144,
	GameObject__ctor_mACDBD7A1F25B33D006A60F67EF901B33DD3D52E9,
	GameObject__ctor_m9829583AE3BF1285861C580895202F760F3A82E8,
	GameObject_Internal_CreateGameObject_mA5BCF00F09243D45B7E9A1A421D8357610AE8633,
	GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B,
	GameObject_get_scene_m7EBF95ABB5037FEE6811928F2E83C769C53F86C2,
	GameObject_get_sceneCullingMask_m35AC32251FCD8B2E5B61B9E5CB33DC8C1047E985,
	GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6,
	GameObject_get_scene_Injected_mE08525424E75EFD26BD152B4AC37563B0D4053F2,
	ManagedStreamHelpers_ValidateLoadFromStream_mDE750EE2AF2986BB8E11941D8513AD18597F3B13,
	ManagedStreamHelpers_ManagedStreamRead_m5E2F163F844AE93A058C5B4E31A011938FAE236B,
	ManagedStreamHelpers_ManagedStreamSeek_mD7B16AF1079F3F11EE782A32F851ABD761BD2E9E,
	ManagedStreamHelpers_ManagedStreamLength_m352520A9914611ADA61F089CCA8D996F62F9857F,
	MonoBehaviour_IsInvoking_m7967B0E7FEE6F3FAD9E7565D37B009A8C43FE9A8,
	MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D,
	MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4,
	MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70,
	MonoBehaviour_CancelInvoke_mAD4E486A74AF79DC1AFA880691EF839CDDE630A9,
	MonoBehaviour_IsInvoking_m6BF9433789CE7CE12E12069DBF043D620DFB0B4F,
	MonoBehaviour_StartCoroutine_m338FBDDDEBF67D9FC1F9E5CDEE50E66726454E2E,
	MonoBehaviour_StartCoroutine_m81C113F45C28D065C9E31DD6D7566D1BF10CF851,
	MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719,
	MonoBehaviour_StartCoroutine_Auto_m78A5B32C9E51A3C64C19BB73ED4A9A9B7536677A,
	MonoBehaviour_StopCoroutine_m3AB89AE7770E06BDB33BF39104BE5C57DF90616B,
	MonoBehaviour_StopCoroutine_m5FF0476C9886FD8A3E6BA82BBE34B896CA279413,
	MonoBehaviour_StopCoroutine_m4DB2A899F9BDF8CA3264DD8C4130E767702B626B,
	MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4,
	MonoBehaviour_get_useGUILayout_m42BE255035467866EB989932C62E99D5BC347524,
	MonoBehaviour_set_useGUILayout_m2FEF8B91090540BBED30EE904B11F0FB5F7C1E27,
	MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090,
	MonoBehaviour_Internal_CancelInvokeAll_mE619220C0A18AE2657DFABDBCC54E54F53C60D83,
	MonoBehaviour_Internal_IsInvokingAll_m898D1B5B37BBFC74C98EA5F86544987A5B41C49B,
	MonoBehaviour_InvokeDelayed_mD3EE47F65885A45357A5822A82F48E17D24C62C7,
	MonoBehaviour_CancelInvoke_m6BBDCBE18EEBE2584EED4F8706DA3BC6771E2529,
	MonoBehaviour_IsInvoking_m2584CADBA55F43D3A1D9955F1E9EAA579B8FD206,
	MonoBehaviour_IsObjectMonoBehaviour_mE580D905186AD91FD74214B643B26F55D54A46AF,
	MonoBehaviour_StartCoroutineManaged_mAA34ABF1564BF65264FF972AF41CF8C0F6A6B6F4,
	MonoBehaviour_StartCoroutineManaged2_m46EFC2DA4428D3CC64BA3F1394D24351E316577D,
	MonoBehaviour_StopCoroutineManaged_m8214F615A10BC1C8C78CEE92DF921C892BE3603D,
	MonoBehaviour_StopCoroutineFromEnumeratorManaged_m17DB11886ABA10DF9749F5E1DDCCD14AD4B5E31E,
	MonoBehaviour_GetScriptClassName_m2968E4771004FC58819BF2D9391DED766D1213E3,
	MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED,
	NULL,
	NULL,
	NULL,
	NoAllocHelpers_Internal_ResizeList_m32452578286848AD58CF77E1CA6B078492F723F6,
	NoAllocHelpers_ExtractArrayFromList_m097334749C829402A9634BF025A54F3918D238DD,
	RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081,
	RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516,
	RuntimeInitializeOnLoadMethodAttribute_set_loadType_m5C045AAF89A8C1541871F7F9090B3C0A289E32C6,
	ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063,
	ScriptableObject_CreateInstance_m5371BDC0B4F60FE15914A7BB3FBE07D0ACA0A8D4,
	NULL,
	ScriptableObject_CreateScriptableObject_m9627DCBB805911280823940601D996E008021D6B,
	ScriptableObject_CreateScriptableObjectInstanceFromType_mA2EB72F4D5FC5643D7CFFD07A29DD726CAB1B9AD,
	ScriptingUtility_IsManagedCodeWorking_m176E49DF0BCA69480A3D9360DAED8DDDB8732F68,
	StackTraceUtility_SetProjectFolder_m4CF077574CDE9A65A3546973395B4A228C1F957C,
	StackTraceUtility_ExtractStackTrace_mDD5E4AEC754FEB52C9FFA3B0675E57088B425EC6,
	StackTraceUtility_ExtractStringFromExceptionInternal_mE6192186E0D4CA0B148C602A5CDA6466EFA23D99,
	StackTraceUtility_ExtractFormattedStackTrace_m956907F6BE8EFF9BE9847275406FFBBB5FE7F093,
	StackTraceUtility__cctor_m8AFBE529BA4A1737BAF53BB4F8028E18D2D84A5F,
	UnityException__ctor_m8E5C592F5F76B6385D4EFDC2C28AA93B020279E7,
	UnityException__ctor_mB8EBFD7A68451D56285E7D51B42FBECFC8A141D8,
	UnityException__ctor_m00AA4E67E35F95220B5A1C46C9B59AF4ED0D6274,
	TextAsset_get_bytes_m5F15438DABBBAAF7434D53B6778A97A498C1940F,
	TextAsset_get_text_m89A756483BA3218E173F5D62A582070714BC1218,
	TextAsset_ToString_m55E5177185AA59560459F579E36C03CF1971EC57,
	TextAsset_DecodeString_m3CD8D6865DCE58592AE76360F4DB856A6962FE13,
	EncodingUtility__cctor_m6BADEB7670563CC438C10AF259028A7FF06AD65F,
	UnhandledExceptionHandler_RegisterUECatcher_m3A92AB19701D52AE35F525E9518DAD6763D66A93,
	U3CU3Ec__cctor_m5B3F5B8A2DD74DC42F3E777C1FDD1C880EFA95BA,
	U3CU3Ec__ctor_mB628041C94E761F86F2A26819A038D6BC59E324D,
	U3CU3Ec_U3CRegisterUECatcherU3Eb__0_0_mB2E6DD6B9C72FA3D5DB8D311DB281F272A587278,
	Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501,
	Object_GetHashCode_m7CC0B54570AA90E51ED2D2D6F6F078BEF9996538,
	Object_Equals_m42425AB7E6C5B6E2BAB15B3184B23371AB0B3435,
	Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499,
	Object_CompareBaseObjects_m412CC4C56457345D91A509C83A0B04E3AE5A0AED,
	Object_IsNativeObjectAlive_mE631050FBED7A72BC8A0394F26FF1984F546B6D4,
	Object_GetCachedPtr_m9F3F26858655E3CE7D471324271E3E32C7AEFD7A,
	Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB,
	Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781,
	Object_Instantiate_mADD3254CB5E6AB56FBB6964FE4B930F5187AB5AD,
	Object_Instantiate_m0B91F1876CDBE46242A9E5B32F9EE53FAF2BDD99,
	Object_Instantiate_m565A02EA45AEA7442E19FDC8E82695491938CB5A,
	Object_Instantiate_mA9A37F53F535C1FE2603DC002263C85337C1FE18,
	NULL,
	NULL,
	NULL,
	NULL,
	Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7,
	Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30,
	Object_DestroyImmediate_m69AA5B06236FACFF316DDFAD131B2622B397AC49,
	Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422,
	Object_FindObjectsOfType_m0AEB81CC6F1D224A6F4DCC7D553482D54FC03C5A,
	Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9,
	Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC,
	NULL,
	Object_CheckNullArgument_mFA979ED3433CACA46AC9AE0029A537B46E17D080,
	Object_FindObjectOfType_mFDEFCE84CE9C644F2B51DCC26CDAC78AC7E89B1B,
	Object_ToString_m2B1E5081A55425442324F437090FAD552931A7E9,
	Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54,
	Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90,
	Object_GetOffsetOfInstanceIDInCPlusPlusObject_m7749BCDBEBAE50378BE38DA313E2D854D77BB18C,
	Object_Internal_CloneSingle_m6C669D602DFD7BC6C47ACA19B2F4D7C853F124BB,
	Object_Internal_CloneSingleWithParent_m56C2AADC1D19A4D84AB18E8A2C0247528B8BF6FB,
	Object_Internal_InstantiateSingle_mE1865EB98332FDFEDC294AD90748D28ED272021A,
	Object_Internal_InstantiateSingleWithParent_m4702089B268A30D3A6FBE780B7E78833585054E4,
	Object_ToString_m4E499FDF54B8B2727FCCE6EB7BEB976BAB670030,
	Object_GetName_m6F0498EEECA37CD27B052F53ECDDA019129F3D7A,
	Object_SetName_mD80C3B5E390937EF4885BE21FBAC3D91A1C6EC96,
	Object__ctor_m4DCF5CDB32C2C69290894101A81F473865169279,
	Object__cctor_mD6AB403C7A4ED74F04167372E52C3C876EC422A1,
	Object_Internal_InstantiateSingle_Injected_mD475F2979EDD95363A9AC52EC1BB3E98E6D1D3C2,
	Object_Internal_InstantiateSingleWithParent_Injected_mC883CE3023EAF9F3336A534D5B698C71457ECD97,
	UnitySynchronizationContext__ctor_mF71A02778FCC672CC2FE4F329D9D6C66C96F70CF,
	UnitySynchronizationContext__ctor_m266FAB5B25698C86EC1AC37CE4BBA1FA244BB4FC,
	UnitySynchronizationContext_Send_mEDA081A69B18358B9239E2A46E570466E5FA77F7,
	UnitySynchronizationContext_Post_mE3277DB5A0DCB461E497EC7B9C13850D4E7AB076,
	UnitySynchronizationContext_CreateCopy_mBB5F2E7947732680B0715AD92187E89211AE5E61,
	UnitySynchronizationContext_Exec_mC89E49BFB922E69AAE753887480031A142016F81,
	UnitySynchronizationContext_HasPendingTasks_mBA93E72DC46200231B7ABCFB5F22FB0617A1B1EA,
	UnitySynchronizationContext_InitializeSynchronizationContext_mB581D86F8675566DC3A885C15DC5B9A7B8D42CD4,
	UnitySynchronizationContext_ExecuteTasks_m323E27C0CD442B806D966D024725D9809563E0DD,
	UnitySynchronizationContext_ExecutePendingTasks_m7FD629962A8BA72CB2F51583DC393A8FDA672DBC,
	WorkRequest__ctor_m13C7B4A89E47F4B97ED9B786DB99849DBC2B5603,
	WorkRequest_Invoke_m1C292B7297918C5F2DBE70971895FE8D5C33AA20,
	WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4,
	YieldInstruction__ctor_mD8203310B47F2C36BED3EEC00CA1944C9D941AEF,
	SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3,
	NULL,
	NULL,
	ComputeShader_FindKernel_mCA2683905A5DAB573D50389E2B24B48B18CD53D0,
	LowerResBlitTexture_LowerResBlitTextureDontStripMe_mFF261E84D36BA5652A3EAB9B45159D6E2FE23A0E,
	PreloadData_PreloadDataDontStripMe_mE542B732FBF4F1E9F01B1D1C2160C43E37E70B7A,
	SystemInfo_IsValidEnumValue_mDF4AFDCB30A42032742988AD9BC5E0E00EFA86C8,
	SystemInfo_SupportsTextureFormat_mE7DA9DC2B167CB7E9A864924C8772307F1A2F0B9,
	SystemInfo_SupportsTextureFormatNative_m1514BFE543D7EE39CEF43B429B52E2EC20AB8E75,
	SystemInfo_IsFormatSupported_m03EDA316B42377504BA47B256EB094F8A54BC75C,
	SystemInfo_GetCompatibleFormat_m02B5C5B1F3836661A92F3C83E44B66729C03B228,
	SystemInfo_GetGraphicsFormat_mE36FE85F87F085503FEAA34112E454E9F2AFEF55,
	Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258,
	Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84,
	Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0,
	Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B,
	RectTransform_SendReapplyDrivenProperties_m9139950FCE6E3C596110C5266174D8B2E56DC9CD,
	ReapplyDrivenProperties__ctor_mD584B5E4A07E3D025352EA0BAE9B10FE5C13A583,
	ReapplyDrivenProperties_Invoke_m5B39EC5205C3910AC09DCF378EAA2D8E99391636,
	ReapplyDrivenProperties_BeginInvoke_mC7625A8FDFF392D73C7828526490DCB88FD87232,
	ReapplyDrivenProperties_EndInvoke_m89A593999C130CA23515BF8A9C02DDE5B39ECF67,
	Transform__ctor_m629D1F6D054AD8FA5BD74296A23FCA93BEB76803,
	Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341,
	Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91,
	Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2,
	Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC,
	Transform_GetLocalEulerAngles_m1E0C16457DA4F17051A6D3A2959C0A7F312F9551,
	Transform_SetLocalEulerAngles_m1D33ECF91062D8AB10C8BC4ABE97B650A6B4A0B6,
	Transform_SetLocalEulerHint_m3D58FF93969AE51D324A487F922E68B2431EBC6F,
	Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F,
	Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E,
	Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545,
	Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B,
	Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE,
	Transform_set_right_m2BD2600E354493BDBFCBA5A888C4B5B970E25EE1,
	Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31,
	Transform_set_up_m3D2B71DA51EA167C367FD275B7B28241C565F127,
	Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053,
	Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D,
	Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200,
	Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4,
	Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5,
	Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C,
	Transform_get_rotationOrder_m18BA74EAEE812AD11503FFC079D81558424503D6,
	Transform_set_rotationOrder_m45763ACEC58CD42DE785C937DA33E8F97FE7EA23,
	Transform_GetRotationOrderInternal_mF2DAB087B528950969DCAD30F3096190086F504F,
	Transform_SetRotationOrderInternal_m57BAB8ECE4952655C1696A533CD8CD344519BE9F,
	Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046,
	Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A,
	Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9,
	Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13,
	Transform_get_parentInternal_m6477F21AD3A2B2F3FE2C365B1AF64BB1AFDA7B4C,
	Transform_set_parentInternal_mED1BC58DB05A14DAC354E5A4B24C872A5D69D0C3,
	Transform_GetParent_mA53F6AE810935DDED00A9FEEE1830F4EF797F73B,
	Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E,
	Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A,
	Transform_get_worldToLocalMatrix_mE22FDE24767E1DE402D3E7A1C9803379B2E8399D,
	Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC,
	Transform_SetPositionAndRotation_m33418A6BDFB6395B98D0B5733F5E522B7EEDDCDE,
	Transform_Translate_mFB58CBF3FA00BD0EE09EC67457608F62564D0DDE,
	Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0,
	Transform_Translate_m22737F202DE67AAAAC408ADE91BD44F5BAF3DD6B,
	Transform_Translate_mC9343E1E646DA8FD42BE37137ACCBB4B52093F5C,
	Transform_Translate_m3B18F08276BB86827D21F01E099B74FD6AD80800,
	Transform_Translate_m4C134F0279CAB507FA5FB3FAD6DE4E29E580628E,
	Transform_Rotate_m61816C8A09C86A5E157EA89965A9CC0510A1B378,
	Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7,
	Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357,
	Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E,
	Transform_RotateAroundInternal_mEEC98EFC71E388DB313FE62D9FAB642724A38216,
	Transform_Rotate_m12614C5FABB1F4A9A6800EE65BBFDB433D6D804D,
	Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC,
	Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE,
	Transform_LookAt_m6037828F5E8329B052D62472060274F9A3261ECA,
	Transform_LookAt_m49185D782014D16DA747C1296BEBAC3FB3CEDC1F,
	Transform_LookAt_m6BB4B39BB829A451C2F63215361D27650AA24D8C,
	Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA,
	Transform_Internal_LookAt_m1A24125A99A766EDA6059424EA3B5FA9C5E8B61B,
	Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91,
	Transform_TransformDirection_m3693D464C30F3F22893B6E784EB8DC47F484BCC0,
	Transform_InverseTransformDirection_m9EB6F7A2598FD8D6B52F0A6EBA96A3BAAF68D696,
	Transform_InverseTransformDirection_m77816405743CC6CFB7A16D472038138D83E0DE18,
	Transform_TransformVector_m7C5F87858E82A686A233D1866443ACAEA296AA2B,
	Transform_TransformVector_mF0828C42687BA71AFBBB03F39D444EA7C6FCC998,
	Transform_InverseTransformVector_mAE27324FC01E136CF80D1A414AC10BA7616024C4,
	Transform_InverseTransformVector_m8CA63DFC2E7AB3B4F2E7B98B674105DF0ECDC249,
	Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2,
	Transform_TransformPoint_m1BCC9D23A57EA333F2849AA3A5121C4746E4ADA3,
	Transform_InverseTransformPoint_m476ABC8F3F14824D7D82FE2C54CEE5A151A669B8,
	Transform_InverseTransformPoint_m2E147F9EC1658515B39932FED5F3E35F34E5737A,
	Transform_get_root_mDEB1F3B4DB26B32CEED6DFFF734F85C79C4DDA91,
	Transform_GetRoot_mAB553501306732D17B508521058BD5D22A759CF0,
	Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05,
	Transform_DetachChildren_m0800099F604AB1B59A72AC83E175B964B1EDFEF2,
	Transform_SetAsFirstSibling_mD5C02831BA6C7C3408CD491191EAF760ECB7E754,
	Transform_SetAsLastSibling_m2AF5610CFC845BB1121152BAB38A1251D210A187,
	Transform_SetSiblingIndex_mC69C3B37E6C731AA2A0B9BD787CF47AA5B8001FC,
	Transform_MoveAfterSibling_mEEBEDA3F217D93209CBB9D94C52ADEEE79388057,
	Transform_GetSiblingIndex_mEF9DF6406920F8EBCFBC87C6D0630FE3E9E3C1EE,
	Transform_FindRelativeTransformWithPath_m8B6DE13079DE11DCCDD2CA40CEC59319FD70A12D,
	Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1,
	Transform_SendTransformChangedScale_mC214E7244B412D35A119A04285556A3C751E8BAE,
	Transform_get_lossyScale_m469A16F93F135C1E4D5955C7EBDB893D1892A331,
	Transform_IsChildOf_m1783A88A490931E98F4D5E361595A518E09FD4BC,
	Transform_get_hasChanged_m59490E3CAC42DF8CB2BCDFC0ED75DB6F89432F06,
	Transform_set_hasChanged_mD1CDCAE366DB514FBECD9DAAED0F7834029E1304,
	Transform_FindChild_m0305D8CCABE0EE6D1D0D1028F64E2205963FE652,
	Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E,
	Transform_RotateAround_m44A5768C3A03673AA7FEB4AE943984DBA1D43168,
	Transform_RotateAroundLocal_m73207F5EE6432864BC42A23A679337A2CE90DB01,
	Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C,
	Transform_GetChildCount_m8EB532F6F7F7C34ACA8FAF363EAB869C202FBB59,
	Transform_get_hierarchyCapacity_m8942DA29F2D64D703BABE386CDF4F4FA9340D47E,
	Transform_set_hierarchyCapacity_mD108C0B4DD243DA144F65D14D84FF7EBE4D1839E,
	Transform_internal_getHierarchyCapacity_m4D049161668E75378CEC7FD32CF7C0FF6A961565,
	Transform_internal_setHierarchyCapacity_m733CAC49839E45E0A7C70D95CD0A7D541BBA2A55,
	Transform_get_hierarchyCount_m2C0C221FCB7749D1A0BB705EDEAA8389E87AAD7A,
	Transform_internal_getHierarchyCount_m44535E904CE785CA00BB9DFBE741F8126D7339FD,
	Transform_IsNonUniformScaleTransform_mA76F0BE4F08FD55B8C4E2351F92550232ABCA047,
	Transform_get_position_Injected_m43CE3FC8FB3C52896D709B07EB77340407800C13,
	Transform_set_position_Injected_m634DE2302555154065001583E6080CC48D58A602,
	Transform_get_localPosition_Injected_mBBD4D1AAD893D9B5DB40E9946A40E2B94E688782,
	Transform_set_localPosition_Injected_m228521F584224C612AEF8ED500AABF31C8E87E02,
	Transform_GetLocalEulerAngles_Injected_mB03E9A811E0E734EC641FE15686F30159E9C8905,
	Transform_SetLocalEulerAngles_Injected_mE07F22A2F00481E419BA148403C22ABA2812E8C5,
	Transform_SetLocalEulerHint_Injected_mC369F2D8C51558BEF82476E03EF730C8DB31635E,
	Transform_get_rotation_Injected_m1F756C98851F36F25BFBAC3401B67A4D2F176DF1,
	Transform_set_rotation_Injected_m9ACF0891D219140A329411F33858C7B0A026407F,
	Transform_get_localRotation_Injected_mCF48B92BAD51A015698EFE3973CD2F595048E74B,
	Transform_set_localRotation_Injected_m19EF26CC5E0F8331297D3FB17EFFC7FD217A9FCA,
	Transform_get_localScale_Injected_mC3D90F76FF1C9876761FBE40C5FF567213B86402,
	Transform_set_localScale_Injected_m7247850A81ED854FD10411376E0EF2C4F7C50B65,
	Transform_get_worldToLocalMatrix_Injected_m8B625E30EDAC79587E1D73943D2486385C403BB1,
	Transform_get_localToWorldMatrix_Injected_m990CE30D1A3D41A3247D4F9E73CA8B725466767B,
	Transform_SetPositionAndRotation_Injected_m935C7A0C1BC5C1BCDC7FFE8B757F58D9540485D3,
	Transform_RotateAroundInternal_Injected_m70D2B2635B5CB12008A8207829D178BF4970E9BD,
	Transform_Internal_LookAt_Injected_mE9622F74A863FA172B36D3F453BE0AA4BDEAC092,
	Transform_TransformDirection_Injected_mB27ACC95D32503153AEBE93976553C0A41BF4D2E,
	Transform_InverseTransformDirection_Injected_m74168CEED775055AA4C960F6B653DDA0B62322B5,
	Transform_TransformVector_Injected_m386F502DB70B8082041933F6C15BC1BF4AD08672,
	Transform_InverseTransformVector_Injected_mD3DECDA4FA1753469330D6B3B7D7C6AE40D93CD8,
	Transform_TransformPoint_Injected_mFCDA82BF83E47142F6115E18D515FA0D0A0E5319,
	Transform_InverseTransformPoint_Injected_mC6226F53D5631F42658A5CA83FEE16EC24670A36,
	Transform_get_lossyScale_Injected_m5D97E86A5663FF57E1977E5CA13EF00022B61648,
	Transform_RotateAround_Injected_mEDC7F96D95DACA6DD6B67F98EE11C34143A31ABB,
	Transform_RotateAroundLocal_Injected_m2F7BCD8F3886F33F9E33DFFC82387E0FC370495D,
	Enumerator__ctor_m052C22273F1D789E58A09606D5EE5E87ABC2C91B,
	Enumerator_get_Current_m1CFECBB7AC3EACD05A11CC6848AE7A94A8123E9F,
	Enumerator_MoveNext_m346F9A121D9E89ADBA8296E6A7EF8763C5B58A14,
	Enumerator_Reset_mFA289646E280C94D82CC223C024E0B615F811C8E,
	SpriteRenderer_get_shouldSupportTiling_m766D3A4A4877366C8E5F01997B92DBDF33F63AA0,
	SpriteRenderer_get_sprite_mCA028D776503304A6A59A5E7FD9F198DCF206387,
	SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A,
	SpriteRenderer_get_drawMode_m13220432BF5376A2B9B8994FAE2F80FD932E8DC4,
	SpriteRenderer_set_drawMode_mD45F452500FE27EF30AFEF295220F72B356457CC,
	SpriteRenderer_get_size_mB0C8D3133ABDB73AA1BCC468F23DD9EE0A8C97C7,
	SpriteRenderer_set_size_m92E5651D28AC04743795244ACC1E3F5FABCF1B49,
	SpriteRenderer_get_adaptiveModeThreshold_m2A8E48E04DF7480E32F42CB75ED157F9E3EF3A60,
	SpriteRenderer_set_adaptiveModeThreshold_mCA99CEBF00B71DD4E7CF8ACEF3F80310C50B4E31,
	SpriteRenderer_get_tileMode_m9DA12130B3EC79745C4A7146A82A9F9457121911,
	SpriteRenderer_set_tileMode_mCB119E57EC0C64D4EEF44554E21ED94C88248884,
	SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC,
	SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33,
	SpriteRenderer_get_maskInteraction_mE205D19E746BE0122589D92F451C9E2661D35EEF,
	SpriteRenderer_set_maskInteraction_m2AF21189C484E01A8107B58424C67DA01B89CCD0,
	SpriteRenderer_get_flipX_mCB73CAF5724B925903C9D9805D3F7B8AD0C509F5,
	SpriteRenderer_set_flipX_mD3AB83CC6D742A69F1B52376D1636CAA2E44B67E,
	SpriteRenderer_get_flipY_m2E857CA3F06D99D03996A2CB58297186DAFD19BB,
	SpriteRenderer_set_flipY_m7DFB2D1118142BE6C76470474F2ECAED234B0A01,
	SpriteRenderer_get_spriteSortPoint_m98FF21DA0A1D95935B418499F3D684D1A4775E8A,
	SpriteRenderer_set_spriteSortPoint_mDDB38BE9F712C6AA5AB52D6E7E5C9E4DF64984BF,
	SpriteRenderer_Internal_GetSpriteBounds_m1C5B4E0EAB26D42903950706BFE4846042C34C4F,
	SpriteRenderer_GetSpriteBounds_mE9B6EF374ECEC8103A5AFBDF408A40E44BDEC24D,
	SpriteRenderer__ctor_m5724D3A61E7A1DDF3EFB2A8B47A8A1348CB188D3,
	SpriteRenderer_get_size_Injected_mD2E28A7BF46C4883D66521C4008011BC9EDADCFB,
	SpriteRenderer_set_size_Injected_mB3E9D735BC04B08AA7317C23C5DCD904CBBC3E3F,
	SpriteRenderer_get_color_Injected_m24E9886D0185C54AB944E0795B1581C4B502E507,
	SpriteRenderer_set_color_Injected_m937728871B33924329697BD582ECB8F6FA3AF628,
	SpriteRenderer_Internal_GetSpriteBounds_Injected_m70E8B5485EA2FBD316315BF6E682E2DEEA101327,
	Sprite__ctor_m121D88C6A901A2A2FA602306D01FDB8D7A0206F0,
	Sprite_GetPackingMode_m398C471B7DDCCA1EA0355217EBB7568E851E597A,
	Sprite_GetPackingRotation_m06FA5511011CD5F4B55ADB2250D037CDA1FBA4C0,
	Sprite_GetPacked_m6AC29F35C9ADE1B6394202132FB77DA9249DF5AE,
	Sprite_GetTextureRect_m6E19823AEA9A3FC4C9FE76E53C30F19316F63954,
	Sprite_GetTextureRectOffset_m497818989C347690FBE0FEA2E38F64720E34FEBE,
	Sprite_GetInnerUVs_m394AF466930BBACE6F45425C418D0A8991600AD9,
	Sprite_GetOuterUVs_mEB9D18CA03A78C02CAF4FAD386A7AF009187ACDD,
	Sprite_GetPadding_mA039E911719B85FBB31F4C235B9EF9973F5E7FF3,
	Sprite_CreateSpriteWithoutTextureScripting_m73A67FF6ACACA774AE2E0A9E13199790C121269D,
	Sprite_CreateSprite_m377BDA8A0AD33EC2E51E4AE5F024D3DAD4C4D4F4,
	Sprite_get_bounds_m364F852DE78702F755D1414FF4465F61F3F238EF,
	Sprite_get_rect_m146D3624E5D8DD6DF5B1F39CE618D701B9008C70,
	Sprite_get_border_m6AEB051C1A675509BB786427883FC2EE957F60A7,
	Sprite_get_texture_mD03E68058C9F727321FE643CBDB3A469F96E49FB,
	Sprite_GetSecondaryTexture_m75C8D02A7387D4671537481529F0916B2B49F55B,
	Sprite_get_pixelsPerUnit_mEA3201EE604FB43CB93E3D309B19A5D0B44C739E,
	Sprite_get_spriteAtlasTextureScale_m0D6B206834275928FA4974B264B2D03D1E243751,
	Sprite_get_associatedAlphaSplitTexture_m212E3C39E4EE3385866E51194F5FC9AEDDEE4F00,
	Sprite_get_pivot_m39B1CFCDA5BB126D198CAEAB703EC39E763CC867,
	Sprite_get_packed_m075910C79D785DC2572B171DA93918CF2793B133,
	Sprite_get_packingMode_m1BF2656F34C1C650D1634F0AE81727074BE85E5F,
	Sprite_get_packingRotation_m1332F3DE0A67AD59EC2CF1BF773F1EF7A4CD712A,
	Sprite_get_textureRect_m5B350C2B122C85549960912CBD6343E4A5B02C35,
	Sprite_get_textureRectOffset_m093913C49C4F9195B2C3B3ED841C0693E055EB91,
	Sprite_get_vertices_m4A5EFBEDA14F12E5358C61831150AE368453F301,
	Sprite_get_triangles_mAE8C32A81703AEF45192E993E6B555AF659C5131,
	Sprite_get_uv_mBD902ADCF1FF8AE211C98881A6E3C310D73494B6,
	Sprite_GetPhysicsShapeCount_m854AF4908F18CDBFC1CC829D62D29DBA17B9EA59,
	Sprite_GetPhysicsShapePointCount_m9525B69C7B95C5506C1D0ABB20FBBFE8BE26AE0D,
	Sprite_Internal_GetPhysicsShapePointCount_mDBE3DF0F616DB9EB15B865D673D3F64A9FF03C4D,
	Sprite_GetPhysicsShape_mC4B024EC518BB44CAB6684DC5FBF2FB5DEA8753D,
	Sprite_GetPhysicsShapeImpl_m76E2FAEC9719208E23D342BD2E1A041299D32775,
	Sprite_OverridePhysicsShape_m051054FB0802170A2DC5D2B46F899401A1531345,
	Sprite_OverridePhysicsShapeCount_mE17ECF1C7F079E5CC3161C827EC39DEA37D3392A,
	Sprite_OverridePhysicsShape_m1B9F6A40847DE060A44E09272D26785241AECBA2,
	Sprite_OverrideGeometry_m7044D0D91BB8CED71236706FAC9BA692999F6825,
	Sprite_Create_m3662813A7A2061C61C50D98BC2DEF4FB20BE6F19,
	Sprite_Create_mE5A2105E146955704E81F089423DF1912680E853,
	Sprite_Create_mD5BE6F7D884D4FAAFE491551966B730ED1D49FEA,
	Sprite_Create_mD5A0E9AA50A9652F90D7591284B454F5EAC986F8,
	Sprite_Create_mD3C27EA17D550CB67E709F9C983B5768CE24D995,
	Sprite_Create_mEFBF5BB7286E8095F42995A5B9EBEC3CDAB6153F,
	Sprite_Create_mB35E2D3FF3906606000D6682E465CC4773ADBACC,
	Sprite_Create_m9817936760193300A6049A788C3446C7ADB49C6B,
	Sprite_GetTextureRect_Injected_m5D5B55E003133B5A537764AF7493BC094685F2BD,
	Sprite_GetTextureRectOffset_Injected_mAEFBF984BCCF2B6C3F9677B56EDE38360A869FEC,
	Sprite_GetInnerUVs_Injected_m6BBD450F64FCAA0EE51E16034E239267E53BADB7,
	Sprite_GetOuterUVs_Injected_m386A7B21043ED228AE4BBAB93060AFBFE19C5BD7,
	Sprite_GetPadding_Injected_m9C8743817FB7CD12F88DA90769BD653EA35273EE,
	Sprite_CreateSpriteWithoutTextureScripting_Injected_m531A7ED5E5361E8474E6C5B9842DBF0B95DE33A0,
	Sprite_CreateSprite_Injected_mD94BD3390A496B1A9A3FA6469B551B766DFEEE7D,
	Sprite_get_bounds_Injected_m4AE096B307AD9788AEDA44AF14C9605D5ABEEE1C,
	Sprite_get_rect_Injected_mE5951AA7D9D0CBBF4AF8263F8B77B8B3E203279D,
	Sprite_get_border_Injected_m7A2673F6D49E5085CA3CC2436763C7C7BE0F75C8,
	Sprite_get_pivot_Injected_mAAE0A9705B766EB97C8732BE5541E800E8090809,
	APIUpdaterRuntimeHelpers_GetMovedFromAttributeDataForType_mEDA7447F4AEBCBDE3B6C5A04ED735FA9BA2E7B52,
	APIUpdaterRuntimeHelpers_GetObsoleteTypeRedirection_mAD9DCC5AEEF51535CB9FCED2F1B38650C766D355,
	SpriteAtlasManager_RequestAtlas_m4EB540E080D8444FE4B53D8F3D44EA9C0C8C49A1,
	SpriteAtlasManager_PostRegisteredAtlas_m0F58C324E58E39D7B13803FBF7B1AE16CF6B4B7B,
	SpriteAtlasManager_Register_m48E996EAD9A5CF419B7738799EB99A78D7095C73,
	SpriteAtlasManager__cctor_mDB99D76724E2DB007B46B61C2833878B624D5021,
	SpriteAtlas_GetSprite_mBDFF27666D4C361D5A5E6F55421A658A045C8DDB,
	Profiler_GetMonoUsedSizeLong_mFE5483CFA8A430C8BC7A44EB67A4C244DFF0CE02,
	DebugScreenCapture_set_rawImageDataReference_m9FE7228E0FB4696A255B2F477B6B50C902D7786B,
	DebugScreenCapture_set_imageFormat_m46E9D97376E844826DAE5C3C69E4AAF4B7A58ECB,
	DebugScreenCapture_set_width_m6928DB2B2BCD54DE97BDA4116D69897921CCACF6,
	DebugScreenCapture_set_height_m6CD0F6872F123966B784E6F356C51986B8B19F84,
	MetaData__ctor_mD7868B95DDB386C2F8AC614F09A6760F420A44D5,
	MemoryProfiler_PrepareMetadata_m571D85DE9BEAF3D3E0ED8269AE350960717D947E,
	MemoryProfiler_WriteIntToByteArray_mEC9056AEB48E7906BAA9FDA7FF538E7341233E8E,
	MemoryProfiler_WriteStringToByteArray_m48936038ADD56D3BF498870F4DA6AB12DE0CA9FC,
	MemoryProfiler_FinalizeSnapshot_m7DE2A0E49B6457B64D53255BE00F7F1C7EA30526,
	MemoryProfiler_SaveScreenshotToDisk_m9419808BAC900EAB213522E34F6268975722495A,
	UnityEventTools_TidyAssemblyTypeName_m7ABD7C25EA8BF24C586CD9BD637421A12D8C5B44,
	ArgumentCache_get_unityObjectArgument_m546FEA2DAB93AD1222C0000A882FFAF66DF88B47,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_mAA84D93F6FE66B3422F4A99D794BCCA11882DE35,
	ArgumentCache_get_intArgument_m8BDE2E1DBF5870F5F91041540EC7F867F0D24CFF,
	ArgumentCache_get_floatArgument_m941EC215EC34675CA224A311BEDBBEF647F02150,
	ArgumentCache_get_stringArgument_mE71B434FCF1AA70BF2A922647F419BED0553E7FB,
	ArgumentCache_get_boolArgument_mB66F6E9F16B1246A82A368E8B6AB94C4E05AF127,
	ArgumentCache_OnBeforeSerialize_mF5B9D962D88C22BC20AE330FFBC33FB3042604D0,
	ArgumentCache_OnAfterDeserialize_mB50D42B36BF948499C4927084E7589D0B1D9C6FB,
	ArgumentCache__ctor_mF667890D72F5C3C3AE197688DEF71A23310248A0,
	BaseInvokableCall__ctor_mC60A356F5535F98996ADF5AF925032449DFAF2BB,
	BaseInvokableCall__ctor_mB7F553CF006225E89AFD3C57820E8FF0E777C3FB,
	NULL,
	NULL,
	BaseInvokableCall_AllowInvoke_m84704F31555A5C8AD726DAE1C80929D3F75A00DF,
	NULL,
	InvokableCall_add_Delegate_mA80FC3DDF9C96793161FED5B38577EC44E8ECCEC,
	InvokableCall_remove_Delegate_m32D6AD4353BD281EAAC1C319D211FF323E87FABF,
	InvokableCall__ctor_mB755E9394048D1920AA344EA3B3905810042E992,
	InvokableCall_Invoke_mE87495638C5A778726A99872D041A22CA957159E,
	InvokableCall_Invoke_mC0E0B4D35F0795DCF2626DC15E5E92417430AAD7,
	InvokableCall_Find_mE1AF062AF0ADE337AEB2728F401CAB23E95D9360,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PersistentCall_get_target_mE1268D2B40A4618C5E318D439F37022B969A6115,
	PersistentCall_get_targetAssemblyTypeName_m2F53F996EA6BFFDFCDF1D10B6361DE2A98DD9113,
	PersistentCall_get_methodName_m249643D059927A05051B73309FCEAC6A6C9F9C88,
	PersistentCall_get_mode_m7041C70D7CA9CC2D7FCB5D31C81E9C519AF1FEB8,
	PersistentCall_get_arguments_m9FDD073B5551520F0FF4A672C60E237CADBC3435,
	PersistentCall_IsValid_m3BDFC18A3D1AD8ECA8822EA4265127B35237E11B,
	PersistentCall_GetRuntimeCall_mCD3A4A0287D8A9C2CF00D894F378BD4E4684287A,
	PersistentCall_GetObjectCall_m5CDF291A373C8FD34513EF1A1D26C9B36ED7A95D,
	PersistentCall_OnBeforeSerialize_mD5109AA99B22304579296F4B6077647839456346,
	PersistentCall_OnAfterDeserialize_m73B5F8F782FC97AE4C4DCE3DB2C1D9F102455D9E,
	PersistentCall__ctor_mCA080B989171E4FE13761074DD7DE76683969140,
	PersistentCallGroup__ctor_m0F94D4591DB6C4D6C7B997FA45A39C680610B1E0,
	PersistentCallGroup_Initialize_m162FC343BA2C41CBBB6358D624CBD2CED9468833,
	InvokableCallList_AddPersistentInvokableCall_mE7A19335649958FBF5522D7468D6075EEC325D9C,
	InvokableCallList_AddListener_m07F4E145332E2059D570D8300CB422F56F6B0BF1,
	InvokableCallList_RemoveListener_mDE659068D9590D54D00436CCBDB3D27DCD263549,
	InvokableCallList_ClearPersistent_m9A59E6161F8E42120439B76FE952A17DA742443C,
	InvokableCallList_PrepareInvoke_mE340D329F5FC08EA77FC0F3662FF5E5BB85AE0B1,
	InvokableCallList__ctor_m986F4056CA5480D44854152DB768E031AF95C5D8,
	UnityEventBase__ctor_m8F423B800E573ED81DF59EB55CD88D48E817B101,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4E84FB82333E5AA5812095E536810C4BCAF55727,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA1BAB2B28C0E4A62BA06FEF5FCAD8A67C3449472,
	NULL,
	NULL,
	UnityEventBase_FindMethod_m665F2360483EC2BE2D190C6EFA4A624FB8722089,
	UnityEventBase_FindMethod_m9F887E46F769E2C1D0CFE3C71F98892F9C461025,
	UnityEventBase_DirtyPersistentCalls_m85DA5AEA84F8C32D2FDF5DD58D9B7EF704B7B238,
	UnityEventBase_RebuildPersistentCallsIfNeeded_m1FF3E4C46BB16B554103FAAAF3BBCE52C991D21F,
	UnityEventBase_AddCall_mB4825708CFE71BBF9B167EB16FC911CFF95D101C,
	UnityEventBase_RemoveListener_m0B091A723044E4120D592AC65CBD152AC3DF929E,
	UnityEventBase_PrepareInvoke_m999D0F37E0D88375576323D30B67ED7FDC7A779D,
	UnityEventBase_ToString_m0A3BBFEC8C23E044D52502CE201FE82EEF60A8E7,
	UnityEventBase_GetValidMethodInfo_mBA413E99550A6AD8B36F5BEB8682B1536E976C36,
	UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA,
	UnityAction_Invoke_mFFF1FFE59D8285F8A81350E6D5C4D791F44F6AC9,
	UnityAction_BeginInvoke_m67AAC6F5871162346CBCCA0CA51AA38307A4ABB8,
	UnityAction_EndInvoke_mC02B54511B4220CF6D1B37F14BF07D522E91786B,
	UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6,
	UnityEvent_FindMethod_Impl_m763FB43F24EF4A092E26FAE6F6DF561CF360475A,
	UnityEvent_GetDelegate_m7AD1450601F49B957A87161BE6D14020720A1A56,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D,
	PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2,
	MovedFromAttributeData_Set_mC5572E6033FCE1C53B8C43A8D5280E71CE388640,
	MovedFromAttribute__ctor_mA4B2632CE3004A3E0EA8E1241736518320806568,
	Scene_GetIsLoadedInternal_m937AE9E73BD838A013A229917558F8C66E2F4082,
	Scene_get_handle_m57967C50E461CD48441CA60645AF8FA04F7B132C,
	Scene_get_isLoaded_m040A3D274F4FAD21BC95C6154FEA557ADE5C8E93,
	Scene_op_Equality_m5B02632BCD075BF2857A75AF21F5F82AE22C4E4D,
	Scene_GetHashCode_mFC620B8CA1EAA64BF0D7B33E8D3EAFAEDC9A6DCB,
	Scene_Equals_m78D2F82F3133AD32F35C7981B65D0980A6C3006D,
	SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_m17ADD5CEBB41A67CFD806CF61CFED9EB0C288C35,
	SceneManagerAPIInternal_LoadSceneAsyncNameIndexInternal_Injected_mE2CC557F3CD2085753D6AE5C5E8280E3C931C37D,
	SceneManagerAPI_get_ActiveAPI_m0D37AAD13BCEA4851A14AD625B3C6E875EF133A4,
	SceneManagerAPI_get_overrideAPI_m481E89994FFE6384A8F0B4F6891E4A0A504C81D7,
	SceneManagerAPI__ctor_m3DD636D3929892F46996A95396A912C589C9EECF,
	SceneManagerAPI_LoadSceneAsyncByNameOrIndex_m351028028E2A19B144F133011B7314DC8714495B,
	SceneManagerAPI_LoadFirstScene_m0ECE028BAA91CE0EEAF39C713493667B6BFFE759,
	SceneManagerAPI__cctor_mE2DB55CFCB089B29C626309084CDBFDAE704F8EB,
	SceneManager_get_sceneCount_m57B8EB790D8B6673BA840442B4F125121CC5456E,
	SceneManager_GetSceneAt_m46AF96028C6A3A09198ABB313E4206D93A8D1F3F,
	SceneManager_UnloadSceneAsyncInternal_m5892CDFCDDEF60109B0C4DB377EC73695AA00B6B,
	SceneManager_LoadSceneAsyncNameIndexInternal_mD72656A5141151775F28886F59D2830B0EC1B659,
	SceneManager_LoadFirstScene_Internal_mC0778CB81D17E92E17A814E25C7481E0747A573E,
	SceneManager_add_sceneUnloaded_m2CE4194528D7AAE67F494EB15C8BBE650FBA4C72,
	SceneManager_remove_sceneUnloaded_mA7CD60918C6EE3B8D8623201A487C2CEC8D4EA5E,
	SceneManager_LoadSceneAsync_m77AB3010DA4EE548FE973D65A2D983F0CC86254E,
	SceneManager_UnloadSceneAsync_m94D080FDA6440916AA8F8F4FA13B1002C96BB589,
	SceneManager_Internal_SceneLoaded_m3546B371F03BC8DC053FFF165AB25ADF44A183BE,
	SceneManager_Internal_SceneUnloaded_m9A68F15B0FA483633F24E0E50574CFB6DD2D5520,
	SceneManager_Internal_ActiveSceneChanged_m4094F4307C6B3DE0BB87ED646EA9BD6640B831DC,
	SceneManager__cctor_m5D677C6723892CAB16E0F9710AF214D7A328B396,
	SceneManager_GetSceneAt_Injected_m2649DB4E01B925CFDF17EBDFB48816447082771A,
	SceneManager_UnloadSceneAsyncInternal_Injected_m01CB1AF502BE43217B0062B45D8A71612FB81FF5,
	LoadSceneParameters_set_loadSceneMode_m8AAA5796E9D642FC5C2C95831F22E272A28DD152,
	PlayerLoopSystem_ToString_mAC7EE13A5561966294881362AD59CB55DBBED9EE,
	UpdateFunction__ctor_mB10AB83A3F547AC95FF726E8A7B5FF9C16EC1319,
	UpdateFunction_Invoke_mB17C55B92FAECE51078028F59A9F1EAC2016B1AD,
	UpdateFunction_BeginInvoke_m1EB0935AB72A286D153ACEBD0E5D66BB38BD6799,
	UpdateFunction_EndInvoke_mB4BC0AA40E9C83274116DF6467D72DD4902DA624,
	MessageEventArgs__ctor_m18272C7358FAC22848ED84A952DCE17E8CD623BA,
	PlayerConnection_get_instance_mA46C9194C4D2FE0BB06C135C4C0521DD516592F6,
	PlayerConnection_get_isConnected_m315E30E90CA2BBEF2C2C366EF68BF0380C2BCF28,
	PlayerConnection_CreateInstance_m761E97DB4F693FC1A92418088B90872AA559BFFE,
	PlayerConnection_OnEnable_m89048E6B2F1A820F9FD99C3237CB55FF7F56F558,
	PlayerConnection_GetConnectionNativeApi_m94F4E5D341869936857659D92591483D5F6EE95F,
	PlayerConnection_Register_m97A0118B2F172AA0236D0D428F2C6F6E8326BF3D,
	PlayerConnection_Unregister_m1D2F6BE95E82D60F6EA02139AA64D7D58B4B889B,
	PlayerConnection_RegisterConnection_m3DA7DB08B44854C6914F9A154C486EFFCC2B4E8D,
	PlayerConnection_RegisterDisconnection_mA0D4C45243C1244339CAF66E45C65A6130E8B87A,
	PlayerConnection_UnregisterConnection_mC44D98524BD2E21B7658AB898B8283C34A5AD96E,
	PlayerConnection_UnregisterDisconnection_m1DB86C159F601428F59DAB3A4BB912C780F8EAFD,
	PlayerConnection_Send_m51137877D6E519903AEA4A506ABD48A30AF684F0,
	PlayerConnection_TrySend_m9AD3046E615B6DECE991776EEFAB10EA95E478A6,
	PlayerConnection_BlockUntilRecvMsg_m3C3CE1A885657B18DEE2C10066425B45F2B15FAC,
	PlayerConnection_DisconnectAll_m2A55CA3DECCC5683315504EACB9311E565D1EFF5,
	PlayerConnection_MessageCallbackInternal_m223C558BDA58E11AE597D73CEC359718A336FD74,
	PlayerConnection_ConnectedCallbackInternal_m9143E59EA71734C96F877CFD685F0935CEEBA13E,
	PlayerConnection_DisconnectedCallback_mCC356CB2D6C8820A435950F630BA349B7CA5E267,
	PlayerConnection__ctor_mBB02EDC32EB6458CB527F9703CFD4400C7CDCB92,
	U3CU3Ec__DisplayClass12_0__ctor_m66E5B9C72A27F0645F9854522ACE087CE32A5EEE,
	U3CU3Ec__DisplayClass12_0_U3CRegisterU3Eb__0_m27C1416BAD7AF0A1BF83339C8A03865A6487B234,
	U3CU3Ec__DisplayClass13_0__ctor_m75A8930EBA7C6BF81519358930656DAA2FBDDEDB,
	U3CU3Ec__DisplayClass13_0_U3CUnregisterU3Eb__0_mBA34804D7BB1B4FFE45D077BBB07BFA81C8DCB00,
	U3CU3Ec__DisplayClass20_0__ctor_m2B215926A23E33037D754DFAFDF8459D82243683,
	U3CU3Ec__DisplayClass20_0_U3CBlockUntilRecvMsgU3Eb__0_m1D6C3976419CFCE0B612D04C135A985707C215E2,
	PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m157C5C69D6E22A8BCBDC40929BA67E1B6E586389,
	PlayerEditorConnectionEvents_AddAndCreate_m9722774BB976599BF8AD8745CA8EC3218828306B,
	PlayerEditorConnectionEvents_UnregisterManagedCallback_mEB4C4FBCBA7A9527475914F61AFDB9676B8BEFB3,
	PlayerEditorConnectionEvents__ctor_m40946A5B9D9FB40849BEA07DCD600891229249A0,
	MessageEvent__ctor_mE4D70D8837C51E422C9A40C3F8F34ACB9AB572BB,
	ConnectionChangeEvent__ctor_m95EBD8B5EA1C4A14A5F2B71CEE1A2C18C73DB0A1,
	MessageTypeSubscribers_get_MessageTypeId_m5A873C55E97728BD12BA52B5DB72FFA366992DD9,
	MessageTypeSubscribers_set_MessageTypeId_m7125FF3E71F4E678644F056A4DC5C29EFB749762,
	MessageTypeSubscribers__ctor_mA51A6D3E38DBAA5B8237BAB1688F669F24982F29,
	U3CU3Ec__DisplayClass6_0__ctor_mEB8DB2BFDA9F6F083E77F1EC1CE3F4861CD1815A,
	U3CU3Ec__DisplayClass6_0_U3CInvokeMessageIdSubscribersU3Eb__0_mEF5D5DFC8F7C2CEC86378AC3BBD40E80A1D9C615,
	U3CU3Ec__DisplayClass7_0__ctor_m97B67DA8AA306A160A790CCDD869669878DDB7F3,
	U3CU3Ec__DisplayClass7_0_U3CAddAndCreateU3Eb__0_mAA3D205F35F7BE200A5DDD14099F56312FBED909,
	U3CU3Ec__DisplayClass8_0__ctor_m18AC0B2825D7BB04F59DC800DFF74D45921A28FA,
	U3CU3Ec__DisplayClass8_0_U3CUnregisterManagedCallbackU3Eb__0_m70805C4CE0F8DD258CC3975A8C90313A0A609BE3,
	DefaultValueAttribute__ctor_m7A9877491C22E8CDCFDAD240D04156D4FAE7D6C6,
	DefaultValueAttribute_get_Value_m333925936EF155BACAEF24CE1ADE07085722606E,
	DefaultValueAttribute_Equals_mD096E8E7C8C6051AD743AAB7CAADB27428871E51,
	DefaultValueAttribute_GetHashCode_mA74BBB26F6C0B49AA44C175DBFDB82F00E497595,
	ExcludeFromDocsAttribute__ctor_mFA14E76D8A30ED8CA3ADCDA83BE056E54753825D,
	GraphicsSettings_get_lightsUseLinearIntensity_m02A37F59F36C77877FC86B93478BC5795F70FFB2,
	OnDemandRendering_get_renderFrameInterval_m7B3F913B3C16E756BE5DF901D86FC7F869759367,
	OnDemandRendering_GetRenderFrameInterval_mF254C740E4C04295AC2EF10D05D22BCA179D7685,
	OnDemandRendering__cctor_m3C3EFF1AE188B9FACE3E94D9F333CF873D110FD0,
	LODParameters_Equals_mF803671C1F46ECEEE0B376EBF3AAF69D1236B4C0,
	LODParameters_Equals_m8F8B356BCB62FAEAE0EFD8C51FFF9C5045A7DB5C,
	LODParameters_GetHashCode_m5310697EE3BF4943F7358EF0EA2E7B3A9D7C1BAD,
	NULL,
	RenderPipeline_ProcessRenderRequests_m730AE65F326D6007A8C3D7C83CAF182C2DD21A11,
	RenderPipeline_InternalRender_mD6C2FEDA607A430F963066B487C02F4D2379FBDA,
	RenderPipeline_InternalRenderWithRequests_m9883F1C0D6400EB2A0364C92F7F8BB39E9398DCF,
	RenderPipeline_get_disposed_m67EE9900399CE2E9783C5C69BFD1FF08DF521C34,
	RenderPipeline_set_disposed_mB375F2860E0C96CA5E7124154610CB67CE3F80CA,
	RenderPipeline_Dispose_mE06FE618AE8AE1B563CAC05585ACBA8832849A7F,
	RenderPipeline_Dispose_mC61059BB1A9F1CE9DB36C49FD01A1EB5BF4CFFE8,
	RenderPipelineAsset_InternalCreatePipeline_mAE0E11E7E8D2D501F7F0F06D37DB484D37533406,
	RenderPipelineAsset_get_renderingLayerMaskNames_m099B8C66F13086B843E997A91D5F29DFA640BD5B,
	RenderPipelineAsset_get_defaultMaterial_mC04A65F5C07B7B186B734ACBDF5DA55DAFC88A4D,
	RenderPipelineAsset_get_autodeskInteractiveShader_mA096E3713307598F4F0714348A137E3A0F450EAF,
	RenderPipelineAsset_get_autodeskInteractiveTransparentShader_m09C4E71DE3D7BD8CDE7BB60CE379DAB150F309FF,
	RenderPipelineAsset_get_autodeskInteractiveMaskedShader_mD5AB54521766DF6A0FF42971D598761BCF7BC74A,
	RenderPipelineAsset_get_terrainDetailLitShader_mB60D130908834F5E6585578DDEA4E175DECCA654,
	RenderPipelineAsset_get_terrainDetailGrassShader_m3F41ABB79E3672A99A77594D7516855906BB71F8,
	RenderPipelineAsset_get_terrainDetailGrassBillboardShader_mED61CD4E3CF38C120FBC20866F99C17C5FE35FBB,
	RenderPipelineAsset_get_defaultParticleMaterial_mA67643D5E509468BC5C20EABAD895B9920636961,
	RenderPipelineAsset_get_defaultLineMaterial_mA76E800387087F9C4FB510E1C9526D43D7A430F3,
	RenderPipelineAsset_get_defaultTerrainMaterial_m29231CDF74C3D74809F5C990F6881E17759CE298,
	RenderPipelineAsset_get_defaultUIMaterial_m73C804E2798E17A2452687AB46A5570DD813AEC7,
	RenderPipelineAsset_get_defaultUIOverdrawMaterial_m75075F78B2FFC61DB9D05B09340D97B588FE6EB6,
	RenderPipelineAsset_get_defaultUIETC1SupportedMaterial_m530DC5B2F50F075DAB4CFA35A693293C8C98CA33,
	RenderPipelineAsset_get_default2DMaterial_mFC4CF7D4F8341D140CC0AB0B471B154AD580C4F0,
	RenderPipelineAsset_get_defaultShader_mE3420265AB2F3629C6C86E88CC7FDB7744B177C1,
	RenderPipelineAsset_get_defaultSpeedTree7Shader_m14E423504D20BC7D296EDD97045790DEA68DBE14,
	RenderPipelineAsset_get_defaultSpeedTree8Shader_mDAB93B789B21F1D58A6FE11858F8C18527ABD50B,
	NULL,
	RenderPipelineAsset_OnValidate_mEFE93B16F8AA5C809D95536A557FA9E29FF7B1DB,
	RenderPipelineAsset_OnDisable_m98A18DF5D40DBB36EF0A33C637CFA41A71FC03C1,
	RenderPipelineAsset__ctor_mC4E1451327751FBCB6F05982262D3B7ED8538DF4,
	RenderPipelineManager_get_currentPipeline_m096347F0BF45316A41A84B9344DB6B29F4D48611,
	RenderPipelineManager_set_currentPipeline_m03D0E14C590848C8D5ABC2C22C026935119CE526,
	RenderPipelineManager_CleanupRenderPipeline_m85443E0BE0778DFA602405C8047DCAE5DCF6D54E,
	RenderPipelineManager_GetCameras_m8BB39469D10975B096634537AC44FB7CF6D52938,
	RenderPipelineManager_DoRenderLoop_Internal_m43950BFAB0F05B48BE90B2B19679C0249D46273F,
	RenderPipelineManager_PrepareRenderPipeline_m4C5F3624BD68AFCAAD4A9D800ED906B9DF4DFB55,
	RenderPipelineManager__cctor_mAF92ABD8F7586C49F9E8AAE720EBE21A2700CF6D,
	ScriptableRenderContext_GetNumberOfCameras_Internal_mDD1E260788000AB57C94A2D3F8F02A2B91DA653F,
	ScriptableRenderContext_GetCamera_Internal_m20F51E42E84B8E2BBF2CC72F8C8EBFDCE6737646,
	ScriptableRenderContext__ctor_mEA592FA995EF36C1F8F05EF2E51BC1089D7371CA,
	ScriptableRenderContext_GetNumberOfCameras_m43FCE097543E85E40FCA641C570A25E718E3D6F6,
	ScriptableRenderContext_GetCamera_mF8D58C4C1BB5667F63A2DCFDB72CE4C3C7ADBBFA,
	ScriptableRenderContext_Equals_mDC10DFED8A46426E355FE7877624EC2D549EA7B7,
	ScriptableRenderContext_Equals_mB04CFBF55095DF6179ED2229F4C9FE907F95799D,
	ScriptableRenderContext_GetHashCode_mACDECBAC76686105322F409089D9A867DF4BD46D,
	ScriptableRenderContext__cctor_m6993C6472C0532CA5057135C3B5D3015C8729C46,
	ScriptableRenderContext_GetNumberOfCameras_Internal_Injected_m65EF6B7DE8AFA868E6D83B2D75791315D9841426,
	ScriptableRenderContext_GetCamera_Internal_Injected_m17CA2AE7513419CA8FC464A270218F599D48C830,
	ShaderTagId__ctor_mC8779BC717DBC52669DDF99900F968216119A830,
	ShaderTagId_get_id_m0340687DA0CA3F820D569CAE280E3EBF10A23B6E,
	ShaderTagId_set_id_m291EC34E5E1EE99B6D6DF664B331347DBE83364D,
	ShaderTagId_Equals_m13F76C51B5ECF4EC9856579496F93D2B5B9041A7,
	ShaderTagId_Equals_m19A2CFBFF4915B92F7E2572CCAB00A7CBD934DA3,
	ShaderTagId_GetHashCode_m6912AAFF83FFD29FBB2BBE51E2611C2A3667FB67,
	ShaderTagId__cctor_mDC50E07281EFBA6C8517F9AB20D187C74BCBE89D,
	SupportedRenderingFeatures_get_active_mE16AFBB742C413071F2DB87563826A90A93EA04F,
	SupportedRenderingFeatures_set_active_m3BC49234CD45C5EFAE64E319D5198CA159143F54,
	SupportedRenderingFeatures_get_defaultMixedLightingModes_m7B53835BDDAF009835F9A0907BC59E4E88C71D9C,
	SupportedRenderingFeatures_get_mixedLightingModes_mE4A171C47A4A685E49F2F382AA51C556B48EE58C,
	SupportedRenderingFeatures_get_lightmapBakeTypes_mAF3B22ACCE625D1C45F411D30C2874E8A847605E,
	SupportedRenderingFeatures_get_lightmapsModes_m69B5455BF172B258A0A2DB6F52846E8934AD048A,
	SupportedRenderingFeatures_get_enlighten_mA4BDBEBFE0E8F1C161D7BE28B328E6D6E36720A9,
	SupportedRenderingFeatures_get_rendersUIOverlay_m8E56255490C51999C7B14F30CD072E49E2C39B4E,
	SupportedRenderingFeatures_FallbackMixedLightingModeByRef_m517CBD3BBD91EEA5D20CD5979DF8841FE0DBEDAC,
	SupportedRenderingFeatures_IsMixedLightingModeSupported_m8AFE3C9D450B1A6E52A31EA84C1B8F626AAC0CD0,
	SupportedRenderingFeatures_IsMixedLightingModeSupportedByRef_mC13FADD4B8DCEB26F58C6F5DD9D7899A621F9828,
	SupportedRenderingFeatures_IsLightmapBakeTypeSupported_m8BBA40E9CBAFFD8B176F3812C36DD31D9D60BBEA,
	SupportedRenderingFeatures_IsLightmapBakeTypeSupportedByRef_mB6F953153B328DBFD1F7E444D155F8C53ABCD876,
	SupportedRenderingFeatures_IsLightmapsModeSupportedByRef_m3477F21B073DD73F183FAD40A8EEF53843A8A581,
	SupportedRenderingFeatures_IsLightmapperSupportedByRef_mEAFB23042E154953C780501A57D605F76510BB11,
	SupportedRenderingFeatures_IsUIOverlayRenderedBySRP_m829585DA31B1BECD1349A02B69657B1D38D2DDC3,
	SupportedRenderingFeatures_FallbackLightmapperByRef_mD2BB8B58F329170010CB9B3BF72794755218046A,
	SupportedRenderingFeatures__ctor_m0612F2A9F55682A7255B3B276E9BAFCFC0CB4EF4,
	SupportedRenderingFeatures__cctor_mCC9E6E14A59B708435BCF2B25BE36943EC590E28,
	BatchCullingContext__ctor_m660602A9A31FEDDB5673F26C3FD11B4194395369,
	BatchRendererGroup_InvokeOnPerformCulling_m164A637514875EBBFF4A12C55521BEA93474739C,
	OnPerformCulling__ctor_m1862F8A67E8CA14A19B80DE61BBC1881DF945E9F,
	OnPerformCulling_Invoke_m804E8422831E63707E5582FBBBFEF08E8A100525,
	OnPerformCulling_BeginInvoke_mAF6E29B6BAC52D4DCAD0C87B8C6939B94566EAA1,
	OnPerformCulling_EndInvoke_m643216ACF662C78BD6F98E09FA5AA45F92E2F9F0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Playable_get_Null_m008AFBC0B01AE584444CF8322CCCC038ED2B8B58,
	Playable__ctor_m4B5AC727703A68C00773F99DE1C711EFC973DCA8,
	Playable_GetHandle_mA646BB041702651694A643FDE1A835112F718351,
	Playable_Equals_m1BFA06EB851EEEB20705520563A2D504637CDA1C,
	Playable__cctor_m83EA9DB5D7F0DAE681E34D0899A2562795301888,
	NULL,
	PlayableAsset_get_duration_m7D57ED7F9E30E414F2981357A61C42CBF3D8367D,
	PlayableAsset_get_outputs_m5F941EB83243BB24A93C27A7583BB55FF18CB38C,
	PlayableAsset_Internal_CreatePlayable_m5E4984A4BF33518FBE69B43A29DB486AF6501537,
	PlayableAsset_Internal_GetPlayableAssetDuration_mE27BD3B5B7D9E80D1C249D6430401BD81E4AFB4C,
	PlayableAsset__ctor_mAE1FA54D280C75ADC9486656C5C36AC1917D5FE4,
	PlayableBehaviour__ctor_m1EA2FC6B8DE3503745344E7BBAA0B40FDCD50FC8,
	PlayableBehaviour_OnGraphStart_mB302433EB8460D88FD2FD035D2BF9BF82BD82AA6,
	PlayableBehaviour_OnGraphStop_m24F852A18B98173C2CBF1B96C0477A1F77BB9CFE,
	PlayableBehaviour_OnPlayableCreate_m62F834EDF37B809612E47CCC9C4848435A89CD12,
	PlayableBehaviour_OnPlayableDestroy_m9F6E1A185DC7C98C312D9F3A0A7308DE3D32F745,
	PlayableBehaviour_OnBehaviourPlay_m71A792D97CD840F5D6C73889D00C2887F41A0F41,
	PlayableBehaviour_OnBehaviourPause_mCC4EAD0766538FE39656D3EE04268239BB091AD2,
	PlayableBehaviour_PrepareFrame_m45F96C7EDDA4ABF4C2271DB16163FBD873AA1432,
	PlayableBehaviour_ProcessFrame_m5EB22A817FFF0662E0E3AFAB34C41D7B09D4326F,
	PlayableBehaviour_Clone_m3265FD7A4EA58C9A607F0F28EFF108EBBD826C3A,
	PlayableBinding__cctor_m75475729474FE236198B967978A11DBA38CC6E47,
	CreateOutputMethod__ctor_m944A1B790F35F52E108EF2CC13BA02BD8DE62EB3,
	CreateOutputMethod_Invoke_mFD551E15D9A6FD8B2C70A2CC96AAD0D2D9D3D109,
	CreateOutputMethod_BeginInvoke_m082AE47F9DFBF0C1787081D2D628E0E5CECCBFF1,
	CreateOutputMethod_EndInvoke_mAA7AE5BF9E6A38C081A972827A16497ACA9FA9CE,
	PlayableHandle_get_Null_mD1C6FC2D7F6A7A23955ACDD87BE934B75463E612,
	PlayableHandle_op_Equality_mFD26CFA8ECF2B622B1A3D4117066CAE965C9F704,
	PlayableHandle_Equals_mEF30690ECF60C980C207198C86E401CB4DA5EF3F,
	PlayableHandle_Equals_mB50663A8BC01BCED4A650EFADE88DCF47309E955,
	PlayableHandle_GetHashCode_m26AD05C2D6209A017CA6A079F026BC8D28600D72,
	PlayableHandle_CompareVersion_m7B2FDE7E81BCBB16D3E2667AEDAA879FA75EA1AA,
	PlayableHandle__cctor_m14F0FDD932E8AB558039FFCD57C2E319AA25C989,
	PlayableOutput__ctor_m69B5F29DF2D785051B714CFEAAAF09A237978297,
	PlayableOutput_GetHandle_mC56A4F912A7AC8640DE78F95DD6C4F346E36820E,
	PlayableOutput_Equals_m5A66A7B50C52A8524BB807F439345D22E96FD5CE,
	PlayableOutput__cctor_m4F7AC7CBF2D9DD26FF45B6D521C4051CDC1A17D3,
	PlayableOutputHandle_get_Null_m33F7D36A76BFDC0B58633BF931E55FA5BBFFD93D,
	PlayableOutputHandle_GetHashCode_mB4BD207CB03FA499179FBEAF64CC66A91F49E59C,
	PlayableOutputHandle_op_Equality_mD24E3FB556D12A8D2B6EBDB6953F667B963F5DA9,
	PlayableOutputHandle_Equals_m06CE237BED975C0A3E9B19D071767EF6E7A3F83C,
	PlayableOutputHandle_Equals_mA8CE0F7B36B440F067705FA90DC4A836E92542E0,
	PlayableOutputHandle_CompareVersion_mB96AE424417CD9E5610906A1508B299679814D8E,
	PlayableOutputHandle__cctor_mFC855E625CBD628602F990B3A15AF0DB8C7E3AC1,
	LinearColor_get_red_mA08BA9496EAFF59F4E1EAD61FDB5F57B0B0CC541,
	LinearColor_set_red_mC0D9E4D96C36785145EAF7B0DBA90359EE193928,
	LinearColor_get_green_m03F2EEBC25E91E1102A2D3252725B2BEDA7D7931,
	LinearColor_set_green_m6E196AC12B067ED8AEF3B7C7E9DA1B80B9550B89,
	LinearColor_get_blue_m0B87E7A2A5A5B6A6F5FD515890F6C3C528FC98FE,
	LinearColor_set_blue_mB65DC7FD20C551501BC181C457D010DCEECDEE7F,
	LinearColor_Convert_m7C35C63BFFDD93EBCC6E8050567B79562B82823A,
	LinearColor_Black_m50DB4626285C618DFD9F561573AA77F84AE7E180,
	LightDataGI_Init_m135DF5CF6CBECA4CBA0AC71F9CDEEF8DE25606DB,
	LightDataGI_Init_m4E8BEBD383D5F6F1A1EDFEC763A718A7271C22A6,
	LightDataGI_Init_mC9948FAC4A191C99E3E7D94677B7CFD241857C86,
	LightDataGI_Init_mA0DF9747C6AD308EAAF2A9530B4225A3D65BB092,
	LightDataGI_Init_mB96C3F3E00F10DD0806BD3DB93B58C2299D59E94,
	LightDataGI_InitNoBake_mF600D612DE9A1CE4355C61317F6173E1AAEFBD57,
	LightmapperUtils_Extract_m32B54C9DC94AE03162E3896C5FA00FE9678F9081,
	LightmapperUtils_ExtractIndirect_mC17A833A46BAAA01B55F8BA8A5821292AB104F54,
	LightmapperUtils_ExtractInnerCone_mEF618AE5A5D8EB690F3BA162196859FE6F662947,
	LightmapperUtils_ExtractColorTemperature_m5052DE4DC7630A7077EA2B8C6AA903257C95AFA5,
	LightmapperUtils_ApplyColorTemperature_mA49FB616EB2F9F31AF4CCB4C964592005DCEA346,
	LightmapperUtils_Extract_mCBEC26CC920C0D87DF6E25417533923E26CB6DFC,
	LightmapperUtils_Extract_mB11D8F3B35F96E176A89517A25CD1A54DCBD7D6E,
	LightmapperUtils_Extract_mB1572C38F682F043180745B7A231FBE07CD205E4,
	LightmapperUtils_Extract_mEABF77895D51E1CA5FD380682539C3DD3FC8A91C,
	LightmapperUtils_Extract_m93B350DDA0CB5B624054835BAF46C177472E9212,
	LightmapperUtils_Extract_mACAC5E823E243A53EDD2A01CF857DD16C3FDBE2A,
	Lightmapping_SetDelegate_m3C7B041BEEBD50C1EF3C0D9D5BC2162E93E19979,
	Lightmapping_GetDelegate_mD44EBB65CFD4038D77119A3F896B55905DF9A9DC,
	Lightmapping_ResetDelegate_m6EF8586283713477679876577D753EFDCA74A6F3,
	Lightmapping_RequestLights_m40C73984B1F2DB34C58965D1C4D321181C7E5976,
	Lightmapping__cctor_m94640A0363C80E0E1438E4EE650AED85AB3E576C,
	RequestLightsDelegate__ctor_m47823FBD9D2EE4ABA5EE8D889BBBC0783FB10A42,
	RequestLightsDelegate_Invoke_m8BC0D55744A3FA2E75444AC72B3D3E4FB858BECB,
	RequestLightsDelegate_BeginInvoke_m75482E3D4E28205DCB4A202F5C144CB7C95622A6,
	RequestLightsDelegate_EndInvoke_mA1FF787B6F3182ACF6157D13F9A84AFCBB66EE60,
	U3CU3Ec__cctor_m2A00D547FF8F6F8A03666B4737BB9A0620719987,
	U3CU3Ec__ctor_m8F825BEC75A119B6CDDA0985A3F7BA3DEA8AD5B2,
	U3CU3Ec_U3C_cctorU3Eb__7_0_m84A19BB5BB12263A1E3C52F1B351E3A24BE546C7,
	CameraPlayable_GetHandle_m12A0FB549E5257C9AEBCF76B6E2DC49FE5F8B7C5,
	CameraPlayable_Equals_m82D036759943861CAB110D108F966C60216E6327,
	MaterialEffectPlayable_GetHandle_mC4AA4C850DFB33EBA45EDA3D0524294EC03044FC,
	MaterialEffectPlayable_Equals_mC805BF647B60EA8517040C2C0716CFCB7F04CAFB,
	TextureMixerPlayable_GetHandle_m44A48E52180084F5A93942EA0AFA454C9DFBFD40,
	TextureMixerPlayable_Equals_m49E1B77DF4F13F35321494AC6B5330538D0A1980,
	BuiltinRuntimeReflectionSystem_TickRealtimeProbes_mDE45D3D3E07AB0FF8C10791544CB29FF6D44DFA2,
	BuiltinRuntimeReflectionSystem_Dispose_mFF4F5CDB37BB93A28975F7BFE970040964B48F5A,
	BuiltinRuntimeReflectionSystem_Dispose_mFBDDD8FE2956E99DB34533F8C29621C3E938BD13,
	BuiltinRuntimeReflectionSystem_BuiltinUpdate_mEDD980F13F6200E5B89742D6868C4EF4858AE405,
	BuiltinRuntimeReflectionSystem_Internal_BuiltinRuntimeReflectionSystem_New_mA4A701BE60FC41AD01F7AC965DACA950B4C9560C,
	BuiltinRuntimeReflectionSystem__ctor_m418427E040351EC086975D95B409F2C6E3E41045,
	NULL,
	ScriptableRuntimeReflectionSystemSettings_set_Internal_ScriptableRuntimeReflectionSystemSettings_system_mE9EF71AD222FC661C616AC9687961D98946D8680,
	ScriptableRuntimeReflectionSystemSettings_get_Internal_ScriptableRuntimeReflectionSystemSettings_instance_mC8110BFC8188AAFC7D4EC56780617AB33CB2D71C,
	ScriptableRuntimeReflectionSystemSettings_ScriptingDirtyReflectionSystemInstance_mE4FFB1863BE37B6E20388C15D2C48F4E01FCFEEF,
	ScriptableRuntimeReflectionSystemSettings__cctor_m24D01EC03C21F2E3A40CC9C0DC4A646C8690096A,
	ScriptableRuntimeReflectionSystemWrapper_get_implementation_mD0D0BB589A80E0B0C53491CC916EE406378649D6,
	ScriptableRuntimeReflectionSystemWrapper_set_implementation_m95A62C63F5D1D50EDCD5351D74F73EEBC0A239B1,
	ScriptableRuntimeReflectionSystemWrapper_Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_mC04DACDD9BF402C3D12DE78F286A36B3A81BA547,
	ScriptableRuntimeReflectionSystemWrapper__ctor_m14586B1A430F0316A379C966D52BDE6410BA5FCC,
	GraphicsFormatUtility_GetGraphicsFormat_mF9AFEB31DE7E63FC76D6A99AE31A108491A9F232,
	GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat_mAE09EC30C3D01C3190767E2590DC0FD9358A1C5C,
	GraphicsFormatUtility_GetGraphicsFormat_mD73B7F075511D7D392D55B146711F19A76E5AF1C,
	GraphicsFormatUtility_GetGraphicsFormat_Native_RenderTextureFormat_m7F44D525B67F585D711628BC2981852A6DA7633B,
	GraphicsFormatUtility_GetGraphicsFormat_m5ED879E5A23929743CD65735DEDDF3BE701946D8,
	GraphicsFormatUtility_IsSRGBFormat_mDA5982709BD21EE1163A90381100F6C7C6F40F1C,
	GraphicsFormatUtility_IsCompressedTextureFormat_m740C48D113EFDF97CD6EB48308B486F68C03CF82,
	GraphicsFormatUtility_IsCrunchFormat_mB31D5C0C0D337A3B00D1AED3A7E036CD52540F66,
};
extern void CachedAssetBundle__ctor_m06149D4DAC04AD0183DC3A92FDF9640C7368322A_AdjustorThunk (void);
extern void CachedAssetBundle_get_name_m08907939D330FAB52F5A08EFA5BBB3849A1893C9_AdjustorThunk (void);
extern void CachedAssetBundle_get_hash_mCD87C4B6DF6F8971992E1F831EE89983251FF8F6_AdjustorThunk (void);
extern void Cache_get_handle_m7E6EBC767CCE0EDB513896C6686A40B9D147A8E8_AdjustorThunk (void);
extern void Cache_GetHashCode_m3EBFBD645A04B9E5421615E5C465C1FEC37CACC8_AdjustorThunk (void);
extern void Cache_Equals_m85EFA79FA7AA290AFE0DEBE73E556C9EE5B2BEC2_AdjustorThunk (void);
extern void Cache_Equals_m60B08970A74F86BB09E97A2BEB301710F54D0555_AdjustorThunk (void);
extern void Cache_get_valid_m02EF848F3AA17F671BA8764BB53EBAFF8CED3688_AdjustorThunk (void);
extern void Cache_get_path_m632CE9F465C2899566AD25CC97D1A13FA7A32946_AdjustorThunk (void);
extern void Cache_set_maximumAvailableStorageSpace_mEC73DB6924F6B13FB9D477D5268B46620B02B7FE_AdjustorThunk (void);
extern void Cache_set_expirationDelay_m81253C0D3CEC4A69B5CBE48B991BA083953DA741_AdjustorThunk (void);
extern void Bounds_GetHashCode_m822D1DF4F57180495A4322AADD3FD173C6FC3A1B_AdjustorThunk (void);
extern void Bounds_Equals_mB759250EA283B06481746E9A247B024D273408C9_AdjustorThunk (void);
extern void Bounds_Equals_mC558BE6FE3614F7B42F5E22D1F155194A0E3DD0F_AdjustorThunk (void);
extern void Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485_AdjustorThunk (void);
extern void Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02_AdjustorThunk (void);
extern void Bounds_ToString_mC2F42E6634E786CC9A1B847ECDD88226B7880E7B_AdjustorThunk (void);
extern void Bounds_ToString_m493BA40092851F60E7CE73A8BD58489DAE18B3AD_AdjustorThunk (void);
extern void Plane_ToString_mD0E5921B1DFC4E83443BA7267E1182ACDD65C5DD_AdjustorThunk (void);
extern void Plane_ToString_m0AF5EF6354605B6F6698346081FBC1A8BE138C4B_AdjustorThunk (void);
extern void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70_AdjustorThunk (void);
extern void Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987_AdjustorThunk (void);
extern void Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74_AdjustorThunk (void);
extern void Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF_AdjustorThunk (void);
extern void Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A_AdjustorThunk (void);
extern void Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5_AdjustorThunk (void);
extern void Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3_AdjustorThunk (void);
extern void Rect_GetHashCode_mE5841C54528B29D5E85173AF1973326793AF9311_AdjustorThunk (void);
extern void Rect_Equals_mF1F747B913CDB083ED803F5DD21E2005736AE4AB_AdjustorThunk (void);
extern void Rect_Equals_mC9EE5E0C234DB174AC16E653ED8D32BB4D356BB8_AdjustorThunk (void);
extern void Rect_ToString_mCB7EA3D9B51213304AB0175B2D5E4545AFBCF483_AdjustorThunk (void);
extern void Rect_ToString_m3DFE65344E06224C23BB7500D069F908D5DDE8F5_AdjustorThunk (void);
extern void Resolution_ToString_m0F17D03CC087E67DAB7F8F383D86A9D5C3E2587B_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_width_m5DD56A0652453FDDB51FF030FC5ED914F83F5E31_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_width_m8D4BAEBB8089FD77F4DC81088ACB511F2BCA41EA_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_height_m661881AD8E078D6C1FD6C549207AACC2B179D201_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_height_m1300AF31BCDCF2E14E86A598AFDC5569B682A46D_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_msaaSamples_m332912610A1FF2B7C05B0BA9939D733F2E7F0646_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_msaaSamples_m84320452D8BF3A8DD5662F6229FE666C299B5AEF_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_volumeDepth_m05E4A20A05286909E65D394D0BA5F6904D653688_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_volumeDepth_mC4D9C6B86B6799BA752855DE5C385CC24F6E3733_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_mipCount_mE713137D106256F44EF3E7B7CF33D5F146874659_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_graphicsFormat_m9D77E42E017808FE3181673152A69CBC9A9B8B85_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_graphicsFormat_m946B6FE4422E8CD33EB13ADAFDB53669EBD361C4_AdjustorThunk (void);
extern void RenderTextureDescriptor_get_depthBufferBits_m92A95D5A1DCA7B844B3AC81AADCDFDD37D26333C_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_depthBufferBits_m68BF4BF942828FF70442841A22D356E5D17BCF85_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_dimension_m4D3F1486F761F3C52308F00267B918BD7DB8137F_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_shadowSamplingMode_m92B77BB68CC465F38790F5865A7402C5DE77B8D1_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_vrUsage_m5E4F43CB35EF142D55AC22996B641483566A2097_AdjustorThunk (void);
extern void RenderTextureDescriptor_set_memoryless_m6C34CD3938C6C92F98227E3864E665026C50BCE3_AdjustorThunk (void);
extern void RenderTextureDescriptor__ctor_m320C821459C7856A088415334267C2963B270A9D_AdjustorThunk (void);
extern void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m33FD234885342E9D0C6450C90C0F2E1B6B6A1044_AdjustorThunk (void);
extern void Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB_AdjustorThunk (void);
extern void Hash128_CompareTo_m577A27A07658268AD07B09F4FFFF1D3216AD963B_AdjustorThunk (void);
extern void Hash128_ToString_mE6E0973B9B42A6AB9BEB8ACC679291CDAD2D03AC_AdjustorThunk (void);
extern void Hash128_Equals_m2FEA62200ECEC6BA066924F3153C9FBA96B0E3FF_AdjustorThunk (void);
extern void Hash128_Equals_mA77DD31AF975A04BF4BD9D1F5B2A143497C9F468_AdjustorThunk (void);
extern void Hash128_GetHashCode_mBEB470B9988886E4EB3FDA22903EBB699D1B7EA6_AdjustorThunk (void);
extern void Hash128_CompareTo_m28ACD34C28C044C2BEF2109446DAAEB53F4EC619_AdjustorThunk (void);
extern void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5_AdjustorThunk (void);
extern void Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494_AdjustorThunk (void);
extern void Color_ToString_m2C9303D88F39CDAE35C613A3C816D8982C58B5FC_AdjustorThunk (void);
extern void Color_ToString_m927424DFCE95E13D26A1F8BF91FA0AB3F6C2FC02_AdjustorThunk (void);
extern void Color_GetHashCode_mAF5E7EE6AFA983D3FA5E3D316E672EE1511F97CF_AdjustorThunk (void);
extern void Color_Equals_m90F8A5EF85416D809F5E3C0ACCADDD4F299AD8FC_AdjustorThunk (void);
extern void Color_Equals_mB531F532B5F7BE6168CFD4A6C89358C16F058D00_AdjustorThunk (void);
extern void Color_RGBMultiplied_mEE82A8761F22790ECD29CD8AE13B1184441CB6C8_AdjustorThunk (void);
extern void Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904_AdjustorThunk (void);
extern void Color_get_maxColorComponent_mAB6964B3523DC9FDDF312F3329EB224DBFECE761_AdjustorThunk (void);
extern void Color32_ToString_m11295D5492D1FB41F25635A83B87C20058DEA256_AdjustorThunk (void);
extern void Color32_ToString_m5BB9D04F00C5B22C5B295F6253C99972767102F5_AdjustorThunk (void);
extern void Matrix4x4__ctor_mFDDCE13D7171353ED7BA9A9B6885212DFC9E1076_AdjustorThunk (void);
extern void Matrix4x4_GetHashCode_m102B903082CD1C786C221268A19679820E365B59_AdjustorThunk (void);
extern void Matrix4x4_Equals_mF6EB7A6D466F5AE1D1A872451359645D1C69843D_AdjustorThunk (void);
extern void Matrix4x4_Equals_mAE7AC284A922B094E4ACCC04A1C48B247E9A7997_AdjustorThunk (void);
extern void Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480_AdjustorThunk (void);
extern void Matrix4x4_ToString_mF45C45AD892B0707C34BEE0C716C91C0B5FD2831_AdjustorThunk (void);
extern void Matrix4x4_ToString_mC2CC8C3C358C9C982F25F633BC21105D2C3BCEFB_AdjustorThunk (void);
extern void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_AdjustorThunk (void);
extern void Vector3_GetHashCode_m9F18401DA6025110A012F55BBB5ACABD36FA9A0A_AdjustorThunk (void);
extern void Vector3_Equals_m210CB160B594355581D44D4B87CF3D3994ABFED0_AdjustorThunk (void);
extern void Vector3_Equals_mA92800CD98ED6A42DD7C55C5DB22DAB4DEAA6397_AdjustorThunk (void);
extern void Vector3_ToString_mD5085501F9A0483542E9F7B18CD09C0AB977E318_AdjustorThunk (void);
extern void Vector3_ToString_m8E771CC90555B06B8BDBA5F691EC5D8D87D68414_AdjustorThunk (void);
extern void Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421_AdjustorThunk (void);
extern void Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3_AdjustorThunk (void);
extern void Quaternion_GetHashCode_mFCEA4CA542544DC9BD222C66F524C2F3CFE60744_AdjustorThunk (void);
extern void Quaternion_Equals_m3EDD7DBA22F59A5797B91820AE4BBA64576D240F_AdjustorThunk (void);
extern void Quaternion_Equals_m02CE0D27C1DA0C037D8721750E30BB1FAF1A7DAD_AdjustorThunk (void);
extern void Quaternion_ToString_mD3D4C66907C994D30D99E76063623F7000F6998E_AdjustorThunk (void);
extern void Quaternion_ToString_mF10FE18AAC385F9CFE721ECD8B66F14346774F31_AdjustorThunk (void);
extern void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_AdjustorThunk (void);
extern void Vector2_ToString_mBD48EFCDB703ACCDC29E86AEB0D4D62FBA50F840_AdjustorThunk (void);
extern void Vector2_ToString_m503AFEA3F57B8529C047FF93C2E72126C5591C23_AdjustorThunk (void);
extern void Vector2_GetHashCode_m9A5DD8406289F38806CC42C394E324C1C2AB3732_AdjustorThunk (void);
extern void Vector2_Equals_m67A842D914AA5A4DCC076E9EA20019925E6A85A0_AdjustorThunk (void);
extern void Vector2_Equals_m6E08A16717F2B9EE8B24EBA6B234A03098D5F05D_AdjustorThunk (void);
extern void Vector4_get_Item_m469B9D88498D0F7CD14B71A9512915BAA0B9B3B7_AdjustorThunk (void);
extern void Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2_AdjustorThunk (void);
extern void Vector4_GetHashCode_mCA7B312F8CA141F6F25BABDDF406F3D2BDD5E895_AdjustorThunk (void);
extern void Vector4_Equals_m71D14F39651C3FBEDE17214455DFA727921F07AA_AdjustorThunk (void);
extern void Vector4_Equals_m0919D35807550372D1748193EB31E4C5E406AE61_AdjustorThunk (void);
extern void Vector4_ToString_mF2D17142EBD75E91BC718B3E347F614AC45E9040_AdjustorThunk (void);
extern void Vector4_ToString_m0EC6AA83CD606E3EB5BE60108A1D9AC4ECB5517A_AdjustorThunk (void);
extern void WorkRequest__ctor_m13C7B4A89E47F4B97ED9B786DB99849DBC2B5603_AdjustorThunk (void);
extern void WorkRequest_Invoke_m1C292B7297918C5F2DBE70971895FE8D5C33AA20_AdjustorThunk (void);
extern void DebugScreenCapture_set_rawImageDataReference_m9FE7228E0FB4696A255B2F477B6B50C902D7786B_AdjustorThunk (void);
extern void DebugScreenCapture_set_imageFormat_m46E9D97376E844826DAE5C3C69E4AAF4B7A58ECB_AdjustorThunk (void);
extern void DebugScreenCapture_set_width_m6928DB2B2BCD54DE97BDA4116D69897921CCACF6_AdjustorThunk (void);
extern void DebugScreenCapture_set_height_m6CD0F6872F123966B784E6F356C51986B8B19F84_AdjustorThunk (void);
extern void MovedFromAttributeData_Set_mC5572E6033FCE1C53B8C43A8D5280E71CE388640_AdjustorThunk (void);
extern void Scene_get_handle_m57967C50E461CD48441CA60645AF8FA04F7B132C_AdjustorThunk (void);
extern void Scene_get_isLoaded_m040A3D274F4FAD21BC95C6154FEA557ADE5C8E93_AdjustorThunk (void);
extern void Scene_GetHashCode_mFC620B8CA1EAA64BF0D7B33E8D3EAFAEDC9A6DCB_AdjustorThunk (void);
extern void Scene_Equals_m78D2F82F3133AD32F35C7981B65D0980A6C3006D_AdjustorThunk (void);
extern void LoadSceneParameters_set_loadSceneMode_m8AAA5796E9D642FC5C2C95831F22E272A28DD152_AdjustorThunk (void);
extern void PlayerLoopSystem_ToString_mAC7EE13A5561966294881362AD59CB55DBBED9EE_AdjustorThunk (void);
extern void LODParameters_Equals_mF803671C1F46ECEEE0B376EBF3AAF69D1236B4C0_AdjustorThunk (void);
extern void LODParameters_Equals_m8F8B356BCB62FAEAE0EFD8C51FFF9C5045A7DB5C_AdjustorThunk (void);
extern void LODParameters_GetHashCode_m5310697EE3BF4943F7358EF0EA2E7B3A9D7C1BAD_AdjustorThunk (void);
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_mDD1E260788000AB57C94A2D3F8F02A2B91DA653F_AdjustorThunk (void);
extern void ScriptableRenderContext_GetCamera_Internal_m20F51E42E84B8E2BBF2CC72F8C8EBFDCE6737646_AdjustorThunk (void);
extern void ScriptableRenderContext__ctor_mEA592FA995EF36C1F8F05EF2E51BC1089D7371CA_AdjustorThunk (void);
extern void ScriptableRenderContext_GetNumberOfCameras_m43FCE097543E85E40FCA641C570A25E718E3D6F6_AdjustorThunk (void);
extern void ScriptableRenderContext_GetCamera_mF8D58C4C1BB5667F63A2DCFDB72CE4C3C7ADBBFA_AdjustorThunk (void);
extern void ScriptableRenderContext_Equals_mDC10DFED8A46426E355FE7877624EC2D549EA7B7_AdjustorThunk (void);
extern void ScriptableRenderContext_Equals_mB04CFBF55095DF6179ED2229F4C9FE907F95799D_AdjustorThunk (void);
extern void ScriptableRenderContext_GetHashCode_mACDECBAC76686105322F409089D9A867DF4BD46D_AdjustorThunk (void);
extern void ShaderTagId__ctor_mC8779BC717DBC52669DDF99900F968216119A830_AdjustorThunk (void);
extern void ShaderTagId_get_id_m0340687DA0CA3F820D569CAE280E3EBF10A23B6E_AdjustorThunk (void);
extern void ShaderTagId_set_id_m291EC34E5E1EE99B6D6DF664B331347DBE83364D_AdjustorThunk (void);
extern void ShaderTagId_Equals_m13F76C51B5ECF4EC9856579496F93D2B5B9041A7_AdjustorThunk (void);
extern void ShaderTagId_Equals_m19A2CFBFF4915B92F7E2572CCAB00A7CBD934DA3_AdjustorThunk (void);
extern void ShaderTagId_GetHashCode_m6912AAFF83FFD29FBB2BBE51E2611C2A3667FB67_AdjustorThunk (void);
extern void BatchCullingContext__ctor_m660602A9A31FEDDB5673F26C3FD11B4194395369_AdjustorThunk (void);
extern void Playable__ctor_m4B5AC727703A68C00773F99DE1C711EFC973DCA8_AdjustorThunk (void);
extern void Playable_GetHandle_mA646BB041702651694A643FDE1A835112F718351_AdjustorThunk (void);
extern void Playable_Equals_m1BFA06EB851EEEB20705520563A2D504637CDA1C_AdjustorThunk (void);
extern void PlayableHandle_Equals_mEF30690ECF60C980C207198C86E401CB4DA5EF3F_AdjustorThunk (void);
extern void PlayableHandle_Equals_mB50663A8BC01BCED4A650EFADE88DCF47309E955_AdjustorThunk (void);
extern void PlayableHandle_GetHashCode_m26AD05C2D6209A017CA6A079F026BC8D28600D72_AdjustorThunk (void);
extern void PlayableOutput__ctor_m69B5F29DF2D785051B714CFEAAAF09A237978297_AdjustorThunk (void);
extern void PlayableOutput_GetHandle_mC56A4F912A7AC8640DE78F95DD6C4F346E36820E_AdjustorThunk (void);
extern void PlayableOutput_Equals_m5A66A7B50C52A8524BB807F439345D22E96FD5CE_AdjustorThunk (void);
extern void PlayableOutputHandle_GetHashCode_mB4BD207CB03FA499179FBEAF64CC66A91F49E59C_AdjustorThunk (void);
extern void PlayableOutputHandle_Equals_m06CE237BED975C0A3E9B19D071767EF6E7A3F83C_AdjustorThunk (void);
extern void PlayableOutputHandle_Equals_mA8CE0F7B36B440F067705FA90DC4A836E92542E0_AdjustorThunk (void);
extern void LinearColor_get_red_mA08BA9496EAFF59F4E1EAD61FDB5F57B0B0CC541_AdjustorThunk (void);
extern void LinearColor_set_red_mC0D9E4D96C36785145EAF7B0DBA90359EE193928_AdjustorThunk (void);
extern void LinearColor_get_green_m03F2EEBC25E91E1102A2D3252725B2BEDA7D7931_AdjustorThunk (void);
extern void LinearColor_set_green_m6E196AC12B067ED8AEF3B7C7E9DA1B80B9550B89_AdjustorThunk (void);
extern void LinearColor_get_blue_m0B87E7A2A5A5B6A6F5FD515890F6C3C528FC98FE_AdjustorThunk (void);
extern void LinearColor_set_blue_mB65DC7FD20C551501BC181C457D010DCEECDEE7F_AdjustorThunk (void);
extern void LightDataGI_Init_m135DF5CF6CBECA4CBA0AC71F9CDEEF8DE25606DB_AdjustorThunk (void);
extern void LightDataGI_Init_m4E8BEBD383D5F6F1A1EDFEC763A718A7271C22A6_AdjustorThunk (void);
extern void LightDataGI_Init_mC9948FAC4A191C99E3E7D94677B7CFD241857C86_AdjustorThunk (void);
extern void LightDataGI_Init_mA0DF9747C6AD308EAAF2A9530B4225A3D65BB092_AdjustorThunk (void);
extern void LightDataGI_Init_mB96C3F3E00F10DD0806BD3DB93B58C2299D59E94_AdjustorThunk (void);
extern void LightDataGI_InitNoBake_mF600D612DE9A1CE4355C61317F6173E1AAEFBD57_AdjustorThunk (void);
extern void CameraPlayable_GetHandle_m12A0FB549E5257C9AEBCF76B6E2DC49FE5F8B7C5_AdjustorThunk (void);
extern void CameraPlayable_Equals_m82D036759943861CAB110D108F966C60216E6327_AdjustorThunk (void);
extern void MaterialEffectPlayable_GetHandle_mC4AA4C850DFB33EBA45EDA3D0524294EC03044FC_AdjustorThunk (void);
extern void MaterialEffectPlayable_Equals_mC805BF647B60EA8517040C2C0716CFCB7F04CAFB_AdjustorThunk (void);
extern void TextureMixerPlayable_GetHandle_m44A48E52180084F5A93942EA0AFA454C9DFBFD40_AdjustorThunk (void);
extern void TextureMixerPlayable_Equals_m49E1B77DF4F13F35321494AC6B5330538D0A1980_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[165] = 
{
	{ 0x06000046, CachedAssetBundle__ctor_m06149D4DAC04AD0183DC3A92FDF9640C7368322A_AdjustorThunk },
	{ 0x06000047, CachedAssetBundle_get_name_m08907939D330FAB52F5A08EFA5BBB3849A1893C9_AdjustorThunk },
	{ 0x06000048, CachedAssetBundle_get_hash_mCD87C4B6DF6F8971992E1F831EE89983251FF8F6_AdjustorThunk },
	{ 0x06000049, Cache_get_handle_m7E6EBC767CCE0EDB513896C6686A40B9D147A8E8_AdjustorThunk },
	{ 0x0600004A, Cache_GetHashCode_m3EBFBD645A04B9E5421615E5C465C1FEC37CACC8_AdjustorThunk },
	{ 0x0600004B, Cache_Equals_m85EFA79FA7AA290AFE0DEBE73E556C9EE5B2BEC2_AdjustorThunk },
	{ 0x0600004C, Cache_Equals_m60B08970A74F86BB09E97A2BEB301710F54D0555_AdjustorThunk },
	{ 0x0600004D, Cache_get_valid_m02EF848F3AA17F671BA8764BB53EBAFF8CED3688_AdjustorThunk },
	{ 0x0600004F, Cache_get_path_m632CE9F465C2899566AD25CC97D1A13FA7A32946_AdjustorThunk },
	{ 0x06000051, Cache_set_maximumAvailableStorageSpace_mEC73DB6924F6B13FB9D477D5268B46620B02B7FE_AdjustorThunk },
	{ 0x06000053, Cache_set_expirationDelay_m81253C0D3CEC4A69B5CBE48B991BA083953DA741_AdjustorThunk },
	{ 0x0600008F, Bounds_GetHashCode_m822D1DF4F57180495A4322AADD3FD173C6FC3A1B_AdjustorThunk },
	{ 0x06000090, Bounds_Equals_mB759250EA283B06481746E9A247B024D273408C9_AdjustorThunk },
	{ 0x06000091, Bounds_Equals_mC558BE6FE3614F7B42F5E22D1F155194A0E3DD0F_AdjustorThunk },
	{ 0x06000092, Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485_AdjustorThunk },
	{ 0x06000093, Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02_AdjustorThunk },
	{ 0x06000094, Bounds_ToString_mC2F42E6634E786CC9A1B847ECDD88226B7880E7B_AdjustorThunk },
	{ 0x06000095, Bounds_ToString_m493BA40092851F60E7CE73A8BD58489DAE18B3AD_AdjustorThunk },
	{ 0x06000096, Plane_ToString_mD0E5921B1DFC4E83443BA7267E1182ACDD65C5DD_AdjustorThunk },
	{ 0x06000097, Plane_ToString_m0AF5EF6354605B6F6698346081FBC1A8BE138C4B_AdjustorThunk },
	{ 0x06000098, Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70_AdjustorThunk },
	{ 0x0600009A, Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987_AdjustorThunk },
	{ 0x0600009B, Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74_AdjustorThunk },
	{ 0x0600009C, Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF_AdjustorThunk },
	{ 0x0600009D, Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A_AdjustorThunk },
	{ 0x0600009E, Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5_AdjustorThunk },
	{ 0x0600009F, Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3_AdjustorThunk },
	{ 0x060000A0, Rect_GetHashCode_mE5841C54528B29D5E85173AF1973326793AF9311_AdjustorThunk },
	{ 0x060000A1, Rect_Equals_mF1F747B913CDB083ED803F5DD21E2005736AE4AB_AdjustorThunk },
	{ 0x060000A2, Rect_Equals_mC9EE5E0C234DB174AC16E653ED8D32BB4D356BB8_AdjustorThunk },
	{ 0x060000A3, Rect_ToString_mCB7EA3D9B51213304AB0175B2D5E4545AFBCF483_AdjustorThunk },
	{ 0x060000A4, Rect_ToString_m3DFE65344E06224C23BB7500D069F908D5DDE8F5_AdjustorThunk },
	{ 0x060000B6, Resolution_ToString_m0F17D03CC087E67DAB7F8F383D86A9D5C3E2587B_AdjustorThunk },
	{ 0x060002C4, RenderTextureDescriptor_get_width_m5DD56A0652453FDDB51FF030FC5ED914F83F5E31_AdjustorThunk },
	{ 0x060002C5, RenderTextureDescriptor_set_width_m8D4BAEBB8089FD77F4DC81088ACB511F2BCA41EA_AdjustorThunk },
	{ 0x060002C6, RenderTextureDescriptor_get_height_m661881AD8E078D6C1FD6C549207AACC2B179D201_AdjustorThunk },
	{ 0x060002C7, RenderTextureDescriptor_set_height_m1300AF31BCDCF2E14E86A598AFDC5569B682A46D_AdjustorThunk },
	{ 0x060002C8, RenderTextureDescriptor_get_msaaSamples_m332912610A1FF2B7C05B0BA9939D733F2E7F0646_AdjustorThunk },
	{ 0x060002C9, RenderTextureDescriptor_set_msaaSamples_m84320452D8BF3A8DD5662F6229FE666C299B5AEF_AdjustorThunk },
	{ 0x060002CA, RenderTextureDescriptor_get_volumeDepth_m05E4A20A05286909E65D394D0BA5F6904D653688_AdjustorThunk },
	{ 0x060002CB, RenderTextureDescriptor_set_volumeDepth_mC4D9C6B86B6799BA752855DE5C385CC24F6E3733_AdjustorThunk },
	{ 0x060002CC, RenderTextureDescriptor_set_mipCount_mE713137D106256F44EF3E7B7CF33D5F146874659_AdjustorThunk },
	{ 0x060002CD, RenderTextureDescriptor_get_graphicsFormat_m9D77E42E017808FE3181673152A69CBC9A9B8B85_AdjustorThunk },
	{ 0x060002CE, RenderTextureDescriptor_set_graphicsFormat_m946B6FE4422E8CD33EB13ADAFDB53669EBD361C4_AdjustorThunk },
	{ 0x060002CF, RenderTextureDescriptor_get_depthBufferBits_m92A95D5A1DCA7B844B3AC81AADCDFDD37D26333C_AdjustorThunk },
	{ 0x060002D0, RenderTextureDescriptor_set_depthBufferBits_m68BF4BF942828FF70442841A22D356E5D17BCF85_AdjustorThunk },
	{ 0x060002D1, RenderTextureDescriptor_set_dimension_m4D3F1486F761F3C52308F00267B918BD7DB8137F_AdjustorThunk },
	{ 0x060002D2, RenderTextureDescriptor_set_shadowSamplingMode_m92B77BB68CC465F38790F5865A7402C5DE77B8D1_AdjustorThunk },
	{ 0x060002D3, RenderTextureDescriptor_set_vrUsage_m5E4F43CB35EF142D55AC22996B641483566A2097_AdjustorThunk },
	{ 0x060002D4, RenderTextureDescriptor_set_memoryless_m6C34CD3938C6C92F98227E3864E665026C50BCE3_AdjustorThunk },
	{ 0x060002D5, RenderTextureDescriptor__ctor_m320C821459C7856A088415334267C2963B270A9D_AdjustorThunk },
	{ 0x060002D6, RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m33FD234885342E9D0C6450C90C0F2E1B6B6A1044_AdjustorThunk },
	{ 0x060002D8, Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB_AdjustorThunk },
	{ 0x060002D9, Hash128_CompareTo_m577A27A07658268AD07B09F4FFFF1D3216AD963B_AdjustorThunk },
	{ 0x060002DA, Hash128_ToString_mE6E0973B9B42A6AB9BEB8ACC679291CDAD2D03AC_AdjustorThunk },
	{ 0x060002DF, Hash128_Equals_m2FEA62200ECEC6BA066924F3153C9FBA96B0E3FF_AdjustorThunk },
	{ 0x060002E0, Hash128_Equals_mA77DD31AF975A04BF4BD9D1F5B2A143497C9F468_AdjustorThunk },
	{ 0x060002E1, Hash128_GetHashCode_mBEB470B9988886E4EB3FDA22903EBB699D1B7EA6_AdjustorThunk },
	{ 0x060002E2, Hash128_CompareTo_m28ACD34C28C044C2BEF2109446DAAEB53F4EC619_AdjustorThunk },
	{ 0x06000303, Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5_AdjustorThunk },
	{ 0x06000304, Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494_AdjustorThunk },
	{ 0x06000305, Color_ToString_m2C9303D88F39CDAE35C613A3C816D8982C58B5FC_AdjustorThunk },
	{ 0x06000306, Color_ToString_m927424DFCE95E13D26A1F8BF91FA0AB3F6C2FC02_AdjustorThunk },
	{ 0x06000307, Color_GetHashCode_mAF5E7EE6AFA983D3FA5E3D316E672EE1511F97CF_AdjustorThunk },
	{ 0x06000308, Color_Equals_m90F8A5EF85416D809F5E3C0ACCADDD4F299AD8FC_AdjustorThunk },
	{ 0x06000309, Color_Equals_mB531F532B5F7BE6168CFD4A6C89358C16F058D00_AdjustorThunk },
	{ 0x0600030A, Color_RGBMultiplied_mEE82A8761F22790ECD29CD8AE13B1184441CB6C8_AdjustorThunk },
	{ 0x0600030B, Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904_AdjustorThunk },
	{ 0x0600030C, Color_get_maxColorComponent_mAB6964B3523DC9FDDF312F3329EB224DBFECE761_AdjustorThunk },
	{ 0x0600030F, Color32_ToString_m11295D5492D1FB41F25635A83B87C20058DEA256_AdjustorThunk },
	{ 0x06000310, Color32_ToString_m5BB9D04F00C5B22C5B295F6253C99972767102F5_AdjustorThunk },
	{ 0x06000319, Matrix4x4__ctor_mFDDCE13D7171353ED7BA9A9B6885212DFC9E1076_AdjustorThunk },
	{ 0x0600031A, Matrix4x4_GetHashCode_m102B903082CD1C786C221268A19679820E365B59_AdjustorThunk },
	{ 0x0600031B, Matrix4x4_Equals_mF6EB7A6D466F5AE1D1A872451359645D1C69843D_AdjustorThunk },
	{ 0x0600031C, Matrix4x4_Equals_mAE7AC284A922B094E4ACCC04A1C48B247E9A7997_AdjustorThunk },
	{ 0x0600031D, Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480_AdjustorThunk },
	{ 0x0600031E, Matrix4x4_ToString_mF45C45AD892B0707C34BEE0C716C91C0B5FD2831_AdjustorThunk },
	{ 0x0600031F, Matrix4x4_ToString_mC2CC8C3C358C9C982F25F633BC21105D2C3BCEFB_AdjustorThunk },
	{ 0x06000321, Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_AdjustorThunk },
	{ 0x06000322, Vector3_GetHashCode_m9F18401DA6025110A012F55BBB5ACABD36FA9A0A_AdjustorThunk },
	{ 0x06000323, Vector3_Equals_m210CB160B594355581D44D4B87CF3D3994ABFED0_AdjustorThunk },
	{ 0x06000324, Vector3_Equals_mA92800CD98ED6A42DD7C55C5DB22DAB4DEAA6397_AdjustorThunk },
	{ 0x0600032C, Vector3_ToString_mD5085501F9A0483542E9F7B18CD09C0AB977E318_AdjustorThunk },
	{ 0x0600032D, Vector3_ToString_m8E771CC90555B06B8BDBA5F691EC5D8D87D68414_AdjustorThunk },
	{ 0x06000336, Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421_AdjustorThunk },
	{ 0x0600033B, Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3_AdjustorThunk },
	{ 0x0600033E, Quaternion_GetHashCode_mFCEA4CA542544DC9BD222C66F524C2F3CFE60744_AdjustorThunk },
	{ 0x0600033F, Quaternion_Equals_m3EDD7DBA22F59A5797B91820AE4BBA64576D240F_AdjustorThunk },
	{ 0x06000340, Quaternion_Equals_m02CE0D27C1DA0C037D8721750E30BB1FAF1A7DAD_AdjustorThunk },
	{ 0x06000341, Quaternion_ToString_mD3D4C66907C994D30D99E76063623F7000F6998E_AdjustorThunk },
	{ 0x06000342, Quaternion_ToString_mF10FE18AAC385F9CFE721ECD8B66F14346774F31_AdjustorThunk },
	{ 0x06000351, Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_AdjustorThunk },
	{ 0x06000352, Vector2_ToString_mBD48EFCDB703ACCDC29E86AEB0D4D62FBA50F840_AdjustorThunk },
	{ 0x06000353, Vector2_ToString_m503AFEA3F57B8529C047FF93C2E72126C5591C23_AdjustorThunk },
	{ 0x06000354, Vector2_GetHashCode_m9A5DD8406289F38806CC42C394E324C1C2AB3732_AdjustorThunk },
	{ 0x06000355, Vector2_Equals_m67A842D914AA5A4DCC076E9EA20019925E6A85A0_AdjustorThunk },
	{ 0x06000356, Vector2_Equals_m6E08A16717F2B9EE8B24EBA6B234A03098D5F05D_AdjustorThunk },
	{ 0x06000359, Vector4_get_Item_m469B9D88498D0F7CD14B71A9512915BAA0B9B3B7_AdjustorThunk },
	{ 0x0600035A, Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2_AdjustorThunk },
	{ 0x0600035B, Vector4_GetHashCode_mCA7B312F8CA141F6F25BABDDF406F3D2BDD5E895_AdjustorThunk },
	{ 0x0600035C, Vector4_Equals_m71D14F39651C3FBEDE17214455DFA727921F07AA_AdjustorThunk },
	{ 0x0600035D, Vector4_Equals_m0919D35807550372D1748193EB31E4C5E406AE61_AdjustorThunk },
	{ 0x0600035F, Vector4_ToString_mF2D17142EBD75E91BC718B3E347F614AC45E9040_AdjustorThunk },
	{ 0x06000360, Vector4_ToString_m0EC6AA83CD606E3EB5BE60108A1D9AC4ECB5517A_AdjustorThunk },
	{ 0x06000475, WorkRequest__ctor_m13C7B4A89E47F4B97ED9B786DB99849DBC2B5603_AdjustorThunk },
	{ 0x06000476, WorkRequest_Invoke_m1C292B7297918C5F2DBE70971895FE8D5C33AA20_AdjustorThunk },
	{ 0x0600056C, DebugScreenCapture_set_rawImageDataReference_m9FE7228E0FB4696A255B2F477B6B50C902D7786B_AdjustorThunk },
	{ 0x0600056D, DebugScreenCapture_set_imageFormat_m46E9D97376E844826DAE5C3C69E4AAF4B7A58ECB_AdjustorThunk },
	{ 0x0600056E, DebugScreenCapture_set_width_m6928DB2B2BCD54DE97BDA4116D69897921CCACF6_AdjustorThunk },
	{ 0x0600056F, DebugScreenCapture_set_height_m6CD0F6872F123966B784E6F356C51986B8B19F84_AdjustorThunk },
	{ 0x060005E9, MovedFromAttributeData_Set_mC5572E6033FCE1C53B8C43A8D5280E71CE388640_AdjustorThunk },
	{ 0x060005EC, Scene_get_handle_m57967C50E461CD48441CA60645AF8FA04F7B132C_AdjustorThunk },
	{ 0x060005ED, Scene_get_isLoaded_m040A3D274F4FAD21BC95C6154FEA557ADE5C8E93_AdjustorThunk },
	{ 0x060005EF, Scene_GetHashCode_mFC620B8CA1EAA64BF0D7B33E8D3EAFAEDC9A6DCB_AdjustorThunk },
	{ 0x060005F0, Scene_Equals_m78D2F82F3133AD32F35C7981B65D0980A6C3006D_AdjustorThunk },
	{ 0x06000608, LoadSceneParameters_set_loadSceneMode_m8AAA5796E9D642FC5C2C95831F22E272A28DD152_AdjustorThunk },
	{ 0x06000609, PlayerLoopSystem_ToString_mAC7EE13A5561966294881362AD59CB55DBBED9EE_AdjustorThunk },
	{ 0x06000640, LODParameters_Equals_mF803671C1F46ECEEE0B376EBF3AAF69D1236B4C0_AdjustorThunk },
	{ 0x06000641, LODParameters_Equals_m8F8B356BCB62FAEAE0EFD8C51FFF9C5045A7DB5C_AdjustorThunk },
	{ 0x06000642, LODParameters_GetHashCode_m5310697EE3BF4943F7358EF0EA2E7B3A9D7C1BAD_AdjustorThunk },
	{ 0x06000669, ScriptableRenderContext_GetNumberOfCameras_Internal_mDD1E260788000AB57C94A2D3F8F02A2B91DA653F_AdjustorThunk },
	{ 0x0600066A, ScriptableRenderContext_GetCamera_Internal_m20F51E42E84B8E2BBF2CC72F8C8EBFDCE6737646_AdjustorThunk },
	{ 0x0600066B, ScriptableRenderContext__ctor_mEA592FA995EF36C1F8F05EF2E51BC1089D7371CA_AdjustorThunk },
	{ 0x0600066C, ScriptableRenderContext_GetNumberOfCameras_m43FCE097543E85E40FCA641C570A25E718E3D6F6_AdjustorThunk },
	{ 0x0600066D, ScriptableRenderContext_GetCamera_mF8D58C4C1BB5667F63A2DCFDB72CE4C3C7ADBBFA_AdjustorThunk },
	{ 0x0600066E, ScriptableRenderContext_Equals_mDC10DFED8A46426E355FE7877624EC2D549EA7B7_AdjustorThunk },
	{ 0x0600066F, ScriptableRenderContext_Equals_mB04CFBF55095DF6179ED2229F4C9FE907F95799D_AdjustorThunk },
	{ 0x06000670, ScriptableRenderContext_GetHashCode_mACDECBAC76686105322F409089D9A867DF4BD46D_AdjustorThunk },
	{ 0x06000674, ShaderTagId__ctor_mC8779BC717DBC52669DDF99900F968216119A830_AdjustorThunk },
	{ 0x06000675, ShaderTagId_get_id_m0340687DA0CA3F820D569CAE280E3EBF10A23B6E_AdjustorThunk },
	{ 0x06000676, ShaderTagId_set_id_m291EC34E5E1EE99B6D6DF664B331347DBE83364D_AdjustorThunk },
	{ 0x06000677, ShaderTagId_Equals_m13F76C51B5ECF4EC9856579496F93D2B5B9041A7_AdjustorThunk },
	{ 0x06000678, ShaderTagId_Equals_m19A2CFBFF4915B92F7E2572CCAB00A7CBD934DA3_AdjustorThunk },
	{ 0x06000679, ShaderTagId_GetHashCode_m6912AAFF83FFD29FBB2BBE51E2611C2A3667FB67_AdjustorThunk },
	{ 0x0600068E, BatchCullingContext__ctor_m660602A9A31FEDDB5673F26C3FD11B4194395369_AdjustorThunk },
	{ 0x0600069E, Playable__ctor_m4B5AC727703A68C00773F99DE1C711EFC973DCA8_AdjustorThunk },
	{ 0x0600069F, Playable_GetHandle_mA646BB041702651694A643FDE1A835112F718351_AdjustorThunk },
	{ 0x060006A0, Playable_Equals_m1BFA06EB851EEEB20705520563A2D504637CDA1C_AdjustorThunk },
	{ 0x060006B9, PlayableHandle_Equals_mEF30690ECF60C980C207198C86E401CB4DA5EF3F_AdjustorThunk },
	{ 0x060006BA, PlayableHandle_Equals_mB50663A8BC01BCED4A650EFADE88DCF47309E955_AdjustorThunk },
	{ 0x060006BB, PlayableHandle_GetHashCode_m26AD05C2D6209A017CA6A079F026BC8D28600D72_AdjustorThunk },
	{ 0x060006BE, PlayableOutput__ctor_m69B5F29DF2D785051B714CFEAAAF09A237978297_AdjustorThunk },
	{ 0x060006BF, PlayableOutput_GetHandle_mC56A4F912A7AC8640DE78F95DD6C4F346E36820E_AdjustorThunk },
	{ 0x060006C0, PlayableOutput_Equals_m5A66A7B50C52A8524BB807F439345D22E96FD5CE_AdjustorThunk },
	{ 0x060006C3, PlayableOutputHandle_GetHashCode_mB4BD207CB03FA499179FBEAF64CC66A91F49E59C_AdjustorThunk },
	{ 0x060006C5, PlayableOutputHandle_Equals_m06CE237BED975C0A3E9B19D071767EF6E7A3F83C_AdjustorThunk },
	{ 0x060006C6, PlayableOutputHandle_Equals_mA8CE0F7B36B440F067705FA90DC4A836E92542E0_AdjustorThunk },
	{ 0x060006C9, LinearColor_get_red_mA08BA9496EAFF59F4E1EAD61FDB5F57B0B0CC541_AdjustorThunk },
	{ 0x060006CA, LinearColor_set_red_mC0D9E4D96C36785145EAF7B0DBA90359EE193928_AdjustorThunk },
	{ 0x060006CB, LinearColor_get_green_m03F2EEBC25E91E1102A2D3252725B2BEDA7D7931_AdjustorThunk },
	{ 0x060006CC, LinearColor_set_green_m6E196AC12B067ED8AEF3B7C7E9DA1B80B9550B89_AdjustorThunk },
	{ 0x060006CD, LinearColor_get_blue_m0B87E7A2A5A5B6A6F5FD515890F6C3C528FC98FE_AdjustorThunk },
	{ 0x060006CE, LinearColor_set_blue_mB65DC7FD20C551501BC181C457D010DCEECDEE7F_AdjustorThunk },
	{ 0x060006D1, LightDataGI_Init_m135DF5CF6CBECA4CBA0AC71F9CDEEF8DE25606DB_AdjustorThunk },
	{ 0x060006D2, LightDataGI_Init_m4E8BEBD383D5F6F1A1EDFEC763A718A7271C22A6_AdjustorThunk },
	{ 0x060006D3, LightDataGI_Init_mC9948FAC4A191C99E3E7D94677B7CFD241857C86_AdjustorThunk },
	{ 0x060006D4, LightDataGI_Init_mA0DF9747C6AD308EAAF2A9530B4225A3D65BB092_AdjustorThunk },
	{ 0x060006D5, LightDataGI_Init_mB96C3F3E00F10DD0806BD3DB93B58C2299D59E94_AdjustorThunk },
	{ 0x060006D6, LightDataGI_InitNoBake_mF600D612DE9A1CE4355C61317F6173E1AAEFBD57_AdjustorThunk },
	{ 0x060006EE, CameraPlayable_GetHandle_m12A0FB549E5257C9AEBCF76B6E2DC49FE5F8B7C5_AdjustorThunk },
	{ 0x060006EF, CameraPlayable_Equals_m82D036759943861CAB110D108F966C60216E6327_AdjustorThunk },
	{ 0x060006F0, MaterialEffectPlayable_GetHandle_mC4AA4C850DFB33EBA45EDA3D0524294EC03044FC_AdjustorThunk },
	{ 0x060006F1, MaterialEffectPlayable_Equals_mC805BF647B60EA8517040C2C0716CFCB7F04CAFB_AdjustorThunk },
	{ 0x060006F2, TextureMixerPlayable_GetHandle_m44A48E52180084F5A93942EA0AFA454C9DFBFD40_AdjustorThunk },
	{ 0x060006F3, TextureMixerPlayable_Equals_m49E1B77DF4F13F35321494AC6B5330538D0A1980_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1802] = 
{
	1306,
	1306,
	2115,
	1117,
	1125,
	1280,
	2115,
	1306,
	2115,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1306,
	1306,
	1306,
	1306,
	1306,
	1306,
	-1,
	-1,
	1902,
	-1,
	-1,
	-1,
	2080,
	2013,
	1005,
	1306,
	1125,
	1306,
	1013,
	1013,
	1271,
	2109,
	2103,
	2103,
	2099,
	2115,
	1580,
	2109,
	2115,
	2115,
	2115,
	2083,
	2081,
	737,
	1306,
	589,
	1125,
	737,
	475,
	110,
	1125,
	2030,
	1119,
	733,
	1280,
	1269,
	1271,
	1271,
	1013,
	985,
	1296,
	2050,
	1280,
	2028,
	1118,
	1911,
	1117,
	1910,
	2083,
	2109,
	1874,
	1874,
	2053,
	1688,
	2031,
	1925,
	2046,
	1693,
	1952,
	1768,
	1952,
	2093,
	2093,
	2078,
	1873,
	1687,
	1692,
	1748,
	1918,
	2076,
	2076,
	2076,
	1306,
	2081,
	2081,
	2081,
	737,
	1125,
	381,
	1125,
	1738,
	737,
	1104,
	362,
	1125,
	1921,
	2081,
	1567,
	1925,
	268,
	739,
	1306,
	2103,
	1612,
	2081,
	1925,
	2081,
	1925,
	1925,
	2081,
	2081,
	1925,
	1925,
	1876,
	2115,
	1306,
	1271,
	1013,
	984,
	1304,
	1304,
	1280,
	589,
	1280,
	589,
	316,
	2108,
	1301,
	1301,
	1301,
	1301,
	1301,
	1301,
	1271,
	1013,
	1024,
	1280,
	589,
	2115,
	2115,
	2081,
	2081,
	1306,
	1119,
	2081,
	2115,
	2115,
	737,
	1306,
	589,
	1125,
	2099,
	2099,
	2079,
	2115,
	1280,
	2099,
	1306,
	2099,
	2079,
	2031,
	2031,
	1271,
	1117,
	2099,
	2079,
	1296,
	2103,
	2081,
	2081,
	2081,
	2053,
	1271,
	1271,
	2115,
	1995,
	2028,
	1995,
	935,
	1271,
	636,
	550,
	1915,
	1916,
	1913,
	1914,
	1716,
	1914,
	1914,
	1569,
	1569,
	2062,
	2073,
	2015,
	2028,
	1716,
	1716,
	1716,
	2028,
	2028,
	2028,
	1992,
	1992,
	1992,
	1914,
	1914,
	1914,
	1716,
	1716,
	1716,
	1914,
	1914,
	1914,
	1928,
	1915,
	1921,
	1910,
	1929,
	1916,
	1919,
	1909,
	1924,
	1913,
	1925,
	1914,
	1743,
	1716,
	1925,
	1914,
	1925,
	1914,
	1579,
	1569,
	1579,
	1569,
	1925,
	1914,
	1925,
	1914,
	1925,
	1914,
	1925,
	1914,
	1925,
	1914,
	1925,
	1914,
	2065,
	2062,
	1995,
	1992,
	2074,
	2073,
	1954,
	1953,
	2016,
	2015,
	2031,
	2028,
	2031,
	2028,
	2031,
	2028,
	2031,
	2028,
	1925,
	1914,
	1925,
	1914,
	1925,
	1914,
	1306,
	1837,
	1807,
	1807,
	1837,
	1807,
	1837,
	1900,
	1807,
	1837,
	1554,
	1921,
	1271,
	890,
	932,
	882,
	882,
	932,
	882,
	932,
	1053,
	1063,
	1059,
	882,
	932,
	409,
	1908,
	1908,
	1908,
	1908,
	1735,
	2031,
	1925,
	1925,
	2081,
	1125,
	1125,
	1125,
	2103,
	2103,
	2103,
	1280,
	1125,
	1256,
	1101,
	1280,
	1125,
	1303,
	1146,
	1303,
	1146,
	882,
	1003,
	1013,
	1271,
	1117,
	1271,
	1125,
	1125,
	1013,
	1271,
	1117,
	1296,
	1139,
	1296,
	1139,
	1271,
	741,
	1013,
	932,
	890,
	739,
	385,
	385,
	590,
	478,
	1003,
	1125,
	1280,
	1125,
	1280,
	1125,
	1271,
	1280,
	1280,
	1125,
	1125,
	1125,
	1125,
	705,
	675,
	694,
	695,
	451,
	695,
	695,
	267,
	267,
	1053,
	819,
	926,
	932,
	451,
	451,
	451,
	451,
	932,
	932,
	932,
	932,
	882,
	882,
	882,
	882,
	695,
	695,
	695,
	695,
	1063,
	707,
	707,
	451,
	451,
	451,
	451,
	695,
	695,
	695,
	695,
	742,
	705,
	735,
	687,
	730,
	675,
	746,
	708,
	738,
	694,
	739,
	695,
	475,
	451,
	739,
	695,
	739,
	695,
	282,
	267,
	282,
	267,
	739,
	695,
	739,
	695,
	739,
	695,
	739,
	695,
	739,
	695,
	739,
	695,
	739,
	695,
	739,
	695,
	1054,
	1053,
	890,
	882,
	820,
	819,
	1064,
	1063,
	927,
	926,
	935,
	932,
	935,
	932,
	935,
	932,
	935,
	932,
	935,
	932,
	739,
	695,
	739,
	695,
	739,
	695,
	739,
	695,
	744,
	707,
	744,
	707,
	1060,
	1059,
	1060,
	1059,
	671,
	671,
	671,
	671,
	671,
	671,
	671,
	1271,
	1301,
	1256,
	1301,
	1301,
	1301,
	1277,
	1271,
	1301,
	1280,
	1095,
	1095,
	1306,
	1306,
	2081,
	1306,
	1306,
	1271,
	1271,
	1271,
	1117,
	1271,
	1117,
	1296,
	1271,
	1271,
	550,
	550,
	1003,
	611,
	935,
	2115,
	1271,
	2103,
	2103,
	2103,
	2103,
	2103,
	2103,
	1139,
	1351,
	1354,
	1296,
	1296,
	751,
	611,
	257,
	337,
	338,
	237,
	301,
	35,
	614,
	1013,
	116,
	115,
	919,
	1272,
	1575,
	1296,
	1296,
	1271,
	1271,
	1117,
	1271,
	1117,
	1296,
	1139,
	1271,
	1271,
	1271,
	1271,
	1306,
	1296,
	1306,
	1119,
	735,
	71,
	1280,
	104,
	196,
	932,
	1280,
	205,
	377,
	588,
	69,
	258,
	258,
	123,
	72,
	125,
	126,
	260,
	687,
	1367,
	445,
	255,
	71,
	124,
	735,
	1125,
	535,
	337,
	536,
	339,
	722,
	1125,
	-1,
	-1,
	-1,
	-1,
	-1,
	751,
	1139,
	1306,
	611,
	237,
	237,
	301,
	492,
	1556,
	735,
	1125,
	71,
	124,
	932,
	1280,
	256,
	256,
	271,
	248,
	1381,
	1389,
	1296,
	447,
	447,
	447,
	258,
	259,
	264,
	449,
	2079,
	1296,
	1338,
	1340,
	123,
	123,
	68,
	123,
	69,
	125,
	72,
	2079,
	1296,
	1350,
	1353,
	123,
	123,
	68,
	70,
	73,
	125,
	2079,
	1296,
	1380,
	1388,
	258,
	258,
	123,
	125,
	126,
	260,
	2079,
	1271,
	1117,
	1271,
	1117,
	1117,
	1139,
	2081,
	1137,
	1290,
	1117,
	1306,
	1137,
	1125,
	258,
	258,
	123,
	123,
	258,
	447,
	123,
	1290,
	1137,
	2082,
	1800,
	1095,
	1095,
	1271,
	1117,
	1271,
	1117,
	1271,
	1117,
	1271,
	1117,
	1117,
	1271,
	1117,
	1271,
	1117,
	1117,
	1117,
	1117,
	1117,
	123,
	749,
	2115,
	1296,
	880,
	1280,
	1978,
	2026,
	1918,
	1978,
	1013,
	1001,
	1271,
	890,
	1861,
	1861,
	1861,
	1918,
	2021,
	268,
	739,
	1280,
	695,
	452,
	452,
	1125,
	1280,
	1125,
	1296,
	1139,
	1271,
	1117,
	1003,
	2031,
	695,
	452,
	452,
	268,
	739,
	2081,
	2081,
	2115,
	1116,
	1125,
	468,
	1306,
	316,
	502,
	1280,
	589,
	1271,
	1013,
	989,
	821,
	1256,
	1301,
	2072,
	1955,
	1280,
	589,
	2101,
	1306,
	1005,
	1306,
	1306,
	1013,
	1013,
	1271,
	317,
	1271,
	1013,
	1012,
	1063,
	1280,
	589,
	2115,
	502,
	1271,
	1013,
	1035,
	2113,
	2113,
	2113,
	2113,
	1899,
	1899,
	1898,
	1280,
	589,
	2115,
	1851,
	2040,
	2041,
	2070,
	1850,
	1851,
	2041,
	316,
	2107,
	1849,
	1897,
	2071,
	1304,
	1675,
	2041,
	1271,
	1013,
	1023,
	1280,
	589,
	2115,
	1703,
	1901,
	1901,
	1901,
	1752,
	1703,
	2067,
	2067,
	2067,
	1889,
	1889,
	1613,
	2115,
	755,
	1280,
	589,
	1271,
	1013,
	1034,
	2112,
	2115,
	1053,
	316,
	1271,
	1013,
	1036,
	2114,
	1280,
	589,
	2115,
	1306,
	1306,
	440,
	407,
	1306,
	1114,
	1114,
	1296,
	440,
	407,
	1306,
	1114,
	1114,
	1306,
	1296,
	1306,
	2109,
	2115,
	2081,
	2081,
	1743,
	1694,
	2115,
	2115,
	1306,
	1839,
	2031,
	1280,
	1280,
	1306,
	2031,
	1839,
	1839,
	1839,
	2081,
	2103,
	2103,
	1306,
	935,
	589,
	589,
	589,
	1125,
	2115,
	1839,
	1839,
	1839,
	2081,
	2080,
	1296,
	1301,
	1117,
	1139,
	1306,
	1306,
	1125,
	1125,
	1306,
	2031,
	2031,
	1995,
	1995,
	1995,
	-1,
	2115,
	1125,
	1306,
	1306,
	1271,
	1306,
	1306,
	2115,
	1280,
	1280,
	1306,
	1306,
	1306,
	2080,
	1923,
	1664,
	1306,
	1306,
	2028,
	-1,
	935,
	737,
	935,
	935,
	590,
	935,
	-1,
	-1,
	590,
	935,
	-1,
	-1,
	62,
	935,
	-1,
	739,
	-1,
	935,
	590,
	-1,
	-1,
	-1,
	-1,
	935,
	590,
	-1,
	-1,
	-1,
	-1,
	616,
	935,
	737,
	2031,
	735,
	735,
	735,
	935,
	935,
	935,
	-1,
	1280,
	1271,
	1117,
	1296,
	1139,
	1139,
	1296,
	1296,
	1139,
	1296,
	1139,
	1296,
	1280,
	1125,
	1013,
	2031,
	2031,
	475,
	739,
	1125,
	475,
	739,
	1125,
	475,
	739,
	1125,
	1125,
	1306,
	739,
	1925,
	2031,
	1297,
	1272,
	1280,
	1095,
	2081,
	1453,
	1571,
	1923,
	1296,
	1306,
	742,
	487,
	1125,
	1013,
	935,
	589,
	935,
	935,
	1125,
	1125,
	1125,
	1306,
	1296,
	1139,
	2081,
	2081,
	2053,
	1583,
	1925,
	1876,
	2053,
	589,
	935,
	1125,
	1125,
	1280,
	1306,
	-1,
	-1,
	-1,
	1921,
	2031,
	1306,
	1117,
	1117,
	1306,
	2031,
	-1,
	2081,
	1840,
	2109,
	2081,
	2103,
	1733,
	2031,
	2115,
	1306,
	1125,
	743,
	1280,
	1280,
	1280,
	2031,
	2115,
	2115,
	2115,
	1306,
	739,
	1271,
	1271,
	1013,
	2053,
	1876,
	2053,
	1273,
	1280,
	1125,
	1671,
	1536,
	2031,
	1666,
	-1,
	-1,
	-1,
	-1,
	1928,
	2081,
	1927,
	2081,
	1840,
	2081,
	1117,
	-1,
	1925,
	1840,
	1280,
	1876,
	1876,
	2099,
	2031,
	1666,
	1671,
	1532,
	2031,
	2031,
	1925,
	1306,
	2115,
	1653,
	1527,
	1117,
	735,
	739,
	739,
	1280,
	1306,
	1296,
	2115,
	2115,
	2051,
	476,
	1306,
	1143,
	1306,
	1306,
	1306,
	1306,
	890,
	1306,
	1306,
	2053,
	2050,
	2050,
	1867,
	1800,
	1992,
	2110,
	2110,
	2099,
	2110,
	2081,
	737,
	1125,
	381,
	1125,
	1306,
	1304,
	1147,
	1304,
	1147,
	1061,
	757,
	1147,
	1304,
	1147,
	1304,
	1147,
	1304,
	1147,
	1304,
	1147,
	1304,
	1147,
	1288,
	1135,
	1288,
	1135,
	1271,
	1117,
	1271,
	1117,
	1304,
	1147,
	1280,
	1125,
	1280,
	1125,
	1280,
	1125,
	741,
	1279,
	1279,
	759,
	757,
	1147,
	314,
	502,
	758,
	315,
	757,
	1147,
	314,
	502,
	760,
	504,
	760,
	505,
	745,
	1125,
	761,
	1147,
	761,
	1062,
	432,
	1062,
	432,
	1062,
	432,
	1062,
	432,
	1062,
	432,
	1062,
	432,
	1280,
	1280,
	1271,
	1306,
	1306,
	1306,
	1117,
	741,
	1271,
	1666,
	935,
	1306,
	1304,
	1013,
	1296,
	1139,
	935,
	1280,
	760,
	760,
	932,
	1271,
	1271,
	1117,
	1271,
	1117,
	1271,
	1271,
	1296,
	1095,
	1095,
	1095,
	1095,
	671,
	641,
	1095,
	1095,
	1095,
	1095,
	1095,
	1095,
	1095,
	1095,
	1095,
	640,
	643,
	640,
	640,
	640,
	640,
	640,
	640,
	640,
	1095,
	643,
	643,
	1125,
	1280,
	1296,
	1306,
	1296,
	1280,
	1125,
	1271,
	1117,
	1303,
	1146,
	1301,
	1143,
	1271,
	1117,
	1256,
	1101,
	1271,
	1117,
	1296,
	1139,
	1296,
	1139,
	1271,
	1117,
	817,
	1253,
	1306,
	1095,
	1095,
	1095,
	1095,
	671,
	1306,
	1271,
	1271,
	1271,
	1289,
	1303,
	1305,
	1305,
	1305,
	1537,
	1335,
	1253,
	1289,
	1305,
	1280,
	932,
	1301,
	1301,
	1280,
	1303,
	1296,
	1271,
	1271,
	1289,
	1303,
	1280,
	1280,
	1280,
	1271,
	882,
	882,
	551,
	1737,
	1125,
	1921,
	1743,
	739,
	1537,
	1672,
	1335,
	1347,
	1374,
	1434,
	1533,
	1667,
	1095,
	1095,
	1095,
	1095,
	1095,
	1509,
	1333,
	1095,
	1095,
	1095,
	1095,
	1551,
	1551,
	2053,
	2081,
	2081,
	2115,
	935,
	2100,
	1091,
	1117,
	1117,
	1117,
	1306,
	2103,
	1618,
	1619,
	1927,
	1357,
	2031,
	1280,
	1280,
	1271,
	1301,
	1280,
	1296,
	1306,
	1306,
	1306,
	1306,
	739,
	1125,
	-1,
	2053,
	620,
	1125,
	1125,
	739,
	1125,
	1306,
	620,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1280,
	1280,
	1280,
	1271,
	1280,
	1296,
	935,
	1664,
	1306,
	1306,
	1306,
	1306,
	739,
	1125,
	1125,
	739,
	1306,
	1280,
	1306,
	1306,
	1306,
	1306,
	589,
	589,
	935,
	210,
	1306,
	1306,
	1125,
	739,
	1280,
	1280,
	1664,
	737,
	1306,
	589,
	1125,
	1306,
	589,
	589,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1125,
	1306,
	305,
	1125,
	2050,
	1271,
	1296,
	1880,
	1271,
	1013,
	1526,
	1522,
	2103,
	2103,
	1306,
	206,
	936,
	2115,
	2099,
	2058,
	1846,
	1526,
	2035,
	2081,
	2081,
	1838,
	2036,
	1931,
	2084,
	1932,
	2115,
	1908,
	1826,
	1117,
	1280,
	737,
	1306,
	589,
	1125,
	1306,
	2103,
	1296,
	2103,
	1306,
	1280,
	647,
	647,
	1125,
	1125,
	1125,
	1125,
	647,
	604,
	603,
	1306,
	1573,
	2079,
	2079,
	1306,
	1306,
	1013,
	1306,
	1013,
	1306,
	1125,
	440,
	930,
	647,
	1306,
	1306,
	1306,
	1268,
	1114,
	1306,
	1306,
	1013,
	1306,
	1013,
	1306,
	1013,
	1125,
	1280,
	1013,
	1271,
	1306,
	2109,
	2099,
	2076,
	2115,
	1009,
	1013,
	1271,
	754,
	501,
	754,
	501,
	1296,
	1139,
	1306,
	1139,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1280,
	1306,
	1306,
	1306,
	2103,
	2081,
	2115,
	2085,
	1740,
	2081,
	2115,
	1271,
	932,
	1119,
	1271,
	932,
	1028,
	1013,
	1271,
	2115,
	1988,
	1826,
	1125,
	1271,
	1117,
	1013,
	1030,
	1271,
	2115,
	2103,
	2081,
	1271,
	1271,
	1271,
	1271,
	1296,
	1296,
	2080,
	2050,
	1912,
	2050,
	1912,
	1912,
	1912,
	2080,
	2080,
	1306,
	2115,
	33,
	1733,
	737,
	573,
	202,
	923,
	489,
	1129,
	1129,
	1129,
	1129,
	748,
	748,
	748,
	488,
	2104,
	1131,
	1285,
	1017,
	2115,
	594,
	1265,
	1280,
	1584,
	1923,
	1306,
	1306,
	1129,
	1129,
	1129,
	1129,
	748,
	748,
	748,
	488,
	1280,
	2115,
	737,
	595,
	217,
	943,
	2105,
	1878,
	1013,
	1019,
	1271,
	1878,
	2115,
	1132,
	1286,
	1020,
	2115,
	2106,
	1271,
	1879,
	1013,
	1021,
	1879,
	2115,
	1301,
	1143,
	1301,
	1143,
	1301,
	1143,
	1824,
	2102,
	640,
	640,
	640,
	640,
	640,
	1117,
	2050,
	2014,
	2065,
	1954,
	1907,
	1918,
	1918,
	1918,
	1918,
	1918,
	1918,
	2081,
	2103,
	2115,
	1738,
	2115,
	737,
	725,
	201,
	1125,
	2115,
	1306,
	725,
	1285,
	986,
	1285,
	1011,
	1285,
	1032,
	1296,
	1306,
	1139,
	2109,
	2103,
	1306,
	1296,
	2081,
	2103,
	2115,
	2115,
	1280,
	1125,
	1095,
	1306,
	1801,
	1801,
	1801,
	1801,
	1800,
	2050,
	2050,
	2050,
};
static const Il2CppTokenRangePair s_rgctxIndices[46] = 
{
	{ 0x02000017, { 0, 7 } },
	{ 0x02000018, { 7, 4 } },
	{ 0x020000D7, { 69, 7 } },
	{ 0x020000D8, { 76, 7 } },
	{ 0x020000D9, { 83, 9 } },
	{ 0x020000DA, { 92, 11 } },
	{ 0x020000DB, { 103, 3 } },
	{ 0x020000E4, { 106, 8 } },
	{ 0x020000E6, { 114, 4 } },
	{ 0x020000E8, { 118, 5 } },
	{ 0x020000EA, { 123, 6 } },
	{ 0x06000024, { 11, 1 } },
	{ 0x06000025, { 12, 1 } },
	{ 0x06000026, { 13, 1 } },
	{ 0x06000268, { 14, 4 } },
	{ 0x06000269, { 18, 1 } },
	{ 0x0600026A, { 19, 4 } },
	{ 0x0600026B, { 23, 2 } },
	{ 0x0600026C, { 25, 2 } },
	{ 0x060003A1, { 27, 2 } },
	{ 0x060003B5, { 29, 1 } },
	{ 0x060003BC, { 30, 1 } },
	{ 0x060003BD, { 31, 2 } },
	{ 0x060003C0, { 33, 1 } },
	{ 0x060003C1, { 34, 2 } },
	{ 0x060003C4, { 36, 2 } },
	{ 0x060003C6, { 38, 1 } },
	{ 0x060003C9, { 39, 2 } },
	{ 0x060003CA, { 41, 1 } },
	{ 0x060003CB, { 42, 1 } },
	{ 0x060003CC, { 43, 1 } },
	{ 0x060003CF, { 44, 1 } },
	{ 0x060003D0, { 45, 2 } },
	{ 0x060003D1, { 47, 1 } },
	{ 0x060003D2, { 48, 2 } },
	{ 0x060003DD, { 50, 2 } },
	{ 0x06000422, { 52, 2 } },
	{ 0x06000423, { 54, 4 } },
	{ 0x06000424, { 58, 1 } },
	{ 0x0600042C, { 59, 2 } },
	{ 0x0600044E, { 61, 1 } },
	{ 0x0600044F, { 62, 1 } },
	{ 0x06000450, { 63, 1 } },
	{ 0x06000451, { 64, 1 } },
	{ 0x06000459, { 65, 2 } },
	{ 0x06000583, { 67, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[129] = 
{
	{ (Il2CppRGCTXDataType)3, 8608 },
	{ (Il2CppRGCTXDataType)3, 8619 },
	{ (Il2CppRGCTXDataType)2, 880 },
	{ (Il2CppRGCTXDataType)3, 3001 },
	{ (Il2CppRGCTXDataType)3, 6109 },
	{ (Il2CppRGCTXDataType)2, 1583 },
	{ (Il2CppRGCTXDataType)3, 6108 },
	{ (Il2CppRGCTXDataType)3, 6111 },
	{ (Il2CppRGCTXDataType)3, 6110 },
	{ (Il2CppRGCTXDataType)3, 3011 },
	{ (Il2CppRGCTXDataType)2, 376 },
	{ (Il2CppRGCTXDataType)2, 190 },
	{ (Il2CppRGCTXDataType)2, 189 },
	{ (Il2CppRGCTXDataType)2, 188 },
	{ (Il2CppRGCTXDataType)3, 6104 },
	{ (Il2CppRGCTXDataType)3, 6105 },
	{ (Il2CppRGCTXDataType)3, 8443 },
	{ (Il2CppRGCTXDataType)3, 8615 },
	{ (Il2CppRGCTXDataType)2, 183 },
	{ (Il2CppRGCTXDataType)3, 6106 },
	{ (Il2CppRGCTXDataType)3, 6107 },
	{ (Il2CppRGCTXDataType)3, 8444 },
	{ (Il2CppRGCTXDataType)3, 8616 },
	{ (Il2CppRGCTXDataType)3, 8617 },
	{ (Il2CppRGCTXDataType)3, 8436 },
	{ (Il2CppRGCTXDataType)3, 8618 },
	{ (Il2CppRGCTXDataType)3, 8437 },
	{ (Il2CppRGCTXDataType)1, 103 },
	{ (Il2CppRGCTXDataType)2, 103 },
	{ (Il2CppRGCTXDataType)1, 125 },
	{ (Il2CppRGCTXDataType)3, 8381 },
	{ (Il2CppRGCTXDataType)1, 127 },
	{ (Il2CppRGCTXDataType)2, 127 },
	{ (Il2CppRGCTXDataType)3, 8384 },
	{ (Il2CppRGCTXDataType)1, 129 },
	{ (Il2CppRGCTXDataType)2, 129 },
	{ (Il2CppRGCTXDataType)1, 130 },
	{ (Il2CppRGCTXDataType)2, 2019 },
	{ (Il2CppRGCTXDataType)1, 120 },
	{ (Il2CppRGCTXDataType)1, 132 },
	{ (Il2CppRGCTXDataType)2, 2021 },
	{ (Il2CppRGCTXDataType)1, 121 },
	{ (Il2CppRGCTXDataType)3, 8389 },
	{ (Il2CppRGCTXDataType)3, 8392 },
	{ (Il2CppRGCTXDataType)1, 123 },
	{ (Il2CppRGCTXDataType)1, 134 },
	{ (Il2CppRGCTXDataType)2, 2023 },
	{ (Il2CppRGCTXDataType)3, 8395 },
	{ (Il2CppRGCTXDataType)1, 119 },
	{ (Il2CppRGCTXDataType)2, 119 },
	{ (Il2CppRGCTXDataType)1, 124 },
	{ (Il2CppRGCTXDataType)2, 124 },
	{ (Il2CppRGCTXDataType)3, 5006 },
	{ (Il2CppRGCTXDataType)3, 5007 },
	{ (Il2CppRGCTXDataType)3, 5003 },
	{ (Il2CppRGCTXDataType)3, 5004 },
	{ (Il2CppRGCTXDataType)3, 5005 },
	{ (Il2CppRGCTXDataType)3, 8457 },
	{ (Il2CppRGCTXDataType)2, 2024 },
	{ (Il2CppRGCTXDataType)1, 178 },
	{ (Il2CppRGCTXDataType)2, 178 },
	{ (Il2CppRGCTXDataType)2, 155 },
	{ (Il2CppRGCTXDataType)2, 157 },
	{ (Il2CppRGCTXDataType)2, 158 },
	{ (Il2CppRGCTXDataType)2, 156 },
	{ (Il2CppRGCTXDataType)1, 154 },
	{ (Il2CppRGCTXDataType)2, 154 },
	{ (Il2CppRGCTXDataType)2, 104 },
	{ (Il2CppRGCTXDataType)1, 104 },
	{ (Il2CppRGCTXDataType)2, 1723 },
	{ (Il2CppRGCTXDataType)3, 8410 },
	{ (Il2CppRGCTXDataType)1, 1723 },
	{ (Il2CppRGCTXDataType)3, 4533 },
	{ (Il2CppRGCTXDataType)3, 8315 },
	{ (Il2CppRGCTXDataType)2, 277 },
	{ (Il2CppRGCTXDataType)3, 6975 },
	{ (Il2CppRGCTXDataType)1, 1730 },
	{ (Il2CppRGCTXDataType)2, 1730 },
	{ (Il2CppRGCTXDataType)3, 8316 },
	{ (Il2CppRGCTXDataType)3, 8319 },
	{ (Il2CppRGCTXDataType)2, 278 },
	{ (Il2CppRGCTXDataType)2, 426 },
	{ (Il2CppRGCTXDataType)3, 6997 },
	{ (Il2CppRGCTXDataType)1, 1734 },
	{ (Il2CppRGCTXDataType)2, 1734 },
	{ (Il2CppRGCTXDataType)3, 8317 },
	{ (Il2CppRGCTXDataType)3, 8320 },
	{ (Il2CppRGCTXDataType)3, 8322 },
	{ (Il2CppRGCTXDataType)2, 279 },
	{ (Il2CppRGCTXDataType)2, 427 },
	{ (Il2CppRGCTXDataType)2, 466 },
	{ (Il2CppRGCTXDataType)3, 7011 },
	{ (Il2CppRGCTXDataType)1, 1736 },
	{ (Il2CppRGCTXDataType)2, 1736 },
	{ (Il2CppRGCTXDataType)3, 8318 },
	{ (Il2CppRGCTXDataType)3, 8321 },
	{ (Il2CppRGCTXDataType)3, 8323 },
	{ (Il2CppRGCTXDataType)3, 8324 },
	{ (Il2CppRGCTXDataType)2, 280 },
	{ (Il2CppRGCTXDataType)2, 428 },
	{ (Il2CppRGCTXDataType)2, 467 },
	{ (Il2CppRGCTXDataType)2, 479 },
	{ (Il2CppRGCTXDataType)3, 7016 },
	{ (Il2CppRGCTXDataType)3, 4530 },
	{ (Il2CppRGCTXDataType)2, 1391 },
	{ (Il2CppRGCTXDataType)3, 4532 },
	{ (Il2CppRGCTXDataType)3, 7021 },
	{ (Il2CppRGCTXDataType)2, 1738 },
	{ (Il2CppRGCTXDataType)1, 329 },
	{ (Il2CppRGCTXDataType)2, 1392 },
	{ (Il2CppRGCTXDataType)3, 4535 },
	{ (Il2CppRGCTXDataType)3, 4534 },
	{ (Il2CppRGCTXDataType)3, 4536 },
	{ (Il2CppRGCTXDataType)2, 329 },
	{ (Il2CppRGCTXDataType)1, 330 },
	{ (Il2CppRGCTXDataType)1, 441 },
	{ (Il2CppRGCTXDataType)2, 1393 },
	{ (Il2CppRGCTXDataType)3, 4565 },
	{ (Il2CppRGCTXDataType)1, 331 },
	{ (Il2CppRGCTXDataType)1, 442 },
	{ (Il2CppRGCTXDataType)1, 474 },
	{ (Il2CppRGCTXDataType)2, 1394 },
	{ (Il2CppRGCTXDataType)3, 4569 },
	{ (Il2CppRGCTXDataType)1, 332 },
	{ (Il2CppRGCTXDataType)1, 443 },
	{ (Il2CppRGCTXDataType)1, 475 },
	{ (Il2CppRGCTXDataType)1, 483 },
	{ (Il2CppRGCTXDataType)2, 1395 },
	{ (Il2CppRGCTXDataType)3, 4573 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_CoreModule_AttributeGenerators[];
static TypeDefinitionIndex s_staticConstructorsToRunAtStartup[8] = 
{
	1478,
	1595,
	1596,
	1597,
	1598,
	1599,
	1600,
	0,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_CoreModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_CoreModule_CodeGenModule = 
{
	"UnityEngine.CoreModule.dll",
	1802,
	s_methodPointers,
	165,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	46,
	s_rgctxIndices,
	129,
	s_rgctxValues,
	NULL,
	g_UnityEngine_CoreModule_AttributeGenerators,
	NULL, // module initializer,
	s_staticConstructorsToRunAtStartup,
	NULL,
	NULL,
};
