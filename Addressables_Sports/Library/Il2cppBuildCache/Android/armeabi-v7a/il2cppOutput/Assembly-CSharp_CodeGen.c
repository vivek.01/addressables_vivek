﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DemoManager::Start()
extern void DemoManager_Start_mBEBCF68F5806EE052A9BA0A002C5B82095066B6D (void);
// 0x00000002 System.Void DemoManager::DisplayNos()
extern void DemoManager_DisplayNos_m3FF358D701F870CDAE7AA73A85AA4EAF0F9715AF (void);
// 0x00000003 System.Void DemoManager::LocationLoaded(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>>)
extern void DemoManager_LocationLoaded_m875644E59C7108C9E21DDD2F4E1D69B02B96C534 (void);
// 0x00000004 System.Collections.IEnumerator DemoManager::SpawnRemote()
extern void DemoManager_SpawnRemote_mB129C4BC5514C10783A5D40101DE4A299C51D6D8 (void);
// 0x00000005 System.Void DemoManager::.ctor()
extern void DemoManager__ctor_m33F8DE4B6DCD96C1459986EFCCF9432F983F1069 (void);
// 0x00000006 System.Void DemoManager/<SpawnRemote>d__6::.ctor(System.Int32)
extern void U3CSpawnRemoteU3Ed__6__ctor_m11C64B7B195AA986600103FF2F5D1D0A6771FB5E (void);
// 0x00000007 System.Void DemoManager/<SpawnRemote>d__6::System.IDisposable.Dispose()
extern void U3CSpawnRemoteU3Ed__6_System_IDisposable_Dispose_mD5B618C8A69A0628B6C75F3A550D7E2D97AC4DCF (void);
// 0x00000008 System.Boolean DemoManager/<SpawnRemote>d__6::MoveNext()
extern void U3CSpawnRemoteU3Ed__6_MoveNext_m244ECC5613A24100AE2A9A86E5A9CDD409A3B8B0 (void);
// 0x00000009 System.Object DemoManager/<SpawnRemote>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnRemoteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA810881A57EDAFBAF1105505917A0FF185D07264 (void);
// 0x0000000A System.Void DemoManager/<SpawnRemote>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSpawnRemoteU3Ed__6_System_Collections_IEnumerator_Reset_m10F717A38B6A2CD356F569D42F3896C2E55D1B4A (void);
// 0x0000000B System.Object DemoManager/<SpawnRemote>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnRemoteU3Ed__6_System_Collections_IEnumerator_get_Current_m52C54CECCDD0AED1823FE23CCCA6D21ABD088B36 (void);
static Il2CppMethodPointer s_methodPointers[11] = 
{
	DemoManager_Start_mBEBCF68F5806EE052A9BA0A002C5B82095066B6D,
	DemoManager_DisplayNos_m3FF358D701F870CDAE7AA73A85AA4EAF0F9715AF,
	DemoManager_LocationLoaded_m875644E59C7108C9E21DDD2F4E1D69B02B96C534,
	DemoManager_SpawnRemote_mB129C4BC5514C10783A5D40101DE4A299C51D6D8,
	DemoManager__ctor_m33F8DE4B6DCD96C1459986EFCCF9432F983F1069,
	U3CSpawnRemoteU3Ed__6__ctor_m11C64B7B195AA986600103FF2F5D1D0A6771FB5E,
	U3CSpawnRemoteU3Ed__6_System_IDisposable_Dispose_mD5B618C8A69A0628B6C75F3A550D7E2D97AC4DCF,
	U3CSpawnRemoteU3Ed__6_MoveNext_m244ECC5613A24100AE2A9A86E5A9CDD409A3B8B0,
	U3CSpawnRemoteU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA810881A57EDAFBAF1105505917A0FF185D07264,
	U3CSpawnRemoteU3Ed__6_System_Collections_IEnumerator_Reset_m10F717A38B6A2CD356F569D42F3896C2E55D1B4A,
	U3CSpawnRemoteU3Ed__6_System_Collections_IEnumerator_get_Current_m52C54CECCDD0AED1823FE23CCCA6D21ABD088B36,
};
static const int32_t s_InvokerIndices[11] = 
{
	1306,
	1306,
	1067,
	1280,
	1306,
	1117,
	1306,
	1296,
	1280,
	1306,
	1280,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	11,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
