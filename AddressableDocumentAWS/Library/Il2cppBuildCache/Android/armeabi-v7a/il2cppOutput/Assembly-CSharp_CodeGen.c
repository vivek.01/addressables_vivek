﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DemoManager::Start()
extern void DemoManager_Start_mBEBCF68F5806EE052A9BA0A002C5B82095066B6D (void);
// 0x00000002 System.Void DemoManager::LocationLoaded(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>>)
extern void DemoManager_LocationLoaded_m875644E59C7108C9E21DDD2F4E1D69B02B96C534 (void);
// 0x00000003 System.Collections.IEnumerator DemoManager::SpawnRemote()
extern void DemoManager_SpawnRemote_mB129C4BC5514C10783A5D40101DE4A299C51D6D8 (void);
// 0x00000004 System.Void DemoManager::.ctor()
extern void DemoManager__ctor_m33F8DE4B6DCD96C1459986EFCCF9432F983F1069 (void);
// 0x00000005 System.Void DemoManager/<SpawnRemote>d__4::.ctor(System.Int32)
extern void U3CSpawnRemoteU3Ed__4__ctor_mA91D4284761AEC101D991C97C8213C781C9BC90E (void);
// 0x00000006 System.Void DemoManager/<SpawnRemote>d__4::System.IDisposable.Dispose()
extern void U3CSpawnRemoteU3Ed__4_System_IDisposable_Dispose_mE1B90A6D504D8C1C2084747B116557AFD6EA9240 (void);
// 0x00000007 System.Boolean DemoManager/<SpawnRemote>d__4::MoveNext()
extern void U3CSpawnRemoteU3Ed__4_MoveNext_mCDA17764C40F8A706A6B5D08BCD81591CC381F7A (void);
// 0x00000008 System.Object DemoManager/<SpawnRemote>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnRemoteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E627FD594AF12A9D15F8130F86DBEF600E5AE79 (void);
// 0x00000009 System.Void DemoManager/<SpawnRemote>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSpawnRemoteU3Ed__4_System_Collections_IEnumerator_Reset_m8C6BCB3699449ABE46DE34E94560032BBB238D9A (void);
// 0x0000000A System.Object DemoManager/<SpawnRemote>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnRemoteU3Ed__4_System_Collections_IEnumerator_get_Current_m786D4A16A32AA2824AE22B4E382BF29ADA73605E (void);
// 0x0000000B System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x0000000C System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x0000000D System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x0000000E UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x0000000F System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x00000010 System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE (void);
// 0x00000011 UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9 (void);
// 0x00000012 System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0 (void);
// 0x00000013 System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D (void);
// 0x00000014 System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA (void);
// 0x00000015 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A (void);
// 0x00000016 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5 (void);
// 0x00000017 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x00000018 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x00000019 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x0000001A System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x0000001B System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x0000001C System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
static Il2CppMethodPointer s_methodPointers[28] = 
{
	DemoManager_Start_mBEBCF68F5806EE052A9BA0A002C5B82095066B6D,
	DemoManager_LocationLoaded_m875644E59C7108C9E21DDD2F4E1D69B02B96C534,
	DemoManager_SpawnRemote_mB129C4BC5514C10783A5D40101DE4A299C51D6D8,
	DemoManager__ctor_m33F8DE4B6DCD96C1459986EFCCF9432F983F1069,
	U3CSpawnRemoteU3Ed__4__ctor_mA91D4284761AEC101D991C97C8213C781C9BC90E,
	U3CSpawnRemoteU3Ed__4_System_IDisposable_Dispose_mE1B90A6D504D8C1C2084747B116557AFD6EA9240,
	U3CSpawnRemoteU3Ed__4_MoveNext_mCDA17764C40F8A706A6B5D08BCD81591CC381F7A,
	U3CSpawnRemoteU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5E627FD594AF12A9D15F8130F86DBEF600E5AE79,
	U3CSpawnRemoteU3Ed__4_System_Collections_IEnumerator_Reset_m8C6BCB3699449ABE46DE34E94560032BBB238D9A,
	U3CSpawnRemoteU3Ed__4_System_Collections_IEnumerator_get_Current_m786D4A16A32AA2824AE22B4E382BF29ADA73605E,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE,
	SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9,
	SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0,
	SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D,
	SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA,
	SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A,
	SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
};
static const int32_t s_InvokerIndices[28] = 
{
	2323,
	1849,
	2268,
	2323,
	1931,
	2323,
	2296,
	2268,
	2323,
	2268,
	2323,
	2323,
	2323,
	2319,
	2323,
	2306,
	2317,
	2296,
	2296,
	2296,
	2296,
	2296,
	2323,
	1944,
	1996,
	757,
	1944,
	2323,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	28,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
